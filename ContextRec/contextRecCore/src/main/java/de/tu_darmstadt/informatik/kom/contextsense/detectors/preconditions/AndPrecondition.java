package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions;

import java.util.ArrayList;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.PreconditionVisitor;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 21.03.2016.
 */
public class AndPrecondition extends Precondition {

    private List<Precondition> conditions;

    // for serialization only
    private AndPrecondition() {
    }

    public AndPrecondition( List<Precondition> conditions ) {
        this();
        this.conditions = new ArrayList<>( conditions );
    }

    @Override
    public <T> T accept( PreconditionVisitor<T> visitor ) {
        return visitor.visit( this );
    }

    public List<Precondition> getConditions() {
        return new ArrayList<>( conditions );
    }
}
