package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015
 */
public class ActivityData extends SensorData {

    private static final long serialVersionUID = 103473596237151649L;

    public enum Activity {
        IN_VEHICLE,
        ON_BICYCLE,
        ON_FOOT,
        STILL,
        UNKNOWN,
        TILTING,
        WALKING,
        RUNNING
    }

    private Activity activity;

    /**
     * The conficende in percent (0 - 100).
     */
    private int conficence;

    // serialization constructor
    private ActivityData() {
    }

    public ActivityData( long timestamp, Activity activity, int conficence ) {
        super( timestamp );
        this.activity = activity;
        this.conficence = conficence;
    }

    @Override
    public SensorData copy() {
        return new ActivityData(getTimestamp(), activity, conficence).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public Activity getActivity() {
        return activity;
    }

    public int getConficence() {
        return conficence;
    }

}
