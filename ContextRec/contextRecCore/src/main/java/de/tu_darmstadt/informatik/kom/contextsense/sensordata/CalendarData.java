package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import org.joda.time.DateTime;

import java.util.Iterator;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.MergeableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 13.12.2015.
 */
public class CalendarData extends MergeableSensorData {

    private static final long serialVersionUID = 7565554863838525332L;

    private List<CalendarEntry> events;

    public static class CalendarEntry {
        private String name;
        private DateTime startTime;
        private DateTime endTime;
        private String location;

        // serialization
        private CalendarEntry() {
        }

        public CalendarEntry( String name, DateTime startTime, DateTime endTime, String location ) {
            this.name = name;
            this.startTime = startTime;
            this.endTime = endTime;
            this.location = location;
        }

        public String getName() {
            return name;
        }

        public DateTime getStartTime() {
            return startTime;
        }

        public DateTime getEndTime() {
            return endTime;
        }

        public String getLocation() {
            return location;
        }

        public CalendarEntry copy(){ return new CalendarEntry(name, startTime, endTime, location); }
    }

    // serialization
    private CalendarData() {
    }

    public CalendarData( long timestamp, List<CalendarEntry> events ) {
        super( timestamp );
        this.events = events;
    }

    public List<CalendarEntry> getEvents() {
        return events;
    }


    @Override
    public MergeableSensorData copy() {
        return new CalendarData( getTimestamp(), stream( events ).map(CalendarEntry::copy).collect(Collectors.toList() )).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    @Override
    protected void mergeIntern(SensorData d) {
        CalendarData data = (CalendarData) d;

        stream( data.events )
                .map(CalendarEntry::copy)
                .forEach(events::add);
    }


    @Override
    public void limitFromTo(DateTime from, DateTime to) {
        Iterator<CalendarEntry> entryIterator = events.iterator();
        while(entryIterator.hasNext()){
            CalendarEntry entry = entryIterator.next();
            if(entry.getStartTime().isAfter(from)
                || entry.getEndTime().isBefore(to)) entryIterator.remove();
        }
    }
}
