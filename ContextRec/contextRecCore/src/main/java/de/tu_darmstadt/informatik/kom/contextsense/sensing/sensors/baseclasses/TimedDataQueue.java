package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.contextsense.utils.TimedPair;
import java8.util.stream.Collectors;
import java8.util.stream.Stream;

import static java8.util.stream.RefStreams.empty;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 31.05.2017
 */
public class TimedDataQueue<T> {

    /**
     * The Queue of sensor events saved.
     */
    private final Queue<TimedPair<T>> eventQueue = new LinkedList<>();

    /**
     * the max size to save.
     */
    private final int maxSize;


    /**
     * Creates a max-Sized SensorQueue.
     * @param maxSize the capacity to set.
     */
    public TimedDataQueue(int maxSize){
        this.maxSize = maxSize;
    }


    /**
     * Adds a new Event.
     * @param event to add.
     */
    public void add(TimedPair<T> event){
        synchronized (eventQueue) {
            eventQueue.add(event);
        }

        if(eventQueue.size() > maxSize) {
            synchronized (eventQueue) { eventQueue.poll(); }
        }
    }

    /**
     * Adds a new Event.
     * @param time when this event took place.
     * @param value to add.
     */
    public void add(long time, T value){
        add(new TimedPair<>(time, value));
    }


    /**
     * Adds a new Event.
     * @param value to add.
     */
    public void add(T value){
        add(System.currentTimeMillis(), value);
    }


    /**
     * Gets the events from to a specific date.
     * @param from to get from
     * @param to to get to.
     * @return a stream of data to operate on.
     */
    public Stream<T> getFromTo(DateTime from, DateTime to){
        if(eventQueue.isEmpty()) return empty();

        final long fromMillis = from.getMillis();
        final long toMillis = to.getMillis();

        synchronized (eventQueue){
            return stream(eventQueue)
                .filter(e -> (e.getFirst() >= fromMillis) && (e.getFirst() <= toMillis))
                .map(Pair::getSecond);
        }
    }

    /**
     * Gets the events from to a specific date.
     * @param from to get from
     * @param to to get to.
     * @return a Collection of data.
     */
    public List<T> getFromToCollected(DateTime from, DateTime to){
        if(eventQueue.isEmpty()) return new ArrayList<>();

        final long fromMillis = from.getMillis();
        final long toMillis = to.getMillis();

        synchronized (eventQueue){
            List<T> data = stream(eventQueue)
                    .filter(e -> (e.getFirst() >= fromMillis) && (e.getFirst() <= toMillis))
                    .map( Pair::getSecond )
                    .collect( Collectors.toList() );

            if(!data.isEmpty()) return data;

            //Always get the best value if is empty:
            return stream( eventQueue )
                    .min( (e1,e2) ->
                            Long.compare(
                                    Math.min( Math.abs( e1.getFirst()-fromMillis), Math.abs( e1.getFirst()-toMillis ) ),
                                    Math.min( Math.abs( e2.getFirst()-fromMillis ), Math.abs( e2.getFirst()-toMillis ) )
                            )
                    ).stream()
                    .map( Pair::getSecond )
                    .collect( Collectors.toList() );
        }
    }
    /**
     * Gets the events from to a specific date.
     * @param from to get from
     * @param to to get to.
     * @return a Collection of data.
     */
    public List<TimedPair<T>> getFromToCollectedTimedPair(DateTime from, DateTime to){
        if(eventQueue.isEmpty()) return new ArrayList<>();

        final long fromMillis = from.getMillis();
        final long toMillis = to.getMillis();

        synchronized (eventQueue){
            List<TimedPair<T>> data = stream(eventQueue)
                    .filter(e -> (e.getFirst() >= fromMillis) && (e.getFirst() <= toMillis))
                    .collect( Collectors.toList() );

            if(!data.isEmpty()) return data;

            //Always get the best value if is empty:
            return stream( eventQueue )
                    .min( (e1,e2) ->
                            Long.compare(
                                    Math.min( Math.abs( e1.getFirst()-fromMillis), Math.abs( e1.getFirst()-toMillis ) ),
                                    Math.min( Math.abs( e2.getFirst()-fromMillis ), Math.abs( e2.getFirst()-toMillis ) )
                            )
                    ).stream()
                    .collect( Collectors.toList() );
        }
    }


    /**
     * Clears everything.
     */
    public void clear(){
        eventQueue.clear();
    }


    /**
     * Returns true if the underlying queue is empty.
     */
    public boolean isEmpty() {
        return eventQueue.isEmpty();
    }

    /**
     * This gives the current amount of items in the queue.
     * @return the current amount.
     */
    public int size() {
        return eventQueue.size();
    }

    /**
     * This gives the Capacity of the Queue.
     * @return the max amount (capacity).
     */
    public int capacity() {
        return this.maxSize;
    }

    /**
     * Returns the first date.
     * @return the first time.
     */
    public long first(){
        if(eventQueue.isEmpty()) return 0;
        return stream( eventQueue) .map( TimedPair::getFirst ).min( Long::compare ).orElse(0L);
    }

    /**
     * Returns the first date.
     * @return the first time.
     */
    public long last(){
        if(eventQueue.isEmpty()) return 0;
        return stream( eventQueue ).map( TimedPair::getFirst ).max( Long::compare ).orElse(0L);
    }
}
