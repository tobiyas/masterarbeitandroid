package de.tu_darmstadt.informatik.kom.contextsense.batch;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.tu_darmstadt.informatik.kom.contextsense.ExtractableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import java8.util.Optional;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import static java8.util.stream.StreamSupport.stream;

/**
 * Holds a bunch of sensor-data.
 */
public class MutableSensorDataBatch extends SensorDataBatch {

    private static final long serialVersionUID = -7141403028419918409L;

    private final Map<Class<? extends SensorData>, List<SensorData>> sensorData;
    private DateTime startTime;
    private DateTime endTime;

    public MutableSensorDataBatch( DateTime startTime ) {
        this();
        this.startTime = startTime;
    }

    public MutableSensorDataBatch() {
        this.sensorData = new ConcurrentHashMap<>();
    }

    public void setStartTime( DateTime startTime ) {
        this.startTime = startTime;
    }


    public void addData( SensorData data ) {
        Optional.ofNullable( sensorData.get( data.getClass() ) ).ifPresentOrElse(
                list -> list.add( data ),
                () -> {
                    List<SensorData> list = new ArrayList<>();
                    list.add( data );
                    sensorData.put( data.getClass(), list );
                }
        );
    }

    public void addData( Collection<SensorData> data ) {
        stream( data ).forEach( this::addData );
    }

    @SuppressWarnings("unchecked")
    @Override
    public<T extends SensorData> Optional<T> getData( Class<T> sensorType ) {
        return Optional.ofNullable( sensorData.get( sensorType ) )
                .flatMap( list -> (Optional<T>) stream( list )
                        .findFirst()
                );
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends SensorData> Optional<T> getData( Class<T> sensorType, String source ) {
        return Optional.ofNullable( sensorData.get( sensorType ) )
                .flatMap( list -> (Optional<T>) stream( list )
                        .filter( data -> data.getSourceName().equals( source ) )
                        .findFirst()
                );
    }


    @SuppressWarnings("unchecked")
    @Override
    public <T extends SensorData> Optional<T> getData( Class<T> sensorType, DeviceType source ) {
        return Optional.ofNullable( sensorData.get( sensorType ) )
                .flatMap( list -> (Optional<T>) stream( list )
                        .filter( data -> data.getSourceType().equals( source ) )
                        .findFirst()
                );
    }

    @Override
    public boolean hasDataFromSource( String sourceName ) {
        return stream( sensorData.values() )
                .flatMap( StreamSupport::stream )
                .filter( data -> data.getSourceName().equals( sourceName ) )
                .findFirst()
                .isPresent();
    }


    @Override
    public boolean hasDataFromSourceType( DeviceType sourceType ) {
        return stream( sensorData.values() )
                .flatMap( StreamSupport::stream )
                .filter( data -> data.getSourceType().equals( sourceType ) )
                .findFirst()
                .isPresent();
    }


    @Override
    public SensorDataBatch extractDataFromTo( DateTime from, DateTime to ) {
        MutableSensorDataBatch newBatch = new MutableSensorDataBatch();
        newBatch.setStartTime( from );
        newBatch.setEndTime( to );

        stream( sensorData.values() )
                .flatMap( StreamSupport::stream )
                .filter( data -> data instanceof ExtractableSensorData)
                .map( data -> (ExtractableSensorData) data )
                .map( data -> data.extract( from, to ) )
                .forEach( newBatch::addData );

        return newBatch;
    }


    /**
     * Returns the time, at which the collection of the data for this sensor batch started.
     */
    public DateTime getStartTime() {
        return startTime;
    }

    /**
     * Returns the time, at which the collection of the data for this sensor batch ended.
     */
    public DateTime getEndTime() {
        return endTime;
    }

    public void setEndTime( DateTime endTime ) {
        this.endTime = endTime;
    }

    public List<SensorData> getAllData() {
        return stream( sensorData.values() )
                .flatMap( StreamSupport::stream )
                .collect( Collectors.toList() );
    }

    /**
     * Returns if the DataBatch is empty.
     * @return true if empty.
     */
    public boolean isEmpty() {
        return sensorData.size() == 0;
    }


}
