package de.tu_darmstadt.informatik.kom.contextsense;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import java8.util.stream.IntStreams;
import java8.util.stream.LongStreams;

/**
 * Created by Toby on 25.08.2017
 */

public abstract class MergeableSensorData extends SensorData {


    // for serialization
    protected MergeableSensorData() {
        super();
    }

    protected MergeableSensorData( long timestamp ) {
        super(timestamp);
    }


    /**
     * Merges the other Sensor Data into this data.
     * @param data to merge.
     */
    public final void merge(SensorData data){
        //Can not merge null data:
        if(data == null) return;

        //Wrong class for Casting:
        if(data.getClass() != getClass()) return;

        //Can not merge if we have no source:
        if(getSourceName() == null || data.getSourceName() == null) return;

        //If we do not have the same source -> Do not merge!
        if(!getSourceName().equals(data.getSourceName())) return;

        //All perquisite are met:
        mergeIntern(data);
    }


    /**
     * Merges the other Sensor Data into this data.
     * The parameter data is safe to cast to the current class.
     * Sources are already checked.
     *
     * @param data to merge.
     */
    protected abstract void mergeIntern(SensorData data);

    /**
     * Removes all Data that is not From To the data.
     * @param from the date to use.
     * @param to the date to use.
     */
    public abstract void limitFromTo(DateTime from, DateTime to);

    @Override
    public MergeableSensorData setSourceName(String sourceName) {
        super.setSourceName(sourceName);
        return this;
    }


    @Override
    public MergeableSensorData setSourceType(DeviceType sourceType) {
        super.setSourceType(sourceType);
        return this;
    }

    @Override
    public abstract MergeableSensorData copy();


    /**
     * Helper Method to apply a change to a complete array.
     * @param array to change.
     * @param change to apply.
     */
    protected void applyChangeToArray(long[] array, long change){
        IntStreams.range( 0, array.length )
            .forEach( i -> array[i] += change );
    }

}
