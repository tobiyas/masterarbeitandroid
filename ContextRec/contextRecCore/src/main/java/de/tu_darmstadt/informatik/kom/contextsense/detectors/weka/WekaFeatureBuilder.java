package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.predefinedFeatures.DefaultWearFeatures;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.predefinedFeatures.WekaFeatures;
import java8.util.function.Supplier;
import java8.util.stream.Collectors;
import java8.util.Optional;
import java8.util.function.Function;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import java8.util.stream.IntStreams;
import java8.util.stream.RefStreams;
import weka.core.Attribute;
import weka.core.Instance;

import static de.tu_darmstadt.informatik.kom.contextsense.DataSources.PHONE;
import static de.tu_darmstadt.informatik.kom.contextsense.DataSources.WEAR;
import static java8.util.stream.StreamSupport.stream;

/**
 * Factory for Weka Features.
 */
public class WekaFeatureBuilder {

    private final Map<String, Supplier<WekaFeature>> features;
    private final Map<WekaFeatureGroup, List<String>> groups;

    public WekaFeatureBuilder( boolean loadDefaultFeatures ) {
        features = new HashMap<>();
        groups = RefStreams.of( WekaFeatureGroup.values() )
                .collect( Collectors.toMap( x -> x, x -> new ArrayList<>() ) );

        if (loadDefaultFeatures) {
            WekaFeatures.registerDefaultFeatures( this, PHONE );
            DefaultWearFeatures.registerDefaultFeatures( this, WEAR );
        }
    }

    public void registerNumericFeature( WekaFeatureGroup group, String name, Function<SensorDataBatch, Optional<Double>> function ) {
        verifyFeatureName( name );
        groups.get( group ).add( name );
        features.put( name, () -> new WekaNumericFeature( name, function ) );
    }

    public void registerNumericFeatureWithDefault( WekaFeatureGroup group, String name, Function<SensorDataBatch, Optional<Double>> function, Double defaultValue ) {
        verifyFeatureName( name );
        groups.get( group ).add( name );
        features.put( name, () -> new WekaNumericFeature( name, function, defaultValue ) );
    }

    public void registerNumericFeatureWithDefault0( WekaFeatureGroup group, String name, Function<SensorDataBatch, Optional<Double>> function ) {
        registerNumericFeatureWithDefault( group, name, function, 0d );
    }

    public void registerNumericFeatureWithDefaultMAX( WekaFeatureGroup group, String name, Function<SensorDataBatch, Optional<Double>> function ) {
        registerNumericFeatureWithDefault( group, name, function, Double.MAX_VALUE );
    }

    public void registerNumericFeatureWithDefaultMIN( WekaFeatureGroup group, String name, Function<SensorDataBatch, Optional<Double>> function ) {
        registerNumericFeatureWithDefault( group, name, function, Double.MIN_VALUE );
    }

    public void registerBooleanFeature( WekaFeatureGroup group, String name, Function<SensorDataBatch, Optional<Boolean>> function ) {
        verifyFeatureName( name );
        groups.get( group ).add( name );
        features.put( name, () -> new WekaBooleanFeature( name, function ) );
    }

    public<T extends Enum<T>> void registerEnumFeature( WekaFeatureGroup group, String name, Class<T> enumType, Function<SensorDataBatch, Optional<T>> function ) {
        verifyFeatureName( name );
        groups.get( group ).add( name );
        features.put( name, () -> new WekaEnumFeature<>( name, enumType, function ) );
    }

    public void registerNominalFeature( WekaFeatureGroup group, String name, String[] values, Function<SensorDataBatch, Optional<String>> function ) {
        verifyFeatureName( name );
        groups.get( group ).add( name );
        features.put( name, () -> new WekaNominalFeature( name, values, function ) );
    }

    public void registerStringFeature( WekaFeatureGroup group, String name, Function<SensorDataBatch, Optional<String>> function ) {
        verifyFeatureName( name );
        groups.get( group ).add( name );
        features.put( name, () -> new WekaStringFeature( name, function ) );
    }

    public void registerMultiNumericFeature( WekaFeatureGroup group, String name, int size, Function<SensorDataBatch, Optional<double[]>> function ) {
        verifyFeatureName( name );
        groups.get( group ).add( name );
        features.put( name, () -> new WekaMultiNumericFeature( name, size, function ) );
    }

    public void registerMultiNumericFeatureDefault( WekaFeatureGroup group, String name, int size, Function<SensorDataBatch, Optional<double[]>> function, double defaultValue ) {
        verifyFeatureName( name );
        groups.get( group ).add( name );
        features.put( name, () -> new WekaMultiNumericFeature( name, size, function, defaultValue ) );
    }

    private void verifyFeatureName( String name ) {
        if (name.startsWith( WekaFeature.SITUATION_FEATURE_PREFIX ))
            throw new IllegalArgumentException( "Feature name "+name+" is invalid. Features names may not start with '" + WekaFeature.SITUATION_FEATURE_PREFIX + "' (it's a reserved prefix, sorry)" );
    }

    public boolean hasFeatureNamed( String name ) {
        return name.startsWith( WekaFeature.SITUATION_FEATURE_PREFIX )
                || features.containsKey( name );
    }

    public WekaFeatureSet buildFeatureSet( List<String> names, SituationGroup situationToDetect ) {
        List<String> missingFeatures = stream( names )
                .filter( name -> !name.startsWith( WekaFeature.SITUATION_FEATURE_PREFIX ) )
                .filter( name -> !hasFeatureNamed( name ) )
                .collect( Collectors.toList() );

        if (missingFeatures.size() > 0)
            throw new IllegalArgumentException( "Features named "+missingFeatures+" do not exist." );

        List<WekaFeature> features = stream( names )
                .map( this::buildFeature )
                .collect( Collectors.toList() );

        return new WekaFeatureSet( features, situationToDetect );
    }

    public WekaFeature buildFeature( String name ) {
        // normal feature, but we don't know it's name
        if (!hasFeatureNamed( name ))
            throw new IllegalArgumentException( "Features named "+ name +" do not exist." );

        // we're looking for a situation feature
        if (isSituationFeature( name )) {
            String situation = name.replaceFirst( WekaFeature.SITUATION_FEATURE_PREFIX, "" );
            return new WekaSituationFeature( situation );
        }

        // we're looking for a normal feature
        return features.get( name ).get();
    }

    private boolean isSituationFeature( String name ) {
        return name.startsWith( WekaFeature.SITUATION_FEATURE_PREFIX );
    }

    public List<String> getFeatureNames() {
        return new ArrayList<>( features.keySet() );
    }

    public List<String> getFeatureNames( List<WekaFeatureGroup> featureGroups ) {
        return stream( featureGroups )
                .flatMap( group -> stream( groups.get( group ) ) )
                .collect( Collectors.toList() );
    }

    private static class WekaBooleanFeature extends WekaFeature {
        private final Function<SensorDataBatch, Optional<Boolean>> function;

        public WekaBooleanFeature( String name, Function<SensorDataBatch, Optional<Boolean>> function ) {
            super( new Attribute( name ) );
            this.function = function;
        }

        @Override
        public void apply( Instance instance, SensorDataBatch batch, Map<String,String> otherLabels ) {
            Optional<Boolean> value = function.apply( batch );
            assert value != null;

            value.ifPresent( val -> instance.setValue( getAttribute(), val ? 1 : 0 ) );
        }
    }

    private static class WekaEnumFeature<T extends Enum<T>> extends WekaFeature {

        private final Function<SensorDataBatch, Optional<T>> function;

        public WekaEnumFeature( String name, Class<T> enumType, Function<SensorDataBatch, Optional<T>> function ) {
            super( new Attribute( name, buildListWithAllEnumValues( enumType) ) );
            this.function = function;
        }

        private static List<String> buildListWithAllEnumValues( Class<?> enumType ) {
            return RefStreams.of( enumType.getEnumConstants() )
                    .map( Object::toString )
                    .collect( Collectors.toList() );
        }

        @Override
        public void apply( Instance instance, SensorDataBatch batch, Map<String,String> otherLabels ) {
            Optional<T> value = function.apply( batch );
            assert value != null;

            value.ifPresent( val -> instance.setValue( getAttribute(), val.toString() ) );
        }
    }

    private static class WekaNominalFeature extends WekaFeature {

        private final Function<SensorDataBatch, Optional<String>> function;

        public WekaNominalFeature( String name, String[] values, Function<SensorDataBatch, Optional<String>> function ) {
            super( new Attribute( name, Arrays.asList( values ) ) );
            this.function = function;
        }

        @Override
        public void apply( Instance instance, SensorDataBatch batch, Map<String,String> otherLabels ) {
            Optional<String> value = function.apply( batch );
            assert value != null;

            value.ifPresent( val -> instance.setValue( getAttribute(), val ) );
        }
    }

    private static class WekaNumericFeature extends WekaFeature {

        private final Function<SensorDataBatch, Optional<Double>> function;

        private final Optional<Double> defaultValue;

        public WekaNumericFeature( String name, Function<SensorDataBatch, Optional<Double>> function ) {
            super( new Attribute( name ) );

            this.function = function;
            this.defaultValue = Optional.empty();
        }

        public WekaNumericFeature( String name, Function<SensorDataBatch, Optional<Double>> function, Double defaultValue ) {
            super( new Attribute( name ) );

            this.function = function;
            this.defaultValue = Optional.of( defaultValue );
        }

        @Override
        public void apply( Instance instance, SensorDataBatch batch, Map<String,String> otherLabels ) {
            Optional<Double> optValue = function.apply( batch );
            assert optValue != null;

            if( !optValue.isPresent() || Double.isNaN( optValue.get() ) || Double.isInfinite( optValue.get() ) ) optValue = defaultValue;
            optValue.ifPresent( val -> instance.setValue( getAttribute(), val ) );
        }
    }

    private static class WekaStringFeature extends WekaFeature {

        private final Function<SensorDataBatch, Optional<String>> function;

        public WekaStringFeature( String name, Function<SensorDataBatch, Optional<String>> function ) {
            super( new Attribute( name, (List<String>)null ) );
            this.function = function;
        }

        @Override
        public void apply( Instance instance, SensorDataBatch batch, Map<String,String> otherLabels ) {
            Optional<String> value = function.apply( batch );
            assert value != null;

            value.ifPresent( val -> instance.setValue( getAttribute(), val ) );
        }
    }

    private static class WekaSituationFeature extends WekaFeature {

        private final String situationName;

        public WekaSituationFeature( String situationName ) {
            super( new Attribute( WekaFeature.SITUATION_FEATURE_PREFIX + situationName, (List<String>)null ) );
            this.situationName = situationName;
        }

        @Override
        public void apply( Instance instance, SensorDataBatch batch, Map<String,String> otherLabels ) {
            Optional
                    .ofNullable( otherLabels.get( situationName ) )
                    .ifPresent( val -> instance.setValue( getAttribute(), val ) );
        }
    }

    private static class WekaMultiNumericFeature extends WekaFeature {
        private final Function<SensorDataBatch, Optional<double[]>> function;
        private final double defaultValue;

        public WekaMultiNumericFeature( String name, int size, Function<SensorDataBatch, Optional<double[]>> function ) {
            super( buildFeatures( name, size ), name );
            this.function = function;
            this.defaultValue = Double.NaN;
        }

        public WekaMultiNumericFeature( String name, int size, Function<SensorDataBatch, Optional<double[]>> function, double defaultValue ) {
            super( buildFeatures( name, size ), name );
            this.function = function;
            this.defaultValue = defaultValue;
        }

        private static List<Attribute> buildFeatures( String name, int size ) {
            return IntStreams.range( 0, size )
                    .mapToObj( i -> new Attribute( name + "(part " + i+")" ) )
                    .collect( Collectors.toList() );
        }

        @Override
        public void apply( Instance instance, SensorDataBatch batch, Map<String,String> otherLabels ) {
            Optional<double[]> value = function.apply( batch );
            assert value != null;

            value.ifPresent( val -> {
                for( int i = 0; i < val.length; i++ ) {
                    instance.setValue( getAttributes().get( i ), val[i] );
                }
            } );
        }
    }
}
