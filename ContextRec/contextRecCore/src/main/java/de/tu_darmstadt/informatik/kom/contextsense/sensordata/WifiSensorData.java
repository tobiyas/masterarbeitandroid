package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.Optional;

/**
 * Created by Oli on 13.12.2015.
 */
public class WifiSensorData extends SensorData {

    private static final long serialVersionUID = 4265622658930798284L;

    private boolean isEnabled;
    private boolean isConnected;
    private Optional<String> currentNetworkSsid;
    private Optional<List<String>> nearbyNetworks;

    public WifiSensorData( long timestamp, boolean isEnabled, boolean isConnected, Optional<String> currentNetworkSsid, Optional<List<String>> nearbyNetworks ) {
        super( timestamp );
        this.isEnabled = isEnabled;
        this.isConnected = isConnected;
        this.currentNetworkSsid = currentNetworkSsid;
        this.nearbyNetworks = nearbyNetworks;
    }

    @Override
    public SensorData copy() {
        return new WifiSensorData(getTimestamp(), isEnabled, isConnected, Optional.ofNullable(currentNetworkSsid.orElse(null)), Optional.ofNullable(nearbyNetworks.orElse(null))).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    // serialization
    private WifiSensorData() {
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public Optional<String> getCurrentNetworkSsid() {
        return currentNetworkSsid;
    }

    public Optional<List<String>> getNearbyNetworks() {
        return nearbyNetworks;
    }
}
