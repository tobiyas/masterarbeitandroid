package de.tu_darmstadt.informatik.kom.contextsense.sensing;

import java.io.Serializable;

/**
 * Created by Toby on 03.09.2017
 */

public class DeviceType implements Serializable {


    public static final DeviceType PHONE = new DeviceType( "PHONE" );
    public static final DeviceType WEAR = new DeviceType( "WEAR" );
    public static final DeviceType IMU_THUNDERBOARD = new DeviceType( "IMU_THUNDERBOARD" );

    public static final DeviceType UNKNOWN = new DeviceType( "UNKNOWN" );


    private String typeName;

    private DeviceType(){
        //Constructor only for Deserialization!
    }

    public DeviceType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) || toString().equals(obj.toString());
    }

    @Override
    public int hashCode() {
        return typeName.hashCode();
    }

    @Override
    public String toString() {
        return typeName;
    }
}
