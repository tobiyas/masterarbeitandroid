package de.tu_darmstadt.informatik.kom.contextsense.sensing.lowpower;

import android.content.Context;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.List;

/**
 * A version of {@link LowPowerModeDetector} which uses the significant-motion sensor.
 */
public class SignificantMotionLowPowerModeDetector extends TriggerEventListener implements LowPowerModeDetector {

    private static final String TAG = "LOCATION Status Sensor";
    private static final Duration NO_MOTION_TIMEOUT = Duration.standardMinutes( 3 );

    private final SensorManager sensorManager;
    private final Handler timeoutHandler;
    private final List<LowPowerModeCallback> listeners;

    private boolean isRunning;
    private boolean isInLowPowerMode;

    public SignificantMotionLowPowerModeDetector( Context context ) {
        this.listeners = new ArrayList<>();
        this.sensorManager = (SensorManager) context.getSystemService( Context.SENSOR_SERVICE );
        this.timeoutHandler = new Handler( Looper.getMainLooper() );
        this.isRunning = false;
        this.isInLowPowerMode = false;
    }

    public void start() {
        isRunning = true;
        registerSignificantMotionSensor();
    }

    public void stop() {
        isRunning = false;
        cancelSignificantMotionSensor();
    }

    @Override
    public void addListener( LowPowerModeCallback listener ) {
        listeners.add( listener );
    }

    @Override
    public void removeListener( LowPowerModeCallback listener ) {
        listeners.remove( listener );
    }

    private void registerSignificantMotionSensor() {
        android.hardware.Sensor sensor = sensorManager.getDefaultSensor( android.hardware.Sensor.TYPE_SIGNIFICANT_MOTION );

        if (sensor != null) {
            timeoutHandler.postDelayed( this::noMotionTimeout, NO_MOTION_TIMEOUT.getMillis() );
            sensorManager.requestTriggerSensor( this, sensor );
        } else {
            Log.w( TAG, "Device has no significant motion sensor. App could drain battery. Sorry :-(" );
        }
    }

    private void cancelSignificantMotionSensor() {
        android.hardware.Sensor sensor = sensorManager.getDefaultSensor( android.hardware.Sensor.TYPE_SIGNIFICANT_MOTION );
        if (sensor != null) {
            sensorManager.cancelTriggerSensor( this, sensor );
            timeoutHandler.removeCallbacksAndMessages( null );
        }
    }

    @Override
    public void onTrigger( TriggerEvent event ) {
        timeoutHandler.removeCallbacksAndMessages( null );
        if (isRunning) {
            if (isInLowPowerMode) {
                isInLowPowerMode = false;
                for (LowPowerModeCallback listener: listeners) {
                    listener.onLowPowerExited();
                }
            }

            registerSignificantMotionSensor();
        }
    }

    private void noMotionTimeout() {
        if (!isInLowPowerMode) {
            isInLowPowerMode = true;
            for (LowPowerModeCallback listener: listeners) {
                listener.onLowPowerEntered();
            }
        }
    }

    public boolean isInLowPowerMode() {
        return isInLowPowerMode;
    }
}
