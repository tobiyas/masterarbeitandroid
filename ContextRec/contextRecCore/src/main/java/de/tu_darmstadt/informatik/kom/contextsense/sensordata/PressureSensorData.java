package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.MergeableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.stream.DoubleStreams;

import static de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayUtils.concat;

/**
 * Created by Oli on 13.12.2015
 */
public class PressureSensorData extends MergeableSensorData {

    private static final long serialVersionUID = 2712825132019268404L;

    private double pressureInHPA;
    private double[] pressureData;

    public PressureSensorData( long timestamp, double[] pressureData ) {
        super( timestamp );
        this.pressureInHPA = DoubleStreams.of( pressureData ).average().getAsDouble();
        this.pressureData = pressureData;
    }

    private PressureSensorData() {
    }

    public double getPressureInHPA() {
        return pressureInHPA;
    }

    public double[] getPressureData() {
        return pressureData;
    }

    @Override
    public MergeableSensorData copy() {
        return new PressureSensorData(getTimestamp(), pressureData).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    @Override
    protected void mergeIntern(SensorData d) {
        PressureSensorData data = (PressureSensorData) d;
        pressureData = concat(data.pressureData, pressureData);
        pressureInHPA = DoubleStreams.of(pressureData).average().getAsDouble();
    }

    @Override
    public void limitFromTo(DateTime from, DateTime to) {
        //Not possible here. :(
    }
}
