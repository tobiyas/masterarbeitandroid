package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import java8.util.stream.Collectors;
import java8.util.stream.Stream;

import static java8.util.stream.RefStreams.empty;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 31.05.2017
 */
public class SensorEventQueue {

    /**
     * The Queue of sensor events saved.
     */
    private final Queue<SensorEventCopy> eventQueue = new LinkedList<>();

    /**
     * the max size to save.
     */
    private final int maxSize;


    /**
     * Creates a max-Sized SensorQueue.
     * @param maxSize the capacity to set.
     */
    public SensorEventQueue(int maxSize){
        this.maxSize = maxSize;
    }


    /**
     * Adds a new Event.
     * @param event to add.
     */
    public void add(SensorEventCopy event){
        synchronized (eventQueue) {
            eventQueue.add(event);
        }

        if(eventQueue.size() > maxSize) {
            synchronized (eventQueue) { eventQueue.poll(); }
        }
    }


    /**
     * Gets the events from to a specific date.
     * @param from to get from
     * @param to to get to.
     * @return a stream of data to operate on.
     */
    public Stream<SensorEventCopy> getFromTo(DateTime from, DateTime to){
        if(eventQueue.isEmpty()) return empty();

        final long fromMillis = from.getMillis();
        final long toMillis = to.getMillis();

        synchronized (eventQueue){
            return stream(eventQueue)
                .filter(e -> (e.timestamp >= fromMillis) && (e.timestamp <= toMillis));
        }
    }

    /**
     * Gets the events from to a specific date.
     * @param from to get from
     * @param to to get to.
     * @return a Collection of data.
     */
    public List<SensorEventCopy> getFromToCollected(DateTime from, DateTime to){
        if(eventQueue.isEmpty()) return new ArrayList<>();

        final long fromMillis = from.getMillis();
        final long toMillis = to.getMillis();

        synchronized (eventQueue){
            List<SensorEventCopy> data = stream(eventQueue)
                    .filter(e -> (e.timestamp >= fromMillis) && (e.timestamp <= toMillis))
                    .collect(Collectors.toList());

            if(!data.isEmpty()) return data;

            //Always get the best value if is empty:
            return stream( eventQueue )
                    .min( (e1,e2) ->
                            Long.compare(
                                    Math.min( Math.abs( e1.timestamp-fromMillis), Math.abs( e1.timestamp-toMillis ) ),
                                    Math.min( Math.abs( e2.timestamp-fromMillis ), Math.abs( e2.timestamp-toMillis ) )
                                )
                    ).stream()
                    .collect(Collectors.toList());
        }
    }


    /**
     * Clears everything.
     */
    public void clear(){
        eventQueue.clear();
    }


    /**
     * Returns true if the underlying queue is empty.
     */
    public boolean isEmpty() {
        return eventQueue.isEmpty();
    }

    /**
     * This gives the current amount of items in the queue.
     * @return the current amount.
     */
    public int size() {
        return eventQueue.size();
    }

    /**
     * This gives the Capacity of the Queue.
     * @return the max amount (capacity).
     */
    public int capacity() {
        return this.maxSize;
    }

    /**
     * Returns the first date.
     * @return the first time.
     */
    public long first(){
        if(eventQueue.isEmpty()) return 0;
        return stream( eventQueue) .map(c -> c.timestamp).min(Long::compare).orElse(0L);
    }

    /**
     * Returns the first date.
     * @return the first time.
     */
    public long last(){
        if(eventQueue.isEmpty()) return 0;
        return stream( eventQueue ).map(c -> c.timestamp).max(Long::compare).orElse(0L);
    }
}
