package de.tu_darmstadt.informatik.kom.contextsense.utils;


import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toby on 20.10.2017
 */

public class TimingsRecords {

    /**
     * The used Timings.
     */
    private final DefaultHashMap<String,Duration> timings = new DefaultHashMap<>( Duration.ZERO );

    /**
     * The Time the action started.
     */
    private DateTime actionStart = DateTime.now();

    /**
     * The started action.
     */
    private String action = "";


    /**
     * Adds new Time.
     * @param action to add for.
     * @param time to add.
     * @return this for chaining.
     */
    public TimingsRecords addTimings( String action, Duration time ){
        if( action == null || time == null ) return this;

        timings.put( action, timings.get( action ).plus( time ) );
        return this;
    }

    /**
     * Gets the Timings added.
     * @return the added timings.
     */
    public Map<String,Duration> getTimings(){
        return new HashMap<>( timings );
    }


    /**
     * Starts the Action passed.
     * @param actionToStart to start.
     */
    public void startAction( String actionToStart ){
        if( actionToStart == null ) actionToStart = "UNKNOWN";

        this.action = actionToStart;
        this.actionStart = DateTime.now();
    }


    /**
     * Ends the current Action and logs it.
     */
    public void endCurrentAction(){
        if( this.action == null || this.action.isEmpty() ) return;

        Duration duration = new Duration( this.actionStart, DateTime.now() );
        addTimings( this.action, duration );
    }

    /**
     * Ends the current Segment and starts a new one.
     * @param actionToStart the action to use for starting the new Segment..
     */
    public void logActionAndStartNew( String actionToStart ){
        endCurrentAction();
        startAction( actionToStart );
    }
}
