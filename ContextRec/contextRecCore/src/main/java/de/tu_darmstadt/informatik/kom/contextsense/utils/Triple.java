package de.tu_darmstadt.informatik.kom.contextsense.utils;

/**
 * Represents a pair of two items.
 */
public class Triple<S, T, U> {

    private final S first;
    private final T second;
    private final U third;

    public Triple(S first, T second, U third ) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public S getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }

    public U getThird() {
        return third;
    }
}
