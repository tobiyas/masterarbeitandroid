package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.predefinedFeatures;


import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Arrays;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureBuilder;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerometerHistoryData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ActivityData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AudioStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.CalendarData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.GpsSpeedHistoryData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.GpsStatusData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.GyroscopeSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LightSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LinearAccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LocationSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MagnetometerData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MagnetometerHistoryData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MicrophoneData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PhoneStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PowerStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PressureSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ProximityData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ScreenStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.StepCounterData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.WifiSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayInterpolator;
import de.tu_darmstadt.informatik.kom.contextsense.utils.FFT;
import de.tu_darmstadt.informatik.kom.contextsense.utils.MathUtils;
import java8.util.stream.Collectors;
import java8.util.stream.DoubleStreams;
import java8.util.stream.IntStreams;
import java8.util.Optional;

import static de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureGroup.*;
import static java8.util.stream.StreamSupport.stream;

/**
 * List of all pre-defined features for the WEKA classifier.
 * These are "unused", because the client fetches the list of features by reflection.
 */
@SuppressWarnings("unused")
public class WekaFeatures {

    private static final double GRAVITY_ON_EARTH = 9.81;

    public static void registerDefaultFeatures( WekaFeatureBuilder registry, String sourceName ) {

        registry.registerNumericFeature( INERTIAL, "Acceleration history stdev", sample -> sample
                .getData( AccelerometerHistoryData.class, sourceName )
                .map( AccelerometerHistoryData::calcMag )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeature( INERTIAL, "Rotation per sec (last 10 sec)", sample -> sample
                .getData( MagnetometerHistoryData.class, sourceName )
                .flatMap( data -> data.getT().length > 1 ? Optional.of( data ) : Optional.empty() )
                .map( data -> {
                    List<double[]> orientations = IntStreams
                            .range( 1, data.getT().length )
                            .mapToObj( i -> {
                                double x = data.getX()[i];
                                double y = data.getY()[i];
                                double z = data.getZ()[i];
                                double length = Math.sqrt( x*x + y*y + z*z );
                                return new double[] { x/length, y/length, z/length };
                            }).collect( Collectors.toList() );

                    double totalRotation = IntStreams.range( 1, orientations.size() )
                            .mapToDouble( i -> Math.acos(
                                    orientations.get( i )[0] * orientations.get( i -1 )[0] +
                                    orientations.get( i )[1] * orientations.get( i -1 )[1] +
                                    orientations.get( i )[2] * orientations.get( i -1 )[2] ) )
                            .sum();

                    long[] t = data.getT();
                    long totalTimeInNs = t[t.length -1] - t[0];

                    double rotPerSec = totalRotation / (totalTimeInNs / 1_000_000_000.0 );
                    return rotPerSec;
                })
        );

        registry.registerNumericFeature( INERTIAL, "Avg. Acceleration (X)", sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getAvgNormalizedX )
        );

        registry.registerNumericFeature( INERTIAL, "Avg. Acceleration (Y)", sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getAvgNormalizedY )
        );

        registry.registerNumericFeature( INERTIAL, "Avg. Acceleration (Z)", sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getAvgNormalizedZ )
        );

        registry.registerNumericFeature( INERTIAL, "Avg. Linear Acc (X)", sample -> sample
                .getData( LinearAccelerationData.class, sourceName )
                .map( LinearAccelerationData::getX )
                .map( acc -> DoubleStreams.of( acc ).average().getAsDouble() )
        );

        registry.registerNumericFeature( INERTIAL, "Avg. Linear Acc (Y)", sample -> sample
                .getData( LinearAccelerationData.class, sourceName )
                .map( LinearAccelerationData::getY )
                .map( acc -> DoubleStreams.of( acc ).average().getAsDouble() )
        );

        registry.registerNumericFeature( INERTIAL, "Avg. Linear Acc (Z)", sample -> sample
                .getData( LinearAccelerationData.class, sourceName )
                .map( LinearAccelerationData::getZ )
                .map( acc -> DoubleStreams.of( acc ).average().getAsDouble() )
        );

        registry.registerNumericFeature( INERTIAL, "Activity Count (Lin acc x)", sample -> sample
                .getData( LinearAccelerationData.class, sourceName )
                .map( LinearAccelerationData::getX )
                .map( data -> {
                    long samplesAboveThreshold = DoubleStreams.of( data )
                            .filter( d -> (Math.abs( d ) > 0.5 * GRAVITY_ON_EARTH ) )
                            .count();

                    return samplesAboveThreshold / (double)data.length;
                })
        );

        registry.registerNumericFeature( INERTIAL, "Activity Count (Lin acc y)", sample -> sample
                .getData( LinearAccelerationData.class, sourceName )
                .map( LinearAccelerationData::getY )
                .map( data -> {
                    long samplesAboveThreshold = DoubleStreams.of( data )
                            .filter( d -> (Math.abs( d ) > 0.5 * GRAVITY_ON_EARTH ) )
                            .count();

                    return samplesAboveThreshold / (double)data.length;
                })
        );

        registry.registerNumericFeature( INERTIAL, "Activity Count (Lin acc z)", sample -> sample
                .getData( LinearAccelerationData.class, sourceName )
                .map( LinearAccelerationData::getZ )
                .map( data -> {
                    long samplesAboveThreshold = DoubleStreams.of( data )
                            .filter( d -> (Math.abs( d ) > 0.5 * GRAVITY_ON_EARTH ) )
                            .count();

                    return samplesAboveThreshold / (double)data.length;
                })
        );

        registry.registerMultiNumericFeature( INERTIAL, "Acceleration (X) 0-10Hz", 30, sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getX )
                .map( a -> ArrayInterpolator.interpolateLinear( a, 256) )
                .map( data -> {
                    double[] ffted = FFT.fft( Arrays.copyOf( data, 256 ) );
                    return Arrays.copyOf( ffted, 30 );
                })
        );

        registry.registerMultiNumericFeature( INERTIAL, "Acceleration (Y) 0-10Hz", 30, sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getY )
                .map( a -> ArrayInterpolator.interpolateLinear( a, 256) )
                .map( data -> {
                    double[] ffted = FFT.fft( Arrays.copyOf( data, 256 ) );
                    return Arrays.copyOf( ffted, 30 );
                })
        );

        registry.registerMultiNumericFeature( INERTIAL, "Acceleration (Z) 0-10Hz", 30, sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getZ )
                .map( a -> ArrayInterpolator.interpolateLinear( a, 256) )
                .map( data -> {
                    double[] ffted = FFT.fft( Arrays.copyOf( data, 256 ) );
                    return Arrays.copyOf( ffted, 30 );
                })
        );

        registry.registerNumericFeature( INERTIAL, "Acceleration Range (X)", sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getX )
                .map( acc -> {
                    double min = DoubleStreams.of( acc ).min().getAsDouble();
                    double max = DoubleStreams.of( acc ).max().getAsDouble();
                    return max - min;
                } )
        );

        registry.registerNumericFeature( INERTIAL, "Acceleration Range (Y)", sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getY )
                .map( acc -> {
                    double min = DoubleStreams.of( acc ).min().getAsDouble();
                    double max = DoubleStreams.of( acc ).max().getAsDouble();
                    return max - min;
                } )
        );

        registry.registerNumericFeature( INERTIAL, "Acceleration Range (Z)", sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getZ )
                .map( acc -> {
                    double min = DoubleStreams.of( acc ).min().getAsDouble();
                    double max = DoubleStreams.of( acc ).max().getAsDouble();
                    return max - min;
                } )
        );

        registry.registerNumericFeature( INERTIAL, "Stdev. Acceleration (X)", sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getX )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeature( INERTIAL, "Stdev. Acceleration (Y)", sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getY )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeature( INERTIAL, "Stdev. Acceleration (Z)", sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( AccelerationData::getZ )
                .map( MathUtils::stdev )
        );

        registry.registerMultiNumericFeature( INERTIAL, "Acceleration X Sliced MiddleValue", 20, sample -> sample
                .getData(AccelerationData.class, sourceName)
                .map( AccelerationData::getX )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 20)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i, i + 10 ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerMultiNumericFeature( INERTIAL, "Acceleration Y Sliced MiddleValue", 20, sample -> sample
                .getData(AccelerationData.class, sourceName)
                .map( AccelerationData::getY )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 20)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i, i + 10 ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerMultiNumericFeature( INERTIAL, "Acceleration Z Sliced MiddleValue", 20, sample -> sample
                .getData(AccelerationData.class, sourceName)
                .map( AccelerationData::getZ )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 20)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i, i + 10 ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerNumericFeature( INERTIAL, "Mag. Field (X)", sample -> sample
                .getData( MagnetometerData.class, sourceName )
                .map( MagnetometerData::getX )
                .map( mag -> DoubleStreams.of( mag ).average().getAsDouble() )
        );

        registry.registerNumericFeature( INERTIAL, "Mag. Field (Y)", sample -> sample
                .getData( MagnetometerData.class, sourceName )
                .map( MagnetometerData::getY )
                .map( mag -> DoubleStreams.of( mag ).average().getAsDouble() )
        );

        registry.registerNumericFeature( INERTIAL, "Mag. Field (Z)", sample -> sample
                .getData( MagnetometerData.class, sourceName )
                .map( MagnetometerData::getZ )
                .map( mag -> DoubleStreams.of( mag ).average().getAsDouble() )
        );

        registry.registerBooleanFeature( LIGHT_PROXIMITY, "Proximity", sample -> sample
                .getData( ProximityData.class, sourceName )
                .map( ProximityData::isClose )
        );

        registry.registerNumericFeature( LIGHT_PROXIMITY, "Brightness", sample -> sample
                .getData( LightSensorData.class, sourceName )
                .map( LightSensorData::getBrightnessInLux )
        );

        registry.registerNumericFeature( LIGHT_PROXIMITY, "Pressure", sample -> sample
                .getData( PressureSensorData.class, sourceName )
                .flatMap( data -> Optional.ofNullable( data.getPressureData() ) )
                .map( data -> {
                    double first = data[0];
                    double last = data[data.length -1];
                    return last - first;
                })
        );

        registry.registerBooleanFeature( USER_INFOS, "Is in calendar event", sample -> sample
                .getData( CalendarData.class )
                .map( data -> {
                    DateTime now = sample.getStartTime();
                    return stream( data.getEvents() )
                            .anyMatch( event -> event.getStartTime().isBefore( now ) && event.getEndTime().isAfter( now ) );
                })
        );

        registry.registerEnumFeature( DEVICE_USAGE, "Screen orientation", ScreenStateData.ScreenOrientation.class, sample -> sample
                .getData( ScreenStateData.class )
                .map( ScreenStateData::getOrientation )
        );

        registry.registerBooleanFeature( DEVICE_USAGE, "Is screen on", sample -> sample
                .getData( ScreenStateData.class, sourceName )
                .map( ScreenStateData::isScreenOn )
        );

        registry.registerBooleanFeature( DEVICE_USAGE, "Is in call", sample -> sample
                .getData( PhoneStateData.class )
                .map( PhoneStateData::isInCall )
        );

        registry.registerEnumFeature( DEVICE_USAGE, "Charging state", PowerStateData.ChargingState.class, sample -> sample
                .getData( PowerStateData.class, sourceName )
                .map( PowerStateData::getChargingState )
        );

        registry.registerNumericFeature( DEVICE_USAGE, "Battery Level", sample -> sample
                .getData( PowerStateData.class, sourceName )
                .map( PowerStateData::getBatteryPercent )
        );

        registry.registerNumericFeature( INERTIAL, "Steps per second", sample -> sample
                .getData( StepCounterData.class, sourceName )
                .map( StepCounterData::getStepsPerSec )
        );

        registry.registerBooleanFeature( CONNECTIVITY, "Is wifi connected", sample -> sample
                .getData( WifiSensorData.class )
                .map( WifiSensorData::isConnected )
        );

        registry.registerBooleanFeature( CONNECTIVITY, "Is wifi enabled", sample -> sample
                .getData( WifiSensorData.class )
                .map( WifiSensorData::isEnabled )
        );

        registry.registerNumericFeature( LOCATION, "Location (latitude)", sample -> sample
                .getData( LocationSensorData.class )
                .map( LocationSensorData::getLatitude )
        );

        registry.registerNumericFeature( LOCATION, "Location (longitude)", sample -> sample
                .getData( LocationSensorData.class )
                .map( LocationSensorData::getLongitude )
        );

        registry.registerBooleanFeature( GPS_INFO, "Is in city", sample -> sample
                .getData( LocationSensorData.class )
                .map( LocationSensorData::getAddress )
                .map( Optional::isPresent )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Number of satellites", sample -> sample
                .getData( GpsStatusData.class )
                .flatMap( GpsStatusData::getNumberOfSatellites )
                .map( Double::valueOf )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Signal to noise", sample -> sample
                .getData( GpsStatusData.class )
                .flatMap( GpsStatusData::getAverageSNR )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Time to fix", sample -> sample
                .getData( GpsStatusData.class )
                .flatMap( GpsStatusData::getTimeToFirstFixInMs )
                .map( Double::valueOf )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Speed", sample -> sample
                .getData( LocationSensorData.class )
                .map( LocationSensorData::getSpeed )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Speed stdev (1 min)", sample -> sample
                .getData( GpsSpeedHistoryData.class )
                .map( data -> data.onlyLastSeconds( Duration.standardSeconds( 65 ) ) )
                .map( GpsSpeedHistoryData::getSpeedInMs )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Speed avg (1 min)", sample -> sample
                .getData( GpsSpeedHistoryData.class )
                .map( data -> data.onlyLastSeconds( Duration.standardSeconds( 65 ) ) )
                .map( GpsSpeedHistoryData::getSpeedInMs )
                .map( speed -> MathUtils.average( speed, Double.NaN ) )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Speed stdev (2.5 min)", sample -> sample
                .getData( GpsSpeedHistoryData.class )
                .map( data -> data.onlyLastSeconds( Duration.standardSeconds( 150 ) ) )
                .map( GpsSpeedHistoryData::getSpeedInMs )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Speed avg (2.5 min)", sample -> sample
                .getData( GpsSpeedHistoryData.class )
                .map( data -> data.onlyLastSeconds( Duration.standardSeconds( 150 ) ) )
                .map( GpsSpeedHistoryData::getSpeedInMs )
                .map( speed -> MathUtils.average( speed, Double.NaN ) )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Speed stdev (5 min)", sample -> sample
                .getData( GpsSpeedHistoryData.class )
                .map( data -> data.onlyLastSeconds( Duration.standardSeconds( 300 ) ) )
                .map( GpsSpeedHistoryData::getSpeedInMs )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeature( GPS_INFO, "Gps: Speed avg (5 min)", sample -> sample
                .getData( GpsSpeedHistoryData.class )
                .map( data -> data.onlyLastSeconds( Duration.standardSeconds( 300 ) ) )
                .map( GpsSpeedHistoryData::getSpeedInMs )
                .map( speed -> MathUtils.average( speed, Double.NaN ) )
        );

        registry.registerNumericFeature( MICROPHONE, "Ambient Noise (db)", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .map( MicrophoneData::getNoiseLevelInDb )
        );

        registry.registerNumericFeature( MICROPHONE, "Audio spectrum mean", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getMean )
        );

        registry.registerNumericFeature( MICROPHONE, "Audio spectrum stdev", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getStdev )
        );

        registry.registerNumericFeature( MICROPHONE, "Audio spectrum dc", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getDc )
        );

        registry.registerNumericFeature( MICROPHONE, "Audio spectrum 0Hz - 3000Hz dominant freq", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getBin1HighestFreq )
        );

        registry.registerNumericFeature( MICROPHONE, "Audio spectrum 3000Hz - 8000Hz dominant freq", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getBin2HighestFreq )
        );

        registry.registerNumericFeature( MICROPHONE, "Audio spectrum 8000Hz - 22000Hz dominant freq", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getBin3HighestFreq )
        );

        registry.registerNumericFeature( MICROPHONE, "Audio spectrum 0Hz - 3000Hz highest power", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( data -> data.getBin1HighestPower()  )
        );

        registry.registerNumericFeature( MICROPHONE, "Audio spectrum 3000Hz - 8000Hz highest power", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( data -> data.getBin2HighestPower()  )
        );

        registry.registerNumericFeature( MICROPHONE, "Audio spectrum 8000Hz - 22000Hz highest power", sample -> sample
                .getData( MicrophoneData.class, sourceName )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( data -> data.getBin3HighestPower()  )
        );

        registry.registerNumericFeature( CONNECTIVITY, "Number of Wifi networks", sample -> sample
                .getData( WifiSensorData.class )
                .map( WifiSensorData::getNearbyNetworks )
                .flatMap( nets -> nets
                        .map( netList -> Optional.of( (double)netList.size() ) )
                        .orElse( Optional.empty() )
                )
        );

        registry.registerStringFeature( LOCATION, "Connected Wifi SSID", sample -> sample
                .getData( WifiSensorData.class )
                .map( wifi -> wifi.getCurrentNetworkSsid().orElse( "Not Connected" ) )
        );

        registry.registerNumericFeature( TIME, "Time of day", sample -> Optional.of(
                (double)(sample.getStartTime().toLocalDateTime().getHourOfDay())
        ));

        registry.registerNominalFeature( TIME, "Day of week", new String[] { "1", "2", "3", "4", "5", "6", "7" }, sample -> Optional.of(
                String.valueOf( sample.getStartTime().toLocalDateTime().getDayOfWeek() )
        ));

        registry.registerBooleanFeature( DEVICE_USAGE, "Is headset connected", sample -> sample
                .getData( AudioStateData.class )
                .map( AudioStateData::isHeadsetConnected )
        );

        registry.registerBooleanFeature( DEVICE_USAGE, "Is playing music", sample -> sample
                .getData( AudioStateData.class )
                .map( AudioStateData::isMusicActive )
        );

        registry.registerEnumFeature( DEVICE_USAGE, "Ringer state", AudioStateData.RingerState.class, sample -> sample
                .getData( AudioStateData.class )
                .map( AudioStateData::getRingerState )
        );

        registry.registerEnumFeature( INERTIAL, "User Activity", ActivityData.Activity.class, sample -> sample
                .getData( ActivityData.class, sourceName )
                .map( ActivityData::getActivity )
        );

        /* //TODO This has to be fixed! We need to get this to 200 length, or something like that....
        registry.registerMultiNumericFeature( RAW, "Raw Accelerometer", 600, sample -> sample
                .getData( AccelerationData.class, sourceName )
                .map( data -> {
                    double[] res = new double[600];
                    System.arraycopy( data.getX(), 0, res, 0, 200 );
                    System.arraycopy( data.getY(), 0, res, 200, 200 );
                    System.arraycopy( data.getZ(), 0, res, 400, 200 );
                    return res;
                })
        );

        registry.registerMultiNumericFeature( RAW, "Raw Gyro", 600, sample -> sample
                .getData( GyroscopeSensorData.class, sourceName )
                .map( data -> {
                    double[] res = new double[600];
                    System.arraycopy( data.getX(), 0, res, 0, 200 );
                    System.arraycopy( data.getY(), 0, res, 200, 200 );
                    System.arraycopy( data.getZ(), 0, res, 400, 200 );
                    return res;
                })
        );

        registry.registerMultiNumericFeature( RAW, "Raw Magnetometer", 300, sample -> sample
                .getData( MagnetometerData.class, sourceName )
                .map( data -> {
                    double[] res = new double[300];
                    System.arraycopy( data.getX(), 0, res, 0, 100 );
                    System.arraycopy( data.getY(), 0, res, 100, 100 );
                    System.arraycopy( data.getZ(), 0, res, 200, 100 );
                    return res;
                })
        );
        */
    }
}
