package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.AndPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.OrPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.RequiresDetectorPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.SensorPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.SituationPrecondition;

/**
 * Created by Oli on 24.03.2016.
 */
public abstract class PreconditionVisitor<T> {

    public abstract T visit( AndPrecondition and );
    public abstract T visit( OrPrecondition or );
    public abstract T visit( NoPrecondition no );
    public abstract T visit( SensorPrecondition sensor );
    public abstract T visit( SituationPrecondition situation );
    public abstract T visit( RequiresDetectorPrecondition detectorPrecondition );

}
