package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.PreconditionVisitor;

/**
 * Created by Oli on 26.03.2016.
 */
public class RequiresDetectorPrecondition extends Precondition {

    private String situationGroupName;

    // for serialization only!
    private RequiresDetectorPrecondition() {
    }

    public RequiresDetectorPrecondition( String situationGroupName ) {
        this();
        this.situationGroupName = situationGroupName;
    }

    @Override
    public <T> T accept( PreconditionVisitor<T> visitor ) {
        return visitor.visit( this );
    }

    public String getSituationGroupName() {
        return situationGroupName;
    }
}
