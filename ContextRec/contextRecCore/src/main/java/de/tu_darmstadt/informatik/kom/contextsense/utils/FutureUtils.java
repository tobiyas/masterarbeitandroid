package de.tu_darmstadt.informatik.kom.contextsense.utils;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import java8.util.Optional;

/**
 * Utils for working with Java's Future<T> Type
 */
public class FutureUtils {

    /**
     * Get the result (using get()) and ignore possible Exception.
     * In case of an Exception, return Optional.Empty.
     */
    public static <T> Optional<T> getAndPropagateInterruptsQuietly( CancelableFuture<T> future ) {
        try {
            return Optional.of( future.get() );
        } catch (InterruptedException | ExecutionException e) {

            try {
                future.cancelAndWait( true );
            } catch (CancellationException e1) {
                // no need to print out the exception, it's just cancellation.
            } catch (Exception e1) {
                // something different went wrong.
                e1.printStackTrace();
            }

            Thread.currentThread().interrupt();
            return Optional.empty();
        }
    }

}
