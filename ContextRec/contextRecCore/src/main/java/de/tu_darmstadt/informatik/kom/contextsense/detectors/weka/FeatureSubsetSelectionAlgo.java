package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka;

import java8.util.function.Function;
import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.attributeSelection.BestFirst;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.CorrelationAttributeEval;
import weka.attributeSelection.GainRatioAttributeEval;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.PrincipalComponents;
import weka.attributeSelection.Ranker;
import weka.attributeSelection.ReliefFAttributeEval;
import weka.classifiers.Classifier;
import weka.classifiers.meta.AttributeSelectedClassifier;

/**
 * Collection of all Weka-Based feature-subset-selection algorithms provided by ContextRec.
 */
public enum FeatureSubsetSelectionAlgo {

    NONE( "None (use all features)", x -> x ),
    CFS( "CFS", FeatureSubsetSelectionAlgo::buildCfs ),
    CORRELATION("Correlation (Ranker)", FeatureSubsetSelectionAlgo::buildCorrelation ),
    GAIN_RATIO("Gain Ratio (Ranker)", FeatureSubsetSelectionAlgo::buildGrainRatio),
    INFO_GAIN("Information Gain (Ranker)", FeatureSubsetSelectionAlgo::buildInfoGain),
    PCA("Principal Component Analysis (Ranker)", FeatureSubsetSelectionAlgo::buildPca),
    RELIEF("ReliefF", FeatureSubsetSelectionAlgo::buildRelief);

    private final String name;
    private final Function<Classifier, Classifier> build;

    FeatureSubsetSelectionAlgo( String name, Function<Classifier,Classifier> apply ) {
        this.name = name;
        this.build = apply;
    }

    public String getName() {
        return name;
    }

    public Classifier wrapAround( Classifier base ) {
        return build.apply( base );
    }


    private static Classifier buildCfs( Classifier classifier ) {
        AttributeSelectedClassifier wrapper = new AttributeSelectedClassifier();

        CfsSubsetEval eval = new CfsSubsetEval();
        ASSearch search = new BestFirst();

        wrapper.setClassifier(classifier);
        wrapper.setEvaluator(eval);
        wrapper.setSearch(search);


        return wrapper;
    }

    private static Classifier buildCorrelation( Classifier classifier ) {
        AttributeSelectedClassifier wrapper = new AttributeSelectedClassifier();

        Ranker ranker = new Ranker();
        ranker.setNumToSelect( 15 );

        wrapper.setClassifier(classifier);
        wrapper.setEvaluator(new CorrelationAttributeEval());
        wrapper.setSearch(ranker);

        return wrapper;
    }

    private static Classifier buildGrainRatio( Classifier classifier ) {
        AttributeSelectedClassifier wrapper = new AttributeSelectedClassifier();

        Ranker ranker = new Ranker();
        ranker.setNumToSelect( 15 );

        wrapper.setClassifier(classifier);
        wrapper.setEvaluator(new GainRatioAttributeEval());
        wrapper.setSearch(ranker);

        return wrapper;
    }

    private static Classifier buildInfoGain( Classifier classifier ) {
        AttributeSelectedClassifier wrapper = new AttributeSelectedClassifier();

        Ranker ranker = new Ranker();
        ranker.setNumToSelect( 15 );

        wrapper.setClassifier(classifier);
        wrapper.setEvaluator(new InfoGainAttributeEval());
        wrapper.setSearch(ranker);

        return wrapper;
    }

    private static Classifier buildPca( Classifier classifier ) {
        AttributeSelectedClassifier wrapper = new AttributeSelectedClassifier();

        Ranker ranker = new Ranker();
        ranker.setNumToSelect( 15 );

        wrapper.setClassifier(classifier);
        wrapper.setEvaluator(new PrincipalComponents());
        wrapper.setSearch(ranker);

        return wrapper;
    }

    private static Classifier buildRelief( Classifier classifier ) {
        AttributeSelectedClassifier wrapper = new AttributeSelectedClassifier();

        Ranker ranker = new Ranker();
        ranker.setNumToSelect( 15 );
        ASEvaluation evaluation = new ReliefFAttributeEval();

        wrapper.setClassifier(classifier);
        wrapper.setEvaluator(evaluation);
        wrapper.setSearch(ranker);

        return wrapper;
    }

}
