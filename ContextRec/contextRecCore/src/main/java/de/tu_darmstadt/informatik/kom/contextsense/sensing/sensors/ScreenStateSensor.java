package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ScreenStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;

/**
 * Virtual sensor for recording the state of the screen (orientation, screen on?, brightness, ...)
 */
public class ScreenStateSensor extends Sensor {

    private final PowerManager powerManager;
    private final Context context;

    public ScreenStateSensor( Context context ) {
        super( context );
        this.context = context;
        this.powerManager = (PowerManager)context.getSystemService( Context.POWER_SERVICE );
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {

        boolean isScreenOn;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            isScreenOn = powerManager.isInteractive();
        } else {
            //noinspection deprecation
            isScreenOn = powerManager.isScreenOn();
        }


        return SensorInvocations.fromResult( new ScreenStateData(
                        DateTime.now().getMillis(),
                        isScreenOn,
                        readScreenBrightness(),
                        readScreenOrientation()
        ));
    }

    int readScreenBrightness() {
        try {
            return Settings.System.getInt( context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS );
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    ScreenStateData.ScreenOrientation readScreenOrientation() {
        int orientation = context.getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_LANDSCAPE: return ScreenStateData.ScreenOrientation.LANDSCAPE;
            case Configuration.ORIENTATION_PORTRAIT: return ScreenStateData.ScreenOrientation.PORTRAIT;
            default:
                Log.e("ScreenState", "received unknown screen orientation "+ orientation );
                return ScreenStateData.ScreenOrientation.PORTRAIT;
        }
    }
}
