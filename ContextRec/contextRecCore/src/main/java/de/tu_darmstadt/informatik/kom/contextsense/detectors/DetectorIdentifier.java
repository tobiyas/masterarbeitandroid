package de.tu_darmstadt.informatik.kom.contextsense.detectors;

import java.io.Serializable;

/**
 * Represents an unique identifier for a detector.
 */
public class DetectorIdentifier implements Serializable {

    private static final long serialVersionUID = -6380169402952772885L;

    private final String moduleName;
    private final String detectorName;

    // serialization only!
    private DetectorIdentifier() {
        moduleName = "";
        detectorName = "";
    }

    public DetectorIdentifier( String moduleName, String detectorName ) {
        this.moduleName = moduleName;
        this.detectorName = detectorName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public String getDetectorName() {
        return detectorName;
    }

    @Override
    public boolean equals( Object o ) {
        if (this == o) return true;
        if (!(o instanceof DetectorIdentifier)) return false;

        DetectorIdentifier that = (DetectorIdentifier) o;

        if (!moduleName.equals( that.moduleName )) return false;
        return detectorName.equals( that.detectorName );

    }

    @Override
    public int hashCode() {
        int result = moduleName.hashCode();
        result = 31 * result + detectorName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return moduleName + "." + detectorName;
    }
}
