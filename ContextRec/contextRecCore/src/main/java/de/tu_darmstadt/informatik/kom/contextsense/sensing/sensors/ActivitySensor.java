package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ActivityData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import java8.util.Optional;

/**
 * A sensor for tracking the user's current activity (standing, sitting, ...)
 */
public class ActivitySensor
        extends Sensor
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String BROADCAST_ACTION = ActivitySensor.class.getCanonicalName() + ".BROADCAST_ACTION";

    private final Context context;
    private final GoogleApiClient client;
    private final LocalBroadcastManager broadcastManager;

    public ActivitySensor( Context context ) {
        super(context);
        this.context = context;
        this.client = new GoogleApiClient.Builder(context)
                .addApi( ActivityRecognition.API)
                .addConnectionCallbacks( this )
                .addOnConnectionFailedListener( this )
                .build();

        this.broadcastManager = LocalBroadcastManager.getInstance( context );
    }

    @Override
    public Set<String> getRequiredPermissions() {
        HashSet<String> permissions = new HashSet<>();
        permissions.add( "com.google.android.gms.permission.ACTIVITY_RECOGNITION" );
        return permissions;
    }

    @Override
    public void start( SensorMode mode ) {
        client.connect();
    }

    @Override
    public void stop() {
        client.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        return new Invocation();
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i( "Activity Recognition", "Could not connect Google API client" );
    }

    private PendingIntent getActivityDetectionPendingIntent() {
        Intent intent = new Intent( context, DetectedActivitiesIntentService.class );
        return PendingIntent.getService( context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static class DetectedActivitiesIntentService extends IntentService {
        public DetectedActivitiesIntentService() {
            super ("Respond to Activity Sensor");
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            Intent localIntent = new Intent( BROADCAST_ACTION );
            localIntent.putExtras( intent );

            LocalBroadcastManager
                    .getInstance (this)
                    .sendBroadcast( localIntent );
        }
    }

    private class Invocation extends BroadcastReceiver implements SensorInvocation {

        private Optional<ActivityData> result;

        @Override
        public void onReceive( Context context, Intent intent ) {
            ActivityRecognitionResult activity = ActivityRecognitionResult.extractResult( intent );

            result = Optional.of( new ActivityData(
                    DateTime.now().getMillis(),
                    toActivity( activity.getMostProbableActivity().getType() ),
                    activity.getMostProbableActivity().getConfidence()
            ) );

            ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates( client, getActivityDetectionPendingIntent() );
        }

        private ActivityData.Activity toActivity( int activity ) {
            switch (activity) {
                case DetectedActivity.IN_VEHICLE: return ActivityData.Activity.IN_VEHICLE;
                case DetectedActivity.ON_BICYCLE: return ActivityData.Activity.ON_BICYCLE;
                case DetectedActivity.ON_FOOT: return ActivityData.Activity.ON_FOOT;
                case DetectedActivity.RUNNING: return ActivityData.Activity.RUNNING;
                case DetectedActivity.STILL: return ActivityData.Activity.STILL;
                case DetectedActivity.TILTING: return ActivityData.Activity.TILTING;
                case DetectedActivity.WALKING: return ActivityData.Activity.WALKING;
                default: return ActivityData.Activity.UNKNOWN;
            }
        }


        @Override
        public void start() {
            result = Optional.empty();
            broadcastManager.registerReceiver( this, new IntentFilter(BROADCAST_ACTION));

            if (client.isConnected()) {
                ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(
                        client,
                        0,
                        getActivityDetectionPendingIntent()
                );
            }
        }

        @Override
        public void stop() {
            broadcastManager.unregisterReceiver( this );

            if (client.isConnected()) {
                ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(
                        client,
                        getActivityDetectionPendingIntent()
                );
            }
        }

        @Override
        public Optional<? extends SensorData> getResult() {
            return result;
        }
    }
}
