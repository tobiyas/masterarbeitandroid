package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

import android.util.Log;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.Optional;
import java8.util.function.Supplier;

/**
 * Represents a few common implementations for {@link SensorInvocation}.
 * Used by the sensors.
 */
public class SensorInvocations {

    /**
     * Reference implementation of SensorInvocation with no result.
     */
    public static SensorInvocation emptyInvocation() {
        return new SensorInvocation() {
            @Override
            public void start() {
            }

            @Override
            public void stop() {
            }

            public Optional<? extends SensorData> getResult() {
                return Optional.empty();
            }
        };
    }

    public static SensorInvocation fromResult( SensorData data ) {
        return new SensorInvocation() {
            @Override
            public void start() {
            }

            @Override
            public void stop() {
            }

            public Optional<? extends SensorData> getResult() {
                return Optional.of( data );
            }
        };
    }

    public static SensorInvocation fromResult( Optional<? extends SensorData> data ) {
        return new SensorInvocation() {
            @Override
            public void start() {
            }

            @Override
            public void stop() {
            }

            public Optional<? extends SensorData> getResult() {
                return data;
            }
        };
    }

    public static SensorInvocation fromAsyncTask( ExecutorService executor, Callable<Optional<? extends SensorData>> action ) {
        return new SensorInvocation() {
            private Future<Optional<? extends SensorData>> task;
            private Optional<? extends SensorData> result;

            @Override
            public void start() {
                task = executor.submit( action );
            }

            @Override
            public void stop() {
                if (task.isDone()) {
                    try {
                        result = task.get();
                    } catch (Exception e) {
                        Log.e( "Async SensorInvocation", "Error executing task: " +e );
                        e.printStackTrace();
                        result = Optional.empty();
                    }
                } else {
                    result = Optional.empty();
                    task.cancel( true );
                }
            }

            @Override
            public Optional<? extends SensorData> getResult() {
                return result;
            }
        };
    }
}
