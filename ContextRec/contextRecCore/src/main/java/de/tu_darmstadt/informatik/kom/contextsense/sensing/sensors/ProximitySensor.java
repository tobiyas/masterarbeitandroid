package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ProximityData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;

/**
 * Sensor for measuring the distance of the user to the Smartphone.
 */
public class ProximitySensor extends Sensor implements SensorEventListener {

    @Override
    public void onAccuracyChanged( android.hardware.Sensor sensor, int accuracy ) {
    }

    private final SensorManager sensorManager;
    private boolean isCurrentlyClose;

    public ProximitySensor( Context context ) {
        super( context );
        sensorManager = (SensorManager)context.getSystemService( Context.SENSOR_SERVICE );
        isCurrentlyClose = false;
    }

    @Override
    public void start( SensorMode mode ) {
        super.start( mode );
        sensorManager.registerListener( this, sensorManager.getDefaultSensor( android.hardware.Sensor.TYPE_PROXIMITY ), 1000 );
    }

    @Override
    public void stop() {
        super.stop();
        sensorManager.unregisterListener( this );
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        return SensorInvocations.fromResult( new ProximityData(
                DateTime.now().getMillis(),
                isCurrentlyClose
        ));
    }

    @Override
    public void onSensorChanged( SensorEvent event ) {
        isCurrentlyClose = event.values[0] < 1;

        // this is a trigger sensor - we have to re-register.
        sensorManager.registerListener( this, sensorManager.getDefaultSensor( android.hardware.Sensor.TYPE_PROXIMITY ), 1000 );
    }
}