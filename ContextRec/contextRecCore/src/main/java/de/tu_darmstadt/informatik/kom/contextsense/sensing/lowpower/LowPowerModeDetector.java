package de.tu_darmstadt.informatik.kom.contextsense.sensing.lowpower;

/**
 * Represents some sensor or combination of sensors, which can detect if the device is in idle mode.
 */
public interface LowPowerModeDetector {

    /**
     * Start the idle-mode detection
     */
    void start();

    /**
     * Stop the idle-mode detection
     */
    void stop();

    /**
     * Add a {@link LowPowerModeCallback} listener
     */
    void addListener( LowPowerModeCallback listener );

    /**
     * Remove a {@link LowPowerModeCallback} listener
     */
    void removeListener( LowPowerModeCallback listener );

    /**
     * Callback for low-power mode.
     * Called, when low-poer mode is entered or left.
     */
    interface LowPowerModeCallback {
        void onLowPowerEntered();
        void onLowPowerExited();
    }
}
