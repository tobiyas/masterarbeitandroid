package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import org.joda.time.DateTime;

import java.util.Arrays;

import de.tu_darmstadt.informatik.kom.contextsense.ExtractableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

import static de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayUtils.concat;

/**
 * Created by Oli on 13.12.2015
 */
public class GyroscopeSensorData extends ExtractableSensorData {

    private static final long serialVersionUID = 5697284794721257895L;

    private long[] t;
    private double[] x;
    private double[] y;
    private double[] z;

    // serialization
    private GyroscopeSensorData() {
    }

    public GyroscopeSensorData( long timestamp, long[] t, double[] x, double[] y, double[] z ) {
        super( timestamp );
        this.t = t;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public ExtractableSensorData copy(){
        return new GyroscopeSensorData(getTimestamp(),
                Arrays.copyOf(t, t.length),
                Arrays.copyOf(x, x.length),
                Arrays.copyOf(y, y.length),
                Arrays.copyOf(z, z.length)
        ).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public long[] getT() {
        return t;
    }

    public double[] getX() {
        return x;
    }

    public double[] getY() {
        return y;
    }

    public double[] getZ() {
        return z;
    }


    @Override
    protected void mergeIntern(SensorData d) {
        GyroscopeSensorData data = (GyroscopeSensorData) d;

        boolean own = d.getTimestamp() > getTimestamp();
        t = own ? concat(t,data.t) : concat(data.t, t);
        x = own ? concat(x,data.x) : concat(data.x, x);
        y = own ? concat(y,data.y) : concat(data.y, y);
        z = own ? concat(z,data.z) : concat(data.z, z);
    }

    @Override
    public void limitFromTo(DateTime from, DateTime to) {
        long fromL = from.getMillis();
        long toL = to.getMillis();

        int first = 0;
        int last = t.length;

        for(int i = 0; i < t.length; i++){
            long time = t[i];

            if(first == 0 && time > fromL) first = i;
            if(last == t.length && time > toL) last = i;
        }

        //Only copy if needed:
        if(first > 0 || last < t.length){
            this.t = Arrays.copyOfRange(t, first, last);
            this.x = Arrays.copyOfRange(x, first, last);
            this.y = Arrays.copyOfRange(y, first, last);
            this.z = Arrays.copyOfRange(z, first, last);
        }
    }



    @Override
    public ExtractableSensorData extract(DateTime from, DateTime to ) {
        long fromL = from.getMillis();
        long toL = to.getMillis();

        int first = 0;
        int last = t.length;

        for(int i = 0; i < t.length; i++){
            long time = t[i];

            if(first == 0 && time > fromL) first = i;
            if(last == t.length && time > toL) last = i;
        }

        return new GyroscopeSensorData( getTimestamp(),
                Arrays.copyOfRange(t, first, last),
                Arrays.copyOfRange(x, first, last),
                Arrays.copyOfRange(y, first, last),
                Arrays.copyOfRange(z, first, last)
        ).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }


    @Override
    public void applyClockSkew(long clockSkew) {
        super.applyClockSkew(clockSkew);
        applyChangeToArray( t, clockSkew );
    }
}
