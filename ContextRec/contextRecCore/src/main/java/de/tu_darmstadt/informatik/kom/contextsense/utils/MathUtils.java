package de.tu_darmstadt.informatik.kom.contextsense.utils;

import java8.util.stream.DoubleStream;
import java8.util.stream.DoubleStreams;
import java8.util.stream.IntStreams;

/**
 * Created by Toby on 07.02.2016.
 */
public class MathUtils {

    public static double stdev( double[] values ) {
        if( values == null || values.length <= 1 ) return 0;
        return stdev( values, 0, values.length );
    }

    public static double average( double[] data, double defaultValue ) {
        return DoubleStreams.of( data )
                .average()
                .orElse( defaultValue );
    }


    public static double stdev( double[] values, int from, int to ) {
        if( values == null || to - from  <= 1 ) return 0;
        if( from > to ) return 0;

        //Be sure there will be no out of bounds:
        from = Math.max( from, 0 );
        to = Math.min( to, values.length );

        DoubleStream dStream = IntStreams.range( from, to ).mapToDouble(i -> values[i] );

        // see http://stackoverflow.com/questions/5543651/computing-standard-deviation-in-a-stream
        double length = to - from;
        double sumValues = dStream.sum();

        dStream = IntStreams.range( from, to ).mapToDouble(i -> values[i] );
        double reduced = dStream.reduce( 0, (sum, x) -> sum + x*x );
        return Math.sqrt((length * reduced - sumValues * sumValues)/(length * (length - 1)));
    }


    public static double stdevSlices( double[] values, int slices ) {
        if( values == null || values.length <= 1 ) return 0;
        if( slices <= 0 ) return 0;

        double size = (double)values.length / (double)slices;
        return IntStreams.range( 0, slices )
                .mapToDouble( i -> stdev( values, (int) ( size * i ), (int) ( size * (i+1) ) ) )
                .sum() / (double) slices;
    }
}
