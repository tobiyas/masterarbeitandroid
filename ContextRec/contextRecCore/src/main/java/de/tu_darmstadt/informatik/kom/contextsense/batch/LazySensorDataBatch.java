package de.tu_darmstadt.informatik.kom.contextsense.batch;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

import de.tu_darmstadt.informatik.kom.contextsense.ExtractableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Lazy;
import java8.util.Optional;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 10.02.2016.
 */
public class LazySensorDataBatch extends SensorDataBatch {

    private static final long serialVersionUID = -1520421115289070244L;

    private final DateTime startTime;
    private final DateTime endTime;
    private final Map<Class<? extends SensorData>, List<Lazy<SensorData>>> data;

    public LazySensorDataBatch( DateTime startTime, DateTime endTime, Map<Class<? extends SensorData>, List<Lazy<SensorData>>> data ) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.data = data;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends SensorData> Optional<T> getData( Class<T> sensorType ) {
        if (!data.containsKey( sensorType ))
            return Optional.empty();

        return stream( data.get( sensorType ) )
                .map( lazy ->  (T)lazy.get() )
                .findFirst();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends SensorData> Optional<T> getData( Class<T> sensorType, String source ) {
        if (!data.containsKey( sensorType ))
            return Optional.empty();

        return stream( data.get( sensorType ) )
                .map( lazy ->  (T)lazy.get() )
                .filter( data -> data.getSourceName().equals( source ) )
                .findFirst();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends SensorData> Optional<T> getData( Class<T> sensorType, DeviceType source ) {
        if (!data.containsKey( sensorType ))
            return Optional.empty();

        return stream( data.get( sensorType ) )
                .map( lazy ->  (T)lazy.get() )
                .filter( data -> data.getSourceType().equals( source ) )
                .findFirst();
    }

    @Override
    public DateTime getStartTime() {
        return startTime;
    }

    @Override
    public DateTime getEndTime() {
        return endTime;
    }

    @Override
    public boolean hasDataFromSource( String sourceName ) {
        return stream( data.values() )
                .flatMap( StreamSupport::stream )
                .map( Lazy::get )
                .filter( data -> data.getSourceName().equals( sourceName ) )
                .findFirst()
                .isPresent();
    }

    @Override
    public boolean hasDataFromSourceType(DeviceType type ) {
        return stream( data.values() )
                .flatMap( StreamSupport::stream )
                .map( Lazy::get )
                .filter( data -> data.getSourceType() == type )
                .findFirst()
                .isPresent();
    }

    @Override
    public List<SensorData> getAllData() {
        return stream( data.values() )
                .flatMap( StreamSupport::stream )
                .map( Lazy::get )
                .collect( Collectors.toList() );
    }


    @Override
    public SensorDataBatch extractDataFromTo( DateTime from, DateTime to ) {
        MutableSensorDataBatch newBatch = new MutableSensorDataBatch();
        newBatch.setStartTime( from );
        newBatch.setEndTime( to );

        stream( data.values() )
                .flatMap( StreamSupport::stream )
                .map( Lazy::get )
                .filter( data -> data instanceof ExtractableSensorData)
                .map( data -> (ExtractableSensorData) data )
                .map( data -> data.extract( from, to ) )
                .forEach( newBatch::addData );

        return newBatch;
    }
}
