package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LinearAccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorEventCopy;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorManagerSensor;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Sensor for tracking a sequence of high-frequency linear acceleration data using android's build-in sensor fusion algo.
 */
public class LinearAccelerationSensor extends SensorManagerSensor {

    public LinearAccelerationSensor( Context context ) {
        super( context );
    }

    @Override
    protected Optional<Sensor> resolveSensor( SensorManager sensorManager ) {
        return Optional.ofNullable( sensorManager.getDefaultSensor( Sensor.TYPE_LINEAR_ACCELERATION ) );
    }

    @Override
    protected Duration resolveSamplingPeriod() {
        return Duration.millis( 10 );
    }

    @Override
    protected LinearAccelerationData convertToResult( List<SensorEventCopy> events, Duration batchDuration ) {
        long[] t = stream( events )
                .mapToLong( event -> event.timestamp )
                .toArray();
        double[] x = stream( events )
                .mapToDouble( event -> event.values[0] )
                .toArray();
        double[] y = stream( events )
                .mapToDouble( event -> event.values[1] )
                .toArray();
        double[] z = stream( events )
                .mapToDouble( event -> event.values[2] )
                .toArray();

        return new LinearAccelerationData(
                DateTime.now().getMillis(), t, x, y, z
        );
    }
}
