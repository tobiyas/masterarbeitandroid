package de.tu_darmstadt.informatik.kom.contextsense.utils;

import java.util.HashMap;

import java8.util.function.Supplier;

/**
 * Created by Toby on 20.10.2017
 */

public class DefaultHashMapSupplier<K,V> extends HashMap<K,V> {

    protected Supplier<V> defaultValue;

    public DefaultHashMapSupplier(Supplier<V> defaultValue) {
        this.defaultValue = defaultValue;
    }

    //Suppression is safe.
    @SuppressWarnings("unchecked")
    @Override
    public V get( Object k ) {
        if( k == null ) return null;

        if( containsKey(k) ) return super.get(k);

        V tmp = defaultValue.get();
        this.put( (K)k, tmp );
        return tmp;
    }
}