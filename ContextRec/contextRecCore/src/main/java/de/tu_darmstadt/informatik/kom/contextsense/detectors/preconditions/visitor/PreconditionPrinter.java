package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.AndPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.OrPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.RequiresDetectorPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.SensorPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.SituationPrecondition;

import static java8.util.stream.StreamSupport.stream;

/**
 * For pretty-printing preconditions
 */
public class PreconditionPrinter extends PreconditionVisitor<String> {

    @Override
    public String visit( AndPrecondition and ) {
        return stream( and.getConditions() )
                .map( p -> p.accept( this ) )
                .reduce( (s1, s2) -> s1 + " && " + s2 )
                .orElse( "TRUE" );
    }

    @Override
    public String visit( OrPrecondition or ) {
        return stream( or.getConditions() )
                .map( p -> p.accept( this ) )
                .reduce( (s1, s2) -> s1 + " || " + s2 )
                .orElse( "FALSE" );
    }

    @Override
    public String visit( NoPrecondition no ) {
        return "TRUE";
    }

    @Override
    public String visit( SensorPrecondition sensor ) {
        StringBuilder sb = new StringBuilder();
        sb.append( "HAS_SENSOR( " );
        sb.append( sensor.getRequiredSensorData().getSimpleName() );

        if (sensor.getSource() != null) {
            sb.append( " FROM " );
            sb.append( sensor.getSource() );
        }

        sb.append( " )" );
        return sb.toString();
    }

    @Override
    public String visit( SituationPrecondition situation ) {
        return situation.getGroupName() + " == " + situation.getAllowedSituation();
    }

    @Override
    public String visit( RequiresDetectorPrecondition detectorPrecondition ) {
        return "RESULT FOR "+ detectorPrecondition.getSituationGroupName();
    }
}
