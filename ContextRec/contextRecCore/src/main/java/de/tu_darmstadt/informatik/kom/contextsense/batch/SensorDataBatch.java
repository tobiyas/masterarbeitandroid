package de.tu_darmstadt.informatik.kom.contextsense.batch;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.Serializable;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Holds a bunch of sensor-data.
 */
public abstract class SensorDataBatch implements Serializable {

    /**
     * Returns any stored data for the given sensor type.
     * If data from more than one source is present, the returned data is from any of the present sources (no guarantees).
     * Returns None, if no data is present.
     */
    public abstract <T extends SensorData> Optional<T> getData( Class<T> sensorType );

    /**
     * Returns the stored data for the given sensor type from the given source.
     * Returns None, if no data is present.
     */
    public abstract <T extends SensorData> Optional<T> getData( Class<T> sensorType, String source );

    /**
     * Returns the stored data for the given sensor type from the given Device Type.
     * Returns None, if no data is present.
     */
    public abstract <T extends SensorData> Optional<T> getData( Class<T> sensorType, DeviceType source );

    /**
     * Returns all stored data.
     */
    public abstract List<SensorData> getAllData();

    /**
     * Returns the time, at which the collection of the data for this sensor batch started.
     */
    public abstract DateTime getStartTime();

    /**
     * Returns the time, at which the collection of the data for this sensor batch ended.
     */
    public abstract DateTime getEndTime();

    /**
     * Returns the duration between the start and the end of this sensor batch.
     */
    public Duration getDuration() {
        return new Duration( getStartTime(), getEndTime() );
    }

    /**
     * Indicates, if this batch has any data from the given source.
     */
    public abstract boolean hasDataFromSource( String sourceName );

    /**
     * Indicates, if this batch has any data from the given source type.
     */
    public abstract boolean hasDataFromSourceType( DeviceType type );

    /**
     * Extracts the Data from a specific point to a specific point.
     *
     * @param from to use.
     * @param to to use.
     *
     * @return the extracted Batch.
     */
    public abstract SensorDataBatch extractDataFromTo( DateTime from, DateTime to );


    /**
     * Merges multiple SensorDataBatches into a single batch with all data
     */
    public static Optional<SensorDataBatch> merge( List<SensorDataBatch> batches ) {
        if (batches.isEmpty())
            return Optional.empty();

        DateTime start = stream( batches )
                .map( SensorDataBatch::getStartTime )
                .min( (t1, t2) -> Long.compare( t1.getMillis(), t2.getMillis() ) )
                .get();

        DateTime end = stream( batches )
                .map( SensorDataBatch::getEndTime )
                .max( (t1, t2) -> Long.compare( t1.getMillis(), t2.getMillis() ) )
                .get();

        MutableSensorDataBatch batch = new MutableSensorDataBatch( start );
        batch.setEndTime( end );

        stream( batches )
                .flatMap( part -> stream( part.getAllData() ) )
                .forEach( batch::addData );

        return Optional.of( batch );
    }

    /**
     * Applies a ClockSkew to all Data.
     * @param clockSkew to use.
     * @return this for chaining
     */
    public SensorDataBatch applyClockSkew( long clockSkew ){
        stream( getAllData() ).forEach( data -> data.applyClockSkew( clockSkew ) );
        return this;
    }

}
