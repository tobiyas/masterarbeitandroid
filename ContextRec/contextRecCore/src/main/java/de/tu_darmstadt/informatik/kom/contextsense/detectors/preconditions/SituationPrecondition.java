package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.PreconditionVisitor;

/**
 * Created by Oli on 21.03.2016.
 */
public class SituationPrecondition extends Precondition {

    private String situationGroupName;
    private String situation;

    // for serialization only!
    private SituationPrecondition() {
    }

    public SituationPrecondition( String situationGroupName, String situation ) {
        this();
        this.situationGroupName = situationGroupName;
        this.situation = situation;
    }

    @Override
    public <T> T accept( PreconditionVisitor<T> visitor ) {
        return visitor.visit( this );
    }

    public String getGroupName() {
        return situationGroupName;
    }

    public String getAllowedSituation() {
        return situation;
    }
}
