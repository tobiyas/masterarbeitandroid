package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.PreconditionVisitor;

/**
 * Created by Oli on 21.03.2016.
 */
public class NoPrecondition extends Precondition {

    @Override
    public <T> T accept( PreconditionVisitor<T> visitor ) {
        return visitor.visit( this );
    }
}
