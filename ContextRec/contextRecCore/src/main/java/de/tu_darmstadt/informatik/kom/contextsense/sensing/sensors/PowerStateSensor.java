package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.PowerManager;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PowerStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;

/**
 * (Virtual) Sensor for measuring the device's power state
 */
public class PowerStateSensor extends Sensor {

    private final PowerManager powerManager;
    private final Context context;

    public PowerStateSensor( Context context ) {
        super( context );
        this.context = context;
        this.powerManager = (PowerManager)context.getSystemService( Context.POWER_SERVICE );
    }


    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryState = context.registerReceiver( null, ifilter );

        boolean isPowerSaveMode = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            isPowerSaveMode = powerManager.isPowerSaveMode();

        return SensorInvocations.fromResult( new PowerStateData(
                DateTime.now().getMillis(),
                getBatteryLevel( batteryState ),
                getChargingState( batteryState ),
                isPowerSaveMode
        ));
    }

    private double getBatteryLevel( Intent batteryState ) {
        int level = batteryState.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryState.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        return 100 *( level / (double)scale );
    }

    private PowerStateData.ChargingState getChargingState( Intent batteryState ) {
        int plugged = batteryState.getIntExtra( BatteryManager.EXTRA_PLUGGED, -1 );

        switch (plugged) {
            case BatteryManager.BATTERY_PLUGGED_AC: return PowerStateData.ChargingState.CONNECTED_AC;
            case BatteryManager.BATTERY_PLUGGED_USB: return PowerStateData.ChargingState.CONNECTED_USB;
            case BatteryManager.BATTERY_PLUGGED_WIRELESS: return PowerStateData.ChargingState.CONNECTED_WIRELESS;
            default: return PowerStateData.ChargingState.DISCONNECTED;
        }
    }
}
