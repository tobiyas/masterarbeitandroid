package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.MergeableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015
 */
public class StepCounterData extends MergeableSensorData {

    private static final long serialVersionUID = -4313600481350402179L;

    private double stepsPerSec;

    private long totalSteps;

    public StepCounterData( long timestamp, double stepsPerSec, long totalSteps ) {
        super( timestamp );
        this.stepsPerSec = stepsPerSec;
        this.totalSteps = totalSteps;
    }

    // serialization
    private StepCounterData() {
    }

    @Override
    protected void mergeIntern(SensorData data) {
        //TODO implement me!
    }

    @Override
    public void limitFromTo(DateTime from, DateTime to) {
        //Not needed!
    }

    @Override
    public MergeableSensorData copy() {
        return new StepCounterData(getTimestamp(), stepsPerSec, totalSteps).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public double getStepsPerSec() {
        return stepsPerSec;
    }

    public long getTotalSteps() {
        return totalSteps;
    }

}
