package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.PreconditionVisitor;

/**
 * Created by Oli on 21.03.2016.
 */
public class SensorPrecondition extends Precondition {

    private Class<? extends SensorData> requiredSensorData;

    // do not use optional here because it might not play well with Serializable
    private String source;

    // for serialization only!
    private SensorPrecondition() {
    }

    public SensorPrecondition( Class<? extends SensorData> requiredSensorData, String source ) {
        this();
        this.requiredSensorData = requiredSensorData;
        this.source = source;
    }

    public SensorPrecondition( Class<? extends SensorData> requiredSensorData ) {
        this( requiredSensorData, null );
    }

    @Override
    public <T> T accept( PreconditionVisitor<T> visitor ) {
        return visitor.visit( this );
    }

    public Class<? extends SensorData> getRequiredSensorData() {
        return requiredSensorData;
    }

    /**
     * Warning! Can be Null!
     */
    public String getSource() {
        return source;
    }
}
