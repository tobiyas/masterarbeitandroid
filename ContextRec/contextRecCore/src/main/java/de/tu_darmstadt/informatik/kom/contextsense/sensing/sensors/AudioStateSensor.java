package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.media.AudioManager;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AudioStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;

/**
 * Tracks the state of the audio settings (muted? headset connected? music playing?
 */
public class AudioStateSensor extends Sensor {

    private final AudioManager audioManager;

    public AudioStateSensor( Context context ) {
        super( context );
        this.audioManager = (AudioManager)context.getSystemService( Context.AUDIO_SERVICE );
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        // there is no other way to detect if a headset is detected.
        // See http://stackoverflow.com/questions/14401611/alternative-to-the-deprecated-audiomanager-iswiredheadseton
        //noinspection deprecation
        return SensorInvocations.fromResult( new AudioStateData(
                DateTime.now().getMillis(),
                audioManager.isWiredHeadsetOn(),
                audioManager.isMusicActive(),
                toRingerState( audioManager.getRingerMode() )
        ) );
    }

    private AudioStateData.RingerState toRingerState( int ringerMode ) {
        switch (ringerMode) {
            case AudioManager.RINGER_MODE_NORMAL: return AudioStateData.RingerState.NORMAL;
            case AudioManager.RINGER_MODE_VIBRATE: return AudioStateData.RingerState.VIBRATE;
            case AudioManager.RINGER_MODE_SILENT: return AudioStateData.RingerState.SILENT;
            default: throw new IllegalArgumentException( ringerMode +" unknown" );
        }
    }
}
