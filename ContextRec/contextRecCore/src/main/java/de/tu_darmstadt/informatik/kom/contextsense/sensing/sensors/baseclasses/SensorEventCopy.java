package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

/**
 * Created by Oli on 16.11.2015
 */
public class SensorEventCopy {

    public final long timestamp;
    public final float[] values;
    public final int accuracy;
    public final Sensor sensor;

    public SensorEventCopy( SensorEvent event ) {
        this.timestamp = event.timestamp;
        this.values = event.values.clone();
        this.accuracy = event.accuracy;
        this.sensor = event.sensor;
    }

    public SensorEventCopy( SensorEvent event, long overwriteTimestamp ) {
        this.timestamp = overwriteTimestamp;
        this.values = event.values.clone();
        this.accuracy = event.accuracy;
        this.sensor = event.sensor;
    }

    public SensorEventCopy( long timestamp, float[] values, int accuracy, Sensor sensor) {
        this.timestamp = timestamp;
        this.values = values.clone();
        this.accuracy = accuracy;
        this.sensor = sensor;
    }

}
