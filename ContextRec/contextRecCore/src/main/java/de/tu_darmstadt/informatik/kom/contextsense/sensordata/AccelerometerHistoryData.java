package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Arrays;

import de.tu_darmstadt.informatik.kom.contextsense.MergeableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

import static de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayUtils.concat;

/**
 * Created by Oli on 26.01.2016.
 */
public class AccelerometerHistoryData extends MergeableSensorData {

    private static final long serialVersionUID = -3306828915601166444L;

    private long[] t;
    private double[] x;
    private double[] y;
    private double[] z;

    private AccelerometerHistoryData() {
    }

    // serialization
    public AccelerometerHistoryData( long timestamp, long[] t, double[] x, double[] y, double[] z ) {
        super( timestamp );
        this.t = t;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public MergeableSensorData copy(){
        return new AccelerometerHistoryData(getTimestamp(),
                Arrays.copyOf(t, t.length),
                Arrays.copyOf(x, x.length),
                Arrays.copyOf(y, y.length),
                Arrays.copyOf(z, z.length)
        ).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    @Override
    protected void mergeIntern(SensorData d) {
        AccelerometerHistoryData data = (AccelerometerHistoryData) d;
        t = concat(t,data.t);
        x = concat(x,data.x);
        y = concat(y,data.y);
        z = concat(z,data.z);
    }


    @Override
    public void limitFromTo(DateTime from, DateTime to) {
        long fromL = from.getMillis();
        long toL = to.getMillis();

        int first = 0;
        int last = t.length;

        for(int i = 0; i < t.length; i++){
            long time = t[i];

            if(first == 0 && time > fromL) first = i;
            if(last == t.length && time > toL) last = i;
        }

        this.t = Arrays.copyOfRange(t, first, last);
        this.x = Arrays.copyOfRange(x, first, last);
        this.y = Arrays.copyOfRange(y, first, last);
        this.z = Arrays.copyOfRange(z, first, last);
    }

    public long[] getT() {
        return t;
    }

    public double[] getX() {
        return x;
    }

    public double[] getY() {
        return y;
    }

    public double[] getZ() {
        return z;
    }

    public double[] calcMag() {
        int length = t.length;
        double[] mag = new double[ length ];

        for( int i = 0; i < length; i++ ) {
            mag[i] = Math.sqrt( x[i]*x[i] + y[i]*y[i] + z[i]*z[i] );
        }

        return mag;
    }

    public AccelerometerHistoryData onlyLastSeconds( Duration timeToKeep ) {
        int size = t.length;
        if (size == 0)
            return this;

        long newest = t[size -1];
        long oldestTimeToKeep = newest - toNanos(timeToKeep);

        int indexToKeep = 0;
        while (t[indexToKeep] < oldestTimeToKeep) {
            indexToKeep++;
        }

        return new AccelerometerHistoryData( getTimestamp(),
            Arrays.copyOfRange( t, indexToKeep, size ),
            Arrays.copyOfRange( x, indexToKeep, size ),
            Arrays.copyOfRange( y, indexToKeep, size ),
            Arrays.copyOfRange( z, indexToKeep, size )
        );
    }

    private long toNanos( Duration t ) {
        return t.getMillis() * 1_000_000;
    }


    @Override
    public void applyClockSkew(long clockSkew) {
        super.applyClockSkew(clockSkew);
        applyChangeToArray( t, clockSkew );
    }
}
