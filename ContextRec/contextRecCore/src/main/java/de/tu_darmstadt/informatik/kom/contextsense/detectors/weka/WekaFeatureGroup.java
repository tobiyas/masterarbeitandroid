package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka;

/**
 * Created by Oli on 05.01.2016.
 */
public enum WekaFeatureGroup {
    INERTIAL("Intertial Sensors"),
    MICROPHONE( "Microphone" ),
    LOCATION("Location"),
    GPS_INFO("Gps Non-Location info"),
    CONNECTIVITY("Connectivity"),
    DEVICE_USAGE( "Device Usage" ),
    LIGHT_PROXIMITY( "Light & Proximity"),
    USER_INFOS( "User Generated Info" ),
    TIME("Date & Time"),
    OTHER( "Other" ),
    RAW( "Raw inertial data"),
    WEAR_SENSORS( "Wear sensors" ),
    IMU_SENSORS( "IMU sensors" );

    private final String name;

    WekaFeatureGroup( String name ) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
}