package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.GpsSpeedHistoryData;

import static java8.util.stream.StreamSupport.stream;

/**
 * Sensor for tracking the user's speed (via GPS) over a long period of time.
 */
public class GpsSpeedHistorySensor extends Sensor implements LocationListener {

    private static final Duration GPS_INTERVAL_NORMAL = Duration.standardSeconds( 30 );
    private static final Duration GPS_INTERVAL_LOW_POWER = Duration.standardMinutes( 10 );
    private static final Duration TIME_TO_KEEP_HISTORY = Duration.standardHours( 1 );

    private final LocationManager locationManager;
    private final Context context;
    private final Handler mainHandler;
    private Queue<GpsSpeedHistoryEvent> history;

    public GpsSpeedHistorySensor( Context context ) {
        super( context );
        this.context = context;
        this.locationManager = (LocationManager) context.getSystemService( Context.LOCATION_SERVICE );
        this.mainHandler = new Handler( Looper.getMainLooper() );
        this.history = new LinkedList<>();
    }

    @Override
    public void start( SensorMode mode ) {
        super.start( mode );
        mainHandler.post( () -> restartGpsAtRate( modeToGpsInterval( mode ) ) );
    }

    private Duration modeToGpsInterval( SensorMode mode ) {
        switch (mode) {
            case LOW_POWER: return GPS_INTERVAL_LOW_POWER;
            case NORMAL: return GPS_INTERVAL_NORMAL;
            default: throw new IllegalArgumentException( mode.toString() );
        }
    }

    @Override
    public void stop() {
        super.stop();
        mainHandler.post( this::disableGps );
    }

    private void restartGpsAtRate( Duration interval ) {
        if (ActivityCompat.checkSelfPermission( context, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED)
            return;

        locationManager.removeUpdates( this );
        locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, interval.getMillis(), 0, this );
    }

    private void disableGps() {
        if (ActivityCompat.checkSelfPermission( context, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED) {
            locationManager.removeUpdates( this );
        }
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        return SensorInvocations.fromResult( buildResult() );
    }

    private GpsSpeedHistoryData buildResult() {
        List<GpsSpeedHistoryEvent> copy = new ArrayList<>( history );
        long[] t = stream( copy ).mapToLong( data -> data.timestamp ).toArray();
        double[] speed = stream( copy ).mapToDouble( data -> data.speedInMs ).toArray();

        return new GpsSpeedHistoryData( DateTime.now().getMillis(), t, speed );
    }


    @Override
    public void onLocationChanged( Location location ) {
        addToHistory( location );
        pruneHistory();
    }

    private void pruneHistory() {
        long oldestToKeep = DateTime.now().minus( TIME_TO_KEEP_HISTORY ).getMillis();

        // ensure we always have one entry left
        while (history.size() > 1 && history.peek().timestamp < oldestToKeep)
            history.poll();
    }

    private void addToHistory( Location location ) {
        history.add( new GpsSpeedHistoryEvent(
                DateTime.now().getMillis(),
                location.getSpeed()
        ) );
    }

    @Override
    public void onStatusChanged( String provider, int status, Bundle extras ) {
    }

    @Override
    public void onProviderEnabled( String provider ) {
    }

    @Override
    public void onProviderDisabled( String provider ) {
    }

    private static class GpsSpeedHistoryEvent {
        public final long timestamp;
        public final double speedInMs;

        private GpsSpeedHistoryEvent( long timestamp, double speedInMs ) {
            this.timestamp = timestamp;
            this.speedInMs = speedInMs;
        }
    }
}
