package de.tu_darmstadt.informatik.kom.contextsense.utils;

import java.util.Iterator;

import java8.util.Spliterator;
import java8.util.Spliterators;
import java8.util.stream.Stream;
import java8.util.stream.StreamSupport;

/**
 * Utility methods for creating (Java-8) Streams from other sources.
 */
public class Streams {

    /**
     * Create a Stream from the given Iterator.
     */
    public static <T> Stream<T> ofIterator(final Iterator<T> iterator) {
        return StreamSupport.stream( Spliterators.spliteratorUnknownSize( iterator, Spliterator.ORDERED), false);
    }

    /**
     * Create a Stream from the given Iterable (using it's .iterator() function).
     */
    public static <T> Stream<T> ofIterable(final Iterable<T> iterable) {
        return ofIterator( iterable.iterator() );
    }

}
