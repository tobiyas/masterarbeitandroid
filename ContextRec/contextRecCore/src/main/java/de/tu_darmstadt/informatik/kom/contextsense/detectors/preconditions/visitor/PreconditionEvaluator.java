package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.AndPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.OrPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.RequiresDetectorPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.SensorPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.SituationPrecondition;

/**
 * Created by Oli on 24.03.2016.
 */
public class PreconditionEvaluator extends PreconditionVisitor<Boolean> {

    private final SensorDataBatch batch;
    private final DetectionResult results;

    public PreconditionEvaluator( SensorDataBatch batch, DetectionResult results ) {
        this.batch = batch;
        this.results = results;
    }

    @Override
    public Boolean visit( AndPrecondition and ) {
        for( Precondition precondition: and.getConditions() ) {
            if (!precondition.accept( this ) )
                return false;
        }

        return true;
    }

    @Override
    public Boolean visit( OrPrecondition or ) {
        for( Precondition precondition: or.getConditions() ) {
            if (precondition.accept( this ) )
                return true;
        }

        return false;
    }

    @Override
    public Boolean visit( NoPrecondition no ) {
        return true;
    }

    @Override
    public Boolean visit( SensorPrecondition sensor ) {
        if ( sensor.getSource() == null) {
            return batch.getData( sensor.getRequiredSensorData() ).isPresent();
        } else
        {
            return batch.getData( sensor.getRequiredSensorData(), sensor.getSource() ).isPresent();
        }
    }

    @Override
    public Boolean visit( SituationPrecondition situation ) {
        //noinspection SimplifiableIfStatement NOT SIMPLER!
        if (!results.hasResultForGroup( situation.getGroupName() ))
            return false;

        return results.getAnyResultForGroup( situation.getGroupName() ).get()
                .getLabelWithHighestProbability().get()
                .equals( situation.getAllowedSituation() );
    }

    @Override
    public Boolean visit( RequiresDetectorPrecondition detectorPrecondition ) {
        return results.hasResultForGroup( detectorPrecondition.getSituationGroupName() );
    }
}
