package de.tu_darmstadt.informatik.kom.contextsense.detectors.base;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Preconditions;
import java8.util.stream.Stream;


/**
 * Base class for all detectors.
 * If you want to implement a new detector, make sure to inherit from this class.
 */
public abstract class Detector implements IDetector {

    private final DetectorDescription description;

    protected Detector( DetectorDescription description ) {
        this.description = description;
    }

    @Override
    public DetectorDescription getDescription() {
        return description;
    }

    public SituationGroup getDetectableSituation() {
        return description.getDetectableGroup();
    }

    /**
     * Returns the identifier of this detector.
     * Is equal to {@link #getDescription()}.getIdentifier()
     */
    public DetectorIdentifier getIdentifier() {
        return description.getId();
    }

    @Override
    public Set<String> getDependentLabelGroups()
    {
        return new HashSet<>();
    }

    @Override
    public Precondition getPrecondition() { return Preconditions.NONE; }

    @Override
    public void trainIncremental( TrainingSample sample ) {
        // override if required
    }

    @Override
    public void trainBatch( Stream<TrainingSample> samples ) {
        // override if required
    }

    @Override
    public void saveState( OutputStream out ) throws IOException {
        // override if required
    }

    @Override
    public void loadState( InputStream in ) throws IOException {
        // override if required
    }

    /**
     * Utility function for building a DetectorResult with a clear situation.
     */
    protected DetectorResult buildGoodResult( String situation ) {
        return DetectorResult.clearSituation(
                getIdentifier(),
                getDetectableSituation(),
                situation
        );
    }

    /**
     * Utility function for building a DetectorResult from a probability distribution.
     */
    protected DetectorResult buildGoodResult( Map<String, Double> probabilities ) {
        return DetectorResult.probabilityDistribution(
                getIdentifier(),
                getDetectableSituation(),
                probabilities
        );
    }

    /**
     * Utility function for building a DetectorResult with no clear winner.
     */
    protected DetectorResult buildBadResult() {
        return DetectorResult.badData(
                getIdentifier(),
                getDetectableSituation()
        );
    }


}
