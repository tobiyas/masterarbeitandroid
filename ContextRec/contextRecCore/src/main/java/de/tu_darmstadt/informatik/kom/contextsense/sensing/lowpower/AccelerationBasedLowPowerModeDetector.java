package de.tu_darmstadt.informatik.kom.contextsense.sensing.lowpower;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import de.tu_darmstadt.informatik.kom.contextsense.utils.MathUtils;

import static java8.util.stream.StreamSupport.stream;

/**
 * A version of {@link LowPowerModeDetector} which calcualtes the standard-deviation of the
 * acceleration over a given timespan to find out if the device is moving.
 */
public class AccelerationBasedLowPowerModeDetector implements LowPowerModeDetector, SensorEventListener {

    private static final long TIME_TO_KEEP_EVENTS_IN_NS = 60_000_000_000L;
    private static final double STDEV_MOVEMENT_THRESHOLD = 0.3;

    private final List<LowPowerModeCallback> listeners;
    private final Queue<AccelerationValue> accelerationValues;
    private final SensorManager sensorManager;
    private boolean isInLowPowerMode;

    public AccelerationBasedLowPowerModeDetector( Context context ) {
        listeners = new ArrayList<>();
        accelerationValues = new ArrayDeque<>();
        sensorManager = (SensorManager) context.getSystemService( Context.SENSOR_SERVICE );
        isInLowPowerMode = false;
    }

    @Override
    public void start() {
        Sensor sensor = sensorManager.getDefaultSensor( Sensor.TYPE_ACCELEROMETER );
        sensorManager.registerListener( this, sensor, SensorManager.SENSOR_DELAY_UI );
        isInLowPowerMode = false;
    }

    @Override
    public void stop() {
        sensorManager.unregisterListener( this );
        accelerationValues.clear();
    }

    @Override
    public void addListener( LowPowerModeCallback listener ) {
        listeners.add( listener );
    }

    @Override
    public void removeListener( LowPowerModeCallback listener ) {
        listeners.remove( listener );
    }

    @Override
    public void onSensorChanged( SensorEvent event ) {
        accelerationValues.add( AccelerationValue.fromSensorEvent( event ) );
        dropOldEvents( event.timestamp );

        double currentStdev = calcCurrentStdev();
        boolean isMoving = currentStdev > STDEV_MOVEMENT_THRESHOLD;

        if (isMoving && isInLowPowerMode) {
            isInLowPowerMode = false;
            System.out.println( "Leave LOW_POWER");
            for( LowPowerModeCallback listener :listeners ) {
                listener.onLowPowerExited();
            }
        } else if (!isMoving && !isInLowPowerMode) {
            isInLowPowerMode = true;
            System.out.println( "Enter LOW_POWER");
            for( LowPowerModeCallback listener :listeners ) {
                listener.onLowPowerEntered();
            }
        }
    }

    private double calcCurrentStdev() {
        return MathUtils.stdev(
                stream( accelerationValues )
                .mapToDouble( acc -> acc.mag )
                .toArray()
        );
    }

    private void dropOldEvents( long now ) {
        long oldestToKeep = now - TIME_TO_KEEP_EVENTS_IN_NS;

        while (!accelerationValues.isEmpty() && accelerationValues.peek().timestamp < oldestToKeep ) {
            accelerationValues.poll();
        }
    }

    @Override
    public void onAccuracyChanged( Sensor sensor, int accuracy ) {

    }

    private static class AccelerationValue {
        public final long timestamp;
        public final double mag;

        private AccelerationValue( long timestamp, double mag ) {
            this.timestamp = timestamp;
            this.mag = mag;
        }

        public static AccelerationValue fromSensorEvent( SensorEvent event ) {
            double mag = Math.sqrt( event.values[0] * event.values[0] + event.values[1] * event.values[1] + event.values[2] * event.values[2] );
            return new AccelerationValue( event.timestamp, mag );
        }
    }
}
