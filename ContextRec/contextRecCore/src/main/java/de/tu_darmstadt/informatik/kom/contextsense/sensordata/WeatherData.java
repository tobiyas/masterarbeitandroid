package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * The state of the Weather for a given point in time and location.
 */
public class WeatherData extends SensorData {

    private static final long serialVersionUID = 7096873030925163594L;

    /**
     * The overall type of weather.
     */
    public enum WeatherType {
        THUNDER_STORM,
        DRIZZLE,
        RAINY,
        SNOW,
        ATMOSPHERE,
        CLEAR,
        CLOUDY,
        EXTREME,
        UNKNOWN
    }

    private WeatherType weather;
    private double tempInDegCelsius;
    private double windSpeedInMs;

    // serialization constructor
    private WeatherData() {
    }

    public WeatherData( long timestamp, WeatherType weather, double tempInDegCelsius, double windSpeedInMs ) {
        super( timestamp );
        this.weather = weather;
        this.tempInDegCelsius = tempInDegCelsius;
        this.windSpeedInMs = windSpeedInMs;
    }

    @Override
    public SensorData copy() {
        return new WeatherData(getTimestamp(), weather, tempInDegCelsius, windSpeedInMs).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    /**
     * Returns the overall weather type
     */
    public WeatherType getWeather() {
        return weather;
    }

    /**
     * Returns the temperature in °C
     */
    public double getTempInDegCelsius() {
        return tempInDegCelsius;
    }

    /**
     * Returns the speed of the wind in m/s
     */
    public double getWindSpeedInMs() {
        return windSpeedInMs;
    }
}
