package de.tu_darmstadt.informatik.kom.contextsense.serialization.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;

/**
 * Created by Oli on 07.12.2015.
 */
public class JsonUtils {

    public static ObjectMapper buildDefaultMapper() {
        ObjectMapper mapper = new ObjectMapper();

        // date / time handling
        mapper.registerModule(new JodaModule());
        mapper.disable( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS );

        return mapper;
    }
}
