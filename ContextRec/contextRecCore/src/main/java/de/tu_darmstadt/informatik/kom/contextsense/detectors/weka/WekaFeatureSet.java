package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import java8.util.stream.Collectors;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 07.01.2016.
 */
public class WekaFeatureSet {

    private final List<WekaFeature> sensorFeatures;
    private final Instances setDescription;
    private final SituationGroup situationGroup;

    public WekaFeatureSet( List<WekaFeature> sensorFeatures, SituationGroup situationGroup ) {
        this.sensorFeatures = sensorFeatures;
        this.setDescription = buildSetDescription( sensorFeatures, situationGroup.getSituations() );
        this.situationGroup = situationGroup;
    }

    private Instances buildSetDescription( List<WekaFeature> sensorFeatures, List<String> labels ) {
        ArrayList<Attribute> attributes = new ArrayList<>();

        for(WekaFeature feature: sensorFeatures) {
            attributes.addAll( feature.getAttributes() );
        }

        Attribute classAttrib = new Attribute( "class", labels );
        attributes.add( classAttrib );

        Instances instances = new Instances( "Rel", attributes, 100 );
        instances.setClass( classAttrib );

        return instances;
    }

    public Instances buildEmptyDataSet() {
        return new Instances( setDescription );
    }

    public Instance toInstance( SensorDataBatch batch, Map<String,String> otherLabels ) {
        Instance instance = new DenseInstance( setDescription.numAttributes() );
        instance.setDataset( setDescription );

        for( WekaFeature feature: sensorFeatures) {
            feature.apply( instance, batch, otherLabels );
        }

        return instance;
    }


    public Instance toInstanceParrallel( SensorDataBatch batch, Map<String,String> otherLabels ) {
        Instance instance = new DenseInstance( setDescription.numAttributes() );
        instance.setDataset( setDescription );

        stream( sensorFeatures )
                .parallel()
                .forEach( f -> f.apply( instance, batch, otherLabels ) );

        return instance;
    }

    public SituationGroup getSituationGroup() {
        return situationGroup;
    }

    public List<String> getSensorFeatureNames() {
        return stream( sensorFeatures ).map( WekaFeature::getName ).collect( Collectors.toList() );
    }

}
