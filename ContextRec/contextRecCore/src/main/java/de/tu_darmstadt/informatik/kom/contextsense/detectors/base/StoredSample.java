package de.tu_darmstadt.informatik.kom.contextsense.detectors.base;

import java.util.Map;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;

/**
 * Represents a Sample with it's data, the original detection results and the user's annotations.
 */
public class StoredSample {

    private final Map<String,String> originalResults;
    private final Map<String, String> annotation;
    private final SensorDataBatch data;

    public StoredSample( Map<String, String> originalResults, Map<String, String> annotation, SensorDataBatch data ) {
        this.originalResults = originalResults;
        this.annotation = annotation;
        this.data = data;
    }

    public Map<String, String> getOriginalResults() {
        return originalResults;
    }

    public Map<String, String> getAnnotations() {
        return annotation;
    }

    public SensorDataBatch getData() {
        return data;
    }
}
