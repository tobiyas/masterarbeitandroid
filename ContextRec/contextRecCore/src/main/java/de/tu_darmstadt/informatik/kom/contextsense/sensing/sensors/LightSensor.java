package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LightSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.OneShotSensorManagerSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorEventCopy;
import java8.util.Optional;

/**
 * Sensor that measures the ambient light level
 */
public class LightSensor extends OneShotSensorManagerSensor<LightSensorData> {

    public LightSensor( Context context ) {
        super( context );
    }

    @Override
    protected LightSensorData convertToResult( SensorEventCopy event ) {
        return new LightSensorData(
                DateTime.now().getMillis(),
                event.values[0]
        );
    }

    @Override
    protected Optional<Sensor> resolveSensor(SensorManager sensorManager) {
        return Optional.ofNullable( sensorManager.getDefaultSensor( Sensor.TYPE_LIGHT ) );
    }
}
