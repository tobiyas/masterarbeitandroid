package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PressureSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorEventCopy;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorManagerSensor;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;


/**
 * Sensor that measures the air pressure in the environment
 */
public class PressureSensor extends SensorManagerSensor {

    public PressureSensor( Context context ) {
        super( context );
    }

    @Override
    protected Optional<Sensor> resolveSensor(SensorManager sensorManager) {
        return Optional.ofNullable( sensorManager.getDefaultSensor( Sensor.TYPE_PRESSURE ) );
    }

    @Override
    protected Duration resolveSamplingPeriod() {
        return Duration.ZERO;
    }

    @Override
    protected PressureSensorData convertToResult( List<SensorEventCopy> events, Duration batchDuration ) {
        double[] values = stream( events )
                .mapToDouble( event -> event.values[0] )
                .toArray();

        return new PressureSensorData(
                DateTime.now().getMillis(),
                values
        );
    }
}