package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors.WekaDetectorMetaData;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Preconditions;
import java8.util.Optional;
import java8.util.function.Consumer;
import java8.util.stream.Stream;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.filters.unsupervised.attribute.StringToNominal;

/**
 * Builder class for WekaDetector.
 * Should make it easier to build an instance of it.
 */
public class WekaDetectorBuilder {

    private DetectorIdentifier identifier;
    private Optional<SituationGroup> group;
    private Optional<String> userDescription;
    private Optional<WekaFeatureSet> featureSet;
    private Optional<Stream<TrainingSample>> samples;
    private Optional<WekaBaseClassifier> classifier;
    private Optional<FeatureSubsetSelectionAlgo> featureSelection;
    private Optional<Consumer<String>> progressReporter;
    private Optional<Precondition> precondition;
    private Optional<Consumer<Instances>> instanceStore;

    public WekaDetectorBuilder( DetectorIdentifier identifier ) {
        this.identifier = identifier;
        this.group = Optional.empty();
        this.userDescription = Optional.empty();
        this.featureSet = Optional.empty();
        this.samples = Optional.empty();
        this.classifier = Optional.empty();
        this.featureSelection = Optional.empty();
        this.progressReporter = Optional.empty();
        this.precondition = Optional.empty();
        this.instanceStore = Optional.empty();
    }

    public WekaDetectorBuilder( String packageName, String detectorName ) {
        this( new DetectorIdentifier( packageName, detectorName ) );
    }

    /**
     * Set the situation which this detector is supposed to detect.
     * You have to set this before calling build().
     */
    public WekaDetectorBuilder forSituation( SituationGroup group ) {
        this.group = Optional.of( group );
        return this;
    }

    /**
     * Sets a (user-understandable) description for this detector.
     * This is an optional parameter.
     */
    public WekaDetectorBuilder withDescription( String description ) {
        this.userDescription = Optional.of( description );
        return this;
    }

    /**
     * Set the training-samples to use to train the classifier.
     * Calling this function twice will replace the previous provided samples.
     *
     * You have to set this before calling build().
     */
    public WekaDetectorBuilder withSamples( Stream<TrainingSample> samples ) {
        this.samples = Optional.of( samples );
        return this;
    }

    /**
     * Set the features to use to train the classifier.
     * Use {@link WekaFeatureBuilder} to build a FetureSet.
     * Calling this function twice will replace the previous provided features.
     *
     * You have to set this before calling build().
     */
    public WekaDetectorBuilder withFeatures( WekaFeatureSet features ) {
        this.featureSet = Optional.of( features );
        return this;
    }

    /**
     * Set the Machine-Learning algo to use.
     * Defaults to Random Forest.
     */
    public WekaDetectorBuilder withClassifier( WekaBaseClassifier classifier ) {
        this.classifier = Optional.of( classifier );
        return this;
    }

    /**
     * Choose a feature-subset-selection algorithm.
     * Defaults to NONE.
     *
     * See https://weka.wikispaces.com/Performing+attribute+selection?responseToken=04fab084e631c546ba20cdab24eabaa7b
     */
    public WekaDetectorBuilder withFeatureSubsetSelection( FeatureSubsetSelectionAlgo featureSelection ) {
        this.featureSelection = Optional.of( featureSelection );
        return this;
    }

    /**
     * Register a callback which will be informed about the progress in training and evaluation.
     * Use this to update the UI while the user is waiting.
     */
    public WekaDetectorBuilder withProgressReporter( Consumer<String> progressReporter ) {
        this.progressReporter = Optional.of( progressReporter );
        return this;
    }

    /**
     * Set a precondition for the detector.
     * Defaults to {@link de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition}.
     */
    public WekaDetectorBuilder withPrecondition( Precondition  precondition ) {
        this.precondition = Optional.of( precondition );
        return this;
    }

    /**
     * Allows to keep a reference on the instances object used for training.
     * This may be handy e.g. if you want to evaluate the detector or store the file on disk.
     */
    public WekaDetectorBuilder saveInstances( Consumer<Instances> instancesConsumer ) {
        this.instanceStore = Optional.of( instancesConsumer );
        return this;
    }

    public WekaDetector build() throws Exception {
        verifyRequirements();

        return trainClassifier(
                buildDescription(),
                featureSet.get(),
                classifier.orElse( WekaBaseClassifier.RANDOM_FOREST ),
                featureSelection.orElse( FeatureSubsetSelectionAlgo.NONE ),
                samples.get(),
                precondition.orElse( Preconditions.NONE ),
                instanceStore.orElse( i -> {} ),
                progressReporter.orElse( s -> {} )
        );
    }

    public WekaDetectorMetaData buildOnlyMetaData() {
        verifyRequirements();

        return new WekaDetectorMetaData(
                buildDescription(),
                featureSet.get().getSensorFeatureNames(),
                precondition.orElse( Preconditions.NONE )
        );
    }

    private void verifyRequirements() {
        if (!group.isPresent())
            throw new IllegalStateException( "Cannot build a detector without setting the situation it is supposed to detect. Call forSituation to set the detectable situation." );
        if (!samples.isPresent())
            throw new IllegalStateException( "Cannot build a detector without training samples. Call withSamples to supply training samples." );
        if (!featureSet.isPresent())
            throw new IllegalStateException( "Cannot build a detector without (ML) features. Call withFeatures to supply features." );
    }

    private DetectorDescription buildDescription() {
        return new DetectorDescription.Builder( identifier )
                .setDescription( userDescription.orElse( "No description set." ) )
                .setSituationGroup( group.get() )
                .build();
    }

    private static WekaDetector trainClassifier( DetectorDescription description, WekaFeatureSet featureSet, WekaBaseClassifier baseClassifier, FeatureSubsetSelectionAlgo selection, Stream<TrainingSample> samples, Precondition precondition, Consumer<Instances> instancesConsumer, Consumer<String> progress ) throws Exception {
        progress.accept( "Step 1: Loading samples from disk" );
        Instances instances = WekaUtils.buildWekaInstanceSet( featureSet, samples, x -> {} );
        Classifier classifier = buildClassifier( baseClassifier, selection );

        progress.accept( "Step 2: Training" );
        classifier.buildClassifier( instances );
        instancesConsumer.accept( instances );

        return new WekaDetector( description, featureSet, classifier, precondition );
    }

    private static Classifier buildClassifier( WekaBaseClassifier baseClassifier, FeatureSubsetSelectionAlgo selection ) {
        Classifier base = baseClassifier.build();
        Classifier featureSelected = selection.wrapAround( base );
        return applyStringToNominalFilter( featureSelected );
    }

    private static Classifier applyStringToNominalFilter( Classifier classifier ) {
        StringToNominal filter = new StringToNominal();
        filter.setAttributeRange( "first-last" );

        FilteredClassifier filtered = new FilteredClassifier();
        filtered.setFilter( filter );
        filtered.setClassifier( classifier );

        return filtered;
    }
}
