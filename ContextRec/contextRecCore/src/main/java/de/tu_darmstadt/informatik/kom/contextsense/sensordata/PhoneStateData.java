package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015
 */
public class PhoneStateData extends SensorData {

    private static final long serialVersionUID = 5977598338678280737L;

    private boolean isInCall;

    public PhoneStateData( long timestamp, boolean isInCall ) {
        super( timestamp );
        this.isInCall = isInCall;
    }

    @Override
    public SensorData copy() {
        return new PhoneStateData(getTimestamp(), isInCall).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    // serialization
    private PhoneStateData() {
    }

    public boolean isInCall() {
        return isInCall;
    }
}
