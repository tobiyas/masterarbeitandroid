package de.tu_darmstadt.informatik.kom.contextsense;

import java.io.Serializable;

import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;

/**
 * Created by Oli on 02.12.2015.
 */
public abstract class SensorData implements Serializable {

    public static final String NO_SOURCE_SET = "Not specified";

    private long timestamp;
    private String sourceName;
    private DeviceType sourceType;


    // for serialization
    protected SensorData() {
        this.sourceName = NO_SOURCE_SET;
        this.sourceType = DeviceType.UNKNOWN;
    }

    protected SensorData( long timestamp ) {
        this();
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp( long timestamp ) {
        this.timestamp = timestamp;
    }

    public String getSourceName() {
        return sourceName;
    }

    public DeviceType getSourceType() {
        return sourceType;
    }

    public SensorData setSourceName(String sourceName) {
        this.sourceName = sourceName;
        return this;
    }

    public SensorData setSourceType(DeviceType sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public void claimSourceIfUnset( String source, DeviceType type ) {
        if (this.sourceName.equals( NO_SOURCE_SET )){
            this.sourceName = source;
        }

        if (this.sourceType == DeviceType.UNKNOWN){
            this.sourceType = type;
        }
    }


    /**
     * Creates a copy of this class.
     * @return a copy.
     */
    public abstract SensorData copy();


    /**
     * Applies a ClockSkew to the Data.
     * @param clockSkew to add.
     */
    public void applyClockSkew(long clockSkew){
        this.timestamp += clockSkew;
    }


}
