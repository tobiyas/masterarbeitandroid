package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions;

import java.io.Serializable;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.PreconditionVisitor;

/**
 * Created by Oli on 21.03.2016.
 */
public abstract class Precondition {

    public abstract <T> T accept( PreconditionVisitor<T> visitor );

}
