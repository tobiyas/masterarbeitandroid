package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.AndPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.OrPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.RequiresDetectorPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.SensorPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.SituationPrecondition;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 26.03.2016.
 */
public class FailingPreconditionFinder extends PreconditionVisitor<Optional<Precondition>> {

    private final PreconditionEvaluator eval;

    public FailingPreconditionFinder( SensorDataBatch batch, DetectionResult otherResults ) {
        eval = new PreconditionEvaluator( batch, otherResults );
    }

    @Override
    public Optional<Precondition> visit( AndPrecondition and ) {
        return stream( and.getConditions() )
                .filter( p -> !p.accept( eval ) )
                .findFirst()
                .flatMap( p -> p.accept( this ) );
    }

    @Override
    public Optional<Precondition> visit( OrPrecondition or ) {
        if (or.accept( eval ))
            return Optional.empty();
        else
            return Optional.of( or );
    }

    @Override
    public Optional<Precondition> visit( NoPrecondition no ) {
        return Optional.empty();
    }

    @Override
    public Optional<Precondition> visit( SensorPrecondition sensor ) {
        if(sensor.accept( eval ))
            return Optional.empty();
        else
            return Optional.of( sensor );
    }

    @Override
    public Optional<Precondition> visit( SituationPrecondition situation ) {
        if(situation.accept( eval ))
            return Optional.empty();
        else
            return Optional.of( situation );
    }

    @Override
    public Optional<Precondition> visit( RequiresDetectorPrecondition detectorPrecondition ) {
        if(detectorPrecondition.accept( eval ))
            return Optional.empty();
        else
            return Optional.of( detectorPrecondition );
    }
}
