package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015
 */
public class HeartRateData extends SensorData {

    private static final long serialVersionUID = 3241410760244351439L;

    public enum Accuracy {
        HIGH,
        MEDIUM,
        LOW,
        BAD
    }

    private double heartRateInBpm;
    private Accuracy accuracy;

    // serialization constructor
    private HeartRateData() {
    }

    public HeartRateData( long timestamp, double heartRateInBpm, Accuracy accuracy ) {
        super( timestamp );
        this.heartRateInBpm = heartRateInBpm;
        this.accuracy = accuracy;
    }

    @Override
    public SensorData copy() {
        return new HeartRateData(getTimestamp(), heartRateInBpm, accuracy).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public double getHeartRateInBpm() {
        return heartRateInBpm;
    }

    public Accuracy getAccuracy() {
        return accuracy;
    }
}
