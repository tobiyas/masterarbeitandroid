package de.tu_darmstadt.informatik.kom.contextsense.serialization.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

/**
 * Hacked version of {@link ObjectInputStream} that ignores SerialVersionUid.
 *
 * USE WITH CARE!
 * Only use this, if you have no chance to set a SerialVersionUid yourself (3rd party code).
 */
public class SerialVersionUidIgnoringObjectInputStream extends ObjectInputStream {

    /**
     * If the errors are printed
     */
    private final boolean printErrors;

    public SerialVersionUidIgnoringObjectInputStream(InputStream in, boolean printErrors) throws IOException {
        super(in);

        this.printErrors = printErrors;
    }

    public SerialVersionUidIgnoringObjectInputStream(InputStream in) throws IOException {
        this( in, true );
    }

    @Override
    protected ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
        ObjectStreamClass resultClassDescriptor = super.readClassDescriptor(); // initially streams descriptor
        Class localClass; // the class in the local JVM that this descriptor represents.
        try {
            localClass = Class.forName(resultClassDescriptor.getName());
        } catch (ClassNotFoundException e) {
            System.err.println( "No local class for " + resultClassDescriptor.getName() + ". " + e );
            return resultClassDescriptor;
        }
        ObjectStreamClass localClassDescriptor = ObjectStreamClass.lookup(localClass);
        if (localClassDescriptor != null) { // only if class implements serializable
            final long localSUID = localClassDescriptor.getSerialVersionUID();
            final long streamSUID = resultClassDescriptor.getSerialVersionUID();
            if (streamSUID != localSUID) { // check for serialVersionUID mismatch.
                final StringBuffer s = new StringBuffer("Overriding serialized class version mismatch: ");
                s.append("local serialVersionUID = ").append(localSUID);
                s.append(" stream serialVersionUID = ").append(streamSUID);
                Exception e = new InvalidClassException(s.toString());
                if( printErrors ) System.err.println( "Potentially Fatal Deserialization Operation. "+ e );
                resultClassDescriptor = localClassDescriptor; // Use local class descriptor for deserialization
            }
        }
        return resultClassDescriptor;
    }
}
