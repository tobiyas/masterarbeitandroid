package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015
 */
public class ScreenStateData extends SensorData {

    private static final long serialVersionUID = 1248321597380159444L;

    public enum ScreenOrientation {
        PORTRAIT,
        LANDSCAPE
    }

    /**
     * The brightness of the screen. Values between [0 - 255]
     */
    private int screenBrightness;
    private boolean screenOn;
    private ScreenOrientation orientation;

    public ScreenStateData( long timestamp, boolean screenOn, int screenBrightness, ScreenOrientation orientation ) {
        super( timestamp );
        this.screenOn = screenOn;
        this.screenBrightness = screenBrightness;
        this.orientation = orientation;
    }

    @Override
    public SensorData copy() {
        return new ScreenStateData(getTimestamp(), screenOn, screenBrightness, orientation).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    // serialization
    private ScreenStateData() {
    }

    public int getScreenBrightness() {
        return screenBrightness;
    }

    public boolean isScreenOn() {
        return screenOn;
    }

    public ScreenOrientation getOrientation() {
        return orientation;
    }
}
