package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Arrays;

import de.tu_darmstadt.informatik.kom.contextsense.MergeableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

import static de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayUtils.concat;

/**
 * Created by Oli on 06.04.2016.
 */
public class GpsSpeedHistoryData extends MergeableSensorData {

    private static final long serialVersionUID = 5880611863855071381L;

    private long[] t;
    private double[] speedInMs;

    // serialization constructor
    private GpsSpeedHistoryData() {
    }

    public GpsSpeedHistoryData( long timestamp, long[] t, double[] speedInMs ) {
        super( timestamp );
        this.t = t;
        this.speedInMs = speedInMs;
    }


    @Override
    public MergeableSensorData copy() {
        return new GpsSpeedHistoryData(
                getTimestamp(),
                Arrays.copyOf(t,t.length),
                Arrays.copyOf(speedInMs,speedInMs.length)
        ).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public long[] getT() {
        return t;
    }

    public double[] getSpeedInMs() {
        return speedInMs;
    }

    public GpsSpeedHistoryData onlyLastSeconds( Duration timeToKeep ) {
        int size = t.length;
        if (size == 0)
            return this;

        long newest = t[size -1];
        long oldestTimeToKeep = newest - timeToKeep.getMillis();

        int indexToKeep = 0;
        while (t[indexToKeep] < oldestTimeToKeep) {
            indexToKeep++;
        }

        return new GpsSpeedHistoryData(
                getTimestamp(),
                Arrays.copyOfRange( t, indexToKeep, size ),
                Arrays.copyOfRange( speedInMs, indexToKeep, size )
        );
    }


    @Override
    protected void mergeIntern(SensorData d) {
        GpsSpeedHistoryData data = (GpsSpeedHistoryData) d;

        boolean own = d.getTimestamp() > getTimestamp();
        t = own ? concat(t,data.t) : concat(data.t, t);
        speedInMs = own ? concat(speedInMs,data.speedInMs) : concat(data.speedInMs, speedInMs);
    }


    @Override
    public void limitFromTo(DateTime from, DateTime to) {
        long fromL = from.getMillis();
        long toL = to.getMillis();

        int first = 0;
        int last = t.length;

        for(int i = 0; i < t.length; i++){
            long time = t[i];

            if(first == 0 && time > fromL) first = i;
            if(last == t.length && time > toL) last = i;
        }

        this.t = Arrays.copyOfRange(t, first, last);
        this.speedInMs = Arrays.copyOfRange(speedInMs, first, last);
    }


    @Override
    public void applyClockSkew(long clockSkew) {
        super.applyClockSkew(clockSkew);
        applyChangeToArray( t, clockSkew );
    }

}
