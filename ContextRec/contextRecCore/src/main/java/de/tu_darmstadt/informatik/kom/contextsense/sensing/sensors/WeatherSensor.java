package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import de.tu_darmstadt.informatik.kom.contextsense.core.R;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.WeatherData;
import java8.util.Optional;

/**
 * Sensor for fetching current weather at the user's location using http://openweathermap.org/
 *
 * IMPORTANT: to use this sensor, you need to supply an API key.
 * Please put your API KEY in a resource-string (e.g. in strings.xml) named "openweathermap_api_key".
 * For example: <string name="openweathermap_api_key">your key</string>
 */
@SuppressWarnings("MissingPermission")
public class WeatherSensor extends Sensor {

    private static final String TAG = "WeatherSensor";
    private static final Duration CACHE_DURATION = Duration.standardMinutes( 10 );
    private static final String OPEN_WEATHERMAP_API_URL = "http://api.openweathermap.org/data/2.5/weather";

    private final GoogleApiClient client;
    private final ExecutorService executor;
    private final RequestQueue queue;
    private Optional<WeatherData> lastResult;
    private Optional<String> apiKey;

    public WeatherSensor( Context context ) {
        super( context );
        lastResult = Optional.empty();
        apiKey = loadApiKey( context );
        executor = Executors.newSingleThreadExecutor();
        queue = Volley.newRequestQueue( context );

        client = new GoogleApiClient.Builder( context )
                .addApi( LocationServices.API )
                .build();
    }

    private Optional<String> loadApiKey( Context context ) {
        String apiKey = context.getResources().getString( R.string.openweathermap_api_key );

        if (apiKey.equals( "PLACEHOLDER" )) {
            Log.d( TAG, "Disabling WeatherSensor - No OpenWeatherMap API KEY provided. Put your api-key into a string named \"openweathermap_api_key\" in the string-resources (e.g. strings.xml)." );
            return Optional.empty();
        } else {
            return Optional.of( apiKey );
        }
    }

    @Override
    public Set<String> getRequiredPermissions() {
        Set<String> permissions = new HashSet<>();
        permissions.add( Manifest.permission.ACCESS_COARSE_LOCATION );
        permissions.add( Manifest.permission.INTERNET );
        return permissions;
    }

    @Override
    public void start( SensorMode mode ) {
        super.start( mode );

        if (apiKey.isPresent()) {
            client.connect();
        }
    }

    @Override
    public void stop() {
        super.stop();

        if (apiKey.isPresent()) {
            client.disconnect();
        }
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        if (!apiKey.isPresent())
            return SensorInvocations.emptyInvocation();

        if (isCachedValueStillGood())
            return SensorInvocations.fromResult( lastResult );

        return fetchWeather();
    }

    private boolean isCachedValueStillGood() {
        if (!lastResult.isPresent())
            return false;

        long lastTimestamp = lastResult.get().getTimestamp();
        DateTime lastTime = new DateTime( lastTimestamp );
        DateTime now = DateTime.now();

        return now.minus( CACHE_DURATION ).isBefore( lastTime );
    }

    private SensorInvocation fetchWeather() {
        Location location = LocationServices.FusedLocationApi.getLastLocation( client );
        if (location == null)
            return SensorInvocations.emptyInvocation();

        String key = apiKey.get();

        return doFetchWeather( location, key );
    }

    private SensorInvocation doFetchWeather( Location location, String key ) {
        String url = String.format(
                Locale.ENGLISH,
                OPEN_WEATHERMAP_API_URL + "?lat=%f&lon=%f&appid=%s&units=metric",
                location.getLatitude(),
                location.getLongitude(),
                key
        );

        return SensorInvocations.fromAsyncTask( executor, () -> {
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            JsonObjectRequest request = new JsonObjectRequest(
                    Request.Method.GET,
                    url,
                    null,
                    future,
                    future
            );

            queue.add(request);

            try {
                JSONObject response = future.get();
                WeatherData parsed = parseResponse( response );
                lastResult = Optional.of( parsed );
                return Optional.of( parsed );
            } catch (JSONException e) {
                Log.e( TAG, "Could not parse response from openweathermap.org. Maybe their API changed?" );
                e.printStackTrace();
                return Optional.empty();
            } catch (Exception e) {
                // probably Network-IO error
                e.printStackTrace();
                return Optional.empty();
            }
        });
    }

    private WeatherData parseResponse( JSONObject response ) throws JSONException {
        return new WeatherData(
                DateTime.now().getMillis(),
                parseWeatherType( response ),
                parseTemperature( response ),
                parseWindStrength( response )
        );
    }

    private WeatherData.WeatherType parseWeatherType( JSONObject response ) throws JSONException {
        int code = response.getJSONArray( "weather" ).getJSONObject( 0 ).getInt( "id" );

        if (isBetween(code, 200, 299 ))
            return WeatherData.WeatherType.THUNDER_STORM;
        else if (isBetween(code, 300, 399))
            return WeatherData.WeatherType.DRIZZLE;
        else if (isBetween(code, 500, 599))
            return WeatherData.WeatherType.RAINY;
        else if (isBetween(code, 600, 699))
            return WeatherData.WeatherType.SNOW;
        else if (isBetween(code, 700, 799))
            return WeatherData.WeatherType.ATMOSPHERE;
        else if (isBetween(code, 800, 800))
            return WeatherData.WeatherType.CLEAR;
        else if (isBetween(code, 801, 809))
            return WeatherData.WeatherType.CLOUDY;
        else if (isBetween(code, 900, 909))
            return WeatherData.WeatherType.EXTREME;
        else
            return WeatherData.WeatherType.UNKNOWN;
    }

    private boolean isBetween( int x, int startInclusive, int endInclusive ) {
        return startInclusive <= x && x <= endInclusive;
    }

    private double parseTemperature( JSONObject response ) throws JSONException {
        return response.getJSONObject( "main" ).getDouble( "temp" );
    }

    private double parseWindStrength( JSONObject response ) throws JSONException {
        return response.getJSONObject( "wind" ).getDouble( "speed" );
    }

}
