package de.tu_darmstadt.informatik.kom.contextsense;

/**
 * Created by Toby on 20.10.2017
 */

public interface Vibrateable {

    /**
     * Does a simple vibration.
     */
    void vibrate();

}
