package de.tu_darmstadt.informatik.kom.contextsense.sensing;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Vibrator;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import de.tu_darmstadt.informatik.kom.contextsense.StreamingDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.Vibrateable;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.batch.MutableSensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.lowpower.LowPowerModeDetector;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.lowpower.LowPowerModeDetectors;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.lowpower.SignificantMotionLowPowerModeDetector;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.StreamableSensor;
import de.tu_darmstadt.informatik.kom.contextsense.utils.CancelableFuture;
import java8.util.Optional;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Represents an implementation of {@link DataSource} which uses the sensors of the current device.
 */
public class LocalDataSource extends DataSource implements SignificantMotionLowPowerModeDetector.LowPowerModeCallback, StreamingDataSource, Vibrateable {

    private final List<Sensor> sensors;
    private final ExecutorService executor;
    private final LowPowerModeDetector lowPowerDetector;
    private final Vibrator vibrator;

    public LocalDataSource( Context context, List<Sensor> sensors, DeviceType type, String name ) {
        super( type, name );

        this.lowPowerDetector = LowPowerModeDetectors.buildForThisDevice( context );
        this.sensors = sensors;
        this.executor = Executors.newSingleThreadExecutor();
        this.vibrator = (Vibrator) context.getSystemService( Context.VIBRATOR_SERVICE );

        lowPowerDetector.addListener( this );
    }

    @Override
    public Set<String> getRequiredPermissions() {
        return stream( sensors )
                .flatMap( s -> stream( s.getRequiredPermissions() ) )
                .collect( Collectors.toSet() );
    }

    @Override
    protected void doStart() {
        executor.submit( () -> {
            stream( sensors ).forEach( sensor -> sensor.start( SensorMode.NORMAL ) );
            lowPowerDetector.start();
        });
    }

    @Override
    protected void doStop() {
        executor.submit( () -> {
            lowPowerDetector.stop();
            stream( sensors ).forEach( Sensor::stop );
        });
    }

    @Override
    public void startStreaming(){
        executor.submit( () -> stream( sensors )
                .filter( sensor -> sensor instanceof StreamableSensor)
                .map( sensor -> (StreamableSensor) sensor)
                .forEach( StreamableSensor::startStreaming )
        );
    }


    @Override
    public void stopStreaming(){
        executor.submit( () -> stream( sensors )
                .filter( sensor -> sensor instanceof StreamableSensor)
                .map( sensor -> (StreamableSensor) sensor)
                .forEach( StreamableSensor::stopStreaming )
        );
    }

    @Override
    public void onLowPowerEntered() {
        executor.submit( () -> {
            if (isRunning()) {
                stream( sensors ).forEach( sensor -> {
                    sensor.stop();
                    sensor.start( SensorMode.LOW_POWER );
                });
            }
        });
    }

    @Override
    public void onLowPowerExited() {
        executor.submit( () -> {
            if (isRunning()) {
                stream( sensors ).forEach( sensor -> {
                    sensor.stop();
                    sensor.start( SensorMode.NORMAL );
                });
            }
        });
    }

    @Override
    public Optional<SensorDataBatch> collectStreamedData(DateTime from, DateTime to){
        //Check args are valid:
        if(from == null || to == null) return Optional.empty();
        if(from.isAfter( to )) return Optional.empty();

        //Catch the data from the Sensors:
        final MutableSensorDataBatch batch = new MutableSensorDataBatch( from );
        batch.setEndTime( to );

        stream( sensors )
            .filter( sensor -> sensor instanceof StreamableSensor )
            .map( sensor -> (StreamableSensor) sensor )
            .map( sensor -> sensor.getStreamingResults(from, to) )
            .filter( Optional::isPresent )
            .map( Optional::get )
            .forEach( d -> {
                //Do not forget to claim source:
                d.claimSourceIfUnset( getName(), getType() );
                batch.addData(d);
            } );


        return Optional.of(batch);
    }


    @Override
    protected CancelableFuture<Optional<SensorDataBatch>> doCollectData( Duration batchDuration ) {
        CancelableFuture<Optional<SensorDataBatch>> task = new CancelableFuture<>( () -> blockingCollectData( batchDuration ) );
        executor.execute( task );
        return task;
    }

    private Optional<SensorDataBatch> blockingCollectData( Duration batchDuration ) {
        final MutableSensorDataBatch batch = new MutableSensorDataBatch( DateTime.now() );

        List<SensorInvocation> invocations = stream( sensors )
                .map( sensor -> sensor.trigger( batchDuration ) )
                .collect( Collectors.toList() );

        stream( invocations )
                .forEach( SensorInvocation::start );

        try {
            Thread.sleep( batchDuration.getMillis() );
        } catch (InterruptedException e) {
            stream( invocations ).forEach( SensorInvocation::stop );

            // stopped during execution --> no result
            return Optional.empty();
        }

        stream( invocations )
                .forEach( SensorInvocation::stop );

        stream( invocations )
                .map( SensorInvocation::getResult )
                .filter( Optional::isPresent )
                .map( Optional::get )
                .forEach( data -> {
                    // do not set the source here, if it is already set!
                    // this would override the source from any data we get from remove devices.
                    data.claimSourceIfUnset( getName(), getType() );
                    batch.addData( data );
                } );

        batch.setEndTime( DateTime.now() );
        return Optional.of( batch );
    }

    /**
     * Gets the names of all Sensors present.
     * @return the name of all registered Sensors.
     */
    public List<String> getSensorNames(){
        return stream( sensors )
                .map( Sensor::getName )
                .collect( Collectors.toList() );
    }

    @SuppressLint("MissingPermission")
    @Override
    public void vibrate() {
        try{ vibrator.vibrate( 200 ); }catch ( Throwable throwable ){ throwable.printStackTrace(); }
    }
}