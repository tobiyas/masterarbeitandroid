package de.tu_darmstadt.informatik.kom.contextsense;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import java8.util.stream.IntStreams;

/**
 * Created by Toby on 25.08.2017
 */

public abstract class ExtractableSensorData extends MergeableSensorData {


    // for serialization
    protected ExtractableSensorData() {
        super();
    }

    protected ExtractableSensorData( long timestamp ) {
        super( timestamp );
    }


    /**
     * Extracts the Data from the time wanted.
     * @param from the time when to start.
     * @param to the time to where to gather.
     */
    public ExtractableSensorData extract( DateTime from, DateTime to ){
        ExtractableSensorData copy = copy();
        copy.limitFromTo( from, to );
        return copy;
    }


    @Override
    public ExtractableSensorData setSourceName(String sourceName) {
        super.setSourceName(sourceName);
        return this;
    }


    @Override
    public ExtractableSensorData setSourceType(DeviceType sourceType) {
        super.setSourceType(sourceType);
        return this;
    }

    @Override
    public abstract ExtractableSensorData copy();

}
