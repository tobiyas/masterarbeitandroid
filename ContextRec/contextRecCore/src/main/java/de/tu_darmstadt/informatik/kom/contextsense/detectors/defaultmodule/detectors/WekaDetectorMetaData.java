package de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors;

import java.util.ArrayList;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Preconditions;

/**
 * Describes all features/requirements of a stored weka detector.
 */
public class WekaDetectorMetaData {

    private final DetectorDescription description;
    private final List<String> featureNames;
    private final Precondition precondition;

    // serialization constructor
    private WekaDetectorMetaData() {
        featureNames = new ArrayList<>();
        precondition = Preconditions.NONE;
        description = null;
    }

    public WekaDetectorMetaData( DetectorDescription description, List<String> featureNames, Precondition precondition ) {
        this.description = description;
        this.featureNames = featureNames;
        this.precondition = precondition;
    }

    public List<String> getFeatureNames() {
        return featureNames;
    }

    public Precondition getPrecondition() {
        return precondition;
    }

    public DetectorDescription getDescription() {
        return description;
    }
}
