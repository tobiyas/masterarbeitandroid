package de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.Detector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultDetectorsModule;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Preconditions;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LocationSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.NearbyPlaceData;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Location;
import de.tu_darmstadt.informatik.kom.contextsense.utils.TimingsRecords;
import java8.util.Optional;
import java8.util.stream.Collectors;

import static de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultSituations.CommonPlaces.AT_FOOD_PLACE;
import static de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultSituations.CommonPlaces.AT_SHOP;
import static de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultSituations.CommonPlaces.AT_UNIVERSITY;
import static de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultSituations.CommonPlaces.SITUATION;
import static de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultSituations.CommonPlaces.UNKNOWN;
import static java8.util.stream.StreamSupport.stream;

/**
 * Detector for detecting commonly visited places of a user.
 * Can detect e.g. Home, Work, Friend's home and University.
 */
public class LocationDetector extends Detector {

    public static final DetectorIdentifier ID = new DetectorIdentifier( DefaultDetectorsModule.ID, "Commonly visited places" );

    public static final DetectorDescription DESCRIPTION = new DetectorDescription.Builder( ID )
            .setDescription( "Detector for detecting commonly visited places of a user. Can detect e.g. Home, Work, Friend's home and University." )
            .setSituationGroup( SITUATION )
            .build();

    private static final double MIN_LIKELIHOOD_FOR_BEING_AT_PLACE = 0.4;
    private static final double MAX_ALLOWED_RADIUS_IN_METER = 100;

    private final Queue<AnnotatedSituation> userLocations;

    public LocationDetector() {
        super( DESCRIPTION );
        userLocations = new LinkedList<>();
    }

    @Override
    public Precondition getPrecondition() {
        return Preconditions.requiresSensorData( NearbyPlaceData.class );
    }

    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults ) {
        return getUserResult( batch )
                .or( () -> getLocationBasedOnNearbyPlaces( batch ) )
                .orElseGet( () -> buildGoodResult( UNKNOWN ) );
    }

    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults, TimingsRecords timings ) {
        if( timings != null ) timings.startAction( "Location Detection" );
        try{
            return getUserResult( batch )
                    .or( () -> getLocationBasedOnNearbyPlaces( batch ) )
                    .orElseGet( () -> buildGoodResult( UNKNOWN ) );
        }finally {
            if( timings != null ) timings.endCurrentAction();
        }
    }

    private Optional<DetectorResult> getUserResult( SensorDataBatch batch ) {
        Optional<Location> locationData = locationFromSensorData( batch );
        if (!locationData.isPresent())
            return Optional.empty();

        Location location = locationData.get();
        if (location.radius > MAX_ALLOWED_RADIUS_IN_METER)
            return Optional.of( buildBadResult() );

        return getClosestUserAssignedLocation( location )
                // no method reference because of a bug in Retrolambda (fixed in 3.3)
            .map( res -> buildGoodResult( res ) );
    }

    private Optional<String> getClosestUserAssignedLocation( Location reference ) {
        Map<String, Long> nearbyLocationsByType = stream( userLocations )
                .filter( loc -> areLocationsCloseToEachOther( loc.location, reference ) )
                .collect( Collectors.groupingBy( location -> location.annotation, Collectors.counting() ) );

        return stream( nearbyLocationsByType.entrySet() )
                .max( (e1, e2) -> Long.compare( e1.getValue(), e2.getValue() ) )
                .map( Map.Entry::getKey );
    }

    private boolean areLocationsCloseToEachOther( Location loc1, Location loc2 ) {
        return loc1.distanceTo( loc2 ) <= MAX_ALLOWED_RADIUS_IN_METER;
    }

    private Optional<Location> locationFromSensorData( SensorDataBatch batch ) {
        return batch.getData( LocationSensorData.class )
                .map( data -> new Location( data.getLatitude(), data.getLongitude(), data.getPrecision() ) );
    }

    private Optional<DetectorResult> getLocationBasedOnNearbyPlaces( SensorDataBatch batch ) {
        NearbyPlaceData data = batch.getData( NearbyPlaceData.class ).get();
        return stream( data.getPlaces() )
                .filter( place -> data.getLikelihoodForPlace( place ) >= MIN_LIKELIHOOD_FOR_BEING_AT_PLACE )
                .sorted( (p1,p2) -> Double.compare( data.getLikelihoodForPlace( p1 ), data.getLikelihoodForPlace( p2 ) ) )
                .map( this::categorizePlace )
                .filter( Optional::isPresent )
                .map( Optional::get )
                // no method reference because of a bug in Retrolambda (fixed in 3.3)
                .map( res -> buildGoodResult( res) )
                .findFirst();
    }

    private Optional<String> categorizePlace( NearbyPlaceData.NearbyPlace place ) {
        if ( place.isOfType( NearbyPlaceData.STORE_TYPES ) )
            return Optional.of( AT_SHOP );
        else if ( place.isOfType( NearbyPlaceData.FOOD_TYPES ) )
            return Optional.of( AT_FOOD_PLACE );
        else if ( place.isOfType( NearbyPlaceData.UNIVERSITY_TYPES ) )
            return Optional.of( AT_UNIVERSITY );
        else
            return Optional.empty();
    }

    @Override
    public void trainIncremental( TrainingSample sample ) {
        String symbolicLocation = sample.getCorrectSituation();
        if (symbolicLocation == null)
            return;

        Optional<Location> locationData = locationFromSensorData( sample.getData() );
        if (!locationData.isPresent())
            return;

        Location geoLocation = locationData.get();
        if (geoLocation.radius > MAX_ALLOWED_RADIUS_IN_METER)
            return;

        userLocations.add( new AnnotatedSituation( geoLocation, symbolicLocation ) );
    }

    @SuppressWarnings("unchecked")
    @Override
    public void loadState( InputStream in ) throws IOException {
        try(ObjectInputStream objectIn = new ObjectInputStream( in )) {
            Queue<AnnotatedSituation> stored = (Queue<AnnotatedSituation>)objectIn.readObject();
            userLocations.clear();
            userLocations.addAll( stored );
        } catch (ClassNotFoundException e) {
            // cannot happen (I hope..)
            e.printStackTrace();
        }
    }

    @Override
    public void saveState( OutputStream out ) throws IOException {
        try(ObjectOutputStream objectOut = new ObjectOutputStream( out )) {
            objectOut.writeObject( userLocations );
        }
    }

    private static class AnnotatedSituation implements Serializable {
        private static final long serialVersionUID = 231577496958636839L;

        public final Location location;
        public final String annotation;

        private AnnotatedSituation( Location location, String annotation ) {
            this.location = location;
            this.annotation = annotation;
        }
    }

}
