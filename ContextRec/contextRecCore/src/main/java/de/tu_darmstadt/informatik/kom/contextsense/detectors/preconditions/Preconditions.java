package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import java8.util.stream.RefStreams;

/**
 * Created by Oli on 21.03.2016.
 */
public class Preconditions {

    public static final Precondition NONE = none();

    public static Precondition none() {
        return new NoPrecondition();
    }

    public static Precondition and( Precondition... conditions ) {
        List<Precondition> list = new ArrayList<>( Arrays.asList( conditions ) );
        return new AndPrecondition( list );
    }

    public static Precondition or( Precondition... conditions ) {
        List<Precondition> list = new ArrayList<>( Arrays.asList( conditions ) );
        return new OrPrecondition( list );
    }

    public static Precondition requiresSensorData( Class<? extends SensorData> sensorDataClass ) {
        return new SensorPrecondition( sensorDataClass );
    }

    public static Precondition requiresSensorData( Class<? extends SensorData> sensorDataClass, String wear ) {
        return new SensorPrecondition( sensorDataClass, wear );
    }

    public static Precondition requiresResultsFromDetector( String situationName ) {
        return new RequiresDetectorPrecondition( situationName );
    }

    public static Precondition requiresSituation( String situationGroupName, String situation ) {
        return new SituationPrecondition( situationGroupName, situation );
    }

    public static Precondition requiresSituations( String situationGroupName, String... situations ) {
        return or(
                RefStreams.of( situations )
                .map( sit -> new SituationPrecondition( situationGroupName, sit ) )
                .toArray( Precondition[]::new )
        );
    }
}
