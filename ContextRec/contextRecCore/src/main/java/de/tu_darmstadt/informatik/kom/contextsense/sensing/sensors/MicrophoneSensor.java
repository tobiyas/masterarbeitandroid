package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.Manifest;
import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MicrophoneData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.utils.FFT;
import de.tu_darmstadt.informatik.kom.contextsense.utils.MathUtils;
import java8.util.Optional;
import java8.util.stream.DoubleStreams;
import java8.util.stream.IntStreams;

/**
 * Sensor for measuring the ambient noise level. Will also create a fingerprint of the frequency-domain spectrum.
 */
public class MicrophoneSensor extends Sensor {

    private static final int POLL_FREQ = 44100;
    private static final int NUMBER_OF_SAMPLES = 32768; // needs to be power of 2 (for fft).

    private final SoundMeter soundMeter;
    private final Context context;
    private final ExecutorService executor;

    public MicrophoneSensor( Context context ) {
        super( context );
        this.soundMeter = new SoundMeter();
        this.context = context;
        this.executor = Executors.newSingleThreadExecutor();
    }

    @Override
    public Set<String> getRequiredPermissions() {
        HashSet<String> permissions = new HashSet<>();
        permissions.add( Manifest.permission.RECORD_AUDIO );
        return permissions;
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {

        return new SensorInvocation() {
            Future<MicrophoneData> resultTask;

            @Override
            public void start() {
                resultTask = executor.submit( () -> {
                    short[] data = soundMeter.record( POLL_FREQ, NUMBER_OF_SAMPLES );

                    double db = calcDb( data );
                    MicrophoneData.FrequencyAnalysis analysis = performFreqAnalysis( data );

                    return new MicrophoneData(
                            DateTime.now().getMillis(),
                            db,
                            analysis
                    );
                });
            }

            @Override
            public void stop() {
            }

            public Optional<? extends SensorData> getResult() {
                try {
                    return Optional.of( resultTask.get() );
                } catch (Exception e) {
                    e.printStackTrace();
                    return Optional.empty();
                }
            }
        };
    }

    private MicrophoneData.FrequencyAnalysis performFreqAnalysis( short[] data ) {
        double[] ffted = FFT.fft( data );

        // we're only interested in the 1. half of the data - the second half is identical.
        double[] halfFft = Arrays.copyOf( ffted, ffted.length / 2 );
        double[] bin1 = Arrays.copyOfRange( halfFft, getPositionOfFreq( 0 ) +1, getPositionOfFreq( 3000 ) );
        double[] bin2 = Arrays.copyOfRange( halfFft, getPositionOfFreq( 3000 ) +1, getPositionOfFreq( 8000 ) );
        double[] bin3 = Arrays.copyOfRange( halfFft, getPositionOfFreq( 8000 ) +1, halfFft.length -1 );

        int bin1Highest = findEntryWithHighestValue( bin1 );
        int bin2Highest = findEntryWithHighestValue( bin2 );
        int bin3Highest = findEntryWithHighestValue( bin3 );

        MicrophoneData.FrequencyAnalysis analysis = new MicrophoneData.FrequencyAnalysis();
        analysis.setMean( DoubleStreams.of( halfFft ).average().getAsDouble() );
        analysis.setStdev( MathUtils.stdev( halfFft ) );
        analysis.setDc( halfFft[0] );
        analysis.setBin1HighestFreq( getFreqOfPosition( bin1Highest ) );
        analysis.setBin2HighestFreq( 3000 + getFreqOfPosition( bin2Highest ) );
        analysis.setBin3HighestFreq( 8000 + getFreqOfPosition( bin3Highest ) );
        analysis.setBin1HighestPower( bin1[ bin1Highest ] );
        analysis.setBin2HighestPower( bin2[ bin2Highest ] );
        analysis.setBin3HighestPower( bin3[ bin3Highest ] );

        return analysis;
    }

    private int findEntryWithHighestValue( double[] numbers ) {
        return IntStreams.range( 0, numbers.length )
                .reduce((i,j) -> numbers[i] < numbers[j] ? j : i)
                .getAsInt();
    }

    private int getPositionOfFreq( int freq ) {
        return (int) Math.round(((double)freq * NUMBER_OF_SAMPLES) / POLL_FREQ);
    }

    private int getFreqOfPosition( int position ) {
        return (int)(position * ((double)POLL_FREQ / NUMBER_OF_SAMPLES));
    }

    private double calcDb( short[] data ) {
        long total = 0;
        for (short s : data)
        {
            total += Math.abs(s);
        }

        double percent = (total / (double)data.length) / 9000;
        percent = Math.min( percent, 1 );

        double dbRef = 80;
        double db =  20.0 * Math.log10(percent) + dbRef;

        // clamp to 0 - 100 range
        return Math.max( 0, Math.min( 100, db ) );
    }

    private static class SoundMeter {

        private AudioRecord ar = null;
        private int minSize;

        public short[] record( int freq, int samples ) {
            short[] buffer = new short[samples];

            start( freq );
            // record twice, because the 1. result is garbage most times...
            ar.read( buffer, 0, 8192 );
            ar.read( buffer, 0, samples );
            stop();

            return buffer;
        }

        private void start( int freq ) {
            minSize = AudioRecord.getMinBufferSize( freq, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
            ar = new AudioRecord(MediaRecorder.AudioSource.MIC, freq,AudioFormat.CHANNEL_IN_DEFAULT, AudioFormat.ENCODING_PCM_16BIT, minSize );
            ar.startRecording();
        }

        private void stop() {
            if (ar != null) {
                ar.stop();
            }
        }
    }
}
