package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.predefinedFeatures;

import java.util.Arrays;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureBuilder;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LightSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LinearAccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MagnetometerData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MicrophoneData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PressureSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ProximityData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ScreenStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.StepCounterData;
import de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayInterpolator;
import de.tu_darmstadt.informatik.kom.contextsense.utils.FFT;
import de.tu_darmstadt.informatik.kom.contextsense.utils.MathUtils;

import java8.util.Optional;
import java8.util.stream.DoubleStreams;
import java8.util.stream.IntStreams;

import static de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureGroup.IMU_SENSORS;
import static de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureGroup.WEAR_SENSORS;

/**
 * Created by Oli on 24.04.2016
 */
public class DefaultWearFeatures {

    private static final double GRAVITY_ON_EARTH = 9.81;

    public static void registerDefaultFeatures( WekaFeatureBuilder registry, String dataSource ) {

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Avg. Acceleration (X)",
                sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getX )
                .map( acc -> DoubleStreams.of( acc ).average().getAsDouble() )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Avg. Acceleration (Y)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getY )
                .map( acc -> DoubleStreams.of( acc ).average().getAsDouble() )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Avg. Acceleration (Z)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getZ )
                .map( acc -> DoubleStreams.of( acc ).average().getAsDouble() )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Activity Count (Lin acc x)", sample -> sample
                .getData( LinearAccelerationData.class, dataSource )
                .map( LinearAccelerationData::getX )
                .map( data -> {
                    long samplesAboveThreshold = DoubleStreams.of( data )
                            .filter( d -> (Math.abs( d ) > 0.5 * GRAVITY_ON_EARTH ) )
                            .count();

                    return samplesAboveThreshold / (double)data.length;
                })
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Activity Count (Lin acc y)", sample -> sample
                .getData( LinearAccelerationData.class, dataSource )
                .map( LinearAccelerationData::getY )
                .map( data -> {
                    long samplesAboveThreshold = DoubleStreams.of( data )
                            .filter( d -> (Math.abs( d ) > 0.5 * GRAVITY_ON_EARTH ) )
                            .count();

                    return samplesAboveThreshold / (double)data.length;
                })
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Activity Count (Lin acc z)", sample -> sample
                .getData( LinearAccelerationData.class, dataSource )
                .map( LinearAccelerationData::getZ )
                .map( data -> {
                    long samplesAboveThreshold = DoubleStreams.of( data )
                            .filter( d -> (Math.abs( d ) > 0.5 * GRAVITY_ON_EARTH ) )
                            .count();

                    return samplesAboveThreshold / (double)data.length;
                })
        );

        registry.registerMultiNumericFeature( WEAR_SENSORS, dataSource + ": Acceleration (X) 0-10Hz", 30, sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getX )
                .map( a -> ArrayInterpolator.interpolateLinear( a, 256) )
                .map( data -> {
                    double[] ffted = FFT.fft( Arrays.copyOf( data, 256 ) );
                    return Arrays.copyOf( ffted, 30 );
                })
        );

        registry.registerMultiNumericFeature( WEAR_SENSORS, dataSource + ": Acceleration (Y) 0-10Hz", 30, sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getY )
                .map( a -> ArrayInterpolator.interpolateLinear( a, 256) )
                .map( data -> {
                    double[] ffted = FFT.fft( Arrays.copyOf( data, 256 ) );
                    return Arrays.copyOf( ffted, 30 );
                })
        );

        registry.registerMultiNumericFeature( WEAR_SENSORS, dataSource + ": Acceleration (Z) 0-10Hz", 30, sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getZ )
                .map( a -> ArrayInterpolator.interpolateLinear( a, 256) )
                .map( data -> {
                    double[] ffted = FFT.fft( Arrays.copyOf( data, 256 ) );
                    return Arrays.copyOf( ffted, 30 );
                })
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Acceleration Range (X)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getX )
                .map( acc -> {
                    double min = DoubleStreams.of( acc ).min().getAsDouble();
                    double max = DoubleStreams.of( acc ).max().getAsDouble();
                    return max - min;
                } )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Acceleration Range (Y)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getY )
                .map( acc -> {
                    double min = DoubleStreams.of( acc ).min().getAsDouble();
                    double max = DoubleStreams.of( acc ).max().getAsDouble();
                    return max - min;
                } )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Acceleration Range (Z)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getZ )
                .map( acc -> {
                    double min = DoubleStreams.of( acc ).min().getAsDouble();
                    double max = DoubleStreams.of( acc ).max().getAsDouble();
                    return max - min;
                } )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Stdev. Acceleration (X)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getX )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Stdev. Acceleration (Y)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getY )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Stdev. Acceleration (Z)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getZ )
                .map( MathUtils::stdev )
        );

        registry.registerMultiNumericFeature(WEAR_SENSORS, dataSource + ": Acceleration X Sliced MiddleValue", 20, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map( AccelerationData::getX )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 20)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i, i + 10 ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerMultiNumericFeature(WEAR_SENSORS, dataSource + ": Acceleration Y Sliced MiddleValue", 20, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map( AccelerationData::getY )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 20)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i, i + 10 ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerMultiNumericFeature(WEAR_SENSORS, dataSource + ": Acceleration Z Sliced MiddleValue", 20, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map( AccelerationData::getZ )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 20)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i, i + 10 ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Mag. Field (X)", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .map( MagnetometerData::getX )
                .map( mag -> DoubleStreams.of( mag ).average().getAsDouble() )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Mag. Field (Y)", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .map( MagnetometerData::getY )
                .map( mag -> DoubleStreams.of( mag ).average().getAsDouble() )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Mag. Field (Z)", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .map( MagnetometerData::getZ )
                .map( mag -> DoubleStreams.of( mag ).average().getAsDouble() )
        );

        registry.registerBooleanFeature( WEAR_SENSORS, dataSource + ": Proximity", sample -> sample
                .getData( ProximityData.class, dataSource )
                .map( ProximityData::isClose )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Brightness", sample -> sample
                .getData( LightSensorData.class, dataSource )
                .map( LightSensorData::getBrightnessInLux )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Pressure", sample -> sample
                .getData( PressureSensorData.class, dataSource )
                .flatMap( data -> Optional.ofNullable( data.getPressureData() ) )
                .map( data -> {
                    double first = data[0];
                    double last = data[data.length -1];
                    return last - first;
                })
        );

        registry.registerBooleanFeature( WEAR_SENSORS, dataSource + ": Is screen on", sample -> sample
                .getData( ScreenStateData.class, dataSource )
                .map( ScreenStateData::isScreenOn )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Steps per second", sample -> sample
                .getData( StepCounterData.class, dataSource )
                .map( StepCounterData::getStepsPerSec )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Ambient Noise (db)", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .map( MicrophoneData::getNoiseLevelInDb )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Audio spectrum mean", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getMean )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Audio spectrum stdev", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getStdev )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Audio spectrum dc", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getDc )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Audio spectrum 0Hz - 3000Hz dominant freq", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getBin1HighestFreq )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Audio spectrum 3000Hz - 8000Hz dominant freq", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getBin2HighestFreq )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Audio spectrum 8000Hz - 22000Hz dominant freq", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( MicrophoneData.FrequencyAnalysis::getBin3HighestFreq )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Audio spectrum 0Hz - 3000Hz highest power", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( data -> data.getBin1HighestPower() / data.calcHighestPower() )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Audio spectrum 3000Hz - 8000Hz highest power", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( data -> data.getBin2HighestPower() / data.calcHighestPower() )
        );

        registry.registerNumericFeature( WEAR_SENSORS, dataSource + ": Audio spectrum 8000Hz - 22000Hz highest power", sample -> sample
                .getData( MicrophoneData.class, dataSource )
                .flatMap( MicrophoneData::getFrequencyAnalysis )
                .map( data -> data.getBin3HighestPower() / data.calcHighestPower() )
        );
    }
}
