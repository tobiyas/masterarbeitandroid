package de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors;

import org.joda.time.Duration;

import de.tu_darmstadt.informatik.kom.contextsense.DataSources;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.Detector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultDetectorsModule;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultSituations;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Preconditions;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerometerHistoryData;
import de.tu_darmstadt.informatik.kom.contextsense.utils.MathUtils;
import de.tu_darmstadt.informatik.kom.contextsense.utils.TimingsRecords;

/**
 * This detector tries to detect, if the user is currently wearing his wear device.
 */
public class WearDevicePositionDetector extends Detector {

    public static final DetectorIdentifier ID = new DetectorIdentifier( DefaultDetectorsModule.ID, "Wear Device Position" );

    public static final DetectorDescription DESCRIPTION = new DetectorDescription.Builder( ID )
            .setDescription( "This detector tries to detect, if the user is currently wearing his wear device." )
            .setSituationGroup( DefaultSituations.WearPosition.SITUATION )
            .build();

    public WearDevicePositionDetector() {
        super( DESCRIPTION );
    }

    @Override
    public Precondition getPrecondition() {
        return Preconditions.and(
                Preconditions.requiresSensorData( AccelerationData.class, DataSources.WEAR ),
                Preconditions.requiresSensorData( AccelerometerHistoryData.class, DataSources.WEAR )
        );
    }

    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults ) {
        double stdDevAccShortTime = calcMaxStdevShort( batch );
        double stdDevAccLongTime = calcMaxStdevLong( batch );
        double maxStdDev = Math.max( stdDevAccShortTime, stdDevAccLongTime );
        boolean atWrist = maxStdDev >= 0.1;

        String result = atWrist ? DefaultSituations.WearPosition.AT_WRIST : DefaultSituations.WearPosition.ON_TABLE;
        return buildGoodResult( result );
    }


    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults, TimingsRecords timingsRecords ) {
        return detect( batch, otherResults );
    }

    private double calcMaxStdevShort( SensorDataBatch batch ) {
        AccelerationData data = batch.getData( AccelerationData.class, DataSources.WEAR ).get();

        double stdevX = MathUtils.stdev( data.getX() );
        double stdevY = MathUtils.stdev( data.getY() );
        double stdevZ = MathUtils.stdev( data.getZ() );

        return Math.max( Math.max( stdevX, stdevY ), stdevZ );
    }

    private double calcMaxStdevLong( SensorDataBatch batch ) {
        AccelerometerHistoryData data = batch
                .getData( AccelerometerHistoryData.class, DataSources.WEAR )
                .map( acc -> acc.onlyLastSeconds( Duration.standardSeconds( 10 ) ) )
                .get();

        double stdevX = MathUtils.stdev( data.getX() );
        double stdevY = MathUtils.stdev( data.getY() );
        double stdevZ = MathUtils.stdev( data.getZ() );

        return Math.max( Math.max( stdevX, stdevY ), stdevZ );
    }
}
