package de.tu_darmstadt.informatik.kom.contextsense.utils;

import java.util.Arrays;

/**
 * Created by Toby on 25.08.2017.
 */

public class ArrayUtils {


    public static <T> T[] concat(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static long[] concat(long[] first, long[] second) {
        long[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static float[] concat(float[] first, float[] second) {
        float[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static double[] concat(double[] first, double[] second) {
        double[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static int[] concat(int[] first, int[] second) {
        int[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static boolean[] concat(boolean[] first, boolean[] second) {
        boolean[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static short[] concat(short[] first, short[] second) {
        short[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }


    public static Byte[] convert( byte[] data){
        Byte[] objects = new Byte[data.length];

        int i = 0;
        for ( byte d : data ) objects[i++] = d; // Autoboxing

        return objects;
    }


    public static byte[] convert( Byte[] data){
        byte[] objects = new byte[data.length];

        int i = 0;
        for ( byte d : data ) objects[i++] = d; // Autoboxing

        return objects;
    }


    public static Integer[] convert( int[] data){
        Integer[] objects = new Integer[data.length];

        int i = 0;
        for ( int d : data ) objects[i++] = d; // Autoboxing

        return objects;
    }



    public static int[] convert( Integer[] data){
        int[] objects = new int[data.length];

        int i = 0;
        for ( int d : data ) objects[i++] = d; // Autoboxing

        return objects;
    }


}
