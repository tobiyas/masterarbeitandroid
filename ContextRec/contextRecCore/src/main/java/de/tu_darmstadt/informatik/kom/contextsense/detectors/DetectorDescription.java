package de.tu_darmstadt.informatik.kom.contextsense.detectors;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;

/**
 * Describes all the capabilities and requirements of an detector.
 */
public class DetectorDescription {

    private final DetectorIdentifier id;
    private final String description;
    private final SituationGroup detectableGroup;

    // serialization
    private DetectorDescription() {
        id = null;
        description = null;
        detectableGroup = null;
    }

    private DetectorDescription( DetectorIdentifier id, String description, SituationGroup group ) {
        this.id = id;
        this.description = description;
        this.detectableGroup = group;
    }

    public DetectorIdentifier getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public SituationGroup getDetectableGroup() {
        return detectableGroup;
    }

    public static class Builder {

        private DetectorIdentifier id;
        private String description;
        private SituationGroup group;

        public Builder( DetectorIdentifier id ) {
            this.id = id;
        }

        public Builder setDescription( String description ) {
            this.description = description;
            return this;
        }

        public Builder setSituationGroup( String id, String... labels ) {
            this.group = new SituationGroup( id, labels );
            return this;
        }

        public Builder setSituationGroup( SituationGroup situationGroup ) {
            this.group = situationGroup;
            return this;
        }

        public DetectorDescription build() {
            if (group == null)
                throw new IllegalStateException( "No SituationGroup set. Use setSituationGroup to specify the detected group" );
            if (description == null)
                throw new IllegalStateException( "No description set. Use setDescription to specify an description for this detector." );

            return new DetectorDescription( id, description, group );
        }
    }
}
