package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.Optional;

/**
 * Created by Oli on 13.12.2015
 */
public class MicrophoneData extends SensorData {

    private static final long serialVersionUID = 7751917857802555957L;

    private double noiseLevelInDb;

    // this is optional, because we collected a lot of samples before introducing the analysis.
    private Optional<FrequencyAnalysis> frequencyAnalysis;

    public MicrophoneData( long timestamp, double noiseLevelInDb, FrequencyAnalysis analysis ) {
        super( timestamp );
        this.noiseLevelInDb = noiseLevelInDb;
        this.frequencyAnalysis = Optional.ofNullable( analysis );
    }

    // serialization
    private MicrophoneData() {
        this.frequencyAnalysis = Optional.empty();
    }

    public double getNoiseLevelInDb() {
        return noiseLevelInDb;
    }

    public Optional<FrequencyAnalysis> getFrequencyAnalysis() {
        return frequencyAnalysis;
    }

    public void setFrequencyAnalysis( Optional<FrequencyAnalysis> frequencyAnalysis ) {
        this.frequencyAnalysis = frequencyAnalysis;
    }

    @Override
    public SensorData copy() {
        return new MicrophoneData( getTimestamp(), noiseLevelInDb, frequencyAnalysis.orElse(null) ).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public static class FrequencyAnalysis {

        public FrequencyAnalysis() {
        }

        /**
         * The power of the base-frequency (0 Hz)
         */
        private double dc;

        /**
         * The mean of the power spectrum
         */
        private double mean;

        /**
         * The standard deviation of the power spectrum
         */
        private double stdev;

        /**
         * bin 1 is from 0 - 3000 Hz
         */
        private double bin1HighestFreq;

        /**
         * bin 1 is from 0 - 3000 Hz
         */
        private double bin1HighestPower;

        /**
         * bin 2 is from 3000 - 8000 Hz
         */
        private double bin2HighestFreq;

        /**
         * bin 2 is from 3000 - 8000 Hz
         */
        private double bin2HighestPower;

        /**
         * bin 3 is from 8000 - 22050 Hz
         */
        private double bin3HighestFreq;

        /**
         * bin 3 is from 8000 - 22050 Hz
         */
        private double bin3HighestPower;

        public double getDc() {
            return dc;
        }

        public void setDc( double dc ) {
            this.dc = dc;
        }

        public double getMean() {
            return mean;
        }

        public void setMean( double mean ) {
            this.mean = mean;
        }

        public double getStdev() {
            return stdev;
        }

        public void setStdev( double stdev ) {
            this.stdev = stdev;
        }

        public double getBin1HighestFreq() {
            return bin1HighestFreq;
        }

        public void setBin1HighestFreq( double bin1HighestFreq ) {
            this.bin1HighestFreq = bin1HighestFreq;
        }

        public double getBin1HighestPower() {
            return bin1HighestPower;
        }

        public void setBin1HighestPower( double bin1HighestPower ) {
            this.bin1HighestPower = bin1HighestPower;
        }

        public double getBin2HighestFreq() {
            return bin2HighestFreq;
        }

        public void setBin2HighestFreq( double bin2HighestFreq ) {
            this.bin2HighestFreq = bin2HighestFreq;
        }

        public double getBin2HighestPower() {
            return bin2HighestPower;
        }

        public void setBin2HighestPower( double bin2HighestPower ) {
            this.bin2HighestPower = bin2HighestPower;
        }

        public double getBin3HighestFreq() {
            return bin3HighestFreq;
        }

        public void setBin3HighestFreq( double bin3HighestFreq ) {
            this.bin3HighestFreq = bin3HighestFreq;
        }

        public double getBin3HighestPower() {
            return bin3HighestPower;
        }

        public void setBin3HighestPower( double bin3HighestPower ) {
            this.bin3HighestPower = bin3HighestPower;
        }

        public double calcHighestPower() {
            return Math.max( bin1HighestPower, Math.max( bin2HighestPower, bin3HighestPower ) );
        }
    }
}
