package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015.
 */
public class AmbientTemperatureData extends SensorData {

    private static final long serialVersionUID = 2773827722106584101L;

    private double temperatureInDegreeCelsius;

    // serialization constructor
    private AmbientTemperatureData() {
    }

    public AmbientTemperatureData( long timestamp, double temperatureInDegreeCelsius ) {
        super( timestamp );
        this.temperatureInDegreeCelsius = temperatureInDegreeCelsius;
    }

    public double getTemperatureInDegreeCelsius() {
        return temperatureInDegreeCelsius;
    }

    @Override
    public SensorData copy() {
        return new AmbientTemperatureData(getTimestamp(), temperatureInDegreeCelsius).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }
}
