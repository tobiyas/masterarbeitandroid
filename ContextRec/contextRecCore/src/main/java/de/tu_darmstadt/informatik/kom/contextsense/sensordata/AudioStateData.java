package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 15.12.2015.
 */
public class AudioStateData extends SensorData {

    private static final long serialVersionUID = -6780169602952772885L;

    public enum RingerState {
        NORMAL,
        SILENT,
        VIBRATE
    }

    private boolean isHeadsetConnected;
    private boolean isMusicActive;
    private RingerState ringerState;

    // serialization
    private AudioStateData() {
    }

    public AudioStateData( long timestamp, boolean isHeadsetConnected, boolean isMusicActive, RingerState ringerState ) {
        super( timestamp );
        this.isHeadsetConnected = isHeadsetConnected;
        this.isMusicActive = isMusicActive;
        this.ringerState = ringerState;
    }

    public boolean isHeadsetConnected() {
        return isHeadsetConnected;
    }

    public boolean isMusicActive() {
        return isMusicActive;
    }

    public RingerState getRingerState() {
        return ringerState;
    }

    @Override
    public SensorData copy() {
        return new AudioStateData(getTimestamp(), isHeadsetConnected, isMusicActive, ringerState).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }
}
