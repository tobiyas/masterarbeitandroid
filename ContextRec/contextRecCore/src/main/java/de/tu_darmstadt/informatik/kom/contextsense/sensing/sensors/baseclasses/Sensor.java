package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import org.joda.time.Duration;

import java.util.HashSet;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Abstract base class for all sensors
 */
public abstract class Sensor {

    private final Context context;
    private boolean isRunning;
    private SensorMode mode;

    protected Sensor( Context context ) {
        this.context = context;
    }

    /**
     * Called, before the sensor is triggered the first time.
     * The sensor may use this function, e.g. to load background info
     * or to start some service (like location service).
     */
    public void start( SensorMode mode ){
        this.isRunning = true;
        this.mode = mode;
    }

    /**
     * Called, when the sensor is no longer needed.
     * Trigger will not be called after this method.
     * Stop may be followed by start to restart the sensor.
     */
    public void stop() {
        isRunning = false;
    }

    /**
     * Indicates, if this sensor is currently active.
     */
    public boolean isRunning() {
        return isRunning;
    }

    public final SensorInvocation trigger( Duration batchDuration ) {
        if (getMissingPermissions().isEmpty()) {
            return doTrigger( batchDuration );
        } else {
            Log.i( getName() + " Sensor", "Missing permissions " + getMissingPermissions() + ". I won't do anything." );
            return SensorInvocations.emptyInvocation();
        }
    }

    /**
     * Trigger this sensor now.
     * It is guaranteed, that all required permissions are present.
     */
    protected abstract SensorInvocation doTrigger( Duration batchDuration );

    /**
     * Returns the name of this sensor.
     * The name is the Name of the sensor-class.
     */
    public final String getName() {
        return getClass().getSimpleName();
    }

    /**
     * Returns a set of permissions (Android.Manifest.Permission...) that this sensor needs to run.
     * Override this function, if your sensor needs some permissions.
     */
    public Set<String> getRequiredPermissions() {
        return new HashSet<>();
    }

    /**
     * Returns a set of permissions this sensor is missing to run.
     * This will actually check for runtime-permissions on an Android >= 6.0 device.
     */
    public final Set<String> getMissingPermissions() {
        return StreamSupport.stream( getRequiredPermissions() )
                .filter( p -> ContextCompat.checkSelfPermission( context, p ) != PackageManager.PERMISSION_GRANTED )
                .collect( Collectors.toSet() );
    }

    /**
     * Returns true, if the sensor is running and the current operation mode is {@link SensorMode#LOW_POWER}
     */
    protected boolean isInLowPowerMode() {
        return isRunning && mode == SensorMode.LOW_POWER;
    }

    @Override
    public String toString() {
        return getName();
    }
}
