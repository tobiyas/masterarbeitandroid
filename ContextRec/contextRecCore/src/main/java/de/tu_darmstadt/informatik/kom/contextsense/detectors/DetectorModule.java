package de.tu_darmstadt.informatik.kom.contextsense.detectors;

import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.IDetector;

/**
 * Represents a collection of detectors.
 */
public abstract class DetectorModule {

    /**
     * Returns the unique id of this module.
     */
    public abstract String getModuleId();

    /**
     * Returns the ids of all sensors of this module.
     */
    public abstract Set<DetectorIdentifier> getProvidedDetectorIds();

    /**
     * Returns the description for the given detector.
     */
    public abstract DetectorDescription describeDetector( DetectorIdentifier id );

    /**
     * Build the given detector.
     */
    public abstract IDetector buildDetector( DetectorIdentifier id );

}
