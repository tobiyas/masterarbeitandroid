package de.tu_darmstadt.informatik.kom.contextsense.detectors.base;

/**
 * Created by Oli on 20.04.2016.
 */
public enum ResultType {
    NO_RESULT_MISSING_PRECONDITION,
    NO_RESULT_NO_CLEAR_SITUATION,
    GOOD_RESULT
}
