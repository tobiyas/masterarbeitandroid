package de.tu_darmstadt.informatik.kom.contextsense.utils;

import java8.util.function.Supplier;

/**
 * Created by Oli on 10.02.2016
 */
public class Lazy<T> {

    private final Supplier<T> supplier;
    private T value;
    private boolean didEval;


    private Lazy( Supplier<T> supplier ) {
        this.supplier = supplier;
        this.didEval = false;
        this.value = null;
    }


    /**
     * directly wraps the value.
     * @param value to wrap.
     */
    private Lazy( T value ){
        this.supplier = () -> value;
        this.didEval = true;
        this.value = value;
    }


    /**
     * Gets the value. Either cached or reads it.
     *
     * @return the value cached or gets a new Instance.
     */
    public T get() {
        if (didEval) {
            return value;
        } else {
            value = supplier.get();
            didEval = true;
            return value;
        }
    }

    /**
     * Forces a value for the Lazy variable.
     * @param value to set.
     */
    public void forceValue( T value ){
        this.didEval = true;
        this.value = value;
    }


    /**
     * Unloads the Value.
     */
    public void unload(){
        this.didEval = false;
        this.value = null;
    }


    /**
     * Returns true if the value is loaded.
     * @return true if loaded.
     */
    public boolean isLoaded(){
        return didEval;
    }


    public static <T> Lazy<T> of( Supplier<T> supplier ) {
        return new Lazy<>( supplier );
    }

    public static <T> Lazy<T> of ( T value ) {
        return new Lazy<>( value );
    }
}
