package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka;

import java8.util.function.Supplier;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.lazy.KStar;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.trees.RandomForest;
import weka.core.SelectedTag;
import weka.filters.unsupervised.attribute.Normalize;

/**
 * Collection of all Weka-Based classifiers provided by ContextRec.
 */
public enum WekaBaseClassifier {

    J48( "J48", weka.classifiers.trees.J48::new, weka.classifiers.trees.J48.class ),
    SVM( "SVM (linear)", WekaBaseClassifier::buildSvm, LibSVM.class ),
    HOEFFDING_TREE( "HoeffdingTree", weka.classifiers.trees.HoeffdingTree::new, weka.classifiers.trees.HoeffdingTree.class ),
    LMT( "LMT", weka.classifiers.trees.LMT::new, weka.classifiers.trees.LMT.class ),
    M5P( "M5P", weka.classifiers.trees.M5P::new, weka.classifiers.trees.M5P.class ),
    REP_TREE( "REPTree", weka.classifiers.trees.REPTree::new, weka.classifiers.trees.REPTree.class ),
    RANDOM_FOREST( "Random Forest", RandomForest::new, RandomForest.class ),
    NAIVE_BAYES( "Naive Bayes", NaiveBayes::new, NaiveBayes.class ),
    NAIVE_BAYES_MULTINOMIAL( "Naive Bayes (Multinomial)", weka.classifiers.bayes.NaiveBayesMultinomial::new, weka.classifiers.bayes.NaiveBayesMultinomial.class ),
    BAYES_NET( "Bayes Net", weka.classifiers.bayes.BayesNet::new, weka.classifiers.bayes.BayesNet.class),
    MULTILAYER_PERCEPTRON( "Multilayer Perceptron", weka.classifiers.functions.MultilayerPerceptron::new, weka.classifiers.functions.MultilayerPerceptron.class),
    K_STAR( "K Star", KStar::new, KStar.class),
    IBK( "K Nearest Neighbors", weka.classifiers.lazy.IBk::new, weka.classifiers.lazy.IBk.class),
    SMO( "SMO", weka.classifiers.functions.SMO::new, weka.classifiers.functions.SMO.class ),
    SIMPLE_LOGISTIC( "Simple Logistic", SimpleLogistic::new, SimpleLogistic.class ),

    NAIVE_BAYES_NORM( "Naive Bayes Norm", () -> wrapWithNormalize( NaiveBayes::new ), NaiveBayes.class ),
    SIMPLE_LOGISTIC_NORM( "Simple Logistic Norm", () -> wrapWithNormalize( SimpleLogistic::new ) , SimpleLogistic.class )
    ;

    //HMM data format does not work here... Maybe rework parts for that.
    //HMM( "Hidden Markov Model", weka.classifiers.bayes.HMM::new, weka.classifiers.bayes.HMM.class );

    private static Classifier buildSvm() {
        LibSVM svm = new LibSVM();
        svm.setNormalize( true );
        svm.setKernelType( new SelectedTag( LibSVM.TAGS_KERNELTYPE[0].getID(), LibSVM.TAGS_KERNELTYPE ) );
        return svm;
    }



    private static Classifier wrapWithNormalize( Supplier<Classifier> buildClassifier ){
        FilteredClassifier fc = new FilteredClassifier();
        fc.setFilter( new Normalize() );
        fc.setClassifier( buildClassifier.get() );

        return fc;
    }


    private final String name;
    private final Supplier<Classifier> buildClassifier;
    private final Class<? extends Classifier> wekaClass;

    WekaBaseClassifier( String name, Supplier<Classifier> buildClassifier, Class<? extends Classifier> wekaClass ) {
        this.buildClassifier = buildClassifier;
        this.name = name;
        this.wekaClass = wekaClass;
    }

    public Class<? extends Classifier> getWekaClass() {
        return wekaClass;
    }

    public Classifier build() {
        return buildClassifier.get();
    }

    @Override
    public String toString() {
        return name;
    }


    public static WekaBaseClassifier fromClassifier( Classifier classifier ){
        if( classifier == null ) return null;

        if( classifier instanceof FilteredClassifier ){
            FilteredClassifier filter = (FilteredClassifier) classifier;
            return fromClassifier( filter.getClassifier() );
        }

        for( WekaBaseClassifier base : values() ){
            if( base.getWekaClass() == classifier.getClass() ) return base;
        }

        return null;
    }

}
