package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.Optional;

/**
 * Represents one invocation of a sensor.
 * Each invocation represents a fixed duration, in which the sensor is supposed to collect data.
 * In the beginning, start is called.
 * After some time, stop is called.
 * After stop, the result can be retrieved as often as desired.
 */
public interface SensorInvocation {

    /**
     * Start collecting data.
     * This may only be called once.
     * It may not be called before stop.
     */
    void start();

    /**
     * Stop collecting data and prepare for a call of getResult().
     * This may only be called once.
     * It has to be called after calling start and before calling getResult().
     */
    void stop();

    /**
     * Fetch all collected data.
     * May be called an arbitrary number of times, but only after calling stop.
     */
    Optional<? extends SensorData> getResult();
}
