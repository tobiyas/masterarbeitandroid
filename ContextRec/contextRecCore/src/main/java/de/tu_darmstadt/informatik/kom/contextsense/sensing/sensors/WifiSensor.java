package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.WifiSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import java8.util.Optional;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;

import static java8.util.stream.StreamSupport.stream;

/**
 * Sensor for fetching the current state of the wifi chip.
 * Is wifi enabled? Is it connected? Which networks are in range?
 */
@SuppressWarnings("MissingPermission")
public class WifiSensor extends Sensor {

    private final Context context;
    private final WifiManager wifiManager;
    private final ConnectivityManager connectivityManager;

    public WifiSensor( Context context ) {
        super(context);
        this.context = context;
        this.wifiManager = (WifiManager) context.getSystemService( Context.WIFI_SERVICE );
        this.connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
    }

    @Override
    public Set<String> getRequiredPermissions() {
        HashSet<String> permissions = new HashSet<>();
        permissions.add( Manifest.permission.ACCESS_WIFI_STATE );
        permissions.add( Manifest.permission.CHANGE_WIFI_STATE );
        return permissions;
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        boolean canScan = wifiManager.isWifiEnabled() || wifiManager.isScanAlwaysAvailable();
        if (canScan) {
            return new WifiReceiver();
        } else {
            return SensorInvocations.fromResult( toSenosrData( Optional.empty() ) );
        }
    }

    private WifiSensorData toSenosrData( Optional<List<String>> wifiNets ) {
        boolean wifiConnected;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            wifiConnected = checkWifiConnectedLollipop();
        } else {
            wifiConnected = checkWifiConnectedLegacy();
        }

        return new WifiSensorData(
                DateTime.now().getMillis(),
                wifiManager.isWifiEnabled(),
                wifiConnected,
                Optional.ofNullable( wifiManager.getConnectionInfo().getSSID() ),
                wifiNets
        );
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private boolean checkWifiConnectedLollipop() {
        return RefStreams.of( connectivityManager.getAllNetworks() )
                .map( connectivityManager::getNetworkInfo )
                .filter( info -> info.getType() == ConnectivityManager.TYPE_WIFI )
                .anyMatch( NetworkInfo::isConnected );
    }

    @SuppressWarnings("deprecation")
    private boolean checkWifiConnectedLegacy() {
        return connectivityManager.getNetworkInfo( ConnectivityManager.TYPE_WIFI ).isConnected();
    }

    private class WifiReceiver extends BroadcastReceiver implements SensorInvocation
    {
        private Optional<WifiSensorData> result;

        public void onReceive(Context c, Intent intent)
        {
            List<String> wifiNetworks = stream( wifiManager.getScanResults() )
                    .map( result -> result.SSID )
                    .collect( Collectors.toList() );

            result = Optional.of( toSenosrData( Optional.of( wifiNetworks ) ) );
        }

        @Override
        public void start() {
            result = Optional.empty();
            context.registerReceiver( this, new IntentFilter( WifiManager.SCAN_RESULTS_AVAILABLE_ACTION ) );
            wifiManager.startScan();
        }

        @Override
        public void stop() {
            context.unregisterReceiver( this );
        }

        @Override
        public Optional<? extends SensorData> getResult() {
            if (!result.isPresent()) {
                Log.d( "Wifi Sensor", "did not respond in time." );
                return Optional.empty();
            }

            return Optional.of( result.get() );
        }
    }
}
