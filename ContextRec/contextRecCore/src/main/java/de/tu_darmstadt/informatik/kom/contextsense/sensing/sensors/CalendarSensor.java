package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.CalendarData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;

/**
 * Software sensor which looks into the user's calendar and reports events of the current day.
 */
public class CalendarSensor extends Sensor {

    private final Context context;

    public CalendarSensor( Context context ) {
        super( context );
        this.context = context;
    }

    @Override
    public Set<String> getRequiredPermissions() {
        HashSet<String> permissions = new HashSet<>();
        permissions.add( Manifest.permission.READ_CALENDAR );
        return permissions;
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        DateTime startTime = DateTime.now().minusDays( 3 );
        DateTime endTime = DateTime.now().plusDays( 3 );
        List<CalendarData.CalendarEntry> entries = readCalendarEntries( startTime, endTime );

        return SensorInvocations.fromResult( new CalendarData(
                DateTime.now().getMillis(),
                entries
        ));
    }

    private List<CalendarData.CalendarEntry> readCalendarEntries( DateTime startTime, DateTime endTime ) {
        ContentResolver contentResolver = context.getContentResolver();
        List<CalendarData.CalendarEntry> entries = new ArrayList<>();
        Uri.Builder builder = Uri.parse( "content://com.android.calendar/instances/when" ).buildUpon();
        ContentUris.appendId( builder, startTime.getMillis() );
        ContentUris.appendId( builder, endTime.getMillis() );

        final String[] INSTANCE_PROJECTION = new String[] {
                CalendarContract.Instances.TITLE,
                CalendarContract.Instances.BEGIN,
                CalendarContract.Instances.END,
                CalendarContract.Instances.EVENT_LOCATION
        };

        try( Cursor eventCursor = contentResolver.query(
                builder.build(),
                INSTANCE_PROJECTION,
                null,
                null,
                "startDay ASC, startMinute ASC"
        ) )
        {
            while (eventCursor.moveToNext()) {
                entries.add( new CalendarData.CalendarEntry(
                        eventCursor.getString( 0 ),                 // title
                        new DateTime( eventCursor.getLong( 1 ) ),   // start time
                        new DateTime( eventCursor.getLong( 2 ) ),   // end time
                        eventCursor.getString( 3 )                  // location
                ) );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return entries;
    }
}
