package de.tu_darmstadt.informatik.kom.contextsense;

/**
 * This indicates, that the base class is calibrateable.
 *
 * Created by Toby on 30.08.2017
 */

public interface CalibrateableDataSource {

    /**
     * Starts a calibration task.
     */
    void calibrate();

}
