package de.tu_darmstadt.informatik.kom.contextsense.sensing.lowpower;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * Utility class for building {@link LowPowerModeDetector}.
 * Will automatically choose the "best" implementation for the current device (based on the avaialble sensors).
 */
public class LowPowerModeDetectors {

    public static LowPowerModeDetector buildForThisDevice( Context context ){
        if (deviceHasSignificantMotionSensor(context))
            return new SignificantMotionLowPowerModeDetector( context );
        else {
            Log.i( "Low Power Mode", "Device has no significant motion detector. Using fallback with usual Accelerometer." );
            return new AccelerationBasedLowPowerModeDetector( context );
        }
    }

    private static boolean deviceHasSignificantMotionSensor( Context context ) {
        SensorManager sensorManager = (SensorManager) context.getSystemService( Context.SENSOR_SERVICE );
        return sensorManager.getDefaultSensor( Sensor.TYPE_SIGNIFICANT_MOTION ) != null;
    }

}
