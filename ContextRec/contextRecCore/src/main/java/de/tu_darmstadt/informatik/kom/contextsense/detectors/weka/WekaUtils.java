package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import java8.util.function.Consumer;
import java8.util.stream.Stream;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Created by Oli on 11.12.2015.
 */
public class WekaUtils {

    public static Instances buildWekaInstanceSet( WekaFeatureSet featureSet, Stream<TrainingSample> samples, Consumer<Integer> progress ) throws Exception {
        Instances instances = featureSet.buildEmptyDataSet();
        fillDataset( instances, featureSet, samples, progress );

        return instances;
    }

    private static void fillDataset( Instances set, WekaFeatureSet featureSet, Stream<TrainingSample> samples, Consumer<Integer> progress ) {
        Attribute classAttrib = set.classAttribute();
        progress.accept( 0 );

        int[] count = {0};
        samples.forEach( sample -> {
            Instance instance = featureSet.toInstance( sample.getData(), sample.getOtherSituations() );
            instance.setValue( classAttrib, sample.getCorrectSituation() );
            set.add( instance );

            count[0]++;
            progress.accept( count[0] );
        });

    }
}
