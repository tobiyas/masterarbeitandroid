package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.telephony.TelephonyManager;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PhoneStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;

/**
 * Checks, if the user is currently in a phone call.
 */
public class PhoneStateSensor extends Sensor {

    private final TelephonyManager telephonyManager;

    public PhoneStateSensor( Context context ) {
        super( context );
        this.telephonyManager = (TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE );
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        boolean isInCall = telephonyManager.getCallState() != TelephonyManager.CALL_STATE_IDLE;

        return SensorInvocations.fromResult( new PhoneStateData(
                DateTime.now().getMillis(),
                isInCall
        ) );
    }
}
