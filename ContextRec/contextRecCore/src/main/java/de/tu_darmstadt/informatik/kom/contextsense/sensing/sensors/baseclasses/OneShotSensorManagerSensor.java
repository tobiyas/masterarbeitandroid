package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

import android.content.Context;

import org.joda.time.Duration;

import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 16.11.2015.
 */
public abstract class OneShotSensorManagerSensor<T extends SensorData> extends SensorManagerSensor {

    public OneShotSensorManagerSensor( Context context ) {
        super( context );
    }

    @Override
    protected boolean needsMoreData( List<SensorEventCopy> data, Duration batchDuration ) {
        return false;
    }

    @Override
    protected final T convertToResult( List<SensorEventCopy> events, Duration batchDuration ) {
        return convertToResult( events.get( 0 ) );
    }

    @Override
    protected Duration resolveSamplingPeriod() {
        return Duration.ZERO;
    }

    protected abstract T convertToResult( SensorEventCopy event );
}
