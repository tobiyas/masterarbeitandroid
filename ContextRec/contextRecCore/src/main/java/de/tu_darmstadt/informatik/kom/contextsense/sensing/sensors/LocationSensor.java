package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.joda.time.Duration;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LocationSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import java8.util.Optional;

/**
 * Sensor for tracking the user's location using google play location API.
 */
public class LocationSensor extends Sensor implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private static final String TAG = "Location Sensor";
    private static final Duration UPDATE_INTERVAL_NORMAL = Duration.standardMinutes( 3 );
    private static final Duration UPDATE_INTERVAL_POWER_SAVE = Duration.standardMinutes( 15 );

    private final GoogleApiClient client;
    private final Context context;
    private final Executor executor;
    private Optional<LocationSensorData> currentLocation;
    private Optional<LocationSensorData.AddressData> currentAddress;

    public LocationSensor(Context context ) {
        super( context );
        this.context = context;
        this.currentLocation = Optional.empty();
        this.executor = Executors.newSingleThreadExecutor();
        this.client = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public Set<String> getRequiredPermissions() {
        HashSet<String> permissions = new HashSet<>();
        permissions.add( Manifest.permission.ACCESS_FINE_LOCATION );
        return permissions;
    }

    @Override
    public void start( SensorMode mode ) {
        if (ContextCompat.checkSelfPermission( context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i("LocationSensor", "Missing permission 'ACCESS_FINE_LOCATION'. Ignoring this sensor." );
        } else {
            client.connect();
        }
    }

    @Override
    public void stop() {
        if (ContextCompat.checkSelfPermission( context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            client.disconnect();
        }
    }

    @SuppressWarnings("ResourceType")
    @Override
    public void onConnected( Bundle bundle ) {
        currentLocation = Optional.ofNullable( LocationServices.FusedLocationApi.getLastLocation( client ) )
                .map( this::toSensorData );

        tryRetrieveGeoLocation();

        LocationRequest request = new LocationRequest()
                .setFastestInterval( Duration.standardMinutes( 1 ).getMillis() )
                .setInterval( intervalForCurrentMode().getMillis() )
                .setPriority( LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY );

        LocationServices.FusedLocationApi.requestLocationUpdates(
                client,
                request,
                this,
                Looper.getMainLooper()
        );
    }

    private Duration intervalForCurrentMode() {
        if (isInLowPowerMode())
            return UPDATE_INTERVAL_POWER_SAVE;
        else
            return UPDATE_INTERVAL_NORMAL;
    }

    @Override
    public void onConnectionSuspended( int i ) {
        LocationServices.FusedLocationApi.removeLocationUpdates( client, this );
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        return SensorInvocations.fromResult( currentLocation );
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = Optional.of( toSensorData( location ) );
        tryRetrieveGeoLocation();
    }

    private void tryRetrieveGeoLocation() {
        currentLocation.ifPresent(
                loc -> {
                    executor.execute( () -> {
                        try {
                            Geocoder coder = new Geocoder( context );
                            List<Address> addressList = coder.getFromLocation( loc.getLatitude(), loc.getLongitude(), 1 );
                            if (addressList == null || addressList.isEmpty())
                                currentAddress = Optional.empty();
                            else {
                                currentAddress = Optional.of( toAddressData( addressList.get( 0 ) ) );
                            }
                        } catch (IOException e) {
                            currentAddress = Optional.empty();
                        }
                    });
                }
        );
    }

    private LocationSensorData.AddressData toAddressData( Address address ) {
        return new LocationSensorData.AddressData(
                Optional.ofNullable( address.getCountryCode() ),
                Optional.ofNullable( address.getAdminArea() ),
                Optional.ofNullable( address.getLocality() ),
                Optional.ofNullable( address.getThoroughfare() ),
                Optional.ofNullable( address.getSubThoroughfare() )
        );
    }

    private LocationSensorData toSensorData( Location location ) {
        return new LocationSensorData(
                currentAddress,
                location.getTime(),
                location.getLatitude(),
                location.getLongitude(),
                location.getAltitude(),
                location.getAccuracy(),
                location.getSpeed(),
                location.getProvider()
        );
    }

    @Override
    public void onConnectionFailed( @NonNull ConnectionResult connectionResult ) {
        Log.d( TAG, "Connection failed" );
    }
}
