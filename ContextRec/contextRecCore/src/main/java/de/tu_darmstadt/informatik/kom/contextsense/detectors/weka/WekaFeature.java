package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import weka.core.Attribute;
import weka.core.Instance;

/**
 * Represents a (Machine Learning) feature used by Weka.
 */
public abstract class WekaFeature {

    public static final String SITUATION_FEATURE_PREFIX = "Label:";

    private final List<Attribute> attributes;
    private final String name;

    protected WekaFeature( Attribute attribute ) {
        this( Collections.singletonList( attribute ), attribute.name() );
    }

    protected WekaFeature( List<Attribute> attributes, String name ) {
        this.attributes = attributes;
        this.name = name;
    }

    public Set<String> getDependentLabels(){
        return new HashSet<>();
    }

    public abstract void apply( Instance instance, SensorDataBatch data, Map<String,String> otherLabels );

    public List<Attribute> getAttributes() {
        return attributes;
    }

    protected Attribute getAttribute() {
        if (attributes.size() > 1)
            throw new IllegalStateException( "Cannot call getAttribute when there is more than one attribute." );

        return attributes.get( 0 );
    }

    public String getName() {
        return name;
    }

    public boolean isSituationFeature() {
        return name.startsWith( SITUATION_FEATURE_PREFIX );
    }

    @Override
    public String toString() {
        return getName();
    }
}
