package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.StreamableSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.TimedDataQueue;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.HeartRateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.contextsense.utils.TimedPair;
import java8.util.Optional;

/**
 * Sensor for tracking the user's pulse (mostly for smartwatches).
 */
public class HeartRateSensor extends Sensor implements SensorEventListener, StreamableSensor {

    private static final Duration TRIGGER_INTERVAL = Duration.standardSeconds( 30 );
    private static final int NUMBER_OF_SAMPLES_UNTIL_GOOD = 5;

    /**
     * The Queue of HR Values.
     */
    private final TimedDataQueue<HeartRateData> dataQueue = new TimedDataQueue<>(60);


    private final SensorManager sensorManager;
    private final Handler restartHandler;
    private Optional<Double> lastResult;
    private HeartRateData.Accuracy accuracy;
    private int numberOfSamplesTaken;


    public HeartRateSensor( Context context ) {
        super( context );
        this.sensorManager = (SensorManager) context.getSystemService( Context.SENSOR_SERVICE );
        this.lastResult = Optional.empty();
        this.restartHandler = new Handler( Looper.getMainLooper() );
        this.accuracy = HeartRateData.Accuracy.LOW;
        this.numberOfSamplesTaken = 0;
    }

    @Override
    public void start( SensorMode mode ) {
        super.start( mode );
        startSensor();
    }

    private void startSensor() {
        android.hardware.Sensor sensor;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT_WATCH) {

            sensor = sensorManager.getDefaultSensor( android.hardware.Sensor.TYPE_HEART_RATE );
            if (sensor != null) {
                sensorManager.registerListener( this, sensor, 0 );
                numberOfSamplesTaken = 0;
            }
        }
    }

    @Override
    public void startStreaming() {
        super.start( SensorMode.NORMAL );
        startSensor();
    }

    @Override
    public void stopStreaming() {
        stop();
    }

    @Override
    public Optional<? extends SensorData> getStreamingResults(DateTime from, DateTime to) {
        List<HeartRateData> data = dataQueue.getFromToCollected(from,to);
        if(data.isEmpty()) return Optional.empty();
        return Optional.of(data.get(0));
    }

    @Override
    public void stop() {
        super.stop();
        restartHandler.removeCallbacksAndMessages( null );
        sensorManager.unregisterListener( this );
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        if (lastResult.isPresent()) {
            return SensorInvocations.fromResult( new HeartRateData(
                    DateTime.now().getMillis(),
                    lastResult.get(),
                    accuracy
            ) );
        }else {
            return SensorInvocations.emptyInvocation();
        }
    }

    @Override
    public void onSensorChanged( SensorEvent event ) {
        double heartRate = event.values[0];

        numberOfSamplesTaken++;
        if (numberOfSamplesTaken >= NUMBER_OF_SAMPLES_UNTIL_GOOD) {
            lastResult = Optional.of( heartRate );

            sensorManager.unregisterListener( this );
            restartHandler.removeCallbacksAndMessages( null );
            restartHandler.postDelayed( this::startSensor, TRIGGER_INTERVAL.getMillis() );

            //Add for Streaming Support:
            dataQueue.add(new HeartRateData(System.currentTimeMillis(), heartRate, accuracy));
        }
    }

    @Override
    public void onAccuracyChanged( android.hardware.Sensor sensor, int accuracy ) {
        switch (accuracy) {
            case SensorManager.SENSOR_STATUS_ACCURACY_LOW:
                this.accuracy = HeartRateData.Accuracy.LOW;
                break;
            case SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM:
                this.accuracy = HeartRateData.Accuracy.MEDIUM;
                break;
            case SensorManager.SENSOR_STATUS_ACCURACY_HIGH:
                this.accuracy = HeartRateData.Accuracy.HIGH;
                break;
            default:
                this.accuracy = HeartRateData.Accuracy.BAD;
        }
    }
}
