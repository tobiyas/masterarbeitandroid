package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

/**
 * Created by Toby on 31.05.2017.
 */

public enum OperationMode {

    Streaming,
    Batching

}
