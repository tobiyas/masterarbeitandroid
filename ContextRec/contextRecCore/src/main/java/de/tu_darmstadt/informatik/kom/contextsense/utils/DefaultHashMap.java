package de.tu_darmstadt.informatik.kom.contextsense.utils;

import java.util.HashMap;

/**
 * Created by Toby on 20.10.2017
 */

public class DefaultHashMap<K,V> extends HashMap<K,V> {

    protected V defaultValue;

    public DefaultHashMap(V defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public V get(Object k) {
        return containsKey(k) ? super.get(k) : defaultValue;
    }
}