package de.tu_darmstadt.informatik.kom.contextsense;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import java8.util.Optional;

/**
 * Created by Toby on 01.06.2017.
 */

public interface StreamingDataSource {


    /**
     * Starts to stream the Sensors in this source.
     */
    void startStreaming();


    /**
     * stops streaming the Sensors in this source.
     */
    void stopStreaming();


    /**
     * This collects streamed data for the
     * @param from when to get the data.
     * @param to when to get the data.
     *
     * @return the batched data.
     */
    Optional<SensorDataBatch> collectStreamedData(DateTime from, DateTime to);


}
