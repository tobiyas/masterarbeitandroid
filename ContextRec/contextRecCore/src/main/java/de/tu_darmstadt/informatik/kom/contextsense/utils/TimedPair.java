package de.tu_darmstadt.informatik.kom.contextsense.utils;


import android.support.annotation.NonNull;

/**
 * A simple Timed Pair with Time-Value -> Other Value.
 *
 * Created by Toby on 28.08.2017
 */

public class TimedPair<T> extends Pair<Long,T> implements Comparable<TimedPair> {


    public TimedPair(long time, T value){
        super(time, value);
    }

    @Override
    public Long getFirst() {
        return super.getFirst();
    }

    @Override
    public int compareTo(@NonNull TimedPair timedPair) {
        return Long.compare(getFirst(), timedPair.getFirst());
    }
}
