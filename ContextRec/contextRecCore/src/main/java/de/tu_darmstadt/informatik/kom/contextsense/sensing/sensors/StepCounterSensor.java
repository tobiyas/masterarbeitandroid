package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.StepCounterData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorEventCopy;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorManagerSensor;
import java8.util.Optional;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Sensor for detecting how may steps the user took during one invocation.
 */
public class StepCounterSensor extends SensorManagerSensor {

    public StepCounterSensor( Context context ) {
        super( context );
    }

    @Override
    protected Optional<Sensor> resolveSensor( SensorManager sensorManager ) {
        return Optional.ofNullable( sensorManager.getDefaultSensor( Sensor.TYPE_STEP_COUNTER ) );
    }

    @Override
    protected Duration resolveSamplingPeriod() {
        return Duration.ZERO;
    }

    @Override
    protected StepCounterData convertToResult( List<SensorEventCopy> events, Duration batchDuration ) {
        if (events.size() == 0)
            return new StepCounterData( DateTime.now().getMillis(), 0, 0 );

        // step counter tends to report old events. We're only interested in the new ones.
        SensorEventCopy last = events.get( events.size() -1 );
        long oldestTimestamp = last.timestamp - millisToNanos( batchDuration.getMillis() );

        List<SensorEventCopy> realEvents = stream( events )
                .filter( x -> x.timestamp >= oldestTimestamp )
                .collect( Collectors.toList() );

        double stepsPerSec = 0;
        long totalSteps = 0;
        if (!realEvents.isEmpty()) {
            SensorEventCopy first = realEvents.get( 0 );

            double steps = last.values[0] - first.values[0];
            double batchDurationSec = ( batchDuration.getMillis() / 1000.0 );
            stepsPerSec = steps / batchDurationSec;

            totalSteps = (long)last.values[0];
        }

        return new StepCounterData( DateTime.now().getMillis(), stepsPerSec, totalSteps );
    }

    private long millisToNanos( long millis ) {
        return 1_000_000 * millis;
    }
}
