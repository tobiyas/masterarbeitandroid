package de.tu_darmstadt.informatik.kom.contextsense.detectors.base;

import java.util.HashMap;
import java.util.Map;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;

/**
 * Represents a sample used for training a detector.
 * It contains sensor data, the expected class label and a map of other results from other detectors.
 */
public class TrainingSample {

    private final SensorDataBatch data;
    private final SituationGroup situation;
    private final String correctSituation;
    private final Map<String, String> otherSituations;

    public static TrainingSample fromStoredSample( StoredSample sample, SituationGroup situationToTrain ) {
        String correctSituation = sample.getAnnotations().get( situationToTrain.getName() );

        // Pull the other situations out of the user annotations (instead of the original detection results).
        // Should give "safer" results.
        // We might think about using a fallback to the original detection results, if there are no annotations.
        Map<String, String> otherSituations = new HashMap<>( sample.getAnnotations() );
        otherSituations.remove( situationToTrain.getName() );

        return new TrainingSample( sample.getData(), situationToTrain, correctSituation, otherSituations );
    }

    public TrainingSample( SensorDataBatch data, SituationGroup situation, String correctSituation, Map<String,String> otherSituations ) {
        this.data = data;
        this.situation = situation;
        this.correctSituation = correctSituation;
        this.otherSituations = otherSituations;
    }

    public SensorDataBatch getData() {
        return data;
    }

    public SituationGroup getSituation() {
        return situation;
    }

    public String getCorrectSituation() {
        return correctSituation;
    }

    public Map<String, String> getOtherSituations() {
        return otherSituations;
    }
}
