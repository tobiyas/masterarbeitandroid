package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.NearbyPlaceData;
import java8.util.Optional;


/**
 * Sensor for detecting nearby "Places" (Restaurants, University, School, Hotel, ....) using Google Places API.
 *
 * If you want to use this sensor, make sure you have your Google-API-Key in the Manifest.
 */
@SuppressWarnings("MissingPermission")
public class NearbyPlacesSensor extends Sensor implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "Location Sensor";

    private final GoogleApiClient client;
    private final ExecutorService executor;
    private Optional<NearbyPlaceData> lastLowPowerResult;

    public NearbyPlacesSensor( Context context ) {
        super( context );
        this.executor = Executors.newSingleThreadExecutor();
        this.lastLowPowerResult = Optional.empty();
        this.client = new GoogleApiClient.Builder( context )
                .addApi( Places.PLACE_DETECTION_API )
                .addConnectionCallbacks( this )
                .addOnConnectionFailedListener( this )
                .build();
    }

    @Override
    public Set<String> getRequiredPermissions() {
        HashSet<String> permissions = new HashSet<>();
        permissions.add( Manifest.permission.ACCESS_FINE_LOCATION );
        return permissions;
    }

    @Override
    public void start( SensorMode mode ) {
        lastLowPowerResult = Optional.empty();
        client.connect();
    }

    @Override
    public void stop() {
        client.disconnect();
    }

    @SuppressWarnings("ResourceType")
    @Override
    public void onConnected( Bundle bundle ) {
    }

    @Override
    public void onConnectionSuspended( int i ) {
    }

    @Override
    public void onConnectionFailed( @NonNull ConnectionResult connectionResult ) {
        Log.d( TAG, "Connection failed" );
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        if (isInLowPowerMode() && lastLowPowerResult.isPresent())
            return SensorInvocations.fromResult( lastLowPowerResult );

        return SensorInvocations.fromAsyncTask( executor, () -> {
            PlaceLikelihoodBuffer result = Places.PlaceDetectionApi
                    .getCurrentPlace(client, null)
                    .await( batchDuration.getStandardSeconds(), TimeUnit.SECONDS );

            if (!result.getStatus().isSuccess()) {
                Log.e( "Nearby Places Sensor", "Lookup of nearby places failed: " + result.getStatus().getStatusMessage() );
                return Optional.empty();
            }


            Map<NearbyPlaceData.NearbyPlace, Double> places = new HashMap<>();
            for( PlaceLikelihood place: result ) {
                NearbyPlaceData.NearbyPlace nearbyPlace = toPlace( place );
                double prob = place.getLikelihood();
                places.put( nearbyPlace, prob );
            }

            // Google wants us to call this
            result.release();

            lastLowPowerResult = Optional.of( new NearbyPlaceData(
                    DateTime.now().getMillis(),
                    places
            ) );
            return lastLowPowerResult;
        });
    }

    private static NearbyPlaceData.NearbyPlace toPlace( PlaceLikelihood place ) {
        return new NearbyPlaceData.NearbyPlace(
                place.getPlace().getName().toString(),
                place.getPlace().getPlaceTypes(),
                place.getPlace().getLatLng().latitude,
                place.getPlace().getLatLng().longitude
        );
    }
}