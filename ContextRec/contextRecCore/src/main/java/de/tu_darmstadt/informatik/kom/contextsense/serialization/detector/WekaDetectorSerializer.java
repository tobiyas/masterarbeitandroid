package de.tu_darmstadt.informatik.kom.contextsense.serialization.detector;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors.WekaDetectorMetaData;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureBuilder;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureSet;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.utils.JsonUtils;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.utils.SerialVersionUidIgnoringObjectInputStream;
import weka.classifiers.Classifier;
import weka.core.SerializationHelper;

/**
 * Utility class for de/serializing an existing WekaDetector.
 * Allows it to store the state of the detector and restore it later (e.g. on a different device).
 */
public class WekaDetectorSerializer {

    /**
     * This is a pure static class. You cannot instantiate it.
     */
    private WekaDetectorSerializer() {
    }

    private static ObjectMapper buildSerializationMapper() {
        ObjectMapper mapper = JsonUtils.buildDefaultMapper();
        mapper.disable( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES );
        mapper.disable( SerializationFeature.FAIL_ON_EMPTY_BEANS );
        mapper.setVisibility( PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE );
        mapper.setVisibility( PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY );
        mapper.enableDefaultTyping();
        return mapper;
    }

    public static void serializeModelAndMetaData( WekaDetector detector, OutputStream modelOut, OutputStream descriptionOut ) throws Exception {
        serializeModel( detector.getModel(), modelOut );
        serializeMetaData( detector.getMetaData(), descriptionOut );
    }

    public static void serializeModel( Classifier classifier, OutputStream modelOut ) throws Exception {
        SerializationHelper.write( modelOut, classifier );
    }

    public static void serializeMetaData( WekaDetectorMetaData data, OutputStream descriptionOut ) throws Exception {
        ObjectMapper mapper = buildSerializationMapper();
        mapper.writeValue( descriptionOut, data );
    }

    public static WekaDetector deserialize( InputStream modelIn, InputStream descriptionIn, WekaFeatureBuilder featureBuilder ) throws Exception {
        ObjectMapper mapper = buildSerializationMapper();

        WekaDetectorMetaData description = mapper.readValue( descriptionIn, WekaDetectorMetaData.class );

        Classifier model = null;
        try(ObjectInputStream in = new SerialVersionUidIgnoringObjectInputStream( modelIn ) )
        {
            model = (Classifier)in.readObject();
        }

        WekaFeatureSet set = featureBuilder.buildFeatureSet( description.getFeatureNames(), description.getDescription().getDetectableGroup() );
        return new WekaDetector( description.getDescription(), set, model, description.getPrecondition() );
    }

    public static WekaDetector deserialize( InputStream modelIn, InputStream descriptionIn ) throws Exception {
        return deserialize( modelIn, descriptionIn, new WekaFeatureBuilder( true ) );
    }

}
