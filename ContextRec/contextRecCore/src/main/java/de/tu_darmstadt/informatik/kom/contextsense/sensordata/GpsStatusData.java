package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.Optional;

/**
 * Created by Oli on 26.12.2015.
 */
public class GpsStatusData extends SensorData {

    private static final long serialVersionUID = 6608567162258019586L;

    private boolean gpsEnabled;
    private Optional<Integer> timeToFirstFixInMs;
    private Optional<Integer> numberOfSatellites;
    private Optional<Double> averageSNR;

    // serialization
    private GpsStatusData() {
    }

    public GpsStatusData(long timestamp, boolean gpsEnabled, Optional<Integer> timeToFirstFixInMs, Optional<Integer> numberOfSatellites, Optional<Double> averageSNR ) {
        super( timestamp );
        this.gpsEnabled = gpsEnabled;
        this.timeToFirstFixInMs = timeToFirstFixInMs;
        this.numberOfSatellites = numberOfSatellites;
        this.averageSNR = averageSNR;
    }


    @Override
    public SensorData copy() {
        return new GpsStatusData(getTimestamp(), gpsEnabled, timeToFirstFixInMs, numberOfSatellites, averageSNR).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public boolean isGpsEnabled() {
        return gpsEnabled;
    }

    public Optional<Integer> getTimeToFirstFixInMs() {
        return timeToFirstFixInMs;
    }

    public Optional<Integer> getNumberOfSatellites() {
        return numberOfSatellites;
    }

    public Optional<Double> getAverageSNR() {
        return averageSNR;
    }
}
