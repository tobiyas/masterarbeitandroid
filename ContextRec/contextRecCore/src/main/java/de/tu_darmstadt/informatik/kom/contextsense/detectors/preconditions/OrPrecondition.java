package de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions;

import java.util.ArrayList;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.PreconditionVisitor;

/**
 * Created by Oli on 21.03.2016.
 */
public class OrPrecondition extends Precondition {

    private List<Precondition> conditions;

    // for serialization only
    private OrPrecondition() {
    }

    public OrPrecondition( List<Precondition> conditions ) {
        this();
        this.conditions = new ArrayList<>( conditions );
    }

    public List<Precondition> getConditions() {
        return new ArrayList<>( conditions );
    }

    @Override
    public <T> T accept( PreconditionVisitor<T> visitor ) {
        return visitor.visit( this );
    }
}
