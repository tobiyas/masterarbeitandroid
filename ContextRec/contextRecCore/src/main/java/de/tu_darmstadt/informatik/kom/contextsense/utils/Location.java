package de.tu_darmstadt.informatik.kom.contextsense.utils;

import java.io.Serializable;

/**
 * Represents a location with latitude, longitude and radius (of precision in meter)
 */
public class Location implements Serializable {
    private static final long serialVersionUID = 6708426779057717433L;

    public final double lat;
    public final double lon;
    public final double radius;

    public Location( double lat, double lon, double radius ) {
        this.lat = lat;
        this.lon = lon;
        this.radius = radius;
    }

    public double distanceTo( Location other ) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians( other.lat - this.lat );
        double dLng = Math.toRadians( other.lon - this.lon );
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(this.lat)) * Math.cos(Math.toRadians(other.lat)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        return earthRadius * c;
    }
}
