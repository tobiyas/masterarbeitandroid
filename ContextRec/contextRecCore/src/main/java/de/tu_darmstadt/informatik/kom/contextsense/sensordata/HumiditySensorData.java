package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015
 */
public class HumiditySensorData extends SensorData {

    private static final long serialVersionUID = 3444486949755072212L;

    private double humidityInPercent;

    public HumiditySensorData( long timestamp, double humidityInPercent ) {
        super( timestamp);
        this.humidityInPercent = humidityInPercent;
    }

    // serialization
    private HumiditySensorData() {
    }

    @Override
    public SensorData copy() {
        return new HumiditySensorData(getTimestamp(), humidityInPercent).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public double getHumidityInPercent() {
        return humidityInPercent;
    }
}
