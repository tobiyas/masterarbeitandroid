package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.HumiditySensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.OneShotSensorManagerSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorEventCopy;
import java8.util.Optional;

/**
 * Sensor that measures the relative air humidity of the environment.
 */
public class HumiditySensor extends OneShotSensorManagerSensor<HumiditySensorData> {

    public HumiditySensor( Context context ) {
        super(context);
    }

    @Override
    protected HumiditySensorData convertToResult( SensorEventCopy event ) {
        return new HumiditySensorData(
                DateTime.now().getMillis(),
                event.values[0]
        );
    }

    @Override
    protected Optional<Sensor> resolveSensor(SensorManager sensorManager) {
        return Optional.ofNullable( sensorManager.getDefaultSensor( Sensor.TYPE_RELATIVE_HUMIDITY ) );
    }
}
