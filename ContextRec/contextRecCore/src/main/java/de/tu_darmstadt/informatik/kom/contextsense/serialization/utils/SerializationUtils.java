package de.tu_darmstadt.informatik.kom.contextsense.serialization.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import de.tu_darmstadt.informatik.kom.contextsense.utils.IOUtils;

/**
 * Created by Oli on 24.03.2016.
 */
public class SerializationUtils {

    public static byte[] zip( byte[] data ) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            try(GZIPOutputStream zip = new GZIPOutputStream( out )) {
                zip.write( data );
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException( e );
        }

        return out.toByteArray();
    }

    public static byte[] unzip( byte[] data ) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            try(GZIPInputStream zip = new GZIPInputStream( new ByteArrayInputStream( data ) )) {
                IOUtils.copy( zip, out );
                out.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException( e );
        }

        return out.toByteArray();
    }
}
