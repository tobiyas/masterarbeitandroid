package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.Optional;

/**
 * Created by Oli on 13.12.2015.
 */
public class LocationSensorData extends SensorData {

    private static final long serialVersionUID = 5585714537383546100L;

    private Optional<AddressData> address;
    private double latitude;
    private double longitude;
    private double altitude;
    private double precision;
    private double speed;
    private String provider;

    public LocationSensorData( Optional<AddressData> address, long timestamp, double latitude, double longitude, double altitude, double precision, double speed, String provider ) {
        super( timestamp );
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.precision = precision;
        this.speed = speed;
        this.provider = provider;
    }

    private LocationSensorData() {
    }

    @Override
    public SensorData copy() {
        return new LocationSensorData(address, getTimestamp(), latitude, longitude, altitude, precision, speed, provider).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getPrecision() {
        return precision;
    }

    public double getSpeed() {
        return speed;
    }

    public String getProvider() {
        return provider;
    }

    public double getAltitude() {
        return altitude;
    }

    public Optional<AddressData> getAddress() {
        return address;
    }

    public static class AddressData {

        private Optional<String> country;
        private Optional<String> admin;
        private Optional<String> city;
        private Optional<String> street;
        private Optional<String> streetNumber;

        public AddressData( Optional<String> country, Optional<String> admin, Optional<String> city, Optional<String> street, Optional<String> streetNumber ) {
            this.country = country;
            this.admin = admin;
            this.city = city;
            this.street = street;
            this.streetNumber = streetNumber;
        }

        // serialization
        private AddressData() {
        }

        public Optional<String> getCountry() {
            return country;
        }

        public Optional<String> getAdmin() {
            return admin;
        }

        public void setAdmin( Optional<String> admin ) {
            this.admin = admin;
        }

        public Optional<String> getCity() {
            return city;
        }

        public Optional<String> getStreetNumber() {
            return streetNumber;
        }
    }
}
