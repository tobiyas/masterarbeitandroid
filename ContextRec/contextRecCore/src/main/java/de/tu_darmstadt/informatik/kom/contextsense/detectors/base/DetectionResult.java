package de.tu_darmstadt.informatik.kom.contextsense.detectors.base;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java8.util.Optional;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;


/**
 * Represents the result of the detection phase.
 * Contains the results of all detectors.
 */
public class DetectionResult implements Serializable {

    private static final long serialVersionUID = 7728036825180005573L;

    private List<DetectorResult> results;
    private DateTime triggerTime;

    public DetectionResult( DateTime triggerTime ) {
        this.results = new ArrayList<>();
        this.triggerTime = triggerTime;
    }

    public void addResult( DetectorResult result ) {
        results.add( result );
    }

    public Optional<DetectorResult> getAnyResultForGroup( SituationGroup group ) {
        return getAnyResultForGroup( group.getName() );
    }

    public Optional<DetectorResult> getAnyResultForGroup( String groupId ) {
        return stream( results )
                .filter( result -> result.getGroup().getName().equals( groupId ) )
                .findAny();
    }

    public Map<String, Double> flattenResults() {
        return stream( results )
                .flatMap( group -> stream( group.getAllProbabilities().entrySet() ) )
                .collect( Collectors.toMap( Map.Entry::getKey, Map.Entry::getValue ) );
    }

    public boolean hasResultForGroup( SituationGroup group ) {
        return hasResultForGroup( group.getName() );
    }

    public boolean hasResultForGroup( String groupId ) {
        return getAnyResultForGroup( groupId ).isPresent();
    }

    public Map<String, String> flattenWithBestLabelsPerGroup() {
        return stream( results )
                .filter( DetectorResult::isGood )
                .collect( Collectors.toMap(
                        entry -> entry.getGroup().getName(),
                        entry -> entry.getLabelWithHighestProbability().get()
                ) );
    }

    public DateTime getTriggerTime() {
        return triggerTime;
    }

    @Override
    public String toString() {
        return stream( results )
                .map( v -> v.getGroup() + " " + v.getLabelWithHighestProbability().orElse( "None" ) )
                .reduce( (s1, s2) -> s1 + "\n" + s2 )
                .orElse( "Empty" );
    }
}
