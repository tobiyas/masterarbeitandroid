package de.tu_darmstadt.informatik.kom.contextsense;

import org.joda.time.Duration;

import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import de.tu_darmstadt.informatik.kom.contextsense.utils.CancelableFuture;
import java8.util.Optional;

/**
 * Created by Oli on 05.03.2016
 */
public abstract class DataSource {

    /**
     * If the Source is currently active.
     */
    private boolean isActive;

    /**
     * The Type of the device.
     */
    private final DeviceType type;

    /**
     * The user friendly name.
     */
    private String name;

    /**
     * The name of the device.
     */
    private String deviceName;


    protected DataSource(DeviceType type, String name) {
        this.isActive = false;

        this.type = type;
        this.deviceName = name;
        this.name = name;
    }

    public abstract Set<String> getRequiredPermissions();

    public final void start() {
        if (isActive)
            throw new IllegalStateException( "Tried to start DataSource twice" );

        isActive = true;
        doStart();
    }

    protected abstract void doStart();

    public final void stop() {
        if (!isActive)
            throw new IllegalStateException( "Tried to stop DataSource even tough it was not active" );

        isActive = false;
        doStop();
    }

    protected abstract void doStop();

    public final CancelableFuture<Optional<SensorDataBatch>> collectData( Duration batchDuration )
    {
        if (!isActive)
            throw new IllegalStateException( "Tried to collect data while not running" );

        return doCollectData( batchDuration );
    }

    protected abstract CancelableFuture<Optional<SensorDataBatch>> doCollectData( Duration batchDuration );

    public boolean isRunning() {
        return isActive;
    }

    /**
     * Returns the Name of this Source.
     * @return the name.
     */
    public String getName(){
        return name;
    }

    /**
     * Gets the name of the device.
     * @return the name of the device.
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * Gets the Type of the device.
     * @return the type.
     */
    public DeviceType getType() {
        return type;
    }

    /**
     * Changes the name of the Source.
     * @param name to set.
     */
    public void changeName(String name) {
        this.name = name;
    }
}
