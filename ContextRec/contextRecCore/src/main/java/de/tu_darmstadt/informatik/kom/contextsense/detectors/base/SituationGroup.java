package de.tu_darmstadt.informatik.kom.contextsense.detectors.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 11.03.2016
 */
public class SituationGroup implements Cloneable {

    private String name;
    private List<String> situations;

    // serialization constructor
    private SituationGroup() {
    }

    public SituationGroup( String name, String... situations ) {
        this( name, Arrays.asList( situations ) );
    }

    public SituationGroup( String name, Collection<? extends  Object> situations ) {
        this();

        //Be sure we do not have empty:
        if( situations == null || situations.isEmpty() ) throw new IllegalArgumentException( "Situations may not be empty!" );

        this.name = name;
        this.situations = stream( situations ).map( Object::toString ).sorted().collect( Collectors.toList() );
    }

    public String getName() {
        return name;
    }

    public List<String> getSituations() {
        return new ArrayList<>( situations );
    }

    public Set<String> getUnorderedSituations() {
        return new HashSet<>( situations );
    }

    @Override
    public boolean equals( Object o ) {
        if (this == o) return true;
        if (!(o instanceof SituationGroup)) return false;

        SituationGroup that = (SituationGroup) o;

        if (name != null ? !name.equals( that.name ) : that.name != null) return false;
        return situations != null ? situations.equals( that.situations ) : that.situations == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (situations != null ? situations.hashCode() : 0);
        return result;
    }

    @Override
    public SituationGroup clone(){
        return new SituationGroup( name, situations.toArray( new String[situations.size()]) );
    }
}
