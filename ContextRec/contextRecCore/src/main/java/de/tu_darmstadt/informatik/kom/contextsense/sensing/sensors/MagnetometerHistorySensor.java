package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.SensorMode;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MagnetometerHistoryData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorEventCopy;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;

import static java8.util.stream.StreamSupport.stream;

/**
 * Sensor for tracking the magnetometer at a low frequency over a long time (low power sensor).
 */
public class MagnetometerHistorySensor extends Sensor implements SensorEventListener {

    private static final Duration SAMPLING_PERIOD = Duration.standardSeconds( 1 );
    private static final Duration MAX_HISTORY_LENGTH = Duration.standardSeconds( 100 );

    private final SensorManager sensorManager;
    private final Deque<SensorEventCopy> events;

    public MagnetometerHistorySensor( Context context ) {
        super( context );
        this.sensorManager = (SensorManager)context.getSystemService( Context.SENSOR_SERVICE );
        events = new LinkedList<>();
    }

    boolean devicesSupportsHardwareBatching(){
        android.hardware.Sensor sensor = sensorManager.getDefaultSensor( android.hardware.Sensor.TYPE_MAGNETIC_FIELD );
        return sensor != null && sensor.getFifoMaxEventCount() > 0;
    }

    @Override
    public void start( SensorMode mode ) {
        super.start( mode );
        events.clear();

        if (devicesSupportsHardwareBatching()) {
            android.hardware.Sensor sensor = sensorManager.getDefaultSensor( android.hardware.Sensor.TYPE_MAGNETIC_FIELD );
            int updateRateUs = (int) (SAMPLING_PERIOD.getMillis() * 1000);
            sensorManager.registerListener( this, sensor, updateRateUs );
        } else {
            Log.i( "Magnetometer History", "Device does not support hardware event batching. Disabling this sensor." );
        }
    }

    @Override
    public void stop() {
        super.stop();
        sensorManager.unregisterListener( this );
    }

    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        if (!devicesSupportsHardwareBatching())
            return SensorInvocations.emptyInvocation();

        return SensorInvocations.fromResult( bufferToHistory() );
    }

    private MagnetometerHistoryData bufferToHistory() {
        // make a copy to avoid ConcurrentModification
        List<SensorEventCopy> events = new ArrayList<>( this.events );

        long[] t = stream( events )
                .mapToLong( event -> event.timestamp )
                .toArray();
        double[] x = stream( events )
                .mapToDouble( event -> event.values[0] )
                .toArray();
        double[] y = stream( events )
                .mapToDouble( event -> event.values[1] )
                .toArray();
        double[] z = stream( events )
                .mapToDouble( event -> event.values[2] )
                .toArray();

        return new MagnetometerHistoryData( DateTime.now().getMillis(), t, x, y, z );
    }

    @Override
    public void onSensorChanged( SensorEvent event ) {
        enqueueNewEvent( new SensorEventCopy( event ) );
        deleteOldEvents();
    }

    private void enqueueNewEvent( SensorEventCopy event ) {
        if (events.isEmpty()) {
            events.add( event );
        } else {
            SensorEventCopy last = events.peekLast();
            long millisSinceLastEvent = (event.timestamp - last.timestamp) / 1_000_000;
            long lowestToAccept = (long) (SAMPLING_PERIOD.getMillis() * 0.9);

            if (millisSinceLastEvent >= lowestToAccept) {
                events.add( event );
            }
        }
    }

    private void deleteOldEvents() {
        while (oldestEventIsTooOld( events ))
            events.poll();
    }

    private boolean oldestEventIsTooOld( Deque<SensorEventCopy> events ) {
        if (events.isEmpty())
            return false;

        SensorEventCopy oldest = events.peekFirst();
        SensorEventCopy newest = events.peekLast();

        long spanInMs = (newest.timestamp - oldest.timestamp) / 1_000_000;
        return spanInMs >= MAX_HISTORY_LENGTH.getMillis();
    }

    @Override
    public void onAccuracyChanged( android.hardware.Sensor sensor, int accuracy ) {

    }
}
