package de.tu_darmstadt.informatik.kom.contextsense.detectors.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * The result of one Detector.
 * Can be in one of 3 states:
 *  - no detection due to missing precondition
 *  - no detection due to bad data
 *  - good result
 */
public class DetectorResult implements Serializable {

    private static final long serialVersionUID = -2348239998573268923L;

    private final DetectorIdentifier source;
    private final SituationGroup group;
    private final Map<String,Double> probabilities;
    private final ResultType type;

    /**
     * Build a good result with a clearly dominant clearSituation.
     */
    public static DetectorResult clearSituation( DetectorIdentifier source, SituationGroup group, String value ) {
        Map<String, Double> probabilities = new HashMap<>();

        for( String situation: group.getSituations() ) {
            probabilities.put( situation, 0.0 );
        }

        probabilities.put( value, 1.0 );

        return new DetectorResult(
                source,
                group,
                probabilities,
                ResultType.GOOD_RESULT
        );
    }

    /**
     * Build a good result with the given probability distribution.
     */
    public static DetectorResult probabilityDistribution( DetectorIdentifier source, SituationGroup group, Map<String, Double> probabilities ) {
        return new DetectorResult( source, group, probabilities, ResultType.GOOD_RESULT );
    }

    /**
     * Return this, if a detector could not run becaues of missing preconditions.
     */
    public static DetectorResult missingPrecondition( DetectorIdentifier source, SituationGroup group ) {
        return new DetectorResult(
                source,
                group,
                createEvenDistribution( group ),
                ResultType.NO_RESULT_MISSING_PRECONDITION
        );
    }

    /**
     * Return this if a detector could not run because of bad data (e.g. bad positions or unclear results).
     */
    public static DetectorResult badData( DetectorIdentifier source, SituationGroup group ) {
        return new DetectorResult(
                source,
                group,
                createEvenDistribution( group ),
                ResultType.NO_RESULT_NO_CLEAR_SITUATION
        );
    }

    private static Map<String,Double> createEvenDistribution( SituationGroup group ) {
        double probPerLabel = 1.0 / group.getSituations().size();
        Map<String,Double> result = new HashMap<>();

        for(String label: group.getSituations()) {
            result.put( label, probPerLabel );
        }

        return result;
    }

    private DetectorResult( DetectorIdentifier source, SituationGroup group, Map<String,Double> probabilities, ResultType type ) {
        this.source = source;
        this.group = group;
        this.type = type;
        this.probabilities = new HashMap<>( probabilities );
    }

    public boolean isGood() {
        return type == ResultType.GOOD_RESULT;
    }

    public Set<String> getLabels() {
        return probabilities.keySet();
    }

    public double getProbability( String labelId ) {
        if (!probabilities.containsKey( labelId ))
            throw new IllegalArgumentException( "Given label " + labelId + " is invalid. Available are: " + getLabels() );

        return probabilities.get( labelId );
    }

    public Optional<String> getLabelWithHighestProbability() {
        if (!isGood())
            return Optional.empty();

        return stream( probabilities.entrySet() )
                .max( (l1, l2) -> Double.compare( l1.getValue(), l2.getValue() ) )
                .map( Map.Entry::getKey );
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        stream( probabilities.entrySet() )
                .sorted( (x, y) -> Double.compare( x.getValue(), y.getValue() ) )
                .forEach( x -> sb.append( String.format( "%s (%f)\n", x.getKey(), x.getValue() ) ) );

        if (probabilities.isEmpty())
            sb.append( "< Empty >" );

        return sb.toString();
    }

    public Map<String,Double> getAllProbabilities() {
        return new HashMap<>( probabilities );
    }

    public SituationGroup getGroup() {
        return group;
    }

    public DetectorIdentifier getSource() {
        return source;
    }
}
