package de.tu_darmstadt.informatik.kom.contextsense.utils;

/**
 * Created by Toby on 27.08.2017
 */
public class Try {

    /**
     * Tries a Runnable and ignores errors.
     * @param run to run.
     */
    public static void Try(Runnable run){
        try{ if(run != null) run.run(); }catch (Throwable ignored){}
    }

    /**
     * Tries a Runnable and ignores errors.
     * But it prints them.
     * @param run to run.
     */
    public static void TryLog(Runnable run){
        try{ if(run != null) run.run(); }catch (Throwable ignored){ ignored.printStackTrace(); }
    }
}
