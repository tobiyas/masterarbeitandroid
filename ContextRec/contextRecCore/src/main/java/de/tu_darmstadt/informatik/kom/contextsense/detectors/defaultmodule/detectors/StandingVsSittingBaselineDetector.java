package de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.Detector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultDetectorsModule;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultSituations;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.utils.MathUtils;
import de.tu_darmstadt.informatik.kom.contextsense.utils.TimingsRecords;

import static de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Preconditions.*;

/**
 * Very simple detector which will check if the user is currently standing or sitting. DO NOT RELY ON THIS! IT IS JUST FOR DEMO!
 */
public class StandingVsSittingBaselineDetector extends Detector {

    public static final DetectorIdentifier ID = new DetectorIdentifier(DefaultDetectorsModule.ID, "Standing vs Sitting (simple)");

    public static final DetectorDescription DESCRIPTION = new DetectorDescription.Builder(ID)
            .setDescription("Very simple detector which will check if the user is currently standing or sitting. DO NOT RELY ON THIS! IT IS JUST FOR DEMO!")
            .setSituationGroup(DefaultSituations.StandingVsSitting.SITUATION)
            .build();

    public StandingVsSittingBaselineDetector() {
        super(DESCRIPTION);
    }

    @Override
    public Precondition getPrecondition() {
        return requiresSensorData(AccelerationData.class);
    }

    @Override
    public DetectorResult detect(SensorDataBatch batch, DetectionResult otherResults) {
        AccelerationData data = batch.getData(AccelerationData.class).get();
        double avgX = MathUtils.average(data.getX(), 0);
        double avgY = MathUtils.average(data.getY(), 0);
        double avgZ = MathUtils.average(data.getZ(), 0);

        double length = Math.sqrt(avgX * avgX + avgY * avgY + avgZ * avgZ);
        double normX = avgX / length;

        String situation = Math.abs(normX) > 0.8 ? "Sitting" : "Standing";
        return buildGoodResult(situation);
    }

    @Override
    public DetectorResult detect(SensorDataBatch batch, DetectionResult otherResults, TimingsRecords timings) {
        return detect(batch, otherResults);
    }
}
