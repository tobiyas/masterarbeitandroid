package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import org.joda.time.DateTime;

import java.util.Arrays;

import de.tu_darmstadt.informatik.kom.contextsense.ExtractableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.MergeableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.stream.DoubleStreams;

import static de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayUtils.concat;

/**
 * Created by Oli on 13.12.2015
 */
public class AccelerationData extends ExtractableSensorData {

    private static final long serialVersionUID = -290870246285207793L;

    private long[] t;
    private double[] x;
    private double[] y;
    private double[] z;

    // serialization constructor
    private AccelerationData() {
    }


    public AccelerationData( long timestamp, long[] t, double[] x, double[] y, double[] z ) {
        super( timestamp );

        if (t.length != x.length || t.length != y.length || t.length != z.length)
            throw new IllegalArgumentException( "t, x, y and z have to be of same size." );

        this.t = t;
        this.x = x;
        this.y = y;
        this.z = z;
    }


    public long[] getT() {
        return t;
    }

    public double[] getX() {
        return x;
    }

    public double[] getY() {
        return y;
    }

    public double[] getZ() {
        return z;
    }

    public double calcSamplingFreq() {
        int samples = t.length;
        double durationNs = t[samples -1] - t[0];
        return samples / (durationNs / 1000_000_000.0);
    }

    public double getAvgNormalizedX() {
        if( x.length == 0 ) return 0;

        double avgX = DoubleStreams.of( x ).average().getAsDouble();
        return avgX / getAvgMagnitude();
    }

    public double getAvgNormalizedY() {
        if( x.length == 0 ) return 0;

        double avgY = DoubleStreams.of( y ).average().getAsDouble();
        return avgY / getAvgMagnitude();
    }

    public double getAvgNormalizedZ() {
        if( x.length == 0 ) return 0;

        double avgZ = DoubleStreams.of( z ).average().getAsDouble();
        return avgZ / getAvgMagnitude();
    }

    public double getAvgMagnitude() {
        if( x.length == 0 ) return 0;

        double avgX = DoubleStreams.of( x ).average().getAsDouble();
        double avgY = DoubleStreams.of( y ).average().getAsDouble();
        double avgZ = DoubleStreams.of( z ).average().getAsDouble();

        return Math.sqrt( avgX*avgX + avgY*avgY + avgZ*avgZ );
    }


    public double getMeanSquareError() {
        if( x.length == 0 ) return 0;

        double avgX = DoubleStreams.of( x ).average().getAsDouble();
        double avgY = DoubleStreams.of( y ).average().getAsDouble();
        double avgZ = DoubleStreams.of( z ).average().getAsDouble();

        double errorX = Math.sqrt( DoubleStreams.of( x ).map( x -> Math.pow( x - avgX, 2 ) ).sum() / (double) x.length );
        double errorY = Math.sqrt( DoubleStreams.of( y ).map( y -> Math.pow( y - avgY, 2 ) ).sum() / (double) y.length );
        double errorZ = Math.sqrt( DoubleStreams.of( z ).map( z -> Math.pow( z - avgZ, 2 ) ).sum() / (double) z.length );

        return errorX + errorY + errorZ;
    }

    @Override
    public ExtractableSensorData copy(){
        return new AccelerationData(getTimestamp(),
                Arrays.copyOf(t, t.length),
                Arrays.copyOf(x, x.length),
                Arrays.copyOf(y, y.length),
                Arrays.copyOf(z, z.length)
        ).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }


    @Override
    public ExtractableSensorData extract( DateTime from, DateTime to ) {
        long fromL = from.getMillis();
        long toL = to.getMillis();

        int first = 0;
        int last = t.length;

        for(int i = 0; i < t.length; i++){
            long time = t[i];

            if(first == 0 && time > fromL) first = i;
            if(last == t.length && time > toL) last = i;
        }

        return new AccelerationData(getTimestamp(),
                Arrays.copyOfRange(t, first, last),
                Arrays.copyOfRange(x, first, last),
                Arrays.copyOfRange(y, first, last),
                Arrays.copyOfRange(z, first, last)
        ).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }


    @Override
    protected void mergeIntern(SensorData d) {
        AccelerationData data = (AccelerationData) d;

        boolean own = d.getTimestamp() > getTimestamp();
        t = own ? concat(t,data.t) : concat(data.t, t);
        x = own ? concat(x,data.x) : concat(data.x, x);
        y = own ? concat(y,data.y) : concat(data.y, y);
        z = own ? concat(z,data.z) : concat(data.z, z);
    }

    @Override
    public void limitFromTo( DateTime from, DateTime to ) {
        long fromL = from.getMillis();
        long toL = to.getMillis();

        int first = 0;
        int last = t.length;

        for(int i = 0; i < t.length; i++){
            long time = t[i];

            if(first == 0 && time > fromL) first = i;
            if(last == t.length && time > toL) last = i;
        }

        this.t = Arrays.copyOfRange(t, first, last);
        this.x = Arrays.copyOfRange(x, first, last);
        this.y = Arrays.copyOfRange(y, first, last);
        this.z = Arrays.copyOfRange(z, first, last);
    }

    @Override
    public void applyClockSkew(long clockSkew) {
        super.applyClockSkew(clockSkew);
        applyChangeToArray( t, clockSkew );
    }
}
