package de.tu_darmstadt.informatik.kom.contextsense.utils;

/**
 * Created by Toby on 14.09.2017
 */

public class ArrayInterpolator {


    /**
     * Does a fancy linear interpolate between the values.
     *
     * @param original to use.
     * @param desiredLength to expand to.
     *
     * @return the newly generated Array with the interpolated array.
     */
    public static double[] interpolateLinear(double[] original, int desiredLength){
        if( original == null || original.length == 0 ) return new double[desiredLength];

        double[] result = new double[ desiredLength ];
        for(int i = 0; i < desiredLength; i++){
            result[ i ] = getInterpolated(original, i, desiredLength);
        }

        return result;
    }


    /**
     * Gets the interpolated value between the values of the Array.
     * Interpolation is linear.
     *
     * @param array to interpolate on
     * @param place the place to interpolate
     * @param max the max value for the place to be.
     *
     * @return the interpreted value.
     */
    private static double getInterpolated( double[] array, int place, int max ){
        double pos = ((double) place / (double) (max-1)) * (double)(array.length-1);
        double offset = pos % 1;

        double minVal = array[(int) Math.floor( pos )];
        double maxVal = array[(int) Math.ceil( pos )];

        double addition = ( maxVal - minVal ) * offset;
        return minVal + addition;
    }

}
