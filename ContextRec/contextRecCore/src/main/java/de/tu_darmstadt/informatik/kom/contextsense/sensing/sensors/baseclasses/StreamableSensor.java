package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.Optional;

/**
 * This interface is an extension for the Sensor to be Streamable.
 *
 * Created by Toby on 31.05.2017
 */

public interface StreamableSensor {

    /**
     * Starts the Sensor's streaming.
     */
    void startStreaming();

    /**
     * Stops the Streaming.
     */
    void stopStreaming();


    /**
     * Gets a Streamed invocation for Sensor Data.
     * @param from the time from when to use.
     * @param to the time to when to search
     *
     * @return the Invocation to use further on.
     */
    Optional<? extends SensorData> getStreamingResults(DateTime from, DateTime to);

}
