package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015.
 */
public class ProximityData extends SensorData {

    private static final long serialVersionUID = -1820829789876553092L;

    private boolean isClose;

    public ProximityData( long timestamp, boolean isClose ) {
        super( timestamp );
        this.isClose = isClose;
    }

    @Override
    public SensorData copy() {
        return new ProximityData(getTimestamp(), isClose).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    // serialization
    private ProximityData() {
    }

    public boolean isClose() {
        return isClose;
    }
}
