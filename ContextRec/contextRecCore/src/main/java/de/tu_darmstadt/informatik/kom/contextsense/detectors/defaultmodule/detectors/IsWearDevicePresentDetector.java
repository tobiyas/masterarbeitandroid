package de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.Detector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultDetectorsModule;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.DefaultSituations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import de.tu_darmstadt.informatik.kom.contextsense.utils.TimingsRecords;

/**
 * This detector tries to detect, if the user has a wear device.
 */
public class IsWearDevicePresentDetector extends Detector {

    public static final DetectorIdentifier ID = new DetectorIdentifier( DefaultDetectorsModule.ID, "Is Wear Device Connected" );

    public static final DetectorDescription DESCRIPTION = new DetectorDescription.Builder( ID )
            .setDescription( "This detector tries to detect, if the user has a wear device" )
            .setSituationGroup( DefaultSituations.WearStatus.SITUATION )
            .build();

    public IsWearDevicePresentDetector() {
        super( DESCRIPTION );
    }

    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults ) {
        boolean hasWearData = batch.hasDataFromSourceType( DeviceType.WEAR );
        return buildGoodResult( hasWearData ? DefaultSituations.WearStatus.ONLINE : DefaultSituations.WearStatus.OFFLINE );
    }

    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults, TimingsRecords timingsRecords ) {
        return detect( batch, otherResults );
    }
}
