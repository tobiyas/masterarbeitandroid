package de.tu_darmstadt.informatik.kom.contextsense.serialization.sensordata;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.utils.JsonUtils;

/**
 * Created by Oli on 14.02.2016.
 */
public class JsonSensorDataSerializer extends SensorDataSerializer {

    private final ObjectMapper mapper;

    public JsonSensorDataSerializer() {
        mapper = buildSerializationMapper();
    }

    private ObjectMapper buildSerializationMapper() {
        ObjectMapper mapper = JsonUtils.buildDefaultMapper();
        mapper.setVisibility( PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        mapper.enableDefaultTyping();
        mapper.disable( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES );
        return mapper;
    }

    @Override
    protected byte[] doSerialize( SensorData data ) throws IOException {
        return mapper.writeValueAsBytes( data );
    }

    @Override
    protected SensorData doDeserialize( byte[] buffer, Class<? extends SensorData> dataType ) throws IOException {
        return mapper.readValue( buffer, dataType );
    }


}
