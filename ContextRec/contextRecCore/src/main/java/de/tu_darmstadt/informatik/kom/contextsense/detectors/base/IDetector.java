package de.tu_darmstadt.informatik.kom.contextsense.detectors.base;

import android.util.TimingLogger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.utils.TimingsRecords;
import java8.util.stream.Stream;

/**
 * Represents the interface each detector has to implement.
 */
public interface IDetector {

    /**
     * Returns the {@link DetectorDescription} of this detector.
     */
    DetectorDescription getDescription();

    // todo: Remove this!
    Set<String> getDependentLabelGroups();

    /**
     * The precondition that needs to be fulfilled before this detector can run.
     */
    Precondition getPrecondition();

    /**
     * Run the detection now.
     * This operation may be expensive.
     * You should NOT run this on the UI thread.
     *
     * @param batch the sensor data for the clearSituation to detect
     * @param otherResults results of other detectors which already ran for this batch.
     */
    DetectorResult detect(SensorDataBatch batch, DetectionResult otherResults);

    /**
     * Run the detection now.
     * This operation may be expensive.
     * You should NOT run this on the UI thread.
     *
     * @param batch the sensor data for the clearSituation to detect
     * @param otherResults results of other detectors which already ran for this batch.
     * @param timings to use for logging timings.
     */
    DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults, TimingsRecords timings );

    /**
     * Allow this detector to learn from the given sample.
     */
    void trainIncremental( TrainingSample sample );

    /**
     * Allow this detector to learn from all of the given samples.
     */
    void trainBatch( Stream<TrainingSample> samples );

    /**
     * Load the state of the detector from the given stream.
     * THE STREAM IS CLOSED BY THE CALLER!
     */
    void loadState( InputStream in ) throws IOException;

    /**
     * Save the state of the detector in an arbitrary data-format to the given stream.
     * THE STREAM IS CLOSED BY THE CALLER!
     */
    void saveState( OutputStream out ) throws IOException;
}
