package de.tu_darmstadt.informatik.kom.contextsense.detectors.weka;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.Detector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors.WekaDetectorMetaData;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.utils.TimingsRecords;
import java8.util.function.BiConsumer;
import java8.util.stream.Stream;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instance;
import weka.core.Instances;

/**
 * A Version of Detector, which uses a WEKA (Machine Learning Toolkit) classifier for detection.
 * Use {@link WekaDetectorBuilder} to build a WekaDetector!
 */
public class WekaDetector extends Detector {

    private final WekaFeatureSet featureSet;
    private final Classifier model;
    private final Precondition precondition;
    private final WekaDetectorMetaData metaData;

    public WekaDetector( DetectorDescription description, WekaFeatureSet featureSet, Classifier model, Precondition precondition ) {
        super( description );
        this.featureSet = featureSet;
        this.model = model;
        this.precondition = precondition;

        this.metaData = new WekaDetectorMetaData( getDescription(), featureSet.getSensorFeatureNames(), precondition );
    }

    @Override
    public Precondition getPrecondition() {
        return precondition;
    }

    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults ) {
        if( otherResults == null ) otherResults = new DetectionResult( DateTime.now() );

        Instance instance = featureSet.toInstance( batch, otherResults.flattenWithBestLabelsPerGroup() );
        Map<String, Double> probabilities = new HashMap<>();

        try {
            List<String> classes = featureSet.getSituationGroup().getSituations();
            double[] distribution = model.distributionForInstance( instance );
            for( int i = 0; i < classes.size(); i++ ) {
                probabilities.put( classes.get(i), distribution[i] );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return buildGoodResult( probabilities );
    }


    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults, TimingsRecords timingsRecords ) {
        if( timingsRecords == null ) return detect( batch, otherResults );

        timingsRecords.startAction( "Feature Extraction" );
        Instance instance = featureSet.toInstanceParrallel( batch, otherResults.flattenWithBestLabelsPerGroup() );
        timingsRecords.logActionAndStartNew( "Detection" );

        Map<String, Double> probabilities = new HashMap<>();
        try {
            List<String> classes = featureSet.getSituationGroup().getSituations();
            double[] distribution = model.distributionForInstance( instance );
            for( int i = 0; i < classes.size(); i++ ) {
                probabilities.put( classes.get(i), distribution[i] );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        timingsRecords.endCurrentAction();
        return buildGoodResult( probabilities );
    }

    public Evaluation evaluate( Stream<TrainingSample> samples, BiConsumer<Evaluation, Instances> performEval ) throws Exception {
        Instances instances = WekaUtils.buildWekaInstanceSet( featureSet, samples, x -> {} );
        Evaluation eval = new Evaluation( instances );
        performEval.accept( eval, instances );
        return eval;
    }

    public Classifier getModel() {
        return model;
    }

    public WekaDetectorMetaData getMetaData() {
        return metaData;
    }

    public WekaFeatureSet getFeatureSet() {
        return featureSet;
    }
}
