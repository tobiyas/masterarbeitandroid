package de.tu_darmstadt.informatik.kom.contextsense;

/**
 * The modes, in which a sensor can run.
 * The sensor is always restarted, if it is supposed to change it's mode.
 */
public enum SensorMode {
    /**
     * Instruct this sensor to work normally.
     */
    NORMAL,

    /**
     * Instruct this sensor to conserve power.
     */
    LOW_POWER
}
