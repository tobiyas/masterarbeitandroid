package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import org.joda.time.DateTime;

import java.util.Arrays;

import de.tu_darmstadt.informatik.kom.contextsense.MergeableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

import static de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayUtils.concat;

/**
 * Created by Oli on 26.01.2016
 */
public class MagnetometerHistoryData extends MergeableSensorData {

    private static final long serialVersionUID = -6790446759078115533L;

    private long[] t;
    private double[] x;
    private double[] y;
    private double[] z;

    private MagnetometerHistoryData() {
    }

    // serialization
    public MagnetometerHistoryData( long timestamp, long[] t, double[] x, double[] y, double[] z ) {
        super( timestamp );
        this.t = t;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public long[] getT() {
        return t;
    }

    public double[] getX() {
        return x;
    }

    public double[] getY() {
        return y;
    }

    public double[] getZ() {
        return z;
    }


    @Override
    public MergeableSensorData copy(){
        return new MagnetometerHistoryData(getTimestamp(),
                Arrays.copyOf(t, t.length),
                Arrays.copyOf(x, x.length),
                Arrays.copyOf(y, y.length),
                Arrays.copyOf(z, z.length)
        ).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    @Override
    protected void mergeIntern(SensorData d) {
        MagnetometerHistoryData data = (MagnetometerHistoryData) d;

        boolean own = d.getTimestamp() > getTimestamp();
        t = own ? concat(t,data.t) : concat(data.t, t);
        x = own ? concat(x,data.x) : concat(data.x, x);
        y = own ? concat(y,data.y) : concat(data.y, y);
        z = own ? concat(z,data.z) : concat(data.z, z);
    }

    @Override
    public void limitFromTo(DateTime from, DateTime to) {
        long fromL = from.getMillis();
        long toL = to.getMillis();

        int first = 0;
        int last = t.length;

        for(int i = 0; i < t.length; i++){
            long time = t[i];

            if(first == 0 && time > fromL) first = i;
            if(last == t.length && time > toL) last = i;
        }

        this.t = Arrays.copyOfRange(t, first, last);
        this.x = Arrays.copyOfRange(x, first, last);
        this.y = Arrays.copyOfRange(y, first, last);
        this.z = Arrays.copyOfRange(z, first, last);
    }


    @Override
    public void applyClockSkew(long clockSkew) {
        super.applyClockSkew(clockSkew);
        applyChangeToArray( t, clockSkew );
    }

}
