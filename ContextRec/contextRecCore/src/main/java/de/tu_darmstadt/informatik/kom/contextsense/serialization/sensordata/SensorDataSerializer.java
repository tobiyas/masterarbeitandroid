package de.tu_darmstadt.informatik.kom.contextsense.serialization.sensordata;

import org.joda.time.DateTime;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.batch.LazySensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerometerHistoryData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ActivityData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AmbientTemperatureData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AudioStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.CalendarData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.GpsSpeedHistoryData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.GpsStatusData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.GyroscopeSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.HeartRateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.HumiditySensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LightSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LinearAccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LocationSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MagnetometerData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MagnetometerHistoryData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MicrophoneData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.NearbyPlaceData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.OrientationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PhoneStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PowerStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PressureSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ProximityData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.ScreenStateData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.StepCounterData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.WeatherData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.WifiSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Lazy;
import java8.util.Optional;

/**
 * Utility class for serializing Sensor data in JSON format using Jackson.
 */
public abstract class SensorDataSerializer {

    private final Map<String, Class<? extends SensorData>> persistentNameToType;
    private final Map<Class<? extends SensorData>, String> typeToPersistentName;

    public SensorDataSerializer() {
        typeToPersistentName = new HashMap<>();
        persistentNameToType = new HashMap<>();

        loadDefaultTypeMapping();
    }

    protected void loadDefaultTypeMapping() {
        addTypeMapping( "Acceleration", AccelerationData.class );
        addTypeMapping( "Acceleration History", AccelerometerHistoryData.class );
        addTypeMapping( "Activity", ActivityData.class );
        addTypeMapping( "Ambient Temperature", AmbientTemperatureData.class );
        addTypeMapping( "Audio State", AudioStateData.class );
        addTypeMapping( "Calendar", CalendarData.class );
        addTypeMapping( "GPS Status", GpsStatusData.class );
        addTypeMapping( "Gyroscope", GyroscopeSensorData.class );
        addTypeMapping( "Humidity", HumiditySensorData.class );
        addTypeMapping( "Light", LightSensorData.class );
        addTypeMapping( "Linear Acceleration", LinearAccelerationData.class );
        addTypeMapping( "Location", LocationSensorData.class );
        addTypeMapping( "Magnetometer", MagnetometerData.class );
        addTypeMapping( "Magnetometer History", MagnetometerHistoryData.class );
        addTypeMapping( "Microphone", MicrophoneData.class );
        addTypeMapping( "Orientation", OrientationData.class );
        addTypeMapping( "Phone State", PhoneStateData.class );
        addTypeMapping( "Power State", PowerStateData.class );
        addTypeMapping( "Pressure", PressureSensorData.class );
        addTypeMapping( "Proximity", ProximityData.class );
        addTypeMapping( "Screen State", ScreenStateData.class );
        addTypeMapping( "Step Counter", StepCounterData.class );
        addTypeMapping( "Wifi", WifiSensorData.class );
        addTypeMapping( "HeartRate", HeartRateData.class );
        addTypeMapping( "Gps Speed History", GpsSpeedHistoryData.class );
        addTypeMapping( "Nearby Places", NearbyPlaceData.class );
        addTypeMapping( "Weather", WeatherData.class );
    }


    public void addTypeMapping( String persistentTypeName, Class<? extends SensorData> dataClass ) {
        persistentNameToType.put( persistentTypeName, dataClass );
        typeToPersistentName.put( dataClass, persistentTypeName );
    }

    public byte[] serialize( SensorDataBatch batch ) {
        try( ByteArrayOutputStream out = new ByteArrayOutputStream();
             DataOutputStream dataOut = new DataOutputStream(out)) {
            dataOut.writeUTF( batch.getStartTime().toDateTimeISO().toString() );
            dataOut.writeUTF( batch.getEndTime().toDateTimeISO().toString() );

            for(SensorData data: batch.getAllData()) {
                String typeName = toPersistentNameOrThrow( data );

                byte[] bytes = doSerialize( data );
                dataOut.writeUTF( typeName );
                dataOut.writeInt( bytes.length );
                dataOut.write( bytes );
            }

            return out.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException( e );
        }
    }

    public SensorDataBatch deserialize( byte[] data ) {
        try(ByteArrayInputStream in = new ByteArrayInputStream( data );
            DataInputStream dataIn = new DataInputStream(in)) {

            DateTime startTime = DateTime.parse( dataIn.readUTF() );
            DateTime endTime = DateTime.parse( dataIn.readUTF() );
            Map<Class<? extends SensorData>, List<Lazy<SensorData>>> parsed = new HashMap<>();

            while( dataIn.available() > 0 ) {
                String typeName = dataIn.readUTF();
                int arraySize = dataIn.readInt();
                byte[] buffer = new byte[ arraySize ];
                dataIn.read( buffer );

                Class<? extends SensorData> dataType = toTypeOrThrow( typeName );
                List<Lazy<SensorData>> list = Optional
                        .ofNullable( parsed.get( dataType ) )
                        .orElse( new ArrayList<>() );

                list.add( Lazy.of( () -> tryParse( buffer, dataType ) ) );
                parsed.put( dataType, list );
            }

            return new LazySensorDataBatch( startTime, endTime, parsed );
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException( e );
        }
    }

    private SensorData tryParse( byte[] buffer, Class<? extends SensorData> dataType ) {
        try {
            return doDeserialize( buffer, dataType );
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException( e );
        }
    }

    private Class<? extends SensorData> toTypeOrThrow( String persistentName ) {
        Class<? extends SensorData> type = persistentNameToType.get( persistentName );

        if (type == null)
            throw new IllegalArgumentException( "Tried to parse data of type "+persistentName+" but not type mapping was registered for this type." );

        return type;
    }

    private String toPersistentNameOrThrow( SensorData data ) {
        String name = typeToPersistentName.get( data.getClass() );

        if (name == null)
            throw new IllegalArgumentException( "Tried to serialize data of type "+data.getClass()+" but not type mapping was registered for this type." );

        return name;
    }

    protected abstract SensorData doDeserialize( byte[] buffer, Class<? extends SensorData> dataType ) throws IOException;
    protected abstract byte[] doSerialize( SensorData data ) throws IOException;
}
