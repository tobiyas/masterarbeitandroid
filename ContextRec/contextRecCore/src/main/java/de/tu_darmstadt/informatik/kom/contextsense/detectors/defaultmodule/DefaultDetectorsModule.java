package de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors.StandingVsSittingBaselineDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors.WearDevicePositionDetector;
import java8.util.function.Supplier;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorModule;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.Detector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.IDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors.IsWearDevicePresentDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule.detectors.LocationDetector;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;

/**
 * Provides all predefined detectors developed during the original master's thesis.
 */
public class DefaultDetectorsModule extends DetectorModule {

    public static final String ID = "de.tu_darmstadt.informatik.kom.contextrec.detectors.defaultmodule";
    private final Map<DetectorIdentifier, Pair<DetectorDescription, Supplier<Detector>>> detectors;

    public DefaultDetectorsModule() {
        detectors = new HashMap<>();
        registerDetector( IsWearDevicePresentDetector.ID, IsWearDevicePresentDetector.DESCRIPTION, IsWearDevicePresentDetector::new );
        registerDetector( LocationDetector.ID, LocationDetector.DESCRIPTION, LocationDetector::new );
        registerDetector( StandingVsSittingBaselineDetector.ID, StandingVsSittingBaselineDetector.DESCRIPTION, StandingVsSittingBaselineDetector::new );
        registerDetector( WearDevicePositionDetector.ID, WearDevicePositionDetector.DESCRIPTION, WearDevicePositionDetector::new );
    }

    private Pair<DetectorDescription, Supplier<Detector>> registerDetector( DetectorIdentifier id, DetectorDescription description, Supplier<Detector> factory ) {
        return detectors.put( id, new Pair<>( description, factory ) );
    }

    @Override
    public String getModuleId() {
        return ID;
    }

    @Override
    public Set<DetectorIdentifier> getProvidedDetectorIds() {
        return new HashSet<>( detectors.keySet() );
    }

    @Override
    public DetectorDescription describeDetector( DetectorIdentifier id ) {
        if (!detectors.containsKey( id ))
            throw new IllegalArgumentException( "This module ('" + ID + "') has no detector with id '"+id+"'. Available are: '"+getProvidedDetectorIds()+"'" );

        return detectors.get( id ).getFirst();
    }

    @Override
    public IDetector buildDetector( DetectorIdentifier id ) {
        if (!detectors.containsKey( id ))
            throw new IllegalArgumentException( "This module ('" + ID + "') has no detector with id '"+id+"'. Available are: '"+getProvidedDetectorIds()+"'" );

        return detectors.get( id ).getSecond().get();
    }
}
