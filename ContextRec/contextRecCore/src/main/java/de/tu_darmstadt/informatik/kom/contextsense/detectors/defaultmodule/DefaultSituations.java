package de.tu_darmstadt.informatik.kom.contextsense.detectors.defaultmodule;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;

/**
 * Created by Oli on 28.04.2016.
 */
public class DefaultSituations {

    ///////////////////////////////////////////
    /////// Is a wear device connected? ///////
    ///////////////////////////////////////////

    public static class WearStatus {

        public static final String ID = "de.tu_darmstadt.informatik.kom.contextsense.detectors.predefined.WearStatus";
        public static final String OFFLINE = "Got no data from Wear device";
        public static final String ONLINE = "Got data from Wear device";
        public static final SituationGroup SITUATION = new SituationGroup( ID, OFFLINE, ONLINE );
    }

    ////////////////////////////////
    /////// Common locations ///////
    ////////////////////////////////

    public static class CommonPlaces {

        public static final String ID = "de.tu_darmstadt.informatik.kom.contextsense.detectors.predefined.CommonPlaces";
        public static final String AT_HOME = "At Home";
        public static final String AT_WORK = "At Work";
        public static final String AT_UNIVERSITY = "At University";
        public static final String AT_FRIEND = "At Friend's Home";
        public static final String AT_SHOP = "At shop";
        public static final String AT_FOOD_PLACE = "At food place";
        public static final String UNKNOWN = "Somewhere Else";

        public static final SituationGroup SITUATION = new SituationGroup(
                ID, AT_HOME, AT_WORK, AT_FOOD_PLACE, AT_FRIEND, AT_SHOP, AT_UNIVERSITY, UNKNOWN
        );

    }


    ////////////////////////////////
    /////// Standing vs Sitting ////
    ////////////////////////////////

    public static class StandingVsSitting {
        public static final String ID = "de.tu_darmstadt.informatik.kom.contextsense.detectors.predefined.StandingVsSitting";
        public static final String STANDING = "Standing";
        public static final String SITTING = "Sitting";
        public static final SituationGroup SITUATION = new SituationGroup( ID, STANDING, SITTING );
    }

    ////////////////////////////////
    /////// Wear position //////////
    ////////////////////////////////

    public static class WearPosition {
        public static String ID = "de.tu_darmstadt.informatik.kom.contextsense.detectors.predefined.WearDevicePosition";
        public static String AT_WRIST = "At wrist";
        public static String ON_TABLE = "On table";
        public static final SituationGroup SITUATION = new SituationGroup( ID, AT_WRIST, ON_TABLE );
    }

    ////////////////////////////////////////////////////////
    /////// Activity Recognition (for Wear device) /////////
    ////////////////////////////////////////////////////////

    public static class ActivityRecognition {
        public static String ID = "de.tu_darmstadt.informatik.kom.contextsense.detectors.predefined.ActivityRecognitionWear";
        public static String WALKING = "walking";
        public static String RUNNING = "running";
        public static String STANDING = "standing";
        public static String STAIRS_UP = "walking stairs up";
        public static String STAIRS_DOWN = "walking stairs down";
        public static String SITTING = "sitting";
        public static String LYING = "lying";

        public static final SituationGroup SITUATION = new SituationGroup( ID, WALKING, RUNNING, STANDING, STAIRS_UP, STAIRS_DOWN, SITTING, LYING );
    }


}
