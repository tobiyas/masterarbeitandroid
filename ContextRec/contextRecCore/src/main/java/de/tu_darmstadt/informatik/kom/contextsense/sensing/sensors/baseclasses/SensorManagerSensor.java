package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses;

import android.content.Context;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import java8.util.stream.Collectors;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.Optional;

/**
 * Abstract base class for regular sensors using SensorManager.
 */
public abstract class SensorManagerSensor extends Sensor implements SensorEventListener, StreamableSensor {

    /**
     * The SensorManager to register for Events.
     */
    private final SensorManager sensorManager;


    /**
     * The queue of events to save when in streaming mode.
     */
    private final SensorEventQueue eventQueue = new SensorEventQueue(50 * 60);


    public SensorManagerSensor( Context context ) {
        super(context);
        this.sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
    }


    @Override
    protected SensorInvocation doTrigger( Duration batchDuration ) {
        Optional<android.hardware.Sensor> sensor = resolveSensor( sensorManager );

        if (sensor.isPresent())
            return new SensorManagerSession( batchDuration );
        else {
            Log.i( "Sensors", "Device has no sensor of type " + getName() + ". Ignoring it." );
            SensorInvocation result = null;
            try {
                result = SensorInvocations.emptyInvocation();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return result;
        }
    }


    @Override
    public void startStreaming() {
        //Clear old stuff:
        eventQueue.clear();

        //Register:
        Optional<android.hardware.Sensor> sensorOptional = resolveSensor(sensorManager);
        if(sensorOptional.isPresent()){
            int samplingPeriodInUs = (int) resolveSamplingPeriod().getMillis() * 1000;
            sensorManager.registerListener(this, resolveSensor(sensorManager).get(), samplingPeriodInUs);
        }
    }


    @Override
    public void stopStreaming() {
        sensorManager.unregisterListener(this);
        eventQueue.clear();
    }


    @Override
    public void onSensorChanged( SensorEvent event ) {
        this.eventQueue.add(new SensorEventCopy(event, System.currentTimeMillis()));
    }



    @Override //Not needed
    public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {}


    @Override
    public Optional<? extends SensorData> getStreamingResults(DateTime from, DateTime to){
        if(from == null) throw new IllegalArgumentException("from may not be null");
        if(to == null) throw new IllegalArgumentException("to may not be null");
        if(from.isAfter(to)) throw new IllegalArgumentException("from must be before to");

        Interval interval = new Interval(from, to);

        //If we have nothing to report -> Do an empty response,
        //If we have data, convert it and go for it!
        if (this.eventQueue.isEmpty()) return Optional.empty();
        else {
            List<SensorEventCopy> data = this.eventQueue.getFromToCollected(from, to);
            if(data.isEmpty()) Log.e("DEBUG WARNING", "Sensor " + getName() + " has empty streaming results with " + eventQueue.size() + " entries! First: " + eventQueue.first() + " now: " + System.currentTimeMillis() + " last: " + eventQueue.last() + ".");
            if(data.isEmpty()) return Optional.empty();

            return Optional.of( convertToResult(data , interval.toDuration() ) );
        }
    }



    private class SensorManagerSession implements SensorInvocation, SensorEventListener {

        private final List<SensorEventCopy> events;
        private final Duration batchDuration;

        private SensorManagerSession( Duration batchDuration ) {
            this.events = new ArrayList<>();
            this.batchDuration = batchDuration;
        }

        @Override
        public void onSensorChanged( SensorEvent event ) {
            events.add( new SensorEventCopy( event ) );

            if (!needsMoreData( events,batchDuration ))
                stop();
        }

        @Override
        public void onAccuracyChanged( android.hardware.Sensor sensor, int accuracy ) {
        }

        @Override
        public void start() {
            int samplingPeriodInUs = (int) resolveSamplingPeriod().getMillis() * 1000;
            sensorManager.registerListener( this, resolveSensor( sensorManager ).get(), samplingPeriodInUs );
        }

        @Override
        public void stop() {
            sensorManager.unregisterListener( this );
        }

        public Optional<SensorData> getResult() {
            List<SensorEventCopy> eventCopy = new ArrayList<>( events );

            if (eventCopy.isEmpty())
                return Optional.empty();
            else
                return Optional.of( convertToResult( new ArrayList<>( events ), batchDuration ) );
        }
    }

    protected boolean needsMoreData( List<SensorEventCopy> data, Duration batchDuration ) { return true; }
    protected abstract Optional<android.hardware.Sensor> resolveSensor( SensorManager sensorManager );
    protected abstract Duration resolveSamplingPeriod();
    protected abstract SensorData convertToResult( List<SensorEventCopy> events, Duration batchDuration );
}
