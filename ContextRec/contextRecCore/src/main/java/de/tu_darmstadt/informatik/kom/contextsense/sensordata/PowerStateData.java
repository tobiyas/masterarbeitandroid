package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015.
 */
public class PowerStateData extends SensorData {

    private static final long serialVersionUID = 3283619614007742784L;

    public enum ChargingState {
        DISCONNECTED,
        CONNECTED_AC,
        CONNECTED_USB,
        CONNECTED_WIRELESS
    }

    private double batteryPercent;
    private ChargingState chargingState;
    private boolean isInPowerSaveMode;

    public PowerStateData( long timestamp, double batteryPercent, ChargingState chargingState, boolean isInPowerSaveMode ) {
        super( timestamp );
        this.batteryPercent = batteryPercent;
        this.chargingState = chargingState;
        this.isInPowerSaveMode = isInPowerSaveMode;
    }

    // serialization
    private PowerStateData() {
    }

    @Override
    public SensorData copy() {
        return new PowerStateData(getTimestamp(), batteryPercent, chargingState, isInPowerSaveMode).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public double getBatteryPercent() {
        return batteryPercent;
    }

    public ChargingState getChargingState() {
        return chargingState;
    }

    public boolean isInPowerSaveMode() {
        return isInPowerSaveMode;
    }
}
