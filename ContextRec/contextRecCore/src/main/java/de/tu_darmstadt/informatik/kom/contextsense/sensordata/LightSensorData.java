package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.MergeableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;

/**
 * Created by Oli on 13.12.2015
 */
public class LightSensorData extends MergeableSensorData {

    private static final long serialVersionUID = -6349817268086879772L;

    private double brightnessInLux;

    public LightSensorData( long timestamp, double brightnessInLux ) {
        super( timestamp );
        this.brightnessInLux = brightnessInLux;
    }

    // serialization
    private LightSensorData() {
    }

    @Override
    public MergeableSensorData copy() {
        return new LightSensorData(getTimestamp(), brightnessInLux).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    public double getBrightnessInLux() {
        return brightnessInLux;
    }


    @Override
    protected void mergeIntern(SensorData data) {
        //TODO implement me maybe?
    }

    @Override
    public void limitFromTo(DateTime from, DateTime to) {
        //Nothing to do!
    }
}
