package de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AmbientTemperatureData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.OneShotSensorManagerSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorEventCopy;
import java8.util.Optional;

/**
 * Sensor that measures the ambient temperature
 */
public class AmbientTemperatureSensor extends OneShotSensorManagerSensor<AmbientTemperatureData> {

    public AmbientTemperatureSensor( Context context ) {
        super( context );
    }

    @Override
    protected Optional<Sensor> resolveSensor(SensorManager sensorManager) {
        return Optional.ofNullable( sensorManager.getDefaultSensor( Sensor.TYPE_AMBIENT_TEMPERATURE ) );
    }

    @Override
    protected AmbientTemperatureData convertToResult( SensorEventCopy event ) {
        return new AmbientTemperatureData(
                DateTime.now().getMillis(),
                event.values[0]
        );
    }
}