package de.tu_darmstadt.informatik.kom.contextsense.sensordata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import java8.util.Optional;
import java8.util.stream.IntStreams;

/**
 * Represents a list of nearby places (names, types, ...).
 * The collected data is tightly coupled to the Google Places API.
 * If you want to collect info about nearby places with a different API, create another SensorData subclass.
 */
public class NearbyPlaceData extends SensorData {

    /**
     * All types of "Stores"
     */
    public static int[] STORE_TYPES = { 11, 12, 17, 18, 19, 20, 25, 26, 29, 32, 37, 40, 41, 43, 46, 49, 52, 53, 54, 55, 56, 71, 72, 83, 84, 88, 95 };

    /**
     * All types of "Food" stores/places
     */
    public static int[] FOOD_TYPES = { 7, 9, 15, 38, 79, };

    /**
     * All types of "Universities"
     */
    public static int[] UNIVERSITY_TYPES = { 94 };

    private static final long serialVersionUID = 637963104344543412L;

    /**
     * Map from place <--> probability of being at this place.
     */
    private Map<NearbyPlace, Double> places;

    // serialization constructor
    private NearbyPlaceData() {
    }

    public NearbyPlaceData( long timestamp, Map<NearbyPlace, Double> places ) {
        super( timestamp );
        this.places = places;
    }

    @Override
    public SensorData copy() {
        return new NearbyPlaceData(getTimestamp(), new HashMap<>(places)).setSourceName( getSourceName() ).setSourceType( getSourceType() );
    }

    /**
     * Returns a list of nearby places (no order)
     */
    public List<NearbyPlace> getPlaces() {
        return new ArrayList<>( places.keySet() );
    }

    /**
     * Returns the chance, that the user is currently at this place.
     * Likelihood as given by Google Places API.
     */
    public double getLikelihoodForPlace( NearbyPlace place ) {
        return Optional
                .ofNullable( places.get( place ) )
                .orElse( 0.0 );
    }

    /**
     * Represents a nearby place.
     * Places are collected with the Google Places API (sorry, but this is hardcoded here. Create your own Place-type if you want to use a different API).
     * See https://developers.google.com/android/reference/com/google/android/gms/location/places/Place#constant-summary for the meaning of the type-ids.
     */
    public static class NearbyPlace {

        /**
         * The name of the place.
         */
        private String name;

        /**
         * A list of type-ids for the place.
         * See https://developers.google.com/android/reference/com/google/android/gms/location/places/Place#constant-summary
         */
        private List<Integer> googlePlaceTypes;

        /**
         * The latitude of the place
         */
        private double latitude;

        /**
         * The longitude of the place
         */
        private double longitude;

        // serialization constructor
        private NearbyPlace() {
        }

        public NearbyPlace( String name, List<Integer> googlePlaceTypes, double latitude, double longitude ) {
            this.name = name;
            this.googlePlaceTypes = googlePlaceTypes;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public String getName() {
            return name;
        }

        public List<Integer> getGooglePlaceTypes() {
            return googlePlaceTypes;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public boolean isOfType( int type ) {
            return this.googlePlaceTypes.contains( type );
        }

        public boolean isOfType( int[] type ) {
            return IntStreams.of( type )
                .filter( this::isOfType )
                .findFirst()
                .isPresent();
        }
    }
}
