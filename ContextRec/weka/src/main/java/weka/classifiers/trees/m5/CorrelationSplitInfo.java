/*
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * CorrelationSplitInfo.java
 * Copyright (C) 2000-2012 University of Waikato, Hamilton, New Zealand
 *
 */

package weka.classifiers.trees.m5;

import java.io.Serializable;

import weka.core.Instances;
import weka.core.RevisionHandler;
import weka.core.RevisionUtils;

/**
 * Finds split points using correlation.
 * 
 * @author Mark Hall (mhall@cs.waikato.ac.nz)
 * @version $Revision$
 */
public final class CorrelationSplitInfo implements Cloneable, Serializable,
  SplitEvaluate, RevisionHandler {

  /** for serialization */
  private static final long serialVersionUID = 4212734895125452770L;

  private int m_position;

  /**
   * the maximum impurity reduction
   */
  private double m_maxImpurity;

  /**
   * the attribute being tested
   */
  private int m_splitAttr;

  /**
   * the best value on which to split
   */
  private double m_splitValue;

  /**
   * the number of instances
   */
  private int m_number;

  /**
   * Constructs an object which contains the split information
   * 
   * @param low the index of the first instance
   * @param high the index of the last instance
   * @param attr an attribute
   */
  public CorrelationSplitInfo(int low, int high, int attr) {
    initialize(low, high, attr);
  }

  /**
   * Makes a copy of this CorrelationSplitInfo object
   */
  @Override
  public final SplitEvaluate copy() throws Exception {
    CorrelationSplitInfo s = (CorrelationSplitInfo) this.clone();

    return s;
  }

  /**
   * Resets the object of split information
   * 
   * @param low the index of the first instance
   * @param high the index of the last instance
   * @param attr the attribute
   */
  public final void initialize(int low, int high, int attr) {
    m_number = high - low + 1;
    m_position = -1;
    m_maxImpurity = -Double.MAX_VALUE;
    m_splitAttr = attr;
    m_splitValue = 0.0;
  }

  /**
   * Finds the best splitting point for an attribute in the instances
   * 
   * @param attr the splitting attribute
   * @param inst the instances
   * @exception Exception if something goes wrong
   */
  @Override
  public final void attrSplit(int attr, Instances inst) throws Exception {
    throw new UnsupportedOperationException( "Stripped out" );
  }

  /**
   * Returns the impurity of this split
   * 
   * @return the impurity of this split
   */
  @Override
  public double maxImpurity() {
    return m_maxImpurity;
  }

  /**
   * Returns the attribute used in this split
   * 
   * @return the attribute used in this split
   */
  @Override
  public int splitAttr() {
    return m_splitAttr;
  }

  /**
   * Returns the position of the split in the sorted values. -1 indicates that a
   * split could not be found.
   * 
   * @return an <code>int</code> value
   */
  @Override
  public int position() {
    return m_position;
  }

  /**
   * Returns the split value
   * 
   * @return the split value
   */
  @Override
  public double splitValue() {
    return m_splitValue;
  }

  /**
   * Returns the revision string.
   * 
   * @return the revision
   */
  @Override
  public String getRevision() {
    return RevisionUtils.extract("$Revision$");
  }
}
