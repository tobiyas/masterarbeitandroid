package de.tu_darmstadt.informatik.kom.contextrec.wear;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.Duration;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.concurrent.ExecutionException;
import java.util.zip.GZIPOutputStream;

import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import java8.util.Optional;

/**
 * Default implementation of a service, which will collect sensor-data and send them back to the requester.
 * Override this service, if you want to use custom sensors on your wear device.
 * Do not forget to add this service to your manifest, if you override it!
 */
public class WearSensingService extends NonStopIntentService {

    public static final String TAG = "WearSensingService";
    public static final String PARAMETER_DURATION = "Duration";
    public static final String PARAMETER_RESULT_RECEIVER_ID = "Result node ID";
    public static final String PARAMETER_REQUEST_ID = "Request ID";

    private static final Duration NO_MESSAGE_SHUTDOWN_TIMEOUT = Duration.standardMinutes( 45 );

    private DataSource dataSource;
    private GoogleApiClient apiClient;
    private Handler shutdownHandler;
    private PowerManager.WakeLock wakeLock;

    public WearSensingService() {
        super( "ContextSense Sensing Service for Android Wear" );
    }

    @Override
    public void onCreate() {
        super.onCreate();

        shutdownHandler = new Handler( Looper.getMainLooper() );
        dataSource = WearDataSourceRegistry.resolveDataSource( this );
        apiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .build();

        dataSource.start();

        PowerManager powerManager = (PowerManager) getSystemService( POWER_SERVICE );
        wakeLock = powerManager.newWakeLock( PowerManager.PARTIAL_WAKE_LOCK, TAG );
    }

    @Override
    public void onDestroy() {
        dataSource.stop();
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent( Intent intent ) {
        // may happen when the service was restarted - we don't care.
        if (intent == null)
            return;

        try {
            //Use a timeout of 3 Second. This should be enough to collect the data and send it.
            wakeLock.acquire( 3 * 1000);
            ensureApiClientIsConnected();

            // we'll subtract 500 ms from the batch duration to compensate the processing & transmission delay between wear and phone.
            Duration duration = new Duration( intent.getLongExtra( PARAMETER_DURATION, 0 ) );
            Duration shortenedDuration = duration.minus( Duration.millis( 500 ) );
            Optional<SensorDataBatch> batch = dataSource.collectData( shortenedDuration ).get();

            batch.ifPresent( data -> {
                String receiver = intent.getStringExtra( PARAMETER_RESULT_RECEIVER_ID );
                Log.i( TAG, "Sening response to " + receiver );
                byte[] message = serialize( data );
                Wearable.MessageApi.sendMessage( apiClient, receiver, "/triggerResult", message );
            });
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } finally {
            wakeLock.release();
            resetShutdownTimeout();
        }
    }

    private void ensureApiClientIsConnected() {
        if (!apiClient.isConnected()) {
            ConnectionResult result = apiClient.blockingConnect();
            if (!result.isSuccess()) {
                throw new IllegalStateException( "Could not connect to Google API client: " +result.getErrorMessage() + " " +result.getResolution() );
            }
        }
    }

    private byte[] serialize( SensorDataBatch batch ) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try(ObjectOutputStream objectOut = new ObjectOutputStream( new GZIPOutputStream( out ) )) {
            objectOut.writeObject( batch );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return out.toByteArray();
    }

    private void resetShutdownTimeout() {
        // remove all pending requests
        shutdownHandler.removeCallbacksAndMessages( null );

        // stopself after timeout
        shutdownHandler.postDelayed( this::stopSelf, NO_MESSAGE_SHUTDOWN_TIMEOUT.getMillis() );
    }

}
