package de.tu_darmstadt.informatik.kom.contextrec.wear;

import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.DataSources;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.LocalDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.AccelerometerHistorySensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.AccelerometerSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.GyroscopeSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.HeartRateSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.LightSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.LinearAccelerationSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.MagnetometerSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.MicrophoneSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.PowerStateSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.PressureSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.ScreenStateSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.StepCounterSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;

/**
 * Default implementations of {@link DataSource} for Android Wear devices.
 */
public class WearDataSources {

    /**
     * Creates a {@link DataSource} with all the sensors commonly available on Android Wear devices.
     * Includes Accelerometer, Magnetometer, Light, Pressure, Heartrate, Stepcounter, etc.
     */
    public static DataSource defaultWearDataSource( Context context, Sensor... extraSensors ) {
        List<Sensor> allSensors = new ArrayList<>( Arrays.asList( extraSensors ) );

        allSensors.add( new AccelerometerSensor( context ) );
        allSensors.add( new AccelerometerHistorySensor( context ) );
        allSensors.add( new LightSensor( context ) );
        allSensors.add( new MagnetometerSensor( context ) );
        allSensors.add( new PressureSensor( context ) );
        allSensors.add( new HeartRateSensor( context ) );
        allSensors.add( new ScreenStateSensor( context ) );
        allSensors.add( new PowerStateSensor( context ) );
        allSensors.add( new MicrophoneSensor( context ) );
        allSensors.add( new StepCounterSensor( context ) );

        return new LocalDataSource( context, allSensors, DeviceType.WEAR, DataSources.WEAR );
    }


    /**
     * Creates a {@link DataSource} with all the sensors commonly available on Android Wear devices.
     * Includes Accelerometer, Magnetometer, Light, Pressure, Heartrate, Stepcounter, etc.
     */
    public static DataSource defaultWearReducedDataSource( Context context, Sensor... extraSensors ) {
        List<Sensor> allSensors = new ArrayList<>( Arrays.asList( extraSensors ) );

        allSensors.add( new AccelerometerSensor( context ) );
        allSensors.add( new LinearAccelerationSensor( context ) );
        allSensors.add( new GyroscopeSensor( context ) );
        allSensors.add( new MagnetometerSensor( context ) );
        allSensors.add( new HeartRateSensor( context ) );
        allSensors.add( new StepCounterSensor( context ) );

        return new LocalDataSource( context, allSensors, DeviceType.WEAR, DataSources.WEAR );
    }
}
