package de.tu_darmstadt.informatik.kom.contextrec.wear;

import android.content.Context;

import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import java8.util.function.Function;

/**
 * Singleton class holding (a factory) for creating the current datasource for Wear devices.
 * Use {@link WearDataSourceRegistry#setDataSource(Function)} if you want to change the sensors ContextRec uses on your watch.
 */
public class WearDataSourceRegistry {

    private static Function<Context, DataSource> factory = context -> WearDataSources.defaultWearDataSource( context );

    // static class. Do you cannot instantiate this.
    private WearDataSourceRegistry() {
    }

    /**
     * Creates an instance of the currently set datasource.
     * Use {@link #setDataSource(Function)} if you want to change the factory which creates the datasources.
     *
     * Defaults to {@link WearDataSources#defaultWearDataSource(Context, Sensor...)},
     * if no other datasource factory was set.
     */
    public static DataSource resolveDataSource( Context context ) {
        return factory.apply( context );
    }

    /**
     * Exchange the factory used to create the DataSource used by the {@link WearSensingService}.
     * Use this, if you want to change the sensors used by the Wear App.
     * You may want to call this function from a custom subclass of Application to ensure
     * that it's called before the first datasource is resolved.
     */
    public static void setDataSource( Function<Context, DataSource> dataSourceFactory ) {
        factory = dataSourceFactory;
    }


}
