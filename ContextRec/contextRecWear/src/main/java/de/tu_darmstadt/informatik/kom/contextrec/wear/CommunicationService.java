package de.tu_darmstadt.informatik.kom.contextrec.wear;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Service for receiving command from ContextSensePhone.
 * Dispatches all work to {@link WearSensingService}.
 *
 * Do not attempt to do any sensing operations in this service!
 * It has a very short lifetime (the time it takes to dispatch the received messages).
 */
public class CommunicationService extends WearableListenerService {

    private static final String TAG = "CommunicationService";

    /**
     * The client to use when sending data back.
     */
    private GoogleApiClient responseClient;

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init( this );

        //Build a Client for Responses and connect it:
        this.responseClient = new GoogleApiClient.Builder( this )
                .addApi(Wearable.API )
                .build();

        this.responseClient.connect();
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        String path = messageEvent.getPath();

        //We shall do a trigger:
        if (path.equals( "trigger/" )) {
            Log.d( TAG, "Trigger requested by " + messageEvent.getSourceNodeId() );
            requestTrigger( messageEvent );
            return;
        }

        //We shall start flooding with our data:
        if(path.equals("StartStreaming/")){
            if(isStreaming()) return;

            Log.d( TAG, "Starting to stream data." );
            startStreaming();
            return;
        }

        //We shall stop flooding with our data:
        if(path.equals("StopStreaming/")){
            if(!isStreaming()) return;

            Log.d( TAG, "Stopping to stream data." );
            stopStreaming();
            return;
        }

        //Get results from to:
        if(path.equals("StreamingFromTo/")){
            if(!isStreaming()) {
                sendEmptyResult( messageEvent );
                return;
            }

            Log.d( TAG, "Getting From-To Results!");
            getFromToResults( messageEvent );
            return;
        }


        //We shall start flooding with our data:
        if(path.equals("StartStreamingFull/")){
            if(isStreamingFull()) return;

            Log.d( TAG, "Starting to stream data to phone." );
            startStreamingFull( messageEvent );
            return;
        }

        //Keep alive for Wear Streaming:
        if(path.equals("KeepAliveStreamingFull/")){
            boolean restartOnNotRunning = messageEvent.getData().length >= 1 && messageEvent.getData()[0] == 0x01;
            boolean isRunning = isStreamingFull();

            Log.d( TAG, "Got keep alive! Restart: " + restartOnNotRunning + ", Running: " + isRunning );
            if( !restartOnNotRunning && !isRunning ) return;

            //Not running, we need to restart it!
            if( restartOnNotRunning && !isRunning ){
                startStreamingFull( messageEvent );
                return;
            }

            Log.d( TAG, "Keep alive for Wear Stream." );
            keepAliveStreamingFull();
            return;
        }

        //We shall stop flooding with our data:
        if(path.equals("StopStreamingFull/")){
            if(!isStreamingFull()) return;

            Log.d( TAG, "Stopping to stream data." );
            stopStreamingFull();
            return;
        }

        //React on Pong messages from the Device:
        if(path.equals("Ping/")) {
            sendPongMessage( messageEvent );
            return;
        }

        //React on Pong messages from the Device:
        if(path.equals("Vibrate/")) {
            Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            Log.d( TAG, "Got Vibrate. Vibrator " + (vibrator == null ? "NOT " : "") + "present." );
            if(vibrator == null) return;

            vibrator.vibrate( 200 );
            return;
        }

        Log.e(TAG, "Received message with unknown path '" + path + "'. Ignoring it.");
    }

    private boolean isStreaming(){
        return isServiceRunning(WearSensorStreamingService.class);
    }

    private boolean isStreamingFull(){
        return isServiceRunning(WearSensorStreamingToPhoneService.class);
    }

    private boolean isServiceRunning(Class<? extends Service> clazz){
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if(manager == null) return false;

        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (clazz.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void requestTrigger( MessageEvent messageEvent ) {
        try (DataInputStream in = new DataInputStream(new ByteArrayInputStream(messageEvent.getData()))) {
            final Duration duration = new Duration(in.readLong());
            String requestId = in.readUTF();

            Intent intent = new Intent(this, WearSensingService.class);
            intent.putExtra(WearSensingService.PARAMETER_DURATION, duration.getMillis());
            intent.putExtra(WearSensingService.PARAMETER_RESULT_RECEIVER_ID, messageEvent.getSourceNodeId());
            intent.putExtra(WearSensingService.PARAMETER_REQUEST_ID, requestId);
            startService(intent);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    /**
     * This starts to stream data to the connected device.
     */
    private void startStreaming(){
        startService(new Intent(this, WearSensorStreamingService.class));
    }

    /**
     * This stops streaming data to the connected device.
     */
    private void stopStreaming(){
        stopService(new Intent(this, WearSensorStreamingService.class));
    }

    /**
     * This starts to stream data to the connected device.
     * @param messageEvent to use.
     */
    private void startStreamingFull(MessageEvent messageEvent){
        Intent intent = new Intent(this, WearSensorStreamingToPhoneService.class);
        intent.putExtra(WearSensorStreamingToPhoneService.PARAMETER_RESULT_RECEIVER_ID, messageEvent.getSourceNodeId());

        startService(intent);
    }

    /**
     * This starts to stream data to the connected device.
     */
    private void keepAliveStreamingFull(){
        Intent intent = new Intent(this, WearSensorStreamingToPhoneService.class);
        intent.putExtra("KEEP_ALIVE", true);
        startService(intent);
    }

    /**
     * This stops streaming data to the connected device.
     */
    private void stopStreamingFull(){
        stopService(new Intent(this, WearSensorStreamingToPhoneService.class));
    }

    /**
     * This requests the streaming result from to a specific place.
     * @param event to parse from.
     */
    private void getFromToResults(MessageEvent event){
        try(DataInputStream in = new DataInputStream( new ByteArrayInputStream( event.getData() ) )) {
            final DateTime from = new DateTime( in.readLong() );
            final DateTime to = new DateTime( in.readLong() );
            String requestId = in.readUTF();

            Intent intent = new Intent( this, WearSensorStreamingService.class );
            intent.putExtra( WearSensorStreamingService.PARAMETER_FROM, from.getMillis() );
            intent.putExtra( WearSensorStreamingService.PARAMETER_TO, to.getMillis() );
            intent.putExtra( WearSensorStreamingService.PARAMETER_RESULT_RECEIVER_ID, event.getSourceNodeId() );
            intent.putExtra( WearSensorStreamingService.PARAMETER_REQUEST_ID, requestId );
            startService( intent );

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Sends an empty result.
     * @param messageEvent to use.
     */
    private void sendEmptyResult( MessageEvent messageEvent ) {
        if( !responseClient.isConnected() ) responseClient.blockingConnect();

        String nodeID = messageEvent.getSourceNodeId();
        Wearable.MessageApi.sendMessage( responseClient, nodeID, "/streamResult", new byte[0] );
    }

    /**
     * Sends an Pong result.
     * @param messageEvent to use.
     */
    private void sendPongMessage( MessageEvent messageEvent ) {
        if( !responseClient.isConnected() ) responseClient.blockingConnect();

        String nodeID = messageEvent.getSourceNodeId();
        ByteBuffer received = ByteBuffer.wrap( messageEvent.getData() );
        long smartPhoneClock = received.getLong();

        ByteBuffer buffer = ByteBuffer.allocate( 8 * 2 );
        buffer.putLong( smartPhoneClock );
        buffer.putLong( System.currentTimeMillis() );

        Wearable.MessageApi.sendMessage( responseClient, nodeID, "Pong/", buffer.array() );
    }

}