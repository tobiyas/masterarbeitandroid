package de.tu_darmstadt.informatik.kom.contextrec.wear;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPOutputStream;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.LocalDataSource;
import java8.util.Optional;

import static de.tu_darmstadt.informatik.kom.contextrec.wear.WearDataSources.defaultWearReducedDataSource;
import static java8.util.stream.Collectors.*;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 19.08.2017
 */
public class WearSensorStreamingToPhoneService extends NonStopIntentService {

    //Static Stuff for Parsing:
    public static final String TAG = "WearStreamPhoneService";
    public static final String PARAMETER_RESULT_RECEIVER_ID = "RESULT_NODE";
    public static final String KEEP_ALIVE_ID = "KEEP_ALIVE";

    /**
     * This is the Path the Listener on the phone may listen to.
     * The Wear devices will ONLY stream on THAT path!
     */
    private static final String PATH_TO_SEND_TO = "streamWear/";



    /**
     * The TimeOut to use then stopping the service if no message can be sent.
     */
    private static final Duration NO_MESSAGE_SHUTDOWN_TIMEOUT = Duration.standardMinutes( 1 );

    /**
     * The Scheduler to use for getting data.
     */
    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    /**
     * The Time between the triggers.
     */
    private static final Duration TIME_BETWEEN_SENDING_BATCH = Duration.millis(500);


    /**
     * The DataSource to query for Data.
     */
    private LocalDataSource dataSource;

    /**
     * The API client to connect to the Smartphone.
     */
    private GoogleApiClient apiClient;

    /**
     * The Handler to shutdown the whole service.
     */
    private Handler shutdownHandler;

    /**
     * The Name of the receiver.
     */
    private String receiver = "";

    /**
     * The last end date the data was collected and sent.
     */
    private DateTime lastSentEnd = null;


    public WearSensorStreamingToPhoneService() {
        super( "ContextSense Streaming Service to phone for Android Wear" );
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Be sure to init this, since it is killed every time!
        JodaTimeAndroid.init( this );


        shutdownHandler = new Handler( Looper.getMainLooper() );
        dataSource = (LocalDataSource) defaultWearReducedDataSource( this );
        apiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .build();

        dataSource.startStreaming();

        //Schedule posting of Data:
        scheduler.scheduleAtFixedRate(this::postData, TIME_BETWEEN_SENDING_BATCH.getMillis(), TIME_BETWEEN_SENDING_BATCH.getMillis(), TimeUnit.MILLISECONDS);

        Log.i(TAG, "Created Streaming Service to phone and started Streaming data");
        Log.i(TAG, "Created following Sensors: " + TextUtils.join(", ", dataSource.getSensorNames() ) );
        resetShutdownTimeout();
    }


    /**
     * Collects the Data of the Sensors.
     */
    private void postData(){
        //We can't send anything to no one!
        if(receiver == null || receiver.isEmpty()) {
            Log.w(TAG, "Working on WearStreaming Server, but got no receiver to send to!");
            return;
        }

        //Create a wakelock to preserve the Thread from dieing.
        PowerManager powerManager = (PowerManager) getSystemService( POWER_SERVICE );
        PowerManager.WakeLock wakeLock = null;
        if(powerManager != null){
            wakeLock = powerManager.newWakeLock( PowerManager.PARTIAL_WAKE_LOCK, UUID.randomUUID().toString() );
            wakeLock.acquire(1000);
        }

        try{
            //Ensure we have valid dates:
            if ( lastSentEnd == null ) lastSentEnd = new DateTime(0);
            DateTime now = new DateTime();

            //Get the data and send it:
            Optional<SensorDataBatch> data = dataSource.collectStreamedData(lastSentEnd, now);
            data.ifPresent(this::sendData);

            //Set the date to the last read:
            lastSentEnd = now;
        }finally {
            if(wakeLock != null) wakeLock.release();
        }
    }

    /**
     * Sends the Data to the Phone.
     * @param data to send.
     */
    private void sendData(SensorDataBatch data) {
        ensureApiClientIsConnected();

        byte[] message = serialize( data );
        Wearable.MessageApi.sendMessage( apiClient, receiver, PATH_TO_SEND_TO, message )
                .setResultCallback( c -> {
                    if(!c.getStatus().isSuccess()) Log.i(TAG, "Response Could not be sent! Message: " + c.getStatus().getStatusMessage());
                }
        );

        int dataSize = data.getAllData().size();
        String sensors =
                TextUtils.join( ",", stream( data.getAllData() )
                .map( Object::getClass )
                .map( Class::getSimpleName)
                .collect( toList() ) );

        Log.d(TAG, "Sending: " + dataSize + " Data, in " + message.length + " Bytes. Sensors: " + sensors + " to: " + receiver );
    }


    @Override
    public void onDestroy() {
        Log.i(TAG, "Wear Streaming service to phone destroyed!");

        dataSource.stopStreaming();

        scheduler.shutdown();
        super.onDestroy();
    }


    @Override
    protected void onHandleIntent( Intent intent ) {
        // may happen when the service was restarted - we don't care.
        if (intent == null) return;

        if(intent.hasExtra( PARAMETER_RESULT_RECEIVER_ID )){
            receiver = intent.getStringExtra( PARAMETER_RESULT_RECEIVER_ID );
        }

        //We got a keep alive signal!
        if( intent.hasExtra( KEEP_ALIVE_ID ) && intent.getBooleanExtra( KEEP_ALIVE_ID, false ) ) resetShutdownTimeout();
    }

    private void ensureApiClientIsConnected() {
        if (!apiClient.isConnected()) {
            ConnectionResult result = apiClient.blockingConnect();
            if (!result.isSuccess()) {
                throw new IllegalStateException( "Could not connect to Google API client: " +result.getErrorMessage() + " " +result.getResolution() );
            }
        }
    }

    private byte[] serialize( SensorDataBatch batch ) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try(ObjectOutputStream objectOut = new ObjectOutputStream( new GZIPOutputStream( out ) )) {
            objectOut.writeObject( batch );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return out.toByteArray();
    }

    private void resetShutdownTimeout() {
        // remove all pending requests
        shutdownHandler.removeCallbacksAndMessages( null );

        // stopself after timeout
        shutdownHandler.postDelayed( this::stopSelf, NO_MESSAGE_SHUTDOWN_TIMEOUT.getMillis() );
    }


}
