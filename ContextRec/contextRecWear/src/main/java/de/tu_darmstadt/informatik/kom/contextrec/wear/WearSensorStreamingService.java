package de.tu_darmstadt.informatik.kom.contextrec.wear;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.LocalDataSource;
import java8.util.Optional;

import static de.tu_darmstadt.informatik.kom.contextrec.wear.WearDataSources.defaultWearReducedDataSource;

/**
 * Created by Tobias on 02.08.2017
 */

public class WearSensorStreamingService extends NonStopIntentService  {


    public static final String TAG = "WearStreamingService";
    public static final String PARAMETER_RESULT_RECEIVER_ID = "Result node ID";
    public static final String PARAMETER_REQUEST_ID = "Request ID";
    public static final String PARAMETER_FROM = "From";
    public static final String PARAMETER_TO = "To";

    private static final Duration NO_MESSAGE_SHUTDOWN_TIMEOUT = Duration.standardMinutes( 45 );

    private LocalDataSource dataSource;
    private GoogleApiClient apiClient;
    private Handler shutdownHandler;

    public WearSensorStreamingService() {
        super( "ContextSense Streaming Service for Android Wear" );
    }

    @Override
    public void onCreate() {
        super.onCreate();

        shutdownHandler = new Handler( Looper.getMainLooper() );
        dataSource = (LocalDataSource) defaultWearReducedDataSource( this );
        apiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .build();

        dataSource.startStreaming();

        Log.i(TAG, "Created Streaming Service and started Streaming");
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Wear Streaming service destroyed!");

        dataSource.stopStreaming();
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent( Intent intent ) {
        // may happen when the service was restarted - we don't care.
        if (intent == null) return;
        if (!intent.hasExtra( PARAMETER_FROM )) return;

        PowerManager powerManager = (PowerManager) getSystemService( POWER_SERVICE );
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock( PowerManager.PARTIAL_WAKE_LOCK, UUID.randomUUID().toString() );
        try {
            wakeLock.acquire(1000);
            ensureApiClientIsConnected();

            // we'll subtract 500 ms from the batch duration to compensate the processing & transmission delay between wear and phone.
            DateTime from = new DateTime( intent.getLongExtra( PARAMETER_FROM, 0 ) );
            DateTime to = new DateTime( intent.getLongExtra( PARAMETER_TO, 0 ) );

            Log.i(TAG, "Getting data FROM: " + from.toString() + " TO: " + to.toString());

            Optional<SensorDataBatch> batch = dataSource.collectStreamedData( from, to);
            batch.ifPresent( data -> {
                String receiver = intent.getStringExtra( PARAMETER_RESULT_RECEIVER_ID );
                byte[] message = serialize( data );
                Wearable.MessageApi.sendMessage( apiClient, receiver, "/streamResult", message )
                        .setResultCallback( c -> {
                            if(c.getStatus().isSuccess()) Log.i( TAG, "Response sent to " + receiver + " size: " + message.length);
                            else Log.i(TAG, "Response Could not be sent! Message: " + c.getStatus().getStatusMessage());
                        });
            });
        } finally {
            wakeLock.release();
            resetShutdownTimeout();
        }
    }

    private void ensureApiClientIsConnected() {
        if (!apiClient.isConnected()) {
            ConnectionResult result = apiClient.blockingConnect();
            if (!result.isSuccess()) {
                throw new IllegalStateException( "Could not connect to Google API client: " +result.getErrorMessage() + " " +result.getResolution() );
            }
        }
    }

    private byte[] serialize( SensorDataBatch batch ) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try(ObjectOutputStream objectOut = new ObjectOutputStream( new GZIPOutputStream( out ) )) {
            objectOut.writeObject( batch );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return out.toByteArray();
    }

    private void resetShutdownTimeout() {
        // remove all pending requests
        shutdownHandler.removeCallbacksAndMessages( null );

        // stopself after timeout
        shutdownHandler.postDelayed( this::stopSelf, NO_MESSAGE_SHUTDOWN_TIMEOUT.getMillis() );
    }

}
