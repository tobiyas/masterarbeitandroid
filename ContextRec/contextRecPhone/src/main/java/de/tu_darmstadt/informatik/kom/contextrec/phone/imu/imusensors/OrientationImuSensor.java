package de.tu_darmstadt.informatik.kom.contextrec.phone.imu.imusensors;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.List;

import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.BTEntry;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorEventCopy;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorEventQueue;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.OrientationData;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 27.08.2017
 */

public class OrientationImuSensor extends AbstractCalibrationIMUSSensor {

    /**
     * The Queue to use.
     */
    private final SensorEventQueue queue = new SensorEventQueue(60 * 50);


    public OrientationImuSensor( Context context, BluetoothGatt server ) {
        super( context, server, BTEntry.ORIENTATION );
    }


    @Override
    public Optional<? extends SensorData> getStreamingResults( DateTime from, DateTime to ) {
        return Optional.of(
                convertToResult(
                        queue.getFromToCollected(from,to),
                        new Duration(from,to)
                )
        );
    }


    @Override
    protected void gotNewCharacteristicsIntern( BluetoothGattCharacteristic characteristic ) {
        //Check for the format:
        if(characteristic.getValue().length < 12) return;

        try{
            float orientationX = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 0) / 100f;
            float orientationY = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 2) / 100f;
            float orientationZ = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16, 4) / 100f;

            long seconds = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 6);
            int ticks = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 10);

            SensorEventCopy copy = new SensorEventCopy(
                    calcCalibratedTime( seconds, ticks ),
                    new float[]{orientationX,orientationY,orientationZ},
                    0,
                    null
                );
            
            queue.add(copy);
        }catch ( Throwable exp ){
            exp.printStackTrace();
        }

    }


    private OrientationData convertToResult(List<SensorEventCopy> events, Duration batchDuration ) {
        long[] t = stream( events )
                .mapToLong( event -> event.timestamp )
                .toArray();
        double[] x = stream( events )
                .mapToDouble( event -> event.values[0] )
                .toArray();
        double[] y = stream( events )
                .mapToDouble( event -> event.values[1] )
                .toArray();
        double[] z = stream( events )
                .mapToDouble( event -> event.values[2] )
                .toArray();

        return new OrientationData(
                DateTime.now().getMillis(), t, x, y, z
        );
    }

}
