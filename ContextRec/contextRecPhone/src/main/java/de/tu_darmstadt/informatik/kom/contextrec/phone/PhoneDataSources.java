package de.tu_darmstadt.informatik.kom.contextrec.phone;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.ImuConnector;
import de.tu_darmstadt.informatik.kom.contextrec.phone.wearstreams.WearStreamingSource;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.DataSources;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.LocalDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.AccelerometerHistorySensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.AccelerometerSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.ActivitySensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.AmbientTemperatureSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.AudioStateSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.GpsSpeedHistorySensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.GpsStatusSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.GyroscopeSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.HumiditySensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.LightSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.LinearAccelerationSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.LocationSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.MagnetometerHistorySensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.MagnetometerSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.MicrophoneSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.NearbyPlacesSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.PhoneStateSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.PowerStateSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.PressureSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.ProximitySensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.ScreenStateSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.StepCounterSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.WeatherSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.WifiSensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import java8.util.Optional;
import java8.util.function.Consumer;
import java8.util.function.Supplier;
import java8.util.stream.Collectors;

import static de.tu_darmstadt.informatik.kom.contextrec.phone.WearDataSource.WEAR_CAPABILITY_NAME;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 10.02.2016
 */
public class PhoneDataSources {

    public static DataSource phoneSensors( Context context, Sensor... extraSensors ) {
        List<Sensor> allSensors = new ArrayList<>( Arrays.asList( extraSensors ) );

        allSensors.add( new AccelerometerSensor( context ) );
        allSensors.add( new AccelerometerHistorySensor( context ) );
        allSensors.add( new LinearAccelerationSensor( context ) );
        allSensors.add( new ActivitySensor( context ) );
        allSensors.add( new AmbientTemperatureSensor( context ) );
//        allSensors.add( new CalendarSensor( context ) );
        allSensors.add( new GyroscopeSensor( context ) );
        allSensors.add( new HumiditySensor( context ) );
        allSensors.add( new LightSensor( context ) );
        allSensors.add( new LocationSensor( context ) );
        allSensors.add( new MagnetometerSensor( context ) );
        allSensors.add( new MagnetometerHistorySensor( context ) );
        allSensors.add( new MicrophoneSensor( context ) );
        allSensors.add( new PowerStateSensor( context ) );
        allSensors.add( new PressureSensor( context ) );
        allSensors.add( new ProximitySensor( context ) );
        allSensors.add( new ScreenStateSensor( context ) );
        allSensors.add( new StepCounterSensor( context ) );
        allSensors.add( new WifiSensor( context ) );
        allSensors.add( new PhoneStateSensor( context ) );
        allSensors.add( new AudioStateSensor( context ) );
        allSensors.add( new GpsStatusSensor( context ) );
        allSensors.add( new GpsSpeedHistorySensor( context ) );
        allSensors.add( new NearbyPlacesSensor( context ) );
        allSensors.add( new WeatherSensor( context ) );

        return new LocalDataSource( context, allSensors, DeviceType.PHONE, DataSources.PHONE );
    }


    /**
     * This set of Sources only contains Sources related to inertial Sensors.
     * It also does NOT contain History sensors.
     * This is ment mostly for Streaming support. Most of those Sensors are not used.
     *
     * @param context to use for loading.
     * @param extraSensors extra sensors to add.
     * @return the generated Source.
     */
    public static DataSource inertialPhoneSensors( Context context, Sensor... extraSensors ) {
        List<Sensor> allSensors = new ArrayList<>( Arrays.asList( extraSensors ) );

        allSensors.add( new AccelerometerSensor( context ) );
        allSensors.add( new LinearAccelerationSensor( context ) );
        allSensors.add( new GyroscopeSensor( context ) );
        allSensors.add( new MagnetometerSensor( context ) );

        return new LocalDataSource( context, allSensors, DeviceType.PHONE, DataSources.PHONE );
    }

    public static DataSource wearConnector( Context context ) {
        return new WearDataSource( context );
    }


    /**
     * This starts an async scan for Devices and adds them to the SourceList.
     * @param context to use.
     * @param dataSourceConsumer to add to.
     */
    public static void thunderboardIMUConnectors(Context context, Consumer<DataSource> dataSourceConsumer){
        if(context == null || dataSourceConsumer == null) return;
        new ImuConnector(context).getConnected(dataSourceConsumer, 10);
    }

    /**
     * Creates Streaming Nodes for Wearable Connectors.
     * They have to be created async, because the Google API does not allow sync calls to their API client.
     *
     * @param context to create to.
     * @param consumer to execute on finish.
     */
    public static void wearStreamingConnectors( Context context, Consumer<Set<DataSource>> consumer ) {
        Executors.newSingleThreadExecutor().submit(
                () -> consumer.accept(
                        stream( findWearDevices( context ) )
                            .map( Node::getId )
                            .map( n -> new WearStreamingSource( context, n, true ) )
                            .collect( Collectors.toSet() )
                )
        );
    }


    /**
     * Finds the next wear device.
     * @return the next wear device.
     */
    private static Set<Node> findWearDevices( Context context ) {
         GoogleApiClient apiClient = new GoogleApiClient.Builder( context )
                .addApi( Wearable.API )
                .build();

        ConnectionResult result = apiClient.blockingConnect();
        if (!result.isSuccess()) {
            Log.e( "WearCreate", "Could not connect to Google API:" + result.getErrorMessage() + " " +result.getResolution() );
            return new HashSet<>();
        }

        Set<Node> nodes = Wearable.CapabilityApi
                .getCapability( apiClient, WEAR_CAPABILITY_NAME, CapabilityApi.FILTER_REACHABLE )
                .await()
                .getCapability()
                .getNodes();

        return nodes;
    }
}
