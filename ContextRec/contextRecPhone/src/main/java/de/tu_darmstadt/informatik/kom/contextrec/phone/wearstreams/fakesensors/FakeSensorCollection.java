package de.tu_darmstadt.informatik.kom.contextrec.phone.wearstreams.fakesensors;

import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;

import de.tu_darmstadt.informatik.kom.contextrec.phone.utils.Tuple;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.batch.MutableSensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import java8.util.Optional;
import java8.util.stream.Stream;
import java8.util.stream.StreamSupport;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 19.08.2017
 */

public class FakeSensorCollection {

    /**
     * The fake map. With all it's present data.
     */
    private final Map<Class<? extends SensorData>,FakeSensor> fakeSensorMap;


    public FakeSensorCollection(){
        this.fakeSensorMap = new HashMap<>();
    }

    /**
     * Applies the Sensor batch.
     * @param batch to apply from.
     */
    public void applyDataBatch(SensorDataBatch batch){
        stream( batch.getAllData() )
                .map(d -> new ApplyTuple(get(d), d))
                .forEach(ApplyTuple::apply);
    }


    /**
     * Retrieves the Sensor from the Data. Creates a new One if no is present.
     * @param data to get from.
     * @return the Sensor.
     */
    private FakeSensor get(SensorData data){
        Class<? extends SensorData> clazz = data.getClass();
        FakeSensor fakeSensor = fakeSensorMap.get(clazz);
        if(fakeSensor != null) return fakeSensor;

        fakeSensor = new FakeSensor();
        fakeSensorMap.put(clazz, fakeSensor);
        return fakeSensor;
    }

    /**
     * This clears all Sensors and Data from the Collection.
     */
    public void clear() {
        fakeSensorMap.clear();
    }


    //// Just a small Class to apply Data directly.
    private static class ApplyTuple extends Tuple<FakeSensor,SensorData> {
        public ApplyTuple(FakeSensor first, SensorData second) { super(first, second); }
        protected void apply(){ getFirst().apply(getSecond()); }
    }


    /**
     * Creates a batch of Data from to specific time.
     * @param from the time to get From.
     * @param to the time to get To
     * @return the Batch of Data passed.
     */
    public Optional<SensorDataBatch> createDataBatch(DateTime from, DateTime to){
        if(fakeSensorMap.isEmpty()) return Optional.empty();

        MutableSensorDataBatch batch = new MutableSensorDataBatch();
        batch.setStartTime(from);
        batch.setEndTime(to);

        stream( fakeSensorMap.values() )
                .map(s -> s.getData(from,to))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach( batch::addData );

        return Optional.of(batch);
    }

}
