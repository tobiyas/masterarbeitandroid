package de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils;

import android.bluetooth.BluetoothGatt;

import java.nio.ByteBuffer;
import java.util.UUID;

import de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayUtils;
import java8.util.Optional;

import static de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.ThunderBoardUuids.*;

/**
 * Created by Toby on 27.08.2017
 */

public class BTEntry {

    public static final BTEntry ORIENTATION = new BTEntry(UUID_SERVICE_ACCELERATION_ORIENTATION, UUID_CHARACTERISTIC_ORIENTATION, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION);
    public static final BTEntry ACCELEROMETER = new BTEntry(UUID_SERVICE_ACCELERATION_ORIENTATION, UUID_CHARACTERISTIC_ACCELERATION, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION);
    public static final BTEntry GYROSCOPE = new BTEntry(UUID_SERVICE_ACCELERATION_ORIENTATION, UUID_CHARACTERISTIC_GYROSCOPE, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION);
    public static final BTEntry CSC = new BTEntry(UUID_SERVICE_CSC, UUID_CHARACTERISTIC_CSC_MEASUREMENT, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION);

    public static final BTEntry DEVICE_NAME = new BTEntry(UUID_SERVICE_GENERIC_ACCESS, UUID_CHARACTERISTIC_DEVICE_NAME, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION);
    public static final BTEntry DEVICE_SERIAL = new BTEntry(UUID_SERVICE_DEVICE_INFORMATION, UUID_CHARACTERISTIC_SERIAL_NUMBER, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION);
    public static final BTEntry DEVICE_BATTERY = new BTEntry(UUID_SERVICE_BATTERY, UUID_CHARACTERISTIC_BATTERY_LEVEL, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION);

    public static final BTEntry CALIBRATE = new BTEntry(UUID_SERVICE_ACCELERATION_ORIENTATION, UUID_CHARACTERISTIC_CALIBRATE, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION);

    public static final BTEntry LEDS = new BTEntry( UUID_SERVICE_USER_INTERFACE, UUID_CHARACTERISTIC_RGB_LEDS, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION);


    /**
     * The Service to use.
     */
    private final UUID service;

    /**
     * The Characteristics to use.
     */
    private final UUID characteristics;

    /**
     * The Descriptor to use.
     */
    private final UUID descriptor;


    /**
     * Creates an new BL Entry.
     * @param service to use.
     * @param characteristics to use.
     * @param descriptor to use.
     */
    private BTEntry( UUID service, UUID characteristics, UUID descriptor ) {
        this.service = service;
        this.characteristics = characteristics;
        this.descriptor = descriptor;
    }


    /**
     * Registers a new Notifier.
     * @param gatt to use.
     * @return true if worked.
     */
    public boolean register( BluetoothGatt gatt ){
        return BleUtils.setCharacteristicNotification(gatt,
                service,
                characteristics,
                descriptor,
                true);
    }

    /**
     * Unregisters the notifier.
     * @param gatt to use.
     */
    public void unregister( BluetoothGatt gatt ){
        BleUtils.setCharacteristicNotification(gatt,
                service,
                characteristics,
                descriptor,
                false);
    }

    /**
     * Does a Write on the device.
     * @param gatt to use.
     * @param data to write
     * @return true if write command is sent.
     */
    public boolean write( BluetoothGatt gatt, byte[] data, int format){
        return BleUtils.writeCharacteristic(
                gatt,
                getService(),
                getCharacteristics(),
                ByteBuffer.wrap( data ).getInt(),
                format, 0);
    }

    /**
     * Does a Write on the device.
     * @param gatt to use.
     * @param data to write
     * @return true if write command is sent.
     */
    public boolean write( BluetoothGatt gatt, byte[] data ){
        return BleUtils.writeCharacteristic(
                gatt,
                getService(),
                getCharacteristics(),
                data);
    }


    /**
     * Does a Write on the device.
     * @param gatt to use.
     * @param value to send
     * @param format to use
     * @param offset to use.
     * @return true if write command is sent.
     */
    public boolean write( BluetoothGatt gatt, int value, int format, int offset ){
        return BleUtils.writeCharacteristic(
                gatt,
                getService(),
                getCharacteristics(),

                value, format, offset);
    }


    /**
     * Does a Read on the device.
     * @param gatt to use.
     * @return true if write command is sent.
     */
    public boolean read( BluetoothGatt gatt ){
        return BleUtils.readCharacteristic(
                gatt,
                getService(),
                getCharacteristics());
    }

    /**
     * Does a Read on Cache of the device for a string value.
     * @param gatt to use.
     * @param offset to use for reading.
     * @return the Cached value if present, or an empty Optional if not.
     */
    public Optional<String> readCacheString( BluetoothGatt gatt, int offset ){
        if(gatt == null) return Optional.empty();

        return Optional.ofNullable( gatt.getService(getService() ) )
                .map( s -> s.getCharacteristic( getCharacteristics() ) )
                .map( c -> c.getStringValue( offset ));
    }

    /**
     * Does a Read on Cache of the device for a int value.
     * @param gatt to use.
     * @param format to use for reading.
     * @param offset to use for reading.
     * @return the Cached value if present, or an empty Optional if not.
     */
    public Optional<Integer> readCacheInt( BluetoothGatt gatt, int format, int offset ){
        if(gatt == null) return Optional.empty();

        return Optional.ofNullable( gatt.getService(getService() ) )
                .map( s -> s.getCharacteristic( getCharacteristics() ) )
                .map( c -> c.getIntValue( format, offset ));
    }

    /**
     * Does a Read on Cache of the device for a float value.
     * @param gatt to use.
     * @param format to use for reading.
     * @param offset to use for reading.
     * @return the Cached value if present, or an empty Optional if not.
     */
    public Optional<Float> readCacheFloat( BluetoothGatt gatt, int format, int offset ){
        if(gatt == null) return Optional.empty();

        return Optional.ofNullable( gatt.getService(getService() ) )
                .map( s -> s.getCharacteristic( getCharacteristics() ) )
                .map( c -> c.getFloatValue( format, offset ));
    }

    /**
     * Does a Read on Cache of the device for a byte[] value.
     * @param gatt to use.
     * @return the Cached value if present, or an empty Optional if not.
     */
    public Optional<Byte[]> readCacheBytes( BluetoothGatt gatt ){
        if(gatt == null) return Optional.empty();

        return Optional.ofNullable( gatt.getService(getService() ) )
                .map( s -> s.getCharacteristic( getCharacteristics() ) )
                .map( c -> ArrayUtils.convert( c.getValue() ) );
    }


    /**
     * Sets the Indication as wanted.
     * @param gatt to set to.
     * @param value to set to.
     */
    public void setIndication(BluetoothGatt gatt, boolean value ) {
        BleUtils.setCharacteristicIndication( gatt, service, characteristics, descriptor, value );
    }


    public UUID getService() {
        return service;
    }

    public UUID getCharacteristics() {
        return characteristics;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BTEntry)) return false;

        BTEntry other = (BTEntry) obj;
        return other.service.equals(service)
                && other.descriptor.equals(descriptor)
                && other.characteristics.equals(characteristics);
    }

    @Override
    public int hashCode() {
        return service.hashCode() + 1_000*descriptor.hashCode() + 1_000_000*characteristics.hashCode();
    }
}
