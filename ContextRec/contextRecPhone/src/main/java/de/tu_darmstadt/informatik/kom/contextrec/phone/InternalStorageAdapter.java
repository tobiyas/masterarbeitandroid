package de.tu_darmstadt.informatik.kom.contextrec.phone;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Version of {@link StorageAdapter} which will store files in the internal storage (within the App's local, private storage).
 */
public class InternalStorageAdapter implements StorageAdapter {

    private static final String TAG = "InternalStorageAdapter";
    private static final String DEFAULT_SUBDIR_NAME = "detector_states";

    private final File baseDir;
    private final Context context;

    public InternalStorageAdapter( Context context ) {
        this( context, new File( context.getFilesDir(), DEFAULT_SUBDIR_NAME ) );
    }

    public InternalStorageAdapter( Context context, File baseDir ) {
        this.context = context;
        this.baseDir = baseDir;
    }

    @Override
    public OutputStream store( String detectorId ) throws IOException {
        if (!baseDir.exists() && !baseDir.mkdir()) {
            Log.e( TAG, "Could not create dir "+ baseDir.getAbsolutePath() );
        }

        File path = new File( baseDir, detectorId );
        return new FileOutputStream( path );
    }

    @Override
    public InputStream load( String detectorId ) throws IOException {
        File path = new File( baseDir, detectorId );
        return new FileInputStream( path );
    }

    @Override
    public boolean hasData( String detectorId ) {
        File path = new File( baseDir, detectorId );
        return path.exists();
    }
}
