package de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by Toby on 23.08.2017.
 */

public class ThreadUtils {

    public static void RunOnMainthread(Runnable runnable){
        new Handler(Looper.getMainLooper()).post(runnable);
    }


    public static void RunOnMainthreadLater(Runnable runnable, long timeInMs){
        new Handler(Looper.getMainLooper()).postDelayed(runnable, timeInMs);
    }
}
