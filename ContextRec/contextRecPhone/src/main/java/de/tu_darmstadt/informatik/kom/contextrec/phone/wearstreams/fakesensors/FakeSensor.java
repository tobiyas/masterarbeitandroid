package de.tu_darmstadt.informatik.kom.contextrec.phone.wearstreams.fakesensors;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.Collections;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.MergeableSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorDataQueue;
import java8.util.Optional;

/**
 * Created by Tobias on 19.08.2017
 */

public class FakeSensor {

    /**
     * The data to get.
     */
    private final SensorDataQueue sensorData = new SensorDataQueue(60 * 60);


    /**
     * Applies sensor Data to the Sensor.
     * @param data to apply to the Sensor.
     */
    public void apply(SensorData data){
        sensorData.add(data);
    }


    /**
     * Gets the data from-To.
     * @param from to get for
     * @param to to get for
     * @return the Data from - to.
     */
    public Optional<List<SensorData>> getData(DateTime from, DateTime to) {
        if(sensorData.isEmpty()) return Optional.empty();

        List<SensorData> datas = sensorData.getFromToCollected(from.minus(Duration.standardSeconds(1)),to.plus(Duration.standardSeconds(1)));
        if(datas.isEmpty()) return Optional.empty();

        //Now merge the rest into them if is mergeable:
        if(datas.get(0) instanceof  MergeableSensorData) {
            MergeableSensorData first = null;
            for (SensorData data : datas) {
                MergeableSensorData mergeableData = (MergeableSensorData) data;
                //Startup:
                if (first == null) {
                    first = mergeableData.copy();
                    continue;
                }

                //Merge the Rest:
                first.merge(data);
                first.limitFromTo(from, to);
            }

            return Optional.ofNullable(Collections.singletonList(first));
        }else{
            return Optional.of(datas);
        }
    }
}
