package de.tu_darmstadt.informatik.kom.contextrec.phone;

import android.util.Log;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.utils.TimingsRecords;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.IDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.FailingPreconditionFinder;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.PreconditionEvaluator;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.visitor.PreconditionPrinter;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 11.12.2015
 */
public class DetectionRunner {

    /**
     * A simple and easy to use class for detecting stuff.
     * @param batch to use for detection.
     * @param detectors to apply the data to.
     * @return the detected result.
     *          Throws an error if something goes wrong!
     */
    public static DetectionResult runDetection( SensorDataBatch batch, List<IDetector> detectors ) {
        return runDetectionWithTimings( batch, detectors ).getFirst();
    }

    /**
     * A simple and easy to use class for detecting stuff.
     * @param batch to use for detection.
     * @param detectors to apply the data to.
     * @return the detected result and timings table.
     *          Throws an error if something goes wrong!
     */
    public static Pair<DetectionResult,TimingsRecords> runDetectionWithTimings(SensorDataBatch batch, List<IDetector> detectors ) {
        DetectionResult result = new DetectionResult( batch.getStartTime() );
        Set<IDetector> remaining = new HashSet<>( detectors );
        TimingsRecords timings = new TimingsRecords();

        while( !remaining.isEmpty() ) {
            timings.startAction( "Preconditions" );
            PreconditionEvaluator preconditionEval = new PreconditionEvaluator( batch, result );

            Optional<IDetector> applicableDetector = stream( remaining )
                    .filter( d -> stream( d.getDependentLabelGroups() ).allMatch( result::hasResultForGroup ) )
                    .filter( d -> d.getPrecondition().accept( preconditionEval ) )
                    .findAny();

            timings.endCurrentAction();
            if ( applicableDetector.isPresent() ) {
                IDetector detector = applicableDetector.get();
                try {
                    remaining.remove( detector );
                    result.addResult( detector.detect( batch, result, timings ) );
                } catch (Exception e) {
                    e.printStackTrace();
                }

                timings.endCurrentAction();
            } else {
                printMissingPreconditions( remaining, batch, result );
                break;
            }
        }

        return new Pair<>( result, timings );
    }

    private static void printMissingPreconditions( Set<IDetector> remainingDetectors, SensorDataBatch batch, DetectionResult result ) {
        FailingPreconditionFinder finder = new FailingPreconditionFinder( batch, result );
        PreconditionPrinter printer = new PreconditionPrinter();

        StringBuilder sb = new StringBuilder();
        sb.append( "Could not run all detectors. Detectors left:\n" );

        for( IDetector detector: remainingDetectors ) {
            Precondition failingPrecondition = detector.getPrecondition().accept( finder ).get();

            sb.append( detector.getDescription().getId() );
            sb.append( " due to missing precondition: " );
            sb.append( failingPrecondition.accept( printer ) );
            sb.append( "\n" );
        }

        Log.w( "SESSION", sb.toString() );
    }

}
