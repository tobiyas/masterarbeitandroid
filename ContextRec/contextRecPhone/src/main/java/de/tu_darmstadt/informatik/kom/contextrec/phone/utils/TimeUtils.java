package de.tu_darmstadt.informatik.kom.contextrec.phone.utils;

import java.util.Date;

/**
 * Utilities for time calculations.
 */
public class TimeUtils {

    /**
     * Converts the timestamp provided by a (SensorManager) sensor to the wall clock time (in MS).
     * @param sensorTimestamp in ns
     * @return wallclock time in ms
     */
    public static long sensorTimestampToWallclockMillis( long sensorTimestamp ) {
        // todo: This might be expensive. Maybe we should cache the basetime. Check it!
        return (new Date()).getTime() + (sensorTimestamp - System.nanoTime()) / 1_000_000L;
    }
}
