package de.tu_darmstadt.informatik.kom.contextrec.phone;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;

/**
 * Listener for detected situations.
 * Will get results for ALL detected situations.
 */
public interface DetectionResultListener {
    void onResult( SensorDataBatch sensorData, DetectionResult result );
}
