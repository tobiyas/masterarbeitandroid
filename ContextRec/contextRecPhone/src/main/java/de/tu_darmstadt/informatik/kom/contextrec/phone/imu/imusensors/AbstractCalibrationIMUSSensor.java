package de.tu_darmstadt.informatik.kom.contextrec.phone.imu.imusensors;

import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.util.Log;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.BTEntry;
import java8.util.stream.LongStreams;

/**
 * Created by Toby on 26.09.2017
 */

public abstract class AbstractCalibrationIMUSSensor extends AbstractImuSensor {



    /**
     * The Max the Ticks may reach.
     * There seems to be a great difference between Calculated and Actual value.
     * The Calculated value of max ticks from the IMU is 16 ^ 4 (65 536).
     * Through observations, the highest value reached (in tests) is 32766.
     *
     *
     * MAX MEASURED: 32766
     * CALCULATED:  16 * 16 * 16 * 16
     */
    private static final int MAX_TICKS = 32766;

    /**
     * The Highest number of Ticks registered:
     */
    private static final int CALIBRATION_SIZE = 50 * 10;

    /**
     * The amount of values off, so we need to recalibrate:
     */
    private static final int MAX_RECALIBRATE = 30;


    /**
     * The Start time of the Device for calibration.
     */
    private long calibrationStart = -1;

    /**
     * The array to help for calibration.
     */
    private long[] calibrations = new long[CALIBRATION_SIZE];

    /**
     * The current pointer to the Calibration array.
     */
    private int calibrationSet = 0;


    protected AbstractCalibrationIMUSSensor( Context context, BluetoothGatt server, BTEntry entry ) {
        super( context, server, entry );
    }

    /**
     * Calculate the Time. This includes Calibration.
     *
     * @param seconds the Board tells.
     * @param ticks the Board tells
     * @return the Time estimated this was.
     */
    protected long calcCalibratedTime(long seconds, int ticks){
        //Check if in calibration mode:
        if(calibrationStart == -1){
            this.calibrations[calibrationSet++] = System.currentTimeMillis() - ( seconds * 1000 ) - (long)( ( (double)ticks / (double)MAX_TICKS) * 1000D );

            //Calibrate:
            if(calibrationSet >= CALIBRATION_SIZE){
                calibrationStart = (long) LongStreams.of( calibrations )
                        .average()
                        .getAsDouble();

                //Reset the calibration data!
                calibrationSet = 0;
                return calibrationStart + (seconds * 1000L) + (long)( ( (double)ticks / (double)MAX_TICKS) * 1000D );
            }

            return System.currentTimeMillis();
        }

        long calibrated = calibrationStart + (seconds * 1000L) + (long)( ( (double)ticks / (double)MAX_TICKS) * 1000D );
        //Check if we need to recalibrate:
        if( Math.abs( calibrated - System.currentTimeMillis() ) > 200){
            calibrationSet++;

            //We need to recalibrate:
            if(calibrationSet >= MAX_RECALIBRATE){
                this.calibrationStart = -1;
                this.calibrationSet = 0;
                //Log.i("RECALIBRATE", "Recalibrating SensorTimer: " + getClass().getSimpleName() );
            }
        }

        return calibrated;
    }
}
