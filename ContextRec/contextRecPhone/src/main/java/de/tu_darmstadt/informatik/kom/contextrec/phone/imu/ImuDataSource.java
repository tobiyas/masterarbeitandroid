package de.tu_darmstadt.informatik.kom.contextrec.phone.imu;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.imusensors.AbstractImuSensor;
import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.ThunderBoard;
import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.ThunderBoard.FlashStep;
import de.tu_darmstadt.informatik.kom.contextsense.CalibrateableDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.StreamingDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.Vibrateable;
import de.tu_darmstadt.informatik.kom.contextsense.batch.MutableSensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.StreamableSensor;
import de.tu_darmstadt.informatik.kom.contextsense.utils.CancelableFuture;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 20.08.2017
 */

public class ImuDataSource extends DataSource implements StreamingDataSource, Closeable, CalibrateableDataSource, Vibrateable {

    /**
     * The IMU Prefix for the Device.
     */
    public static final String BOARD_DEVICE_PRE = "IMU_";

    /**
     * The task handler to read the battery.
     */
    private final Handler batteryHandler;


    /**
     * The context to use for calls.
     */
    private final Context context;

    /**
     * The device to get a stream result from.
     */
    private final ThunderBoard board;

    /**
     * The Sources to listen to.
     */
    private final List<Sensor> sensors = new ArrayList<>();


    /**
     * Creates a new Source with the Device and a context.
     * @param context to use for calls.
     * @param board to use for streaming.
     */
    public ImuDataSource( Context context, ThunderBoard board, List<Sensor> sensors ){
        super( DeviceType.IMU_THUNDERBOARD, BOARD_DEVICE_PRE + board.getSerial() );

        this.batteryHandler = new Handler( Looper.getMainLooper() );
        this.context = context;
        this.board = board;
        this.sensors.addAll(sensors);

        this.batteryHandler.postDelayed( this::battery, 5_000 );

        //Register all Callback stuff and Entries:
        board.registerCharacteristicCallback( this::gotCharacteristics );
        stream( sensors )
                .filter( s -> s instanceof AbstractImuSensor )
                .map( s -> (AbstractImuSensor) s )
                .map( AbstractImuSensor::getEntry )
                .forEach( board::addEntry );
    }


    /**
     * Starts to read the Battery of the Device.
     */
    private void battery(){
        board.readBattery();
        this.batteryHandler.postDelayed( this::battery, 5_000 );
    }

    /**
     * We got a characteristic.
     * @param characteristic we got.
     */
    protected void gotCharacteristics( BluetoothGattCharacteristic characteristic ){
        UUID serviceID = characteristic.getService().getUuid();
        UUID charID = characteristic.getUuid();

        stream( sensors )
            .filter( s -> s instanceof AbstractImuSensor )
            .map( s -> (AbstractImuSensor) s )
            .filter( s -> s.getEntry().getService().equals( serviceID )
                    && s.getEntry().getCharacteristics().equals( charID ) )
            .forEach( s -> s.gotNewCharacteristics( characteristic ) );
    }

    @Override
    public void calibrate() {
        this.board.startCalibration();
    }

    @Override
    public void startStreaming() {
        board.startReading();
    }


    @Override
    public void stopStreaming() {
        board.stopReading();
    }


    @Override
    public Optional<SensorDataBatch> collectStreamedData(DateTime from, DateTime to) {
        MutableSensorDataBatch batch = new MutableSensorDataBatch();
        batch.setStartTime( from );
        batch.setEndTime( to );

        stream( sensors )
                .filter(StreamableSensor.class::isInstance)
                .map(StreamableSensor.class::cast)
                .map(s -> s.getStreamingResults(from,to))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach( d -> {
                    d.claimSourceIfUnset( getName(), getType() );
                    batch.addData(d);
                } );

        return batch.isEmpty() ? Optional.empty() : Optional.of( batch );
    }

    @Override
    public Set<String> getRequiredPermissions() {
        return null;
    }


    @Override
    protected void doStart() {
        //Not implemented.
    }

    @Override
    protected void doStop() {
        //Not implemented.
    }

    @Override
    protected CancelableFuture<Optional<SensorDataBatch>> doCollectData(Duration batchDuration) {
        return new CancelableFuture<>(Optional::empty);
    }

    @Override
    public void close() throws IOException {
        //Close the BT connection
        board.close();

        //stop reading the battery.
        batteryHandler.removeCallbacksAndMessages( null );
    }

    @Override
    public void vibrate() {
        board.flashLEDs(
                new FlashStep( 0xFF, 0x00, 0x00, 1000 ),
                new FlashStep( 0x00, 0x00, 0x00, 1000 ),
                new FlashStep( 0x00, 0xFF, 0x00, 1000 ),
                new FlashStep( 0x00, 0x00, 0x00, 1000 ),
                new FlashStep( 0x00, 0x00, 0xFF, 1000 )
            );
    }


    /**
     * Gets the current Battery level of the Board.
     *
     * @return the level between 0 and 100 in Percent.
     * Returns -1 if no value is read yet!
     */
    public double getBatteryLevel(){
        return board.getCurrentBatteryLevel();
    }
}
