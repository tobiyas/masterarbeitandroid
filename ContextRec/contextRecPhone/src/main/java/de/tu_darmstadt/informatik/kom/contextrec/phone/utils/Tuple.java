package de.tu_darmstadt.informatik.kom.contextrec.phone.utils;

/**
 * Created by Tobias on 19.08.2017
 */

public class Tuple<S,T> {

    private final S first;
    private final T second;

    public Tuple(S first, T second){
        this.first = first;
        this.second = second;
    }

    public S getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }
}
