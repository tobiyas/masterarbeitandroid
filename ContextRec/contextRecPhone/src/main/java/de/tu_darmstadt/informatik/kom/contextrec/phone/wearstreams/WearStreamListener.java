package de.tu_darmstadt.informatik.kom.contextrec.phone.wearstreams;

import android.util.Log;

import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;

import java.io.ByteArrayInputStream;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;

import de.tu_darmstadt.informatik.kom.contextrec.phone.wearstreams.fakesensors.FakeSensorCollection;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.utils.SerialVersionUidIgnoringObjectInputStream;
import java8.util.Optional;

/**
 * Created by Tobias on 19.08.2017
 */

public class WearStreamListener implements MessageApi.MessageListener {

    private static final String TAG = "WearStream";

    /**
     * This is the Path the Listener may listen to.
     * The Wear devices will ONLY stream on THAT path!
     */
    private static final String PATH_TO_LISTEN_TO = "streamWear/";

    /**
     * The Path pongs are received from.
     */
    private static final String PONG_PATH = "Pong/";

    /**
     * A new Thread to execute stuff on, since we don't want to flood the Main-Thread!
     */
    private final Executor executor = Executors.newSingleThreadExecutor();

    /**
     * The node to listen to.
     */
    private final String nodeID;

    /**
     * The fake sensors to copy stuff in them.
     */
    private final FakeSensorCollection fakeSensors;

    /**
     * The pingClockSkewer to use for reading the ClockSkew.
     */
    private final PingClockSkewer pingClockSkewer = new PingClockSkewer();


    /**
     * Creates a listener for a specific node.
     * @param nodeID to read on.
     * @param fakeSensors to use for pushing data to.
     */
    public WearStreamListener(String nodeID, FakeSensorCollection fakeSensors){
        this.nodeID = nodeID;
        this.fakeSensors = fakeSensors;
    }


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        //First check if we are listening to the correct stuff:
        if(messageEvent == null) return; //Should never be true!

        //Only listen to the Node we want to:
        if( !nodeID.equals( messageEvent.getSourceNodeId() ) ) return;

        String path = messageEvent.getPath();

        //If we have streaming data: Go for it:
        if(PATH_TO_LISTEN_TO.equals( path )) {
            //Prerequisites done! Now read data:
            executor.execute(() ->
                deserializeData( messageEvent.getData() )
                    .map( data -> data.applyClockSkew( (long) pingClockSkewer.getCurrentClockSkew() ) )
                    .ifPresent( fakeSensors::applyDataBatch )
            );

            return;
        }

        //Got a pong message: Let's try to get our Clock-Skew:
        if( PONG_PATH.equalsIgnoreCase( path ) ){
            pingClockSkewer.readFromPong( messageEvent.getData() );
            return;
        }

        Log.i(TAG, "Got unknown Path: " + path);
    }


    /**
     * This does deserialize the data the watch has sent.
     * @param data to deserialize
     * @return the Data batch or nothing if not possible.
     */
    private Optional<SensorDataBatch> deserializeData(byte[] data){
        try(ObjectInputStream in = new SerialVersionUidIgnoringObjectInputStream( new GZIPInputStream( new ByteArrayInputStream( data ) ), false )) {
            return Optional.of( (SensorDataBatch) in.readObject() );
        } catch (InvalidClassException e) {
            // probably wrong App-Version on the watch.
            Log.e( TAG, "Could not deserialize data from wear device in static stream." );
            return Optional.empty();
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }


    /**
     * This clears all data in the Listener.
     */
    public void clear() {
        this.fakeSensors.clear();
    }


    /**
     * Resets the Clock Skewer to force a new ClockSkew.
     */
    public void resetClockSkew(){
        this.pingClockSkewer.reset();
    }
}
