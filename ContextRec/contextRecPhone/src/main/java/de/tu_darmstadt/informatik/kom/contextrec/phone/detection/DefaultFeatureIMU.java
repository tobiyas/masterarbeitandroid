package de.tu_darmstadt.informatik.kom.contextrec.phone.detection;

import java.util.Arrays;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureBuilder;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LightSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LinearAccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MagnetometerData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.PressureSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayInterpolator;
import de.tu_darmstadt.informatik.kom.contextsense.utils.FFT;
import de.tu_darmstadt.informatik.kom.contextsense.utils.MathUtils;

import java8.util.OptionalDouble;
import java8.util.stream.DoubleStream;
import java8.util.stream.IntStream;
import java8.util.Optional;
import java8.util.stream.DoubleStreams;
import java8.util.stream.IntStreams;
import java8.util.stream.RefStreams;

import static de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureGroup.IMU_SENSORS;

/**
 * Created by Toby on 05.09.2017
 */

public class DefaultFeatureIMU {

    private static final double GRAVITY_ON_EARTH = 9.81;

    public static void registerDefaultFeatures(WekaFeatureBuilder registry, String dataSource ) {

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Avg. Acceleration (X)",
                sample -> sample
                        .getData(AccelerationData.class, dataSource)
                        .map(AccelerationData::getX)
                        .map(acc -> DoubleStreams.of(acc).average().getAsDouble())
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Avg. Acceleration (Y)", sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getY)
                .map(acc -> DoubleStreams.of(acc).average().getAsDouble())
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Avg. Acceleration (Z)", sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getZ)
                .map(acc -> DoubleStreams.of(acc).average().getAsDouble())
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Activity Count (Lin acc x)", sample -> sample
                .getData(LinearAccelerationData.class, dataSource)
                .map(LinearAccelerationData::getX)
                .map(data -> {
                    long samplesAboveThreshold = DoubleStreams.of(data)
                            .filter(d -> (Math.abs(d) > 0.5 * GRAVITY_ON_EARTH))
                            .count();

                    return samplesAboveThreshold / (double) data.length;
                })
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Activity Count (Lin acc y)", sample -> sample
                .getData(LinearAccelerationData.class, dataSource)
                .map(LinearAccelerationData::getY)
                .map(data -> {
                    long samplesAboveThreshold = DoubleStreams.of(data)
                            .filter(d -> (Math.abs(d) > 0.5 * GRAVITY_ON_EARTH))
                            .count();

                    return samplesAboveThreshold / (double) data.length;
                })
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Activity Count (Lin acc z)", sample -> sample
                .getData(LinearAccelerationData.class, dataSource)
                .map(LinearAccelerationData::getZ)
                .map(data -> {
                    long samplesAboveThreshold = DoubleStreams.of(data)
                            .filter(d -> (Math.abs(d) > 0.5 * GRAVITY_ON_EARTH))
                            .count();

                    return samplesAboveThreshold / (double) data.length;
                })
        );

        registry.registerMultiNumericFeature(IMU_SENSORS, dataSource + ": Acceleration X Sliced MiddleValue", 20, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map( AccelerationData::getX )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 20)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i, i + 10 ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerMultiNumericFeature(IMU_SENSORS, dataSource + ": Acceleration Y Sliced MiddleValue", 20, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map( AccelerationData::getY )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 20)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i, i + 10 ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerMultiNumericFeature(IMU_SENSORS, dataSource + ": Acceleration Z Sliced MiddleValue", 20, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map( AccelerationData::getZ )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 20)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i, i + 10 ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerMultiNumericFeature(IMU_SENSORS, dataSource + ": Acceleration (X) 0-10Hz", 30, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getX)
                .map( a -> ArrayInterpolator.interpolateLinear(a, 256) )
                .map(data -> {
                    double[] ffted = FFT.fft(Arrays.copyOf(data, 256));
                    return Arrays.copyOf(ffted, 30);
                })
        );

        registry.registerMultiNumericFeature(IMU_SENSORS, dataSource + ": Acceleration (Y) 0-10Hz", 30, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getY)
                .map( a -> ArrayInterpolator.interpolateLinear(a, 256) )
                .map(data -> {
                    double[] ffted = FFT.fft(Arrays.copyOf(data, 256));
                    return Arrays.copyOf(ffted, 30);
                })
        );

        registry.registerMultiNumericFeature(IMU_SENSORS, dataSource + ": Acceleration (Z) 0-10Hz", 30, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getZ)
                .map( a -> ArrayInterpolator.interpolateLinear(a, 256) )
                .map(data -> {
                    double[] ffted = FFT.fft(Arrays.copyOf(data, 256));
                    return Arrays.copyOf(ffted, 30);
                })
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Acceleration Range (X)", sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getX)
                .map(acc -> {
                    double min = DoubleStreams.of(acc).min().getAsDouble();
                    double max = DoubleStreams.of(acc).max().getAsDouble();
                    return max - min;
                })
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Acceleration Range (Y)", sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getY)
                .map(acc -> {
                    double min = DoubleStreams.of(acc).min().getAsDouble();
                    double max = DoubleStreams.of(acc).max().getAsDouble();
                    return max - min;
                })
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Acceleration Range (Z)", sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getZ)
                .map(acc -> {
                    double min = DoubleStreams.of(acc).min().getAsDouble();
                    double max = DoubleStreams.of(acc).max().getAsDouble();
                    return max - min;
                })
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Stdev. Acceleration (X)", sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getX)
                .map(MathUtils::stdev)
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Stdev. Acceleration (Y)", sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getY)
                .map(MathUtils::stdev)
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Stdev. Acceleration (Z)", sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map(AccelerationData::getZ)
                .map(MathUtils::stdev)
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Mag. Field (X)", sample -> sample
                .getData(MagnetometerData.class, dataSource)
                .map(MagnetometerData::getX)
                .map(mag -> DoubleStreams.of(mag).average().getAsDouble())
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Mag. Field (Y)", sample -> sample
                .getData(MagnetometerData.class, dataSource)
                .map(MagnetometerData::getY)
                .map(mag -> DoubleStreams.of(mag).average().getAsDouble())
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Mag. Field (Z)", sample -> sample
                .getData(MagnetometerData.class, dataSource)
                .map(MagnetometerData::getZ)
                .map(mag -> DoubleStreams.of(mag).average().getAsDouble())
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Brightness", sample -> sample
                .getData(LightSensorData.class, dataSource)
                .map(LightSensorData::getBrightnessInLux)
        );

        registry.registerNumericFeature(IMU_SENSORS, dataSource + ": Pressure", sample -> sample
                .getData(PressureSensorData.class, dataSource)
                .flatMap(data -> Optional.ofNullable(data.getPressureData()))
                .map(data -> {
                    double first = data[0];
                    double last = data[data.length - 1];
                    return last - first;
                })
        );
    }

}
