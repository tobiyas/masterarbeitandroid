package de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.ImuDataSource;
import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.imusensors.AcceleratorImuSensor;
import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.imusensors.GyroscopeImuSensor;
import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.imusensors.OrientationImuSensor;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import java8.util.function.Consumer;


/**
 * Created by Toby on 23.08.2017
 */

public class ThunderBoardIdentifier extends BluetoothGattCallback {

    private static final String TAG = "BoardIdentifier";
    private static final long TIMEOUT_DISCOVERY_IN_MILLIS = 1000;
    private static final long TIME_ADD_PER_ATTEMPT = 50;
    private static final long MAX_ATTEMPTS = 100;


    public static final String BOARD_NAME = "Thunder Sense #";

    /**
     * If we should also get Gyro and Orientation Data
     */
    public static boolean GET_GYRO_AND_ORIENTATION_DATA = false;


    /**
     * The Executor to use for Scheduling Async.
     */
    private static final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    /**
     * The context to use for System calls.
     */
    private final Context context;

    /**
     * The Bluetooth Device.
     */
    private final BluetoothDevice device;

    /**
     * The callback to call after done.
     */
    private final Consumer<DataSource>  consumer;

    /**
     * The Gatt service to use.
     */
    private BluetoothGatt gattService;

    /**
     * The task to kill if timeout is reached.
     */
    private ScheduledFuture<Void> stopTask;

    /**
     * The current state of the Connection.
     */
    private boolean isDiscarded = false;

    /**
     * The Timeout to use.
     */
    private long attemptTimeout = TIMEOUT_DISCOVERY_IN_MILLIS;

    /**
     * This is simply a flag if we are already done:
     */
    private boolean consumed = false;

    /**
     * The attempt number.
     */
    private int attempt = 0;

    /**
     * The read name:
     */
    private String name = null;
    /**
     * The read serial:
     */
    private String serial = null;


    public ThunderBoardIdentifier(Context context, BluetoothDevice device, Consumer<DataSource> consumer){
        this.context = context;
        this.device = device;
        this.consumer = consumer;

        Log.i(TAG, "Connecting GATT on " + device.getName());
        reconnect();
   }


    /**
     * Starting a reconnect attempt.
     */
    private void reconnect(){
        this.gattService = this.device.connectGatt(context, false, this);
        this.gattService.connect();

        this.stopTask = executor.schedule(this::stopOnTimeout, attemptTimeout, TimeUnit.MILLISECONDS);
    }


    /**
     * Stops the Service and disconnects the Device.
     *
     * We need a Void callback to be able to use ScheduledFuture.
     * This will ALWAYS return null!
     */
    private Void stopOnTimeout(){
        if(stopTask != null) stopTask.cancel(false);

        gattService.disconnect();
        gattService.close();

        attempt ++;

        //Restart if we have less attempts as the max attempts:
        if(!isDiscarded && attempt < MAX_ATTEMPTS){
            Log.i(TAG, "Reconnecting to " + device.getName() + " attempt: " + attempt + " Failed with Timeout: " + attemptTimeout + "ms.");

            //Always increase the Timeout:
            attemptTimeout += TIME_ADD_PER_ATTEMPT;
            reconnect();

            return null;
        }

        //Too many attempts:
        if(attempt >= MAX_ATTEMPTS){
            Log.i(TAG, "No attempts left. Aborting!");
        }

        Log.i(TAG, "Connection to " + device.getName() + " has been closed.");
        return null;
    }


    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        super.onConnectionStateChange(gatt, status, newState);
        if(isDiscarded) return;

        Log.i(TAG, gatt.getDevice().getName() + " Connection state changed! Status: " + status + " new State: " + newState
                + ". Got " + gatt.getServices().size() + " Services on Start.");

        //If we have a connection:
        if(status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothGatt.STATE_CONNECTED) {
            //This has to be sceduled, because BT seems to be sketchy about doing this right after...
            //See https://stackoverflow.com/questions/41434555/onservicesdiscovered-never-called-while-connecting-to-gatt-server
            ThreadUtils.RunOnMainthreadLater( () -> {
                    if(gatt.discoverServices()) Log.i(TAG, "Starting discovery of services!");
                    else Log.i(TAG, "Could not start discovery of services!");
                }, 0 );
        }
    }


    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        super.onServicesDiscovered(gatt, status);
        if (isDiscarded) return;

        Log.i(TAG, "Services discovered. Status: " + status + " Got: " + gatt.getServices().size() + " Services.");

        //This is because sometimes it will only show some services?!
        if( gatt.getServices().size() < 5 ){
            this.stopOnTimeout();
            return;
        }

        //Check if we have the Service for IMU data:
        BluetoothGattService imuService = gatt.getService( BTEntry.ACCELEROMETER.getService() );
        if(imuService == null){
            this.isDiscarded = true;
            this.stopOnTimeout();
            return;
        }

        //We got a valid service callback! Get the name of the Device:
        if (status == BluetoothGatt.GATT_SUCCESS) {
            Log.i(TAG, gatt.getDevice().getName() + " Discovery done.");

            String name = BTEntry.DEVICE_NAME.readCacheString(gatt, 0).orElse(null);
            String serial = BTEntry.DEVICE_SERIAL.readCacheString(gatt, 0).orElse(null);

            //If not already present -> Read!
            if (name == null || serial == null) {
                //initiate a read. Seems to always return false?! Still does a read!
                if(!BTEntry.DEVICE_NAME.read( gatt ) ||  BTEntry.DEVICE_SERIAL.read( gatt )){
                    stopOnTimeout();
                    return;
                }

                Log.i(TAG, "Reading Name and Serial");
                return;
            }

            //Got the device stuff! Now check if it's correct:
            verifyDevice(gatt, name, serial);
        }else {
            //Something has gone wrong! Break connection:
            stopOnTimeout();
        }
    }

    /**
     * Veriefies the Device with the given Name + Serial.
     */
    private void verifyDevice(BluetoothGatt gatt, String name, String serial){
        Log.i(TAG, "Got Device with name: " + name);

        //check the name of the Board.
        if(name.startsWith(BOARD_NAME)){
            stopTask.cancel(true);
            if(consumed) return;

            //Connect the existing gatt and start a new one in the Board:
            gatt.disconnect();

            ThunderBoard board = new ThunderBoard( serial, context, device );
            ImuDataSource newSource = new ImuDataSource(context, board, generateGattSensors(gatt));

            executor.execute( () -> consumer.accept(newSource) );
            consumed = true;
        }else{
            isDiscarded = true;
            Log.i(TAG, "Discarded device " + device.getName() + " because it's not " + BOARD_NAME);
        }
    }

    private List<Sensor> generateGattSensors( BluetoothGatt gatt ) {
        List<Sensor> sensors = new ArrayList<>();
        sensors.add( new AcceleratorImuSensor( context, gatt ) );

        //Only add this if the BT on the device is good enough!
        if( GET_GYRO_AND_ORIENTATION_DATA ){
            sensors.add( new OrientationImuSensor( context, gatt ) );
            sensors.add( new GyroscopeImuSensor( context, gatt ) );
        }

        return Collections.unmodifiableList( sensors );
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
        super.onCharacteristicRead(gatt, characteristic, status);
        if(isDiscarded) return;

        evaluate( gatt, characteristic );
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        super.onCharacteristicChanged(gatt, characteristic);

        this.evaluate(gatt,characteristic);
    }


    private void evaluate( BluetoothGatt gatt, BluetoothGattCharacteristic characteristic ){
        if(isDiscarded) return;

        boolean isName = characteristic.getUuid().equals(ThunderBoardUuids.UUID_CHARACTERISTIC_DEVICE_NAME);
        boolean isSerial = characteristic.getUuid().equals(ThunderBoardUuids.UUID_CHARACTERISTIC_SERIAL_NUMBER);

        if( isName && this.serial == null ) BTEntry.DEVICE_SERIAL.read( gatt );
        if( isSerial && this.name == null ) BTEntry.DEVICE_NAME.read( gatt );

        String name = isName ? characteristic.getStringValue( 0 ) : this.name;
        String serial = isSerial ? characteristic.getStringValue( 0 ) : this.serial;

        if( this.name == null ) this.name = name;
        if( this.serial == null ) this.serial = serial;

        //If not present, we don't need them!
        if ( this.name == null || this.serial == null) return;

        //Got the device stuff! Now check if it's correct:
        verifyDevice(gatt, name, serial);
    }
}
