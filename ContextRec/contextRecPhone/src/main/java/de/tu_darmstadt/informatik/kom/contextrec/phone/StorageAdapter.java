package de.tu_darmstadt.informatik.kom.contextrec.phone;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Adapter for storing and loading (Detector) data.
 */
public interface StorageAdapter {

    /**
     * Create some place for writing data for the given detector.
     */
    OutputStream store( String detectorId ) throws IOException;

    /**
     * Load data for some detector.
     */
    InputStream load( String detectorId ) throws IOException;

    /**
     * Indicates, if data for the given detector is present.
     */
    boolean hasData( String detectorId );
}
