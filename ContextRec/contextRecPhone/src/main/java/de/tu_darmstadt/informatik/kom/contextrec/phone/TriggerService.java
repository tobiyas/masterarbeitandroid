package de.tu_darmstadt.informatik.kom.contextrec.phone;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by Oli on 31.01.2016.
 */
public class TriggerService extends IntentService {

    public static final String INTENT_NAME = "Name of intent to trigger";

    public TriggerService() {
        super( "Trigger Service" );
    }

    @Override
    protected void onHandleIntent( Intent intent ) {
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance( this );

        String broadcastName = intent.getStringExtra( INTENT_NAME );
        Intent localBroadcast = new Intent( broadcastName );
        broadcastManager.sendBroadcast( localBroadcast );
    }
}
