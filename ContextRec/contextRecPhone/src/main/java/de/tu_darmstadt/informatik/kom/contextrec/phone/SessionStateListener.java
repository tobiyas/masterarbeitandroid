package de.tu_darmstadt.informatik.kom.contextrec.phone;

/**
 * Listener for state changes of Sessions.
 */
public interface SessionStateListener {
    void onSessionStateChanged( ContextRecSession.SessionState state );
}
