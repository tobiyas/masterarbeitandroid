package de.tu_darmstadt.informatik.kom.contextrec.phone.imu.imusensors;

import android.bluetooth.BluetoothGattCharacteristic;

import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.BTEntry;

/**
 * Created by Toby on 27.08.2017
 */

public interface CharacteristicsChangedNotifier {

    /**
     * Gets the entry to listen to to check against characteristics..
     * @return the Entry listening to..
     */
    BTEntry getEntry();


    /**
     * We got a new Characteristic. Please parse and read your stuff!
     * @param characteristic to read.
     */
    void gotNewCharacteristics(BluetoothGattCharacteristic characteristic);
}
