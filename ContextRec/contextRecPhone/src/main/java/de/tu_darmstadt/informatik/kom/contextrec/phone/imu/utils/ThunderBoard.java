package de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import java8.util.function.Consumer;

import static de.tu_darmstadt.informatik.kom.contextsense.utils.Try.Try;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 22.08.2017
 */

public class ThunderBoard {

    /**
     * The callback to evaluate stuff.
     */
    private final BoringGattCallback callback = new BoringGattCallback();

    /**
     * The Device to use.
     */
    private final BluetoothDevice device;

    /**
     * The gatt client to use.
     */
    private final BluetoothGatt gatt;

    /**
     * The Entries we listen to.
     */
    private final Set<BTEntry> entriesToListenTo = new HashSet<>();

    /**
     * The consumer to call when we got a characteristics callback.
     */
    private Consumer<BluetoothGattCharacteristic> characteristicConsumer;

    /**
     * The Name of the device.
     */
    private final String serial;

    /**
     * The current battery level.
     */
    private double currentBatteryLevel = -1;


    public ThunderBoard( String serial, Context context, BluetoothDevice device ){
        this.serial = device.getName().replace(ThunderBoardIdentifier.BOARD_NAME, "");
        this.device = device;
        this.gatt = device.connectGatt(context, true, callback);
        this.gatt.connect();
    }


    /**
     * Adds an Entry to listen.
     * @param entry to add.
     */
    public void addEntry(BTEntry entry){
        this.entriesToListenTo.add(entry);

        //Late init Entry:
        if(callback.isConnected) entry.register(gatt);
    }

    /**
     * Removes an Entry to listen.
     * @param entry to remove.
     */
    public void removeEntry(BTEntry entry){
        this.entriesToListenTo.remove(entry);
        if( callback.isConnected ) entry.unregister( gatt );
    }

    /**
     * Sets the consumer of the Characteristics.
     * @param consumer to use.
     */
    public void registerCharacteristicCallback(Consumer<BluetoothGattCharacteristic> consumer){
        this.characteristicConsumer = consumer;
    }


    /**
     * Starts reading stuff from the Board.
     */
    public void startReading(){
        if(!callback.isConnected) {
            callback.startAfterConnecting = true;
            return;
        }

        //Register gatt entries:
        register ( new ArrayList<>( entriesToListenTo ) );
        callback.startAfterConnecting = false;
    }


    /**
     * Reads the current battery level.
     */
    public void readBattery(){
        BTEntry.DEVICE_BATTERY.read( gatt );
    }

    /**
     * Gets the current Battery level.
     *
     *
     * @return battery level 0-100%. -1 = none retrieved yet.
     */
    public double getCurrentBatteryLevel() {
        return currentBatteryLevel;
    }

    private static final int MAX_REGISTER_BEFORE_DISCOVERY = 30;
    private void register(List<BTEntry> entries){
        Handler handler = new Handler( Looper.getMainLooper() );
        handler.postDelayed(
                new Runnable() {
                    private int step = MAX_REGISTER_BEFORE_DISCOVERY;

                    @Override
                    public void run() {
                        List<BTEntry> failed = new ArrayList<>();
                        for( BTEntry entry : entries ){
                            if( !entry.register( gatt ) ) failed.add( entry );
                        }

                        if( !failed.isEmpty() ) {
                            if(step > MAX_REGISTER_BEFORE_DISCOVERY) {
                                gatt.discoverServices();
                                step = 0;
                            }

                            step++;
                            handler.postDelayed( this, 100);
                        }
                    }
                }
        , 100);
    }

    /**
     * Stops reading stuff from the board.
     */
    public void stopReading(){
        stream( entriesToListenTo )
                .forEach( e -> e.unregister(gatt) );
    }


    /**
     * Starts a calibration task.
     * @return true if started
     */
    public boolean startCalibration() {
        return BTEntry.CALIBRATE.write(gatt, 0x01, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
    }

    /**
     * Gets the Serial Number of the Device.
     * @return the serial of the device.
     */
    public String getSerial() {
        return serial;
    }

    /**
     * Close the Connection completely.
     */
    public void close() {
        Try(this::stopReading);
        Try(this.gatt::close);
    }


    /**
     * Flashes the LEDs in the pattern passed.
     * WARNING: Funny, but not working.... Needs to be fixed.
     *
     * @param steps to use.
     */
    public void flashLEDs( FlashStep... steps ){
        long time = 0;

        Handler handler = new Handler( Looper.getMainLooper() );
        handler.post( () -> BTEntry.LEDS.setIndication( gatt, true ) );

        List<FlashStep> pattern = new ArrayList<>( Arrays.asList( steps ) );
        pattern.add( new FlashStep( 0,0,0, 0 ) );

        for( FlashStep step : pattern ){
            handler.postDelayed( () -> setRGBColor( step.r, step.g, step.b ) , time);
            time += step.timeInMS;
        }
    }

    /**
     * Sets the color of the RGP Lights.
     * Use 0,0,0 to disable the leds.
     *
     * @param r red color. 0 - 255
     * @param g green color 0 - 255
     * @param b blue color 0 - 255
     */
    public void setRGBColor( int r, int g, int b ){
        byte[] data = new byte[4];

        data[0] = (byte) (r & 0xff);
        data[1] = (byte) (g & 0xff);
        data[2] = (byte) (b & 0xff);
        data[3] = (byte) ((r == 0 && g == 0 && b == 0) ? 0x00 : 0x0f);

        if( !BTEntry.LEDS.write( gatt, data ) ) Log.i( "BT_IMU", "Could not write LED color: " + r + " " + g + " " + b );
    }


    public static class FlashStep{

        private int r,g,b;
        private long timeInMS;

        public FlashStep(int r, int g, int b, long timeInMS) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.timeInMS = timeInMS;
        }
    }



    private class BoringGattCallback extends BluetoothGattCallback {

        private final Executor executor = Executors.newSingleThreadExecutor();

        boolean isConnected = false;

        boolean startAfterConnecting = false;

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            boolean old = isConnected;
            isConnected = newState == BluetoothGatt.STATE_CONNECTED;

            //If freshly connected -> Start Reading!
            if( startAfterConnecting && isConnected && !old ){
                gatt.discoverServices();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);

            if( startAfterConnecting && isConnected ){
                startReading();
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);

            executor.execute( () -> evaluate( characteristic ) );
        }

        @Override
        public void onCharacteristicRead( BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status ) {
            super.onCharacteristicRead( gatt, characteristic, status );

            //We got a battery update!
            if( characteristic.getUuid().equals( BTEntry.DEVICE_BATTERY.getCharacteristics() ) ){
                currentBatteryLevel = characteristic.getValue()[0];
            }
        }

        /**
         * Evaluates the Characteristics.
         * @param characteristic to evaluate.
         */
        private void evaluate(BluetoothGattCharacteristic characteristic){
            if( characteristicConsumer != null ) characteristicConsumer.accept( characteristic );
        }
    }

}
