package de.tu_darmstadt.informatik.kom.contextrec.phone;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.tu_darmstadt.informatik.kom.contextsense.CalibrateableDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.StreamingDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.IDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.StoredSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.utils.CancelableFuture;
import de.tu_darmstadt.informatik.kom.contextsense.utils.FutureUtils;
import java8.util.Optional;
import java8.util.function.Supplier;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 29.11.2015
 */
public class ContextRecSession implements Closeable {


    private static final Duration defaultSensorBatchDuration = Duration.standardSeconds( 4 );

    public enum SessionState {
        STOPPED,
        WAITING_FOR_NEXT_TRIGGER,
        TRIGGERING_COLLECTING_DATA,
        TRIGGERING_RUNNING_DETECTION
    }

    private final Map<DetectionResultListener, Optional<Handler>> resultListener;
    private final Map<SessionStateListener, Optional<Handler>> stateListener;
    private final Map<SourcesChangedListener, Optional<Handler>> sourceListener;

    private final List<IDetector> detectors;
    private final List<DataSource> dataSources;
    private final ScheduledExecutorService executor;
    private final PowerManager.WakeLock wakeLock;
    private final LocalBroadcastManager broadcastManager;
    private final IntentFilter triggerIntentFilter;
    private final BroadcastReceiver triggerReceiver;
    private final PendingIntent pendingAlarmIntent;
    private final AlarmManager alarmManager;
    private final String sessionId;
    private StorageAdapter storageAdapter;

    private Supplier<DateTime> nextTriggerTimeProvider;
    private SessionState state;
    private boolean isClosed;
    private boolean isRunning;
    private boolean isStreaming = false;
    private Optional<SensorDataBatch> lastBatch;
    private Optional<DetectionResult> lastResult;
    private Optional<DateTime> nextTriggerTime;

    public ContextRecSession( Context context ) {
        this.storageAdapter = new InternalStorageAdapter( context );
        this.alarmManager = (AlarmManager) context.getSystemService( Context.ALARM_SERVICE );
        this.detectors = new ArrayList<>();
        this.dataSources = new ArrayList<>();
        this.nextTriggerTimeProvider = () -> DateTime.now().plus( Duration.standardSeconds( 60 ) );
        this.executor = Executors.newSingleThreadScheduledExecutor();

        this.resultListener = new ConcurrentHashMap<>();
        this.stateListener = new ConcurrentHashMap<>();
        this.sourceListener = new ConcurrentHashMap<>();

        this.lastResult = Optional.empty();
        this.lastBatch = Optional.empty();
        this.nextTriggerTime = Optional.empty();
        this.state = SessionState.STOPPED;
        this.broadcastManager = LocalBroadcastManager.getInstance( context );
        this.sessionId = UUID.randomUUID().toString();
        this.isStreaming = false;

        PowerManager powerManager = (PowerManager) context.getSystemService( Context.POWER_SERVICE );
        assert powerManager != null;
        this.wakeLock = powerManager.newWakeLock( PowerManager.PARTIAL_WAKE_LOCK, "Context Sense ContextRecSession "+ sessionId );
        this.triggerReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive( Context context, Intent intent ) {
                triggerNow();
            }
        };
        this.triggerIntentFilter = new IntentFilter( this.sessionId );

        Intent alarm = new Intent( context, TriggerService.class );
        alarm.putExtra( TriggerService.INTENT_NAME, getSessionId() );
        this.pendingAlarmIntent = PendingIntent.getService( context, 123, alarm, PendingIntent.FLAG_UPDATE_CURRENT );
    }

    /**
     * This starts the complete system.
     * @param triggerOnStart if the system should Trigger uppon start
     */
    public void start(boolean triggerOnStart) {
        if (isClosed)
            throw new IllegalStateException( "Cannot start the session - the session was closed. Create a new instance of ContextRecSession instead." );

        if (isRunning)
            return;

        broadcastManager.registerReceiver( triggerReceiver, triggerIntentFilter );
        isRunning = true;

        executor.execute( () -> {
            stream( dataSources ).forEach( DataSource::start );
            if(triggerOnStart) trigger();
        });
    }

    public void stop() {
        if (!isRunning)
            return;

        broadcastManager.unregisterReceiver( triggerReceiver );
        isRunning = false;

        executor.submit( this::doStop );
    }


    @Override
    public void close(){
        this.close(false);
    }


    public void startStreaming(){
        if(isStreaming) return;
        isStreaming = true;

        executor.submit(
            () -> stream( dataSources )
                .filter( source -> source instanceof StreamingDataSource )
                .map( source -> (StreamingDataSource) source )
                .forEach( StreamingDataSource::startStreaming )
        );

        long streamingSourcesStarted = stream( dataSources ).filter(s -> s instanceof StreamingDataSource).count();
        Log.i("SENSORS", "We have started streaming " + streamingSourcesStarted + " Sources.");
    }


    public void stopStreaming(){
        if(!isStreaming) return;
        isStreaming = false;

        executor.submit(
            () -> stream( dataSources )
                .filter( source -> source instanceof StreamingDataSource )
                .map( source -> (StreamingDataSource) source )
                .forEach( StreamingDataSource::stopStreaming )
        );
    }



    public void close( boolean executePendingTasks ) {
        if(isClosed) return;

        isClosed = true;
        isRunning = false;

        //Close all closeables:
        stream( dataSources )
                .filter( Closeable.class::isInstance )
                .map( Closeable.class::cast )
                .forEach( c -> {
                    try{ c.close(); }catch (Throwable ignored){}
                });

        if (executePendingTasks) {
            executor.shutdown();
        } else {
            executor.shutdownNow();
        }

        try {
            executor.awaitTermination( 10, TimeUnit.SECONDS );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        doStop();

        //Remove all listeners:
        sourceListener.clear();
        stateListener.clear();
        resultListener.clear();
    }

    private void doStop() {
        //Stop normal Sources:
        stream( dataSources )
                .filter( DataSource::isRunning )
                .forEach( DataSource::stop );

        //Stop streaming:
        stream( dataSources )
                .filter( source -> source instanceof StreamingDataSource )
                .map( source -> (StreamingDataSource) source )
                .forEach( StreamingDataSource::stopStreaming );

        alarmManager.cancel( pendingAlarmIntent );
        nextTriggerTime = Optional.empty();
        setState( SessionState.STOPPED );
    }

    /**
     * This starts a calibration task when called
     */
    public void doCalibration(){
        stream( dataSources )
                .filter( CalibrateableDataSource.class::isInstance )
                .map( CalibrateableDataSource.class::cast )
                .forEach( CalibrateableDataSource::calibrate );
    }


    private void trigger(){
        trigger(defaultSensorBatchDuration);
    }

    private void trigger(Duration sensorBatchDuration) {
        if (!isRunning)
            return;

        try {
            wakeLock.acquire(10*60*1000L /*10 minutes*/);

            // definsive copy in case someone changes the state in the mean time.
            List<DataSource> sourcesToUse = new ArrayList<>( dataSources );
            List<IDetector> detectorsToUse = new ArrayList<>( detectors );

            activiteDisabledSensors( sourcesToUse );

            setState( SessionState.TRIGGERING_COLLECTING_DATA );
            Optional<SensorDataBatch> batch = collectAndAggregateData( sourcesToUse, sensorBatchDuration);
            setState( SessionState.TRIGGERING_RUNNING_DETECTION );

            batch.ifPresent( data -> {
                DetectionResult result = DetectionRunner.runDetection( data, detectorsToUse );
                report( data, result );
            });
        } catch (Throwable e) {
            // make sure we do not lose the info - we are on a background thread.
            e.printStackTrace();
        } finally {
            wakeLock.release();

            if (isRunning) {
                DateTime nextTrigger = nextTriggerTimeProvider.get();
                Duration timeToNextTrigger = new Duration( DateTime.now(), nextTrigger );
                scheduleNextTrigger( timeToNextTrigger );
            }
        }
    }

    private void activiteDisabledSensors( List<DataSource> sources ) {
        stream( sources )
                .filter( source -> !source.isRunning() )
                .forEach( DataSource::start );
    }

    private Optional<SensorDataBatch> collectAndAggregateData( List<DataSource> sources, Duration sensorBatchDuration ) {
        // start all tasks before we consume the results. Otherwise, they would not run in parallel
        List<CancelableFuture<Optional<SensorDataBatch>>> tasks = stream( sources )
                .map( source -> source.collectData( sensorBatchDuration ) )
                .collect( Collectors.toList() );

        List<SensorDataBatch> batches = stream( tasks )
                .map( FutureUtils::getAndPropagateInterruptsQuietly )
                .map( val -> val.flatMap( s -> s ) )
                .filter( Optional::isPresent )
                .map( Optional::get )
                .collect( Collectors.toList() );

        if (Thread.currentThread().isInterrupted())
            return Optional.empty();
        else
            return SensorDataBatch.merge( batches );
    }

    /**
     * This aggregates all Streaming data from all Streaming DataSources.
     * @param sources to search
     * @param from the start time to search from.
     * @param to the end time to search to.
     * @return an optional with the collected data or empty.
     */
    private Optional<SensorDataBatch> aggregateStreamingData( List<DataSource> sources, DateTime from, DateTime to ) {
        List<SensorDataBatch> batches = stream( sources )
                .filter( source -> source instanceof StreamingDataSource )
                .filter( source -> !source.getName().equalsIgnoreCase( "NotDefined" ) )
                .map( source -> (StreamingDataSource) source )
                .map( source -> source.collectStreamedData( from, to ) )
                .filter( Optional::isPresent )
                .map( Optional::get )
                .collect( Collectors.toList() );

        if (Thread.currentThread().isInterrupted())
            return Optional.empty();
        else
            return SensorDataBatch.merge( batches );
    }


    /**
     * This aggregates all Streaming data from all Streaming DataSources.
     * @param sources to search
     * @param from the start time to search from.
     * @param to the end time to search to.
     * @return an optional with the collected data or empty.
     */
    private Optional<SensorDataBatch> aggregateStreamingDataWithUnused( List<DataSource> sources, DateTime from, DateTime to ) {
        List<SensorDataBatch> batches = stream( sources )
                .filter( source -> source instanceof StreamingDataSource )
                .map( source -> (StreamingDataSource) source )
                .map( source -> source.collectStreamedData( from, to ) )
                .filter( Optional::isPresent )
                .map( Optional::get )
                .collect( Collectors.toList() );

        if (Thread.currentThread().isInterrupted())
            return Optional.empty();
        else
            return SensorDataBatch.merge( batches );
    }

    private void scheduleNextTrigger( Duration timeToNextTrigger ) {
        DateTime now = DateTime.now();
        nextTriggerTime = Optional.of( now.plus( timeToNextTrigger ) );

        alarmManager.set( AlarmManager.RTC_WAKEUP, nextTriggerTime.get().getMillis(), pendingAlarmIntent );

        setState( SessionState.WAITING_FOR_NEXT_TRIGGER );
    }


    /**
     * Starts a trigger run.
     * This is run on the Sensor Thread and is NOT running / blocking main Thread.
     * Therefore it is safe to run from any thread.
     *
     *
     * @param sensorBatchDuration the time sensors should collect data.
     */
    public void triggerNow(Duration sensorBatchDuration) {
        if (state != SessionState.WAITING_FOR_NEXT_TRIGGER)
            return;

        executor.submit( () -> {
            if (!isRunning)
                return;

            if (state == SessionState.WAITING_FOR_NEXT_TRIGGER) {
                alarmManager.cancel( pendingAlarmIntent );
                trigger(sensorBatchDuration);
            }
        });
    }


    /**
     * Same as {@link #triggerNow(Duration)}, but with a fixed SensorCollecting time.
     * The time is fixed to 4 Seconds.
     */
    public void triggerNow() {
        this.triggerNow(defaultSensorBatchDuration);
    }


    /**
     * This triggers the Sensors + Analysis in the {@param duration} time.
     *
     * @param duration to trigger in.
     */
    public void triggerIn( Duration duration ) {
        if (state != SessionState.WAITING_FOR_NEXT_TRIGGER)
            return;

        executor.submit( () -> {
            if (!isRunning)
                return;

            if (state == SessionState.WAITING_FOR_NEXT_TRIGGER) {
                alarmManager.cancel( pendingAlarmIntent );
                scheduleNextTrigger( duration );
            }
        });
    }


    /**
     * Collects the Streaming data from to a specific date.
     * @param from the time point to get from.
     * @param to the time point to get to.
     */
    public Optional<SensorDataBatch> collectStreamingData( DateTime from, DateTime to ){
        return aggregateStreamingData(dataSources, from, to);
    }

    /**
     * Collects the Streaming data from to a specific date.
     * @param from the time point to get from.
     * @param to the time point to get to.
     */
    public Optional<SensorDataBatch> collectStreamingDataWithUnused( DateTime from, DateTime to ){
        return aggregateStreamingDataWithUnused(dataSources, from, to);
    }

    /**
     * Collects the Streaming data from to a specific date.
     * This does a callback to the {@link DetectionResultListener} registered.
     *
     * @param from the time point to get from.
     * @param to the time point to get to.
     */
    public void collectStreamingDataAsync( DateTime from, DateTime to){
        executor.submit( () -> {
            Optional<SensorDataBatch> data = aggregateStreamingData(dataSources, from, to);
            if(data.isPresent()) this.report(data.get(), new DetectionResult(from) );
        });
    }


    /**
     * Collects the Streaming data from to a specific date.
     * This does a callback to the {@link DetectionResultListener} registered.
     *
     * @param from the time point to get from.
     * @param to the time point to get to.
     */
    public void collectStreamingDataAsyncWithUnused( DateTime from, DateTime to){
        executor.submit( () -> {
            Optional<SensorDataBatch> data = aggregateStreamingDataWithUnused(dataSources, from, to);
            if(data.isPresent()) this.report(data.get(), new DetectionResult(from) );
        });
    }


    /**
     * Collects the Streaming data from to a specific date.
     * This does a callback to the {@link DetectionResultListener} registered.
     *
     * @param from the time point to get from.
     * @param to the time point to get to.
     */
    public void collectStreamingDataAndAnalyseAsync( DateTime from, DateTime to){
        executor.submit( () -> {
            Optional<SensorDataBatch> data = aggregateStreamingData(dataSources, from, to);
            if(data.isPresent()) this.report(data.get(), classifyData(data.get()) );
        });
    }

    /**
     * Collects the Streaming data from to a specific date.
     * This does a callback to the {@link DetectionResultListener} registered.
     *  @param from the time point to get from.
     * @param to the time point to get to.
     */
    public Optional<DetectionResult> collectStreamingDataAndAnalyse(DateTime from, DateTime to){
        Optional<SensorDataBatch> data = aggregateStreamingData(dataSources, from, to);
        if(data.isPresent()) return Optional.of( classifyData(data.get()) );
        else return Optional.empty();
    }


    /**
     * Classify the given data with the current session config (the list of registered detectors).
     * This is a blocking operation. You should not call this on UI-Thread.
     */
    public DetectionResult classifyData( SensorDataBatch data ) {
        return DetectionRunner.runDetection( data, getAllDetectors() );
    }


    ///
    /// callbacks
    ///

    private void report( SensorDataBatch data, DetectionResult result ) {
        lastBatch = Optional.of( data );
        lastResult = Optional.of( result );

        for( Map.Entry<DetectionResultListener, Optional<Handler>> entry : resultListener.entrySet() ) {
            DetectionResultListener consumer = entry.getKey();
            Optional<Handler> handler = entry.getValue();
            handler.ifPresentOrElse(
                    h -> h.post( () -> consumer.onResult( data, result ) ),
                    () -> consumer.onResult( data, result )
            );
        }
    }

    private void setState( SessionState state ) {
        this.state = state;

        for( Map.Entry<SessionStateListener, Optional<Handler>> entry : stateListener.entrySet() ) {
            SessionStateListener consumer = entry.getKey();
            Optional<Handler> handler = entry.getValue();
            handler.ifPresentOrElse(
                    h -> h.post( () -> consumer.onSessionStateChanged( state ) ),
                    () -> consumer.onSessionStateChanged( state )
            );
        }
    }

    public Set<String> getRequiredPermissions() {
        return stream( dataSources )
            .flatMap( source -> stream( source.getRequiredPermissions() ) )
            .collect( Collectors.toSet() );
    }


    public boolean isClosed() {
        return isClosed;
    }

    ///
    /// manage detectors
    ///

    public void addDetector( IDetector detector ) {
        DetectorIdentifier id = detector.getDescription().getId();
        boolean duplicateId = stream( detectors )
                .map( IDetector::getDescription )
                .map( DetectorDescription::getId )
                .anyMatch( detectorId -> detectorId.equals( id ) );

        if ( duplicateId )
            throw new IllegalStateException( String.format( "Cannot register the same identifier twice. A detector with ID '%s' was already registered.", detector.getDescription().getId() ) );

        IDetector decorated = new AutosaveDetectorDecorator( detector, () -> storageAdapter );
        detectors.add( decorated );
    }

    public void removeDetector( IDetector detector ) {
        detectors.remove( detector );
    }

    public List<IDetector> getAllDetectors() {
        return new ArrayList<>( detectors );
    }

    ///
    /// manage DataSources
    ///

    /**
     * Adds a new Datasource to the System.
     * If needed it will be started.
     * @param dataSource to add.
     */
    public void addDataSource( DataSource dataSource ) {
        dataSources.add( dataSource );
        if(isRunning) dataSource.start();
        if(isStreaming && dataSource instanceof StreamingDataSource) ((StreamingDataSource)dataSource).startStreaming();

        if(isStreaming || isRunning) Log.i("Session", "Late started another Source: " + dataSource.getName() );

        stream( sourceListener.entrySet() ).forEach( e ->
                {
                    Runnable run = () -> e.getKey().sourceAdded( dataSource );
                    e.getValue().ifPresentOrElse(
                            h -> h.post( run ),
                            run
                    );
                }
        );
    }

    /**
     * Adds multiple DataSources.
     * @param dataSource to add
     */
    public void addDataSources( DataSource... dataSource ) {
        RefStreams.of( dataSource ).forEach( this::addDataSource );
    }

    /**
     * Adds multiple Datasources.
     * @param dataSource to add
     */
    public void addDataSources( Collection<DataSource> dataSource ) {
        stream( dataSource ).forEach( this::addDataSource );
    }

    /**
     * Removes a DataSource
     * @param dataSource to remove.
     */
    public void removeDataSource( DataSource dataSource ) {
        if(!dataSources.contains( dataSource )) return;

        //Remove the source
        dataSources.remove( dataSource );

        //Call the removed handlers.
        stream( sourceListener.entrySet() ).forEach( e ->
                {
                    Runnable run = () -> e.getKey().sourceRemoved( dataSource );
                    e.getValue().ifPresentOrElse(
                            h -> h.post( run ),
                            run
                    );
                }
        );
    }

    public List<DataSource> getDataSources() {
        return new ArrayList<>( dataSources );
    }

    // No remove for now. It's not clear who should stop the sensors.
    // Feel free to add a remove function, if you have a nice idea how to stop the sensor.

    ///
    /// Listeners
    ///

    public void registerResultListener( DetectionResultListener callback, boolean triggerNow ) {
        registerResultListener( callback, triggerNow, null );
    }

    public void registerResultListener( DetectionResultListener callback, boolean triggerNow, Handler handler ) {
        resultListener.put( callback, Optional.ofNullable( handler ) );

        if (triggerNow && getLastResult().isPresent() ) {
            if (handler != null) {
                handler.post( () -> callback.onResult( getLastSensorData().get(), getLastResult().get() ) );
            } else {
                callback.onResult( getLastSensorData().get(), getLastResult().get() );
            }
        }
    }

    public void unregisterResultListener( DetectionResultListener callback ) {
        resultListener.remove( callback );
    }

    public void registerStateListener( SessionStateListener listener, boolean triggerNow ) {
        registerStateListener( listener, triggerNow, null );
    }

    public void registerStateListener( SessionStateListener listener, boolean triggerNow, Handler handler ) {
        stateListener.put( listener, Optional.ofNullable( handler ) );

        if (triggerNow) {
            if (handler != null) {
                handler.post( () -> listener.onSessionStateChanged( state ) );
            } else {
                listener.onSessionStateChanged( state );
            }
        }
    }

    public void unregisterStateListener( SessionStateListener callback ) {
        stateListener.remove( callback );
    }

    public void registerSourcesChangedListener( SourcesChangedListener listener ) {
        registerSourcesChangedListener( listener, null );
    }

    public void registerSourcesChangedListener( SourcesChangedListener listener, Handler handler ) {
        sourceListener.put( listener, Optional.ofNullable( handler ) );
    }

    public void unregisterSourcesChangedListener( SourcesChangedListener listener ) {
        sourceListener.remove( listener );
    }

    ///
    /// Change state
    ///

    public Optional<DetectionResult> getLastResult() {
        return lastResult;
    }

    public Optional<SensorDataBatch> getLastSensorData() {
        return lastBatch;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public SessionState getState() {
        return this.state;
    }

    public Optional<DateTime> getNextTriggerTime() {
        return nextTriggerTime;
    }

    /**
     * Trigger on a regular basis with a custom interval.
     * This will override custom triggering (set with {@link #switchToCustomTriggering(Supplier)}}), if it was set before.
     */
    public void switchToPeriodicTriggering( Duration timeBetweenTrigger ) {
        this.nextTriggerTimeProvider = () -> DateTime.now().plus( timeBetweenTrigger );
    }

    /**
     * Use a custom function to determine the next trigger time.
     * This will override periodic triggering (set with {@link #switchToPeriodicTriggering(Duration)}}), if it was set before.
     */
    public void switchToCustomTriggering( Supplier<DateTime> nextTriggerTimeProvider ) {
        this.nextTriggerTimeProvider = nextTriggerTimeProvider;
    }

    /**
     * Each session has a unique ID.
     * The id is used for internal purposes, but won't change over time.
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * Change the adapter used to store the detector's states at a persistent location.
     *
     * You may change the adapter at any time, but note that it makes sense to set the adapter before registering any detector.
     * Otherwise, the detector might use the old adapter to load it's state (which might not be what you want).
     */
    public void setStorageAdapter( StorageAdapter adapter ) {
        this.storageAdapter = adapter;
    }

    ///
    /// Provide feedback
    ///

    /**
     * Let the registered detectors trainIncremental from user feedback.
     * This will invoke the {@link IDetector#trainIncremental(TrainingSample)} method on all detectors.
     * They will be asked to learn from the given sample.
     */
    public void learnFromFeedback( final StoredSample sample ) {
        executor.submit( () -> {
           if (isRunning()) {
               for( IDetector detector: detectors ) {
                   SituationGroup situation = detector.getDescription().getDetectableGroup();
                   TrainingSample trainingSample = TrainingSample.fromStoredSample( sample, situation );
                   detector.trainIncremental( trainingSample );
               }
           }
        });
    }

    /**
     * Returns a copy of the DataSources present.
     * This is ment to change the name of the sources or check for specific sources.
     * The collection passed is Immutable!
     *
     * @return a copy of the Collection of the registered sources.
     */
    public Collection<DataSource> getRegisteredSources(){
        return Collections.unmodifiableCollection(dataSources);
    }
}
