package de.tu_darmstadt.informatik.kom.contextrec.phone.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.Arrays;

import de.tu_darmstadt.informatik.kom.contextrec.phone.ContextRecSession;

import static java8.util.stream.StreamSupport.stream;

/**
 * Utility methods for simplified use of ContextRec.
 */
public class ContextRecUtils {

    /**
     * On Android >= 6.0, this function will show a permission-request for all missing permissions of the given instance of ContextSense.
     * On Android < 6.0, this function will do nothing (except when you do not have the required permissions declared in your AndroidManifest. In this case it will throw an exception).
     *
     * @param activity the activity hosting the permission request
     * @param session the ContextSense session with missing permissions.
     */
    public static void requestPermissionsIfMissing( Activity activity, ContextRecSession session ) {
        String[] missingPermissions = stream( session.getRequiredPermissions() )
                .filter( p -> ContextCompat.checkSelfPermission( activity, p ) != PackageManager.PERMISSION_GRANTED )
                .toArray( String[]::new );

        if (missingPermissions.length > 0) {
            Log.i( "ContextRecUtils", "Requesting permissions "+ Arrays.toString( missingPermissions ) );

            ActivityCompat.requestPermissions(
                    activity,
                    missingPermissions,
                    251
            );
        }
    }

}
