package de.tu_darmstadt.informatik.kom.contextrec.phone.imu;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.util.Log;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.ThunderBoardIdentifier;
import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.ThunderBoardUuids;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import java8.util.function.Consumer;

import static de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.ThunderBoardIdentifier.BOARD_NAME;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 20.08.2017
 */

public class ImuConnector {

    /**
     * The Filter to use for Searching.
     */
    private static final UUID[] ID_FILTER = {
            ThunderBoardUuids.UUID_SERVICE_ACCELERATION_ORIENTATION,
            ThunderBoardUuids.UUID_SERVICE_GENERIC_ACCESS,
            ThunderBoardUuids.UUID_SERVICE_DEVICE_INFORMATION
    };

    /**
     * To run stuff async.
     */
    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

    /**
     * The context we need to invoke Stuff.
     */
    private final Context context;

    /**
     * The Adapter to call BT stuff on.
     */
    private final BluetoothAdapter btAdapter;

    /**
     * The method for calling stop.
     */
    private ScheduledFuture stopFuture;

    /**
     * The current scanTask.
     */
    private ScanTask scanTask;

    /**
     * The already discovered Devices.
     */
    private final Set<BluetoothDevice> discovered = new HashSet<>();


    @SuppressLint("NewApi")
    public ImuConnector(Context context) throws IllegalStateException {
        this.context = context;

        BluetoothManager manager = ((BluetoothManager) context.getSystemService(Service.BLUETOOTH_SERVICE));
        if(manager == null) throw new IllegalStateException("No BluetoothManager present!");

        this.btAdapter = manager.getAdapter();
    }


    /**
     * Gets a callback for the connected devices.
     * @param callback to call when something is found.
     * @param timeoutInSeconds the timeout to wait for in seconds.
     */
    public void getConnected(Consumer<DataSource> callback, int timeoutInSeconds){
        if(stopFuture != null) stopFuture.cancel(false);
        if(scanTask != null) btAdapter.stopLeScan(scanTask);

        Log.i("BT", "Starting LE Scan");
        //This is a option to only get devices with the services we actually need.
        //btAdapter.startLeScan(ID_FILTER, scanTask = new ScanTask(callback));

        //First check through bonded devices first:
        stream( btAdapter.getBondedDevices() )
            .filter( device -> device.getName().startsWith( BOARD_NAME ) )
            .forEach( device -> foundDevice( device, callback ) );

        //Add all discovered to the list, since we know they are not IMUs, since we have filtered them already.
        stream ( btAdapter.getBondedDevices() )
                .forEach( this.discovered::add );

        //This is a debug option for testing.
        btAdapter.startLeScan( scanTask = new ScanTask(callback) );

        stopFuture = executor.schedule(this::stopScanning, timeoutInSeconds, TimeUnit.SECONDS);
    }

    /**
     * Forces a stop of scanning, if still running.
     */
    public void forceStop(){
        if(stopFuture != null) {
            stopFuture.cancel(false);
            this.stopScanning();
        }
    }

    /**
     * This resets the already seen devices and stops the scan if running.
     */
    public void reset() {
        forceStop();
        this.discovered.clear();
    }


    private class ScanTask implements BluetoothAdapter.LeScanCallback{
        private final Consumer<DataSource> callback;
        private ScanTask(Consumer<DataSource> callback){ this.callback = callback; }
        @Override public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
            foundDevice(bluetoothDevice, callback);
        }
    }


    /**
     * Found a device, calling consumer.
     * @param bluetoothDevice to use.
     * @param callback to use.
     */
    private void foundDevice(BluetoothDevice bluetoothDevice, Consumer<DataSource> callback) {
        if(bluetoothDevice == null || callback == null) return;

        //If already discovered -> already done.
        if(discovered.contains(bluetoothDevice)) return;
        this.discovered.add(bluetoothDevice);

        //We do not have a ThunderBoard device!
        if(bluetoothDevice.getName() == null || !bluetoothDevice.getName().startsWith(BOARD_NAME) ) return;

        Log.i("BT", bluetoothDevice.getName() + " found on BT.");
        new ThunderBoardIdentifier(context, bluetoothDevice, callback);
    }


    /**
     * Stops scanning.
     */
    private void stopScanning(){
        this.stopFuture = null;
        btAdapter.stopLeScan(scanTask);

        Log.i("BT", "Stopping LE Scan");
    }



}
