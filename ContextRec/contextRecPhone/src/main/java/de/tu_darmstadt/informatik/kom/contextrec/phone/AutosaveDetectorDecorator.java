package de.tu_darmstadt.informatik.kom.contextrec.phone;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.IDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.Precondition;
import de.tu_darmstadt.informatik.kom.contextsense.utils.TimingsRecords;
import java8.util.function.Supplier;
import java8.util.stream.Stream;

/**
 * Decorator around IDetector which will automatically save it's state after each detection and learning.
 */
class AutosaveDetectorDecorator implements IDetector {

    private final IDetector inner;
    private final Supplier<StorageAdapter> storageProvider;
    boolean didLoad;

    AutosaveDetectorDecorator( IDetector inner, Supplier<StorageAdapter> storageProvider ) {
        this.inner = inner;
        this.storageProvider = storageProvider;
        this.didLoad = false;
    }

    @Override
    public DetectorDescription getDescription() {
        return inner.getDescription();
    }

    @Override
    public Set<String> getDependentLabelGroups() {
        return inner.getDependentLabelGroups();
    }

    @Override
    public Precondition getPrecondition() {
        return inner.getPrecondition();
    }

    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults ) {
        if (!didLoad)
            loadState();

        DetectorResult result = inner.detect( batch, otherResults );
        saveState();
        return result;
    }

    @Override
    public DetectorResult detect( SensorDataBatch batch, DetectionResult otherResults, TimingsRecords timingsRecords ) {
        if (!didLoad)
            loadState();

        DetectorResult result = inner.detect( batch, otherResults, timingsRecords );
        saveState();
        return result;
    }

    @Override
    public void trainIncremental( TrainingSample sample ) {
        inner.trainIncremental( sample );
        saveState();
    }

    @Override
    public void trainBatch( Stream<TrainingSample> samples ) {
        inner.trainBatch( samples );
        saveState();
    }

    @Override
    public void loadState( InputStream in ) throws IOException {
        inner.loadState( in );
        didLoad = true;
    }

    @Override
    public void saveState( OutputStream out ) throws IOException {
        inner.saveState( out );
    }

    private void loadState() {
        StorageAdapter adapter = storageProvider.get();
        String detectorId = getDescription().getId().toString();
        if (!adapter.hasData( detectorId ))
            return;

        try( InputStream in = adapter.load( detectorId )) {
            loadState( in );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveState() {
        StorageAdapter adapter = storageProvider.get();
        String detectorId = getDescription().getId().toString();
        try( OutputStream out = adapter.store( detectorId )) {
            saveState( out );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
