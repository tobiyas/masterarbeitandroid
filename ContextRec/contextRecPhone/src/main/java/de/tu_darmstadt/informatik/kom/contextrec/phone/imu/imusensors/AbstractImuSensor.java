package de.tu_darmstadt.informatik.kom.contextrec.phone.imu.imusensors;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.utils.BTEntry;
import de.tu_darmstadt.informatik.kom.contextsense.SensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.Sensor;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocation;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.SensorInvocations;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.sensors.baseclasses.StreamableSensor;
import java8.util.Optional;

/**
 * Created by Toby on 27.08.2017
 */

public abstract class AbstractImuSensor extends Sensor implements StreamableSensor, CharacteristicsChangedNotifier {

    /**
     * The maximum time before a restart of the Sensor.
     */
    private static final Duration MAX_RESTART_TIME = Duration.standardSeconds( 5 );

    /**
     * The ID to read from a sensor.
     */
    private final BTEntry entry;

    /**
     * The server to use.
     */
    private final BluetoothGatt server;

    /**
     * The last time we got data.
     */
    private DateTime lastDataReceive = new DateTime( 0 );


    protected AbstractImuSensor(Context context, BluetoothGatt server, BTEntry entry) {
        super(context);

        this.entry = entry;
        this.server = server;
    }


    @Override
    public void startStreaming() {
        //This is done in the IMUSource.
    }

    @Override
    public void stopStreaming() {
        //This is done in the IMUSource.
    }

    @Override
    public abstract Optional<? extends SensorData> getStreamingResults(DateTime from, DateTime to);


    @Override
    public BTEntry getEntry() {
        return entry;
    }


    @Override
    public final void gotNewCharacteristics( BluetoothGattCharacteristic characteristic ){
        lastDataReceive = DateTime.now();
        gotNewCharacteristicsIntern( characteristic );
    }


    /**
     * This tells if the Time from the last receive is greater than the defined standart time.
     * @return true if needs a restart.
     */
    public boolean needsToBeRestarted() {
        return DateTime.now().isAfter( lastDataReceive.plus( MAX_RESTART_TIME ) );
    }


    /**
     * If the Data passed is recent.
     * @return true if has recent data.
     */
    public boolean hasRecentData() {
        return DateTime.now().isBefore( lastDataReceive.plus( MAX_RESTART_TIME ) );
    }

    /**
     * Gets the last time we received data.
     * May be 01-01-1970 if none received at all.
     *
     * @return the last time receiving data.
     */
    public DateTime getLastDataReceive() {
        return lastDataReceive;
    }

    /**
     * This is called when we get a new Characteristic.
     *
     * @param characteristic that has been received.
     */
    protected abstract void gotNewCharacteristicsIntern( BluetoothGattCharacteristic characteristic );


    @Override
    protected SensorInvocation doTrigger(Duration batchDuration) {
        //This is not supported at the moment
        return SensorInvocations.emptyInvocation();
    }
}
