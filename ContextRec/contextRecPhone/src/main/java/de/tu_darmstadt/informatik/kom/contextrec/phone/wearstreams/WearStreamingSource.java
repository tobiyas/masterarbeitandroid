package de.tu_darmstadt.informatik.kom.contextrec.phone.wearstreams;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import de.tu_darmstadt.informatik.kom.contextrec.phone.wearstreams.fakesensors.FakeSensorCollection;
import de.tu_darmstadt.informatik.kom.contextsense.CalibrateableDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.StreamingDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.Vibrateable;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import de.tu_darmstadt.informatik.kom.contextsense.utils.CancelableFuture;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 19.08.2017
 */

public class WearStreamingSource extends DataSource implements StreamingDataSource, Closeable, Vibrateable, CalibrateableDataSource {

    /**
     * The Prefix of the Device name.
     */
    private static final String WEAR_DEVICE_PREFIX = "Wear_";

    /**
     * The Offset for the keep alive signal:
     */
    private static final long KEEP_ALIVE_OFFSET = 5 * 1000;

    /**
     * The Offset for the Ping signal:
     */
    private static final long KEEP_PING_OFFSET = 3 * 1001;

    /**
     * The executor for keep alive.
     */
    private static final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    /**
     * The ID of the Node.
     */
    private final String nodeID;


    /**
     * The API Client to use to connect to the Wear device.
     */
    private final GoogleApiClient apiClient;

    /**
     * The listener for the Wear Stream.
     */
    private final WearStreamListener wearStreamListener;

    /**
     * The Map of fake Sensors.
     */
    private final FakeSensorCollection fakeSensors = new FakeSensorCollection();

    /**
     * The keep alive task to cancel when done.
     */
    private ScheduledFuture keepAlive;

    /**
     * If the client should restart the service if it's dead.
     */
    private final boolean restartWearServiceOnDeath;


    public WearStreamingSource(Context context, String wearID, boolean restartWearServiceOnDeath){
        super( DeviceType.WEAR, WEAR_DEVICE_PREFIX + wearID );

        this.nodeID = wearID;
        this.restartWearServiceOnDeath = restartWearServiceOnDeath;
        this.wearStreamListener = new WearStreamListener(wearID, fakeSensors);

        apiClient = new GoogleApiClient.Builder( context )
                .addApi( Wearable.API )
                .build();
    }


    @Override
    public void startStreaming() {
        apiClient.connect();

        //Be sure to clear the Data present, so we have new Fresh data!
        wearStreamListener.clear();

        Wearable.MessageApi.addListener( apiClient, wearStreamListener );
        Wearable.MessageApi.sendMessage( apiClient, nodeID, "StartStreamingFull/", new byte[0] );

        //Keep sending keep alive + Ping:
        keepAlive = scheduler.scheduleWithFixedDelay(this::sendKeepAlive, KEEP_ALIVE_OFFSET, KEEP_ALIVE_OFFSET, TimeUnit.MILLISECONDS);
        keepAlive = scheduler.scheduleWithFixedDelay(this::sendPing, KEEP_PING_OFFSET, KEEP_PING_OFFSET, TimeUnit.MILLISECONDS);
    }


    private void sendKeepAlive(){
        Wearable.MessageApi.sendMessage( apiClient, nodeID, "KeepAliveStreamingFull/", new byte[]{ restartWearServiceOnDeath ? (byte)0x1 : (byte)0x0 } );
    }


    private void sendPing(){
        ByteBuffer buffer = ByteBuffer.allocate( 8 );
        buffer.putLong( System.currentTimeMillis() );

        if(!apiClient.isConnected()) apiClient.connect();
        Wearable.MessageApi.sendMessage( apiClient, nodeID, "Ping/", buffer.array() );
    }


    @Override
    public void stopStreaming() {
        apiClient.connect();
        Wearable.MessageApi.sendMessage( apiClient, nodeID, "StopStreamingFull/", new byte[0] );
        Wearable.MessageApi.removeListener( apiClient, wearStreamListener );

        if( keepAlive != null && !keepAlive.isCancelled() ) keepAlive.cancel( false );
    }

    @Override
    public Optional<SensorDataBatch> collectStreamedData(DateTime from, DateTime to) {
        return fakeSensors.createDataBatch(from,to)
                .map( this::applyOwnSource );
    }

    /**
     * Applies the own Source Name to the Data.
     * @param sensorDataBatch to use.
     * @return the Batch from the input for mapping.
     */
    private SensorDataBatch applyOwnSource(SensorDataBatch sensorDataBatch) {
        stream( sensorDataBatch.getAllData() )
                .forEach( d -> d.setSourceName( getName() ) );
        return sensorDataBatch;
    }


    //Only Streaming Support! The methods below will do nothing!!!

    @Override
    public Set<String> getRequiredPermissions() {
        return new HashSet<>();
    }

    @Override
    protected void doStart() {
        //Not supported!!!
    }

    @Override
    protected void doStop() {
        //Not supported!!!
    }

    @Override
    protected CancelableFuture<Optional<SensorDataBatch>> doCollectData(Duration batchDuration) {
        //Not supported!!!
        return new CancelableFuture<>(Optional::empty);
    }


    @Override
    public void close() throws IOException {
        scheduler.shutdownNow();
    }

    @Override
    public void vibrate() {
        if(!apiClient.isConnected()) apiClient.connect();
        Wearable.MessageApi.sendMessage( apiClient, nodeID, "Vibrate/", new byte[0] );
    }

    @Override
    public void calibrate() {
        this.wearStreamListener.resetClockSkew();
    }
}
