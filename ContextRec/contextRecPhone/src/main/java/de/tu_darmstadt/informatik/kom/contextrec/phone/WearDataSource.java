package de.tu_darmstadt.informatik.kom.contextrec.phone;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.CapabilityApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.StreamingDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensing.DeviceType;
import de.tu_darmstadt.informatik.kom.contextsense.utils.CancelableFuture;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Oli on 05.03.2016
 */
public class WearDataSource extends DataSource implements StreamingDataSource {

    // corresponds to "android_wear_capabilities" in values/wear.xml of wear project.
    public static final String WEAR_CAPABILITY_NAME = "collect_data";
    public static final String TAG = "WearDataSource";

    private static final Duration WEAR_RESPONSE_TIMEOUT = Duration.standardSeconds( 3 );

    private final GoogleApiClient apiClient;
    private final ExecutorService executor;

    public WearDataSource( Context context ) {
        super( DeviceType.WEAR, TAG );

        executor = Executors.newSingleThreadExecutor();
        apiClient = new GoogleApiClient.Builder( context )
                .addApi( Wearable.API )
                .build();
    }

    @Override
    public Set<String> getRequiredPermissions() {
        return new HashSet<>();
    }

    @Override
    protected void doStart() {
        apiClient.connect();
    }

    @Override
    protected void doStop() {
        apiClient.disconnect();
    }

    @Override
    public void startStreaming() {
        apiClient.connect();

        findWearDevice().ifPresent(
                node -> Wearable.MessageApi.sendMessage( apiClient, node.getId(), "StartStreaming/", new byte[0] )
        );
    }

    @Override
    public void stopStreaming() {
        apiClient.disconnect();

        findWearDevice().ifPresent(
                node -> Wearable.MessageApi.sendMessage( apiClient, node.getId(), "StopStreaming/", new byte[0] )
        );
    }



    @Override
    protected CancelableFuture<Optional<SensorDataBatch>> doCollectData( Duration batchDuration ) {
        CancelableFuture<Optional<SensorDataBatch>> task = new CancelableFuture<>( new TriggerInvocation( batchDuration ) );
        executor.execute( task );
        return task;
    }

    @Override
    public Optional<SensorDataBatch> collectStreamedData(DateTime from, DateTime to) {
        CancelableFuture<Optional<SensorDataBatch>> task = new CancelableFuture<>( new StreamingInvocation( from, to ));
        executor.execute( task );

        try {
            return task.get(WEAR_RESPONSE_TIMEOUT.getMillis(), TimeUnit.MILLISECONDS);
        } catch (Throwable e) {
            Log.i(TAG, "Timeout from Watch");
            return Optional.empty();
        }
    }


    /**
     * Finds the next wear device.
     * @return the next wear device.
     */
    private Optional<Node> findWearDevice() {
        ConnectionResult result = apiClient.blockingConnect();
        if (!result.isSuccess()) {
            Log.e( TAG, "Could not connect to Google API:" + result.getErrorMessage() + " " +result.getResolution() );
            return Optional.empty();
        }

        Set<Node> nodes = Wearable.CapabilityApi
                .getCapability( apiClient, WEAR_CAPABILITY_NAME, CapabilityApi.FILTER_REACHABLE )
                .await()
                .getCapability()
                .getNodes();

        // todo: more sophisticated selection
        return stream( nodes )
                .filter( Node::isNearby )
                .findFirst();
    }

    private class TriggerInvocation implements Callable<Optional<SensorDataBatch>> {

        private final Duration batchDuration;
        private byte[] wearResponse;

        private TriggerInvocation(Duration batchDuration ) {
            this.batchDuration = batchDuration;
        }

        @Override
        public Optional<SensorDataBatch> call() throws Exception {
            return triggerOnWearDevice();
        }

        private Optional<SensorDataBatch> triggerOnWearDevice() {
            return findWearDevice()
                    .flatMap( this::sendRequest )
                    .flatMap( this::awaitResponse )
                    .flatMap( this::parseResult );
        }

        private Optional<Node> findWearDevice() {
            ConnectionResult result = apiClient.blockingConnect();
            if (!result.isSuccess()) {
                Log.e( TAG, "Could not connect to Google API:" + result.getErrorMessage() + " " +result.getResolution() );
                return Optional.empty();
            }

            Set<Node> nodes = Wearable.CapabilityApi
                    .getCapability( apiClient, WEAR_CAPABILITY_NAME, CapabilityApi.FILTER_REACHABLE )
                    .await()
                    .getCapability()
                    .getNodes();

            // todo: more sophisticated selection
            return stream( nodes )
                    .filter( Node::isNearby )
                    .findFirst();
        }

        private Optional<Node> sendRequest( Node receiver ) {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            try(DataOutputStream dataOut = new DataOutputStream( out )) {
                dataOut.writeLong( batchDuration.getMillis() );
                dataOut.writeUTF( UUID.randomUUID().toString() );
            } catch (IOException e) {
                e.printStackTrace();
            }

            byte[] message = out.toByteArray();
            Log.i( TAG, "Sending trigger request to "+receiver.getDisplayName() );
            MessageApi.SendMessageResult result = Wearable.MessageApi
                    .sendMessage( apiClient, receiver.getId(), "trigger/", message )
                    .await();

            return result.getStatus().isSuccess() ?
                    Optional.of( receiver ) :
                    Optional.empty();
        }

        private Optional<byte[]> awaitResponse( Node node ) {
            CountDownLatch latch = new CountDownLatch( 1 );
            wearResponse = null;

            MessageApi.MessageListener listener = messageEvent -> {
                if (messageEvent.getSourceNodeId().equals( node.getId() )) {
                    wearResponse = messageEvent.getData();
                    latch.countDown();
                }
            };

            Wearable.MessageApi.addListener( apiClient, listener );

            boolean didReceiveResponse;
            try {
                Duration timeToWait = batchDuration.plus( WEAR_RESPONSE_TIMEOUT );
                didReceiveResponse = latch.await( timeToWait.getMillis(), TimeUnit.MILLISECONDS );
                Log.i( TAG, "Did receive result in time: " + didReceiveResponse );
            } catch (InterruptedException e) {
                return Optional.empty();
            } finally {
                Wearable.MessageApi.removeListener( apiClient, listener );
            }

            return didReceiveResponse ? Optional.of( wearResponse ) : Optional.empty();
        }

        private Optional<SensorDataBatch> parseResult( byte[] bytes ) {
            try(ObjectInputStream in = new ObjectInputStream( new GZIPInputStream( new ByteArrayInputStream( bytes ) ) )) {
                return Optional.of( (SensorDataBatch) in.readObject() );
            } catch (InvalidClassException e) {
                // probably wrong App-Version on the watch.
                Log.e( TAG, "Could not deserialize data from wear device. Is the App-Version on the wear device up-to-date? " );
                return Optional.empty();
            } catch (Exception e) {
                e.printStackTrace();
                return Optional.empty();
            }
        }
    }


    private class StreamingInvocation implements Callable<Optional<SensorDataBatch>> {

        private final DateTime from;
        private final DateTime to;

        private byte[] wearResponse;

        private StreamingInvocation( DateTime from, DateTime to ) {
            this.from = from;
            this.to = to;
        }

        @Override
        public Optional<SensorDataBatch> call() throws Exception {
            return triggerOnWearDevice();
        }

        private Optional<SensorDataBatch> triggerOnWearDevice() {
            return findWearDevice().flatMap(this::execute);
        }

        private Optional<Node> findWearDevice() {
            ConnectionResult result = apiClient.blockingConnect();
            if (!result.isSuccess()) {
                Log.e( TAG, "Could not connect to Google API:" + result.getErrorMessage() + " " +result.getResolution() );
                return Optional.empty();
            }

            Set<Node> nodes = Wearable.CapabilityApi
                    .getCapability( apiClient, WEAR_CAPABILITY_NAME, CapabilityApi.FILTER_REACHABLE )
                    .await()
                    .getCapability()
                    .getNodes();

            // todo: more sophisticated selection
            return stream( nodes )
                    .filter( Node::isNearby )
                    .findFirst();
        }

        private Optional<SensorDataBatch> execute( Node receiver ) {
            long timeBefore = System.currentTimeMillis();
            CountDownLatch latch = new CountDownLatch( 1 );
            wearResponse = null;

            //First add listener, then send!!
            MessageApi.MessageListener listener = messageEvent -> {
                if (messageEvent.getSourceNodeId().equals( receiver.getId() )) {
                    wearResponse = messageEvent.getData();
                    latch.countDown();
                }
            };
            Wearable.MessageApi.addListener( apiClient, listener );


            ByteArrayOutputStream out = new ByteArrayOutputStream();

            try(DataOutputStream dataOut = new DataOutputStream( out )) {
                dataOut.writeLong( from.getMillis() );
                dataOut.writeLong( to.getMillis() );
                dataOut.writeUTF( UUID.randomUUID().toString() );
            } catch (IOException e) {
                e.printStackTrace();
            }

            byte[] message = out.toByteArray();
            Log.i( TAG, "Sending Stream from-to request to "+receiver.getDisplayName() );
            Wearable.MessageApi
                    .sendMessage( apiClient, receiver.getId(), "StreamingFromTo/", message )
                    .setResultCallback(r -> {
                        if(!r.getStatus().isSuccess()){
                            latch.countDown();
                        }
                    });


            //Wait for the Response to settle in!
            try {
                latch.await( WEAR_RESPONSE_TIMEOUT.getMillis(), TimeUnit.MILLISECONDS );
                long tookTime = System.currentTimeMillis() - timeBefore;
                Log.i( TAG, "Did receive result in time: " +  tookTime + "ms, Received Result: " + (wearResponse != null));
            } catch (InterruptedException e) {
                return Optional.empty();
            } finally {
                Wearable.MessageApi.removeListener( apiClient, listener );
            }

            //We did not receive a response in time!
            if(wearResponse == null) return Optional.empty();

            //We must parse the response and return it!
            try(ObjectInputStream in = new ObjectInputStream( new GZIPInputStream( new ByteArrayInputStream( wearResponse ) ) )) {
                return Optional.of( (SensorDataBatch) in.readObject() );
            } catch (InvalidClassException e) {
                // probably wrong App-Version on the watch.
                Log.e( TAG, "Could not deserialize data from wear device. Is the App-Version on the wear device up-to-date? " );
                return Optional.empty();
            } catch (Exception e) {
                e.printStackTrace();
                return Optional.empty();
            }
        }
    }
}
