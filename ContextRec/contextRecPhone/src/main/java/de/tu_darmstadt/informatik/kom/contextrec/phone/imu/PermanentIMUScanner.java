package de.tu_darmstadt.informatik.kom.contextrec.phone.imu;

import android.content.Context;

import de.tu_darmstadt.informatik.kom.contextrec.phone.ContextRecSession;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import java8.util.function.Consumer;

/**
 * Created by Toby on 25.08.2017
 */
public class PermanentIMUScanner {

    /**
     * The consumer to call with datasources.
     */
    private Consumer<DataSource> consumer;

    /**
     * The Connector to use.
     */
    private final ImuConnector connector;


    public PermanentIMUScanner(Context context, Consumer<DataSource> consumer) {
        this.consumer = consumer;
        this.connector = new ImuConnector(context);
    }


    public PermanentIMUScanner(Context context) {
        this(context, null);
    }

    /**
     * Sets the current Consumer
     * @param consumer to set.
     */
    public PermanentIMUScanner setConsumer(Consumer<DataSource> consumer) {
        this.consumer = consumer;
        return this;
    }

    /**
     * Starts scanning.
     * @return this for chaining
     */
    public PermanentIMUScanner start(){
        //Timeout to 4 hours. Should be more than enough!
        int timeOut = 1000 * 60 * 60 * 4;
        connector.getConnected(consumer, timeOut);

        return this;
    }


    /**
     * Stops scanning.
     * @return this for chaining
     */
    public PermanentIMUScanner stop(){
        connector.forceStop();
        return this;
    }

    /**
     * Resets the internal state.
     * @return this for chaining
     */
    public PermanentIMUScanner reset(){
        connector.reset();
        return this;
    }
}
