package de.tu_darmstadt.informatik.kom.contextrec.phone;

import de.tu_darmstadt.informatik.kom.contextsense.DataSource;

/**
 * Created by Toby on 29.08.2017
 */

public interface SourcesChangedListener {

    /**
     * We added a new Source to the Session.
     * @param source that was added
     */
    void sourceAdded( DataSource source );

    /**
     * We Removed a Source from the Session.
     * @param source that was removed
     */
    void sourceRemoved( DataSource source );

}
