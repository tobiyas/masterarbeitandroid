package de.tu_darmstadt.informatik.kom.contextrec.phone.wearstreams;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Queue;

import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;

import static java8.util.stream.StreamSupport.stream;


/**
 * Created by Toby on 09.10.2017
 */

public class PingClockSkewer {

    /**
     * The Default value for max ClockSkews.
     */
    private static final int DEFAULT_MAX_CLOCK_SKEWS = 30;

    /**
     * The messured ClockSkews.
     */
    private final Queue<Pair<Long,Long>> rttClockSkews = new LinkedList<>();

    /**
     * The maximal amount of clockskews saved.
     */
    private final int maxClockSkews;

    /**
     * the currently cached ClockSkew.
     */
    private double cachedClockSkew = 0;

    /**
     * the currently RTT ClockSkew.
     */
    private double cachedRTT = 0;

    /**
     * Creates a PingClockSkewer.
     */
    public PingClockSkewer(){
        this( DEFAULT_MAX_CLOCK_SKEWS );
    }

    /**
     * Creates a PingClockSkewer with the maximal amount of saved skews.
     * @param maxClockSkews to save.
     */
    public PingClockSkewer(int maxClockSkews ){
        this.maxClockSkews = maxClockSkews;
    }


    /**
     * Gets the current ClockSkew.
     * @return the current ClockSkew.
     */
    public double getCurrentClockSkew() {
        return cachedClockSkew;
    }


    /**
     * Gets the current RTT needed.
     * @return the current RTT.
     */
    public double getCurrentRTT() {
        return cachedRTT;
    }


    /**
     * Adds a clockSkew Value.
     * @param rtt to add.
     * @param clockSkew to add.
     */
    public void addValue( long rtt, long clockSkew ){
        rttClockSkews.add( new Pair<>( rtt, clockSkew ) );
        if( rttClockSkews.size() >= maxClockSkews ) rttClockSkews.poll();

        this.cachedClockSkew = stream( rttClockSkews )
                .map( Pair::getSecond )
                .mapToLong( Long::longValue )
                .average()
                .orElse( 0 );

        this.cachedRTT = stream( rttClockSkews )
                .map( Pair::getFirst )
                .mapToLong( Long::longValue )
                .average()
                .orElse( 0 );
    }


    /**
     * Adds a calculated value based on the RTT.
     * This does a calculation of the Time before to the time after and uses the half RTT.
     *
     * @param ownClockBefore the time before sending Ping message.
     * @param otherClock the time of the other clock.
     */
    public void addCalculatedValueNow(long ownClockBefore, long otherClock){
        long now = System.currentTimeMillis();
        long rtt = now - ownClockBefore;
        long ownTimeToOtherClockTime = now - ( rtt / 2);
        long clockSkew = ownTimeToOtherClockTime - otherClock;

        addValue( rtt, clockSkew );
    }


    /**
     * This reads the ClockSkew from the Pong response.
     * The Pong is a Byte Array consisting of 2 Longs.
     * First the send time of this device,
     * second the Time of send from the other device.
     *
     * @param data to read from.
     */
    public void readFromPong( byte[] data ){
        //This is needed to be sure we do not get an error.
        if(data.length < 8 * 2) return;

        ByteBuffer buffer = ByteBuffer.wrap( data );
        long before = buffer.getLong();
        long otherClock = buffer.getLong();

        addCalculatedValueNow( before, otherClock );
    }

    /**
     * Forces a reset.
     */
    public void reset() {
        this.rttClockSkews.clear();
        this.cachedClockSkew = 0;
        this.cachedRTT = 0;
    }
}
