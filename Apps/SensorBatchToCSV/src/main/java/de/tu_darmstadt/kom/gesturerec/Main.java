package de.tu_darmstadt.kom.gesturerec;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.io.FileUtils;

import com.google.common.base.Joiner;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


public class Main {

    private static final String[] types = { "LinearAcceleration", "Gyroscope", "Acceleration", "Orientation" };

    private static final char DEL = ';';
    private static final String NL = "\r\n";


    public static void main( String[] args ) throws Exception {
        File rootFolder;
        if( args == null || args.length == 0 ) {
            //Open GUI for searching:
            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );

            int returnValue = chooser.showOpenDialog( null );
            if( returnValue == JFileChooser.APPROVE_OPTION ){
                rootFolder = chooser.getSelectedFile();
            }else return;
        }else{
            rootFolder = new File( args[0] );
        }

        if( ! rootFolder.exists() ){
            JOptionPane.showMessageDialog( null, "No folder found at the location.");
            return;
        }

        //Read if we want it to the clipboard.
        boolean copyToClipboard = args != null && args.length > 1 && args[1].equalsIgnoreCase("clipboard");

        //Now start reading the Json:
        File batchFile = new File( rootFolder, "batch" );
        File gestureFile = new File( rootFolder, "gestures" );

        //Check if files are present.
        if( !batchFile.exists() ) throw new IOException( "Batch file does not exist: " + batchFile.getPath() );
        if( !gestureFile.exists() ) throw new IOException( "Batch file does not exist: " + gestureFile.getPath() );


        //Read SensorData:
        String content = FileUtils.readFileToString( batchFile, Charset.defaultCharset() );
        content = Main.fixFormat( content );
        JsonArray array = new JsonParser().parse( content ).getAsJsonArray();
        Collection<JsonObject> roots = IntStream.range( 0, array.size() )
                .mapToObj( array::get )
                .map( JsonElement::getAsJsonObject )
                .collect( Collectors.toList() );

        //Get the lowest.
        long firstTime = roots.stream().map( s -> s.get("t").getAsJsonArray().get(0) )
                .mapToLong( JsonElement::getAsLong )
                .min()
                .orElse( 0 );

        //Init the Builder and set Excel stuff:
        StringBuilder builder = new StringBuilder();
        builder.append( "sep=" + DEL ).append(NL);

        //Read Gestures:
        List<String> lines = FileUtils.readLines( gestureFile, Charset.defaultCharset() );
        List<Object[]> gestureData = reformatGestures( lines, firstTime );

        //Iterate the Types:
        for( String type : types ){
            builder.append( type ).append(NL);

            JsonObject leftHand = roots.stream().filter( r -> r.get("sourceName").getAsString().equals("LeftHand") ).filter( r -> r.get( "type" ).getAsString().equals( type ) ).findFirst().orElse( null );
            JsonObject rightHand = roots.stream().filter( r -> r.get("sourceName").getAsString().equals("RightHand") ).filter( r -> r.get( "type" ).getAsString().equals( type ) ).findFirst().orElse( null );
            JsonObject leftFoot = roots.stream().filter( r -> r.get("sourceName").getAsString().equals("LeftFoot") ).filter( r -> r.get( "type" ).getAsString().equals( type ) ).findFirst().orElse( null );
            JsonObject rightFoot = roots.stream().filter( r -> r.get("sourceName").getAsString().equals("RightFoot") ).filter( r -> r.get( "type" ).getAsString().equals( type ) ).findFirst().orElse( null );

            //The Accel values of the IMUs have to be adjusted to m/s not gravity.
            double multi = type.equals( "Acceleration" ) ? 9.81 : 1;
            writeToBuilder( builder, leftFoot, firstTime, multi, gestureData );
            writeToBuilder( builder, rightFoot, firstTime, multi, gestureData );
            writeToBuilder( builder, leftHand, firstTime, 1, gestureData );
            writeToBuilder( builder, rightHand, firstTime, 1, gestureData );
        }


        builder.append( formatGestures( lines, firstTime ) );
        String finishedString = builder.toString();

        //Only copy to clipboard if wanted!
        if( copyToClipboard ){
            StringSelection stringSelection = new StringSelection( finishedString );
            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
            clpbrd.setContents(stringSelection, null);
        }


        //Save to CSV file:
        File saveFile = new File( rootFolder, "ConvertedExcelFile.csv" );
        if( saveFile.exists() ) saveFile.delete();
        FileUtils.write( saveFile, finishedString, Charset.defaultCharset() );

        System.out.println( "Done" );
        System.exit( 0 );
    }

    private static String formatGestures( List<String> lines, long start ) {
        List<String[]> splits = lines.stream()
                .map( line -> line.split( Pattern.quote( ":"  ) ) )
                .collect( Collectors.toList() );

        //First format everything:
        for( int i = 0; i < splits.size(); i++ ){
            String first = splits.get( i )[1].replaceAll("[^\\d.]", "");
            String second = splits.get( i )[2].replaceAll("[^\\d.]", "");
            String third = splits.get( i )[3].replace( " g", "").trim();
            String fourth = splits.get( i )[4].trim();

            splits.get( i )[1] = first;
            splits.get( i )[2] = second;
            splits.get( i )[3] = third;
            splits.get( i )[4] = fourth;
        }

        //First: Add the header line + :
        StringBuilder builder = new StringBuilder();
        builder.append( "Gestures" ).append( NL );
        for( int i = 0; i < splits.size(); i++ ){
            double value1 = ( Long.parseLong( splits.get( i )[1] ) - start ) / 1000d;
            double value2 = ( Long.parseLong( splits.get( i )[2] ) - start ) / 1000d;

            builder.append( value1-0.001d )
                    .append( DEL )
                    .append( value1 )
                    .append( DEL )
                    .append( value2 )
                    .append( DEL )
                    .append( value2 + 0.001d )
                    .append( DEL );
        }

        builder.append( NL );


        //now add the Values for the Gestures:
        for( int i = 0; i < splits.size(); i++ ){
            builder.append( 0 ).append( DEL ).append( "1" ).append( DEL ).append( "1" ).append( DEL ).append( "0" ).append( DEL );
        }
        builder.append( NL );

        //Write the Gestures:
        for( int i = 0; i < splits.size(); i++ ){
            builder.append( "" ).append(DEL);
            builder.append( splits.get( i )[3] ).append(DEL);
            builder.append( splits.get( i )[3] ).append(DEL);
            builder.append( "" ).append(DEL);
        }

        builder.append( NL );
        builder.append( NL );
        return builder.toString();
    }


    private static void writeToBuilder( StringBuilder toWriteTo, JsonObject root, long start, double multiAccel, List<Object[]> gestureData ){
        if( root == null || toWriteTo == null || multiAccel == 0 ) return;

        String source = root.get( "sourceName" ).getAsString();
        String type = root.get( "type" ).getAsString();
        JsonArray elements = root.get( "t" ).getAsJsonArray();
        Double[] times = IntStream.range( 0, elements.size() )
                .mapToObj( elements::get )
                .mapToLong( JsonElement::getAsLong )
                .map( t -> t - start )
                .mapToDouble( t -> t / 1000d )
                .mapToObj( Double::valueOf )
                .toArray( Double[]::new );

        elements = root.get( "x" ).getAsJsonArray();
        Double[] x = IntStream.range( 0, elements.size() )
                .mapToObj( elements::get )
                .mapToDouble( JsonElement::getAsDouble )
                .map( s -> s*multiAccel )
                .mapToObj( Double::valueOf )
                .toArray( Double[]::new );

        elements = root.get( "y" ).getAsJsonArray();
        Double[] y = IntStream.range( 0, elements.size() )
                .mapToObj( elements::get )
                .mapToDouble( JsonElement::getAsDouble )
                .map( s -> s*multiAccel )
                .mapToObj( Double::valueOf )
                .toArray( Double[]::new );

        elements = root.get( "z" ).getAsJsonArray();
        Double[] z = IntStream.range( 0, elements.size() )
                .mapToObj( elements::get )
                .mapToDouble( JsonElement::getAsDouble )
                .map( s -> s*multiAccel )
                .mapToObj( Double::valueOf )
                .toArray( Double[]::new );


        double min = Arrays.stream( new Double[][]{ x, y, z } )
                .flatMap( Arrays::stream )
                .mapToDouble( Double::doubleValue )
                .min()
                .orElse( 0 );

        double max = Arrays.stream( new Double[][]{ x, y, z } )
                .flatMap( Arrays::stream )
                .mapToDouble( Double::doubleValue )
                .max()
                .orElse( 0 );

        //At last format the Gesture data:
        Double[] gestures = new Double[ times.length ];
        double last = min;
        for( int i = 0; i < times.length; i++ ){
            if( isGesture1(gestureData, times[i]) ) {
                if( last == min && i > 0 ) gestures[i-1] = min;

                gestures[i] = max;
                last = max;
                continue;
            }

            if( last == min ){
                gestures[i] = 0d;
                continue;
            }

            gestures[i] = min;
            last = min;
        }


        //do a write to as CSF:
        StringBuilder builder = new StringBuilder();
        builder.append( type ).append( DEL ).append( source ).append( NL );
        builder.append("time").append( DEL ).append( Joiner.on( DEL ).join( times ) ).append( NL );
        builder.append("x").append( DEL ).append( Joiner.on( DEL ).join( x )  ).append( NL );
        builder.append("y").append( DEL ).append( Joiner.on( DEL ).join( y )  ).append( NL );
        builder.append("z").append( DEL ).append( Joiner.on( DEL ).join( z )  ).append( NL );
        builder.append("gesture").append( DEL ).append( Joiner.on( DEL ).join( gestures ).replace( DEL+"0.0"+DEL, DEL+""+DEL).replace( DEL+"0.0"+DEL, DEL+""+DEL)  ).append( NL );

        toWriteTo.append( builder.toString() );
        toWriteTo.append( NL );
        toWriteTo.append( NL );
    }


    private static boolean isGesture1( List<Object[]> elements, double time ) {
        for( Object[] array : elements ){
            if( ( time > (double)array[0] ) && ( time < (double)array[1] ) ) return true;
        }

        return false;
    }



    public static List<Object[]> reformatGestures( List<String> lines, long start ){
        List<String[]> splits = lines.stream()
                .map( line -> line.split( Pattern.quote( ":"  ) ) )
                .collect( Collectors.toList() );

        List<Object[]> fixedGestures = new ArrayList<>();

        //First format everything:
        for( int i = 0; i < splits.size(); i++ ){
            Object[] newPart = new Object[4];

            String[] old = splits.get( i );
            String first = old[1].replaceAll("[^\\d.]", "");
            String second = old[2].replaceAll("[^\\d.]", "");
            String third = old[3].replace( " g", "").trim();
            String fourth = old[4].trim();

            newPart[0] = ( Long.valueOf( first ) - start ) / 1000d;
            newPart[1] = ( Long.valueOf( second ) - start ) / 1000d;
            newPart[2] = third;
            newPart[3] = fourth;
            fixedGestures.add( newPart );
        }

        return fixedGestures;
    }



    public static String fixFormat( String brokenJsonString ){
        char[] charred = brokenJsonString.toCharArray();

        int brackets = 0;
        StringBuilder builder = new StringBuilder().append("[");
        String tmpNone = "";
        int pos = 0;
        int found = 0;
        String foundType = "";


        while( pos < charred.length ){
            char current = charred[ pos ];

            //We are in detect for Type Mode:
            if( brackets == 0 && foundType.isEmpty() ){
                tmpNone += current;

                for( int i = 0; i < types.length; i++ ){
                    String splitter = types[i];
                    if( tmpNone.endsWith( splitter ) ){
                        foundType = splitter;
                        found++;
                        break;
                    }
                }

                pos++;
                continue;
            }

            //We know the Type -> Now we are skipping till we reach a bracket!
            if( current != '{' && !foundType.isEmpty() ){
                pos++;
                continue;
            }

            //We have the start of a bracket: Now we start writing!
            if( current == '{' && !foundType.isEmpty() ){
                if( found != 1 ) builder.append(DEL);

                builder.append( "{\"type\":\"" + foundType + "\"," );
                pos++;
                foundType = "";
                brackets++;
                continue;
            }


            builder.append( current );
            if( current == '{' ) brackets ++;
            if( current == '}' ) brackets --;
            pos++;
        }

        return builder.append("]").toString();
    }
}
