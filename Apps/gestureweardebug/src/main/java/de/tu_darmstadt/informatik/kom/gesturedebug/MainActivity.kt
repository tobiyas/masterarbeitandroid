package de.tu_darmstadt.informatik.kom.gesturedebug

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import android.util.Log
import android.view.View
import android.widget.Button

class MainActivity : WearableActivity() {

    fun r(f: () -> Unit): Runnable = object : Runnable {override fun run() {f()}}


    private val STOP_COMMAND = "STOP_HAMMER_TIME"
    private val SERVICE_NAME = "de.tu_darmstadt.informatik.kom.contextrec.wear.WearSensorStreamingToPhoneService"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Enables Always-on
        setAmbientEnabled()

        val button = this.findViewById<Button>(R.id.button)
        button.postDelayed(r { updateView( button ) }, 1000 )
        button.setOnClickListener{ kill(button) }

        val button2 = this.findViewById<Button>(R.id.button2)
        button2.setOnClickListener{ list() }
    }


    fun updateView(view: View){
        view.isEnabled = isEnabled(SERVICE_NAME)
        view.postDelayed(r { updateView( view ) }, 1000 )
    }


    fun kill(view : View) {
        try {
            if( isEnabled(SERVICE_NAME) ) {
                startService( genIntent() )
                view.isEnabled = false
                Log.i("TEST_KILL", "Sent kill request to: " + SERVICE_NAME)
            }
        }catch (e : Throwable){
            e.printStackTrace()
        }
    }


    fun genIntent() : Intent{
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val service = manager.getRunningServices(Integer.MAX_VALUE).filter { SERVICE_NAME == it.service.className }.first()

        val intent = Intent(this, service.service.javaClass)
        intent.putExtra(STOP_COMMAND, true)

        return intent
    }


    fun isEnabled(name : String) : Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        return manager.getRunningServices(Integer.MAX_VALUE).any { name == it.service.className }
    }


    fun list() {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        return manager.getRunningServices(Integer.MAX_VALUE).forEach { t -> Log.e("TASK", "Task active: " + t.service.className) }
    }
}
