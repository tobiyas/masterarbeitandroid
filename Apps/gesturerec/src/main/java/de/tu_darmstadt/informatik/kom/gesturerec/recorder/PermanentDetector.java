package de.tu_darmstadt.informatik.kom.gesturerec.recorder;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.Closeable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResult;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.GestureDetector;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;

import static java8.util.stream.StreamSupport.stream;

/**
 * This represents the attempt of a sliding Window.
 * We need some Infos for this to work:
 * - the time a gesture may take
 * - the Detector to use
 * - The Session to use
 *
 * Then we start to perm-detect.
 *
 * @author Tobias Welther
 */
public class PermanentDetector implements Closeable {


    public enum Mode{
        MIN_ACC,
        DETECT_QUEUE,
        DETECT_QUEUE_MIN_ACC
    }

    /**
     * The Minimal security that the Detector has to give.
     */
    private static final double DEFAULT_MIN_SECURITY_ACC = 0.5;

    /**
     * The amount of executions per second to execute.
     */
    private static final int DEFAULT_EXECUTIONS_PER_SECOND = 6;

    /**
     * The maximal amount of detects to detect.
     */
    private static final int MAX_DETECT_QUEUE_SIZE = 3;



    /**
     * The Executor to use as scheduler.
     */
    private final ScheduledExecutorService executor;

    /**
     * The session to hook to.
     */
    private final GestureRecSession session;

    /**
     * The Intervall to detect.
     */
    private final long detectInterval;

    /**
     * The Timeout after a successful detect:
     */
    private final long timeoutAfterDetect;

    /**
     * The amount of detects per second.
     */
    private final int detectsPerSecond;

    /**
     * The minimal security to use for triggering.
     */
    private double minSecurity;

    /**
     * The actual Detector to use.
     */
    private final GestureDetector detector;

    /**
     * The Set of Consumers to call when got a gesture.
     */
    private final Collection<GestureCallback> consumers = new HashSet<>();

    /**
     * The current to use task.
     */
    private ScheduledFuture currentTask;

    /**
     * if the System is currently running.
     */
    private boolean running = false;

    /**
     * The current DetectMode.
     */
    private Mode detectMode = Mode.MIN_ACC;

    /**
     * The time to skip the same gesture.
     */
    private Duration skipSameGestureFor = Duration.millis( 0L );

    /**
     * The last detected Gesture.
     */
    private Pair<DateTime,Gesture> lastDetected = new Pair<>( new DateTime( 0L ), GestureManager.UNKNOWN() );

    /**
     * The last top detections.
     */
    private final LinkedList<GestureResult> lastTop = new LinkedList<>() ;

    /**
     * The Set of Gestures to use as Skip-Gestures.
     * They cause a shorter Hault.
     * This is supposed to be for some sort of IDLE stuff.
     */
    private final Collection<Gesture> skipGestures = new HashSet<>();


    /**
     * This creates a new Perm-Detector.
     * WARNING: This may take some time, since the Model is loading in here.
     * WARNING 2: This may throw an Error, if the Loading of the Model fails.
     *
     * @param session to crate with.
     * @param detectInterval to use for estimating intervals.
     * @param timeoutAfterDetect to apply after detection + interval.
     * @param detectsPerSecond the amount of detects to run per second.
     * @param minSecurity the minimal security to have before triggering ( between 0 and 1 ).
     * @param model to use for detection.
     *
     * @throws Exception May throw this exception if the Model fails to load.
     */
    @SuppressWarnings({"JavaDoc", "WeakerAccess"})
    public PermanentDetector( GestureRecSession session,
                              long detectInterval,
                              long timeoutAfterDetect,
                              int detectsPerSecond,
                              double minSecurity,
                              ModelProxy model ) {

        this.executor = Executors.newScheduledThreadPool( detectsPerSecond );

        this.session = session;
        this.detectInterval = detectInterval;
        this.timeoutAfterDetect = timeoutAfterDetect;
        this.detectsPerSecond = detectsPerSecond;
        this.minSecurity = minSecurity;

        this.detector = model.getGestureDetector();
    }

    /**
     * This creates a new Perm-Detector.
     * WARNING: This may take some time, since the Model is loading in here.
     * WARNING 2: This may throw an Error, if the Loading of the Model fails.
     *
     * @param session to crate with.
     * @param detectInterval to use for estimating intervals.
     * @param timeoutAfterDetect to apply after detection + interval.
     * @param model to use for detection.
     *
     * @throws Exception May throw this exception if the Model fails to load.
     */
    @SuppressWarnings("JavaDoc")
    public PermanentDetector( GestureRecSession session,
                             long detectInterval,
                             long timeoutAfterDetect,
                             ModelProxy model ) {

        this( session, detectInterval, timeoutAfterDetect, DEFAULT_EXECUTIONS_PER_SECOND, DEFAULT_MIN_SECURITY_ACC, model );
    }


    /**
     * These Gestures will be used as Placeholders.
     * When they occur, there will be only a shorter timeout after detection.
     */
    public void addSkipGestures( Gesture... gestures ){
        this.skipGestures.addAll( Arrays.asList( gestures ) );
    }

    /**
     * This skips the same gesture for a duration.
     * @param duration to choose.
     */
    public void skipSameGestureFor( Duration duration ){
        this.skipSameGestureFor = duration;
    }

    /**
     * Changes the Detection Mode of the Detector.
     * @param detectMode to set.
     */
    public void setDetectMode( Mode detectMode ) {
        if( detectMode != null ) this.detectMode = detectMode;
    }

    /**
     * Gets the currently used Detect Mode.
     * @return the currently used DetectMode.
     */
    public Mode getDetectMode() {
        return detectMode;
    }


    public void changeMinAccuracy( double minSecurity ){
        this.minSecurity = minSecurity;
    }

    public double getMinSecurity() {
        return minSecurity;
    }

    /**
     * Starts the Detection.
     * This waits the length specified in the Constructor for the first detection.
     */
    public PermanentDetector start(){
        if( executor.isShutdown() ) throw new IllegalStateException( "Executor is already Shutdown! Please create a new Permanent Detector." );

        //If we are already running -> Do not do anything!
        if( running ) return this;

        //If a task is still running, something is wrong here! Kill it and restart:
        if( currentTask != null ) {
            currentTask.cancel( true );
            currentTask = null;
        }

        running = true;
        executor.schedule(
                this::startNow,
                detectInterval,
                TimeUnit.MILLISECONDS
        );

        return this;
    }


    /**
     * Stops the Detection.
     */
    public void stop(){
        running = false;
        if( currentTask != null ) {
            currentTask.cancel( true );
            currentTask = null;
        }
    }

    /**
     * If the System is currently running.
     * @return true if running.
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Starts a detection now.
     */
    private void startNow(){
        if( !running ) return;

        if( currentTask != null ) {
            currentTask.cancel( true );
            currentTask = null;
        }

        currentTask = executor.scheduleAtFixedRate(
                this::detectNow,
                timeoutAfterDetect,
                1000L / detectsPerSecond,
                TimeUnit.MILLISECONDS
        );
    }

    /**
     * Starts a detection cycle.
     * This detects the Last {@link #detectInterval} Millis.
     */
    private void detectNow() {
        if( !running ) {
            //We have a race condition: Check if we need to stop here:
            if( currentTask != null ){
                currentTask.cancel( true );
                currentTask = null;
            }

            return;
        }

        DateTime now = DateTime.now();
        DateTime end = now.minus( Duration.millis( 200 ) );
        DateTime start = end.minus( Duration.millis( detectInterval ) );

        DataRecorder recorder = session.getDataRecorder();
        SensorDataBatch batch = recorder.getDataNow( start, end );

        //Can't do anything here!
        if( batch == null ) return;

        //Analyse the result:
        GestureResultWrapper result = detector.analyseGesture( batch );
        GestureResult top = result.getTopResult();

        //No result -> Go away!
        if( top == null ) return;

        //Check for Queue Detect-Mode.
        if( detectMode == Mode.DETECT_QUEUE || detectMode == Mode.DETECT_QUEUE_MIN_ACC ){
            //Add to detect Queue:
            this.lastTop.add( top );
            if( lastTop.size() > MAX_DETECT_QUEUE_SIZE ) lastTop.poll();

            //Check for triple detects:
            boolean onlyOne = stream( lastTop ).map( GestureResult::getFirst ).distinct().count() == 1;
            if( !onlyOne ) return;

            //No full queue yet!
            if( lastTop.size() != MAX_DETECT_QUEUE_SIZE ) return;

            //No full queue yet!
            if( lastTop.size() != MAX_DETECT_QUEUE_SIZE ) return;

            //Never evaluate UNKNOWN Gestures!
            if( top.getFirst() == GestureManager.UNKNOWN() ) return;

            //Check if we have any below min-detect:
            if( detectMode == Mode.DETECT_QUEUE_MIN_ACC ){
                boolean belowMinSecurity = stream( lastTop ).anyMatch( d -> d.getSecond() < minSecurity );
                if( belowMinSecurity ) return;
            }

            //Check if we need to skip due to too early:
            if( top.getFirst().equals( lastDetected.getSecond() )
                    && DateTime.now().isBefore( lastDetected.getFirst().plus( skipSameGestureFor ) ) ){
                return;
            }

            //We have a valid result!
            lastTop.clear();
            gestureDetected( batch , result );
            return;
        }

        //Check for simple Min-Acc:
        if( detectMode == Mode.MIN_ACC ){
            //Check if we have a valid result:
            if( top.getFirst() == GestureManager.UNKNOWN() ) return;

            //Check if we have more than the max acc:
            if( top.getSecond() < minSecurity ) return;

            //Check if we need to skip due to too early:
            if( top.getFirst().equals( lastDetected.getSecond() )
                    && DateTime.now().isBefore( lastDetected.getFirst().plus( skipSameGestureFor ) ) ){
                return;
            }

            gestureDetected( batch, result );
        }
    }

    /**
     * We got a result!
     *
     * @param top that was detected.
     * @param batch to use for time stuff.
     */
    private void gestureDetected( SensorDataBatch batch, GestureResultWrapper top ) {
        //If this is present, we have already got a result and got some tasks afterwards that are still running.
        if( currentTask == null ) return;
        if( !running ) return;

        currentTask.cancel( true );
        currentTask = null;

        //Update the last detected:
        this.lastDetected = new Pair<>( DateTime.now(), top.getTopResult().getFirst() );

        //Schedule a restart:
        long offset = skipGestures.contains( top.getTopResult().getFirst() ) ? timeoutAfterDetect : ( detectInterval / 2 );
        executor.schedule( this::startNow, offset, TimeUnit.MILLISECONDS );

        //Do a callback to all listeners:
        MainThread.runOnMT( () ->
                stream( new HashSet<>( consumers ) )
                    .forEach( c -> callback( c, batch, top ) )
        );
    }


    /**
     * A wrapper for the Callback mainly for Try-Catch.
     *
     * @param callback to call.
     * @param batch to use.
     * @param top to use.
     */
    private void callback( GestureCallback callback, SensorDataBatch batch, GestureResultWrapper top ){
        try{ callback.gotGesture( batch, top ); }catch ( Throwable exp ){ exp.printStackTrace(); }
    }


    /**
     * This adds a new callback.
     * The Callback WILL be on the MainThread!!!
     *
     * @param callback to add.
     */
    public void addCallback( GestureCallback callback ) {
        if( callback != null ) this.consumers.add( callback );
    }

    /**
     * This removes the callback past.
     * @param callback to remove.
     */
    public void removeCallback( GestureCallback callback ) {
        if( callback != null ) this.consumers.remove( callback );
    }


    /**
     * Closes this Detector and makes it unuseable.
     */
    @Override
    public void close(){
        stop();

        this.executor.shutdownNow();
        this.consumers.clear();
    }


    /**
     * Gets the used detector.
     * @return the used detector.
     */
    public GestureDetector getDetector() {
        return detector;
    }


    public interface GestureCallback {

        /**
         * We got a gesture detected.
         *
         * @param batch that was used.
         * @param wrapper that was detected.
         */
        void gotGesture( SensorDataBatch batch, GestureResultWrapper wrapper );
    }
}
