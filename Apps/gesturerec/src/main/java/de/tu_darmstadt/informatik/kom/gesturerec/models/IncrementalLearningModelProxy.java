package de.tu_darmstadt.informatik.kom.gesturerec.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetectorBuilder;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.Collectors2;
import weka.classifiers.Classifier;

import static de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxyFactory.JSON_INCREMENTAL_LEARNER_PATH;
import static de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxyFactory.JSON_INCREMENTAL_LEARNER_SAMPLES_PATH;
import static java8.util.stream.StreamSupport.stream;

/**
 * This class represents a Subclass of the {@link ModelProxy}.
 * It adds an incremental Learning Component.
 */

public class IncrementalLearningModelProxy extends ModelProxy {

    /**
     * The used samples in the current Iteration.
     */
    private final SampleBatch usedSamples = new SampleBatch();


    /**
     * Creates a new Proxy with the passed data.
     * This links to a not yet loaded Classifier.
     *
     * @param session this belongs to.
     * @param classifier type to use
     * @param name to use
     * @param gestures to use
     * @param batch of present samples
     */
    public IncrementalLearningModelProxy( GestureRecSession session,
                                         WekaBaseClassifier classifier,
                                         String name,
                                         List<Gesture> gestures,
                                         SampleBatch batch ) {

        super( session, classifier, name, gestures );

        this.usedSamples.addBatch( batch );
    }


    /**
     * Creates a new Proxy with the passed data.
     * This links to a loaded Classifier.
     *
     * @param name to use
     * @param gestures to use
     * @param batch of present samples
     */
    public IncrementalLearningModelProxy( GestureRecSession session,
                                         String name,
                                         List<Gesture> gestures,
                                         Classifier classifier,
                                         SampleBatch batch ) {

        super( session, name, gestures, classifier );

        this.usedSamples.addBatch( batch );
    }


    /**
     * Adds the passed proxies to the Model, Builds a new Model out of the Data and Saves it.
     *
     * @param proxies to add.
     * @throws Exception if something goes wrong!!
     */
    public void incrementalLearn( SampleProxy... proxies ) throws Exception {
        incrementalLearn( new SampleBatch( proxies ) );
    }


    /**
     * Adds the passed proxies to the Model, Builds a new Model out of the Data and Saves it.
     *
     * @param batch to add.
     * @throws Exception if something goes wrong!!
     */
    public void incrementalLearn( SampleBatch batch ) throws Exception {
        SampleBatch newBatch = new SampleBatch( usedSamples ).addBatch( batch );
        //If no new Samples -> Break!
        if( newBatch.size() == this.usedSamples.size() ) return;

        //Create the new Model with the Samples present + new.
        ModelProxy tmp = create( session, getName(), getClassifierType(), newBatch );

        Classifier newClassifier = tmp.getClassifier();
        if( newClassifier == null ) throw new Exception( "Classifier is Null" );

        //Update present model in this proxy:
        lazyClassifier.forceValue( newClassifier );

        this.usedSamples.clear();
        this.usedSamples.addBatch( newBatch );

        //Save changes to be persistent.
        save();
    }


    /**
     * This additionally saves the Incremental learning part.
     *
     * @return true if worked.
     */
    @Override
    protected JsonObject generateMeta() {
        JsonObject root = super.generateMeta();
        root.addProperty(JSON_INCREMENTAL_LEARNER_PATH, true );

        //Generate array with Timestamps of Samples:
        JsonArray samples = stream( usedSamples.getSamples() )
                .map( SampleProxy::getRecordTime )
                .map( DateTime::getMillis )
                .collect( Collectors2.toGsonArray() );

        root.add( JSON_INCREMENTAL_LEARNER_SAMPLES_PATH, samples );

        return root;
    }


    /**
     * Gets a copied sample batch of the Samples used in the current state.
     * @return the present samples.
     */
    public SampleBatch getPresentSamples(){
        return new SampleBatch( usedSamples );
    }



    //Copied from SimpleModelCreator:
    private static ModelProxy create( GestureRecSession session, String name, WekaBaseClassifier type, SampleBatch batch ){
        //Create a situationGroup to create.
        SituationGroup situationGroup = new SituationGroup( name, batch.getGestures() );

        //Load all samples:
        List<TrainingSample> trainingSamples = batch.loadSamples( situationGroup );

        try{
            WekaDetector detector = new WekaDetectorBuilder( "de.tu_darmstadt.gestures.Detector", name )
                    .forSituation( situationGroup )
                    .withClassifier( type )
                    .withFeatures( session.getFeatureFactory().buildFeatures( new ArrayList<>(  batch.getGestures() ) ) )
                    .withPrecondition( new NoPrecondition() )
                    .withSamples( stream( trainingSamples ) )
                    .build();

            return new ModelProxy( session, name, batch.getGestures(), detector.getModel() );
        }catch ( Throwable exp ){
            throw new RuntimeException( "Error on creation", exp );
        }
    }


}