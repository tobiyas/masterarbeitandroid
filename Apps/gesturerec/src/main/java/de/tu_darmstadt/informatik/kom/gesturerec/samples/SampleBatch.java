package de.tu_darmstadt.informatik.kom.gesturerec.samples;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java8.util.Objects;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import java8.util.stream.Collectors;
import java8.util.stream.Stream;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 06.12.2017
 */

public class SampleBatch {

    /**
     * The samples to work on.
     */
    private final Collection<SampleProxy> samples = new HashSet<>();

    /**
     * All gestures present.
     */
    private final Collection<Gesture> gestures = new HashSet<>();


    public SampleBatch( Collection<SampleProxy> samples, Collection<Gesture> gestures ) {
        if( samples != null ) this.samples.addAll( samples );
        if( gestures != null ) this.gestures.addAll( gestures );
    }


    /**
     * Creates a new Batch.
     * The gestures are extracted from the Samples.
     *
     *  @param samples to add.
     */
    public SampleBatch( SampleProxy... samples ) {
        this( samples == null ? null : Arrays.asList( samples ) );
    }

    /**
     * Creates a new Batch.
     * The gestures are extracted from the Samples.
     *
     *  @param samples to add.
     */
    public SampleBatch( Collection<SampleProxy> samples ) {
        if( samples != null ) this.samples.addAll( samples );
        if( samples != null ) {
            stream( samples )
                .map( SampleProxy::getGesture )
                .distinct()
                .forEach( gestures::add );
        }
    }


    /**
     * Creates an empty Sample Batch.
     */
    public SampleBatch(){}


    /**
     * Creates a copied Batch.
     */
    public SampleBatch( SampleBatch other ){
        if( other != null ) {
            this.samples.addAll( other.getSamples() );
            this.gestures.addAll( other.getGestures() );
        }
    }


    /**
     * This physically loads the Trainingssamples.
     *
     * @param group to load for.
     *
     * @return the loaded Samples.
     */
    public List<TrainingSample> loadSamples( SituationGroup group ){
        return stream( samples )
                .map( SampleProxy::loadIfNotPresent )
                .map( sample -> sample.toTrainingsSample( group ) )
                .collect( Collectors.toList() );
    }

    /**
     * This physically loads the Trainingssamples.
     *
     * @param situationName to load with.
     *
     * @return the loaded Samples.
     */
    public List<TrainingSample> loadSamplesWithName( String situationName ){
        return loadSamples( new SituationGroup( situationName, getGestures() ) );
    }

    /**
     * This physically loads the Trainingssamples.
     *
     * @return the loaded Samples.
     */
    public List<TrainingSample> loadSamplesWithRandomName(){
        return loadSamplesWithName( String.valueOf( System.currentTimeMillis() ) );
    }


    /**
     * This physically loads the Trainingssamples.
     *
     * @param group to load for.
     *
     * @return the loaded Samples.
     */
    public Stream<TrainingSample> streamLoadSamples( SituationGroup group ){
        return stream( samples )
                .map( SampleProxy::loadIfNotPresent )
                .map( sample -> sample.toTrainingsSample( group ) );
    }


    /**
     * This physically loads the Trainingssamples.
     *
     * @param name to load for.
     *
     * @return the loaded Samples.
     */
    public Stream<TrainingSample> streamLoadSamplesWithName( String name ){
        return streamLoadSamples( new SituationGroup( name, getGestures() ) );
    }

    /**
     * This physically loads the Trainingssamples.
     *
     * @return the loaded Samples.
     */
    public Stream<TrainingSample> streamLoadSamplesWithRandomName( ){
        return streamLoadSamplesWithName( String.valueOf( System.currentTimeMillis() ) );
    }



    /**
     * Gets the Size of the Samples present.
     */
    public int size(){
        return samples.size();
    }


    /**
     * Returns true when empty.
     * @return true when empty.
     */
    public boolean isEmpty(){
        return size() <= 0;
    }

    /**
     * Simply gets the Samples.
     * This is a copy!
     *
     * @return the samples.
     */
    public Collection<SampleProxy> getSamples(){
        return new ArrayList<>( samples );
    }

    /**
     * Simply gets the Samples.
     * @return the samples.
     */
    public SampleFilterStream query( GestureRecSession session ){
        return new SampleFilterStream( session.getGestureManager(), samples );
    }


    /**
     * Streams the present Proxies.
     * @return a stream of the present Proxies.
     */
    public Stream<SampleProxy> streamSamples(){
        return stream( samples );
    }

    /**
     * Gets all Gestures present.
     *
     * @return the present gestures.
     */
    public List<Gesture> getGestures() {
        return new ArrayList<>( gestures );
    }


    /**
     * Adds all Data from the other batch to this one.
     * @param batch to add.
     */
    public SampleBatch addBatch( SampleBatch batch ){
        if( batch == null ) return this;

        this.samples.addAll( batch.getSamples() );
        this.gestures.addAll( batch.getGestures() );

        return this;
    }

    /**
     * Adds multiple batches.
     * @param otherBatches to add.
     * @return this for chaining.
     */
    public SampleBatch addBatches( Collection<SampleBatch> otherBatches ) {
        if( otherBatches == null ) return this;
        stream( otherBatches )
                .filter( Objects::nonNull )
                .forEach( this::addBatch );

        return this;
    }


    /**
     * Adds the Sample passed plus it's Gesture if not prsent..
     * @param proxy to add.
     */
    public SampleBatch addSample( SampleProxy proxy ){
        if( proxy == null ) return this;

        this.samples.add( proxy );
        this.gestures.add( proxy.getGesture() );

        return this;
    }


    /**
     * Adds the Sample passed plus it's Gesture if not prsent..
     * @param proxys to add.
     */
    public SampleBatch addSamples( SampleProxy... proxys ){
        if( proxys == null || proxys.length == 0 ) return this;
        return addSamples( Arrays.asList( proxys ) );
    }


    /**
     * Adds the Sample passed plus it's Gesture if not prsent..
     * @param proxys to add.
     */
    public SampleBatch addSamples( Collection<SampleProxy> proxys ){
        if( proxys == null || proxys.isEmpty() ) return this;
        stream( proxys ).forEach( this::addSample );

        return this;
    }


    /**
     * This removes all Samples present in the other Batch.
     *
     * @param otherBatch to use.
     * @return this for chaining.
     */
    public SampleBatch removeSamples( SampleBatch otherBatch ) {
        return removeSamples( otherBatch.getSamples() );
    }


    /**
     * This removes all Samples passed.
     *
     * @param samples to remove.
     * @return this for chaining.
     */
    public SampleBatch removeSamples( Collection<SampleProxy> samples ) {
        for( SampleProxy proxy : samples ) {
            this.samples.remove( proxy );
        }

        return this;
    }


    /**
     * This purges all Gestures with 0 occurances.
     * @return this for chaining.
     */
    public SampleBatch purgeUnusedGestures(){
        //Clear the Gestures and add only present:
        this.gestures.clear();
        stream( samples )
                .map( SampleProxy::getGesture )
                .distinct()
                .forEach( gestures::add );

        //Return for chaining.
        return this;
    }



    /**
     * Clears the Batch and it's gestures.
     */
    public void clear(){
        this.gestures.clear();
        this.samples.clear();
    }


    /**
     * Saves this batch as ARFF File.
     * This simply redirects to {@link ARFFCreator}
     *
     * @param session to use for creating the ARFF.
     * @param file to save to.
     */
    public void saveToARFFFile( GestureRecSession session, File file ) throws Exception {
        ARFFCreator.createARFFile( session, file, this );
    }
}
