package de.tu_darmstadt.informatik.kom.gesturerec.gestures;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.utils.DefaultHashMap;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 09.10.2017
 */

public class GestureResultWrapper {

    /**
     * The Resultset to wrap
     */
    private final Set<GestureResult> resultSet;

    /**
     * A debug map for timings taken in Milliseconds.
     * Key is the Action, Value is the Time.
     */
    private final Map<String,Long> debugTimeTaken = new DefaultHashMap<>( 0L );

    public GestureResultWrapper( Set<GestureResult> resultSet ) {
        this.resultSet = resultSet;
    }

    /**
     * Gets the Result for the Gestures.
     * @return the results.
     */
    public Set<GestureResult> getResultSet() {
        return resultSet;
    }

    /**
     * Returns the debug time taken.
     * @return the debug time.
     */
    public Map<String,Long> getDebugTimeTaken(){
        return new HashMap<>( debugTimeTaken );
    }

    /**
     * Adds a new Debug time.
     *
     * @param action to add.
     * @param time to add.
     *
     * @return self for chaining.
     */
    public GestureResultWrapper addDebugTime( String action, long time ){
        debugTimeTaken.put( action, debugTimeTaken.get( action ) + time);
        return this;
    }

    /**
     * Gets the Top result of Gestures of the Wrapper.
     * @return the top result.
     */
    public GestureResult getTopResult(){
        return stream( resultSet )
                .max( (r,s) -> Double.compare( r.getSecond(), s.getSecond()) )
                .orElse( GestureResult.empty() );
    }


    /**
     * Gets the total time taken. This is the addition of ALL Debug timing data.
     * @return the time in MS
     */
    public long getTotalTimeTaken() {
        return stream( debugTimeTaken.values() )
                .mapToLong( e -> e )
                .sum();
    }


    /**
     * Creates an Empty Wrapper with only Unknown.
     * @return an empty wrapper.
     */
    public static final GestureResultWrapper empty(){
        return new GestureResultWrapper( GestureResult.emptySet() );
    }
}
