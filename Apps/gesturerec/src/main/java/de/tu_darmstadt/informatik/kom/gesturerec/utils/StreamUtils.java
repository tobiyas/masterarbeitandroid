package de.tu_darmstadt.informatik.kom.gesturerec.utils;

import java.util.Iterator;

import java8.util.Spliterator;
import java8.util.stream.Stream;
import java8.util.stream.StreamSupport;

/**
 * Created by Toby on 12.12.2017
 */

public class StreamUtils {


    /**
     * Generates a stream of something Iteratable.
     *
     * @param iterable to use.
     * @param <T> the type to convert to.
     * @return the stream of the Iterator.
     */
    public static <T> Stream<T> stream( Iterable<T> iterable ){
        Iterator<T> it = iterable.iterator();
        Spliterator<T> split = java8.util.Spliterators.spliteratorUnknownSize( it, Spliterator.ORDERED );
        return StreamSupport.stream( split, false );
    }

}
