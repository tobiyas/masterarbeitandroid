package de.tu_darmstadt.informatik.kom.gesturerec.users;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.File;
import java.util.Collection;
import java.util.regex.Pattern;

import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import java8.util.stream.RefStreams;

/**
 * Created by Tobias on 06.12.2017
 */

public class UserManager {


    private static final String SETTINGS_PATH = "settings_detection";
    private static final String USER_NAME_PATH = "userName";


    /**
     * The Context to query the private Data for.
     */
    private final Context context;

    /**
     * The used Session.
     */
    private final GestureRecSession session;


    /**
     * Creates a new UserManager managing the current User and all present users.
     * @param context to use.
     * @param session
     */
    public UserManager(Context context, GestureRecSession session){
        this.context = context;
        this.session = session;
    }



    /**
     * Gets the used shared Prefs.
     * @return the shared prefs.
     */
    private SharedPreferences get(){
        return context.getSharedPreferences( SETTINGS_PATH, Context.MODE_PRIVATE );
    }


    /**
     * Gets the current Username.
     * @return the current username.
     */
    public String getCurrentUserName() {
        if( context == null ) return "DEFAULT";
        return get().getString( USER_NAME_PATH, "DEFAULT" );
    }


    /**
     * Changes the current username.
     * @param userName to set.
     */
    public void setCurrentUserName( String userName ) {
        if( context == null ) return;
        get().edit().putString( USER_NAME_PATH, userName ).apply();
    }

    /**
     * Checks if the User has any Data.
     *
     * @param name to check.
     * @return the User passed.
     */
    public boolean userPresent( String name ){
        return RefStreams.of( readAllPresentUsers() )
                .anyMatch( name::equalsIgnoreCase );
    }


    /**
     * Gets all Present Usernames.
     * @return all present usernames.
     */
    public String[] readAllPresentUsers() {
        return RefStreams.of( session.getFileConstants().getBaseGestureFolder() )
                .map( File::listFiles )
                .flatMap( RefStreams::of )
                .map( File::list )
                .flatMap( RefStreams::of )
                .map( s -> s.split(Pattern.quote( "_" ) )[0] )
                .sorted()
                .distinct()
                .toArray( String[]::new );
    }


    /**
     * Reads all Present users and returns them. If none found, return DEFAULT.
     * @return all users or DEFAULT.
     */
    public String[] readAllPresentUsersOrDefault() {
        String[] users = readAllPresentUsers();
        return users == null || users.length == 0 ? new String[]{"DEFAULT"} : users;
    }


    /**
     * If the File is associated with any of the Users passed.
     *
     * @param file to check.
     * @param users to check.
     *
     * @return true if is contained.
     */
    public boolean containsUser( File file, Collection<String> users ){
        if( users == null ) return true;
        if( users.isEmpty() ) return false;
        if( !file.getName().contains( "_" ) ) return false;

        return users.contains( file.getName().split( Pattern.quote( "_" ) )[0] );
    }

}
