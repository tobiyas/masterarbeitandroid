package de.tu_darmstadt.informatik.kom.gesturerec.samples;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;

import java.io.File;
import java.util.HashMap;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.sensordata.JsonSensorDataSerializer;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Lazy;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;

/**
 * Created by Tobias on 06.12.2017
 */

public class SampleProxy {

    /**
     * The serializer / Deserializer to use.
     */
    private static final JsonSensorDataSerializer serializer = new JsonSensorDataSerializer();

    /**
     * The user linked to this sample.
     */
    private final String user;

    /**
     * The Gesture linked to this.
     */
    private final Gesture gesture;

    /**
     * The time this was recorded.
     */
    private final DateTime recordTime;

    /**
     * The file this proxy represents.
     */
    private final File file;

    /**
     * This is the loaded Batch. May be loaded Lazy.
     */
    private Lazy<SensorDataBatch> batch = Lazy.of( this::loadSample );



    /**
     * Creates a new Proxy Sample.
     *
     * @param user to use
     * @param gesture to use
     * @param recordTime to use
     * @param file to use.
     */
    public SampleProxy( String user, Gesture gesture, DateTime recordTime, File file ) {
        this.user = user;
        this.gesture = gesture;
        this.recordTime = recordTime;
        this.file = file;
    }


    /**
     * Creates a new Proxy Sample.
     *
     * @param user to use
     * @param gesture to use
     * @param recordTime to use
     */
    public SampleProxy( String user, Gesture gesture, DateTime recordTime ) {
        this.user = user;
        this.gesture = gesture;
        this.recordTime = recordTime;
        this.file = generateFile( user, gesture, recordTime );
    }


    /**
     * Creates a new Proxy Sample.
     *
     * @param user to use
     * @param gesture to use
     * @param recordTime to use
     */
    public SampleProxy( String user, Gesture gesture, DateTime recordTime, SensorDataBatch batch ) {
        this.user = user;
        this.gesture = gesture;
        this.recordTime = recordTime;
        this.file = generateFile( user, gesture, recordTime );
        this.batch = Lazy.of( batch );
    }


    /**
     * Gets the user that recorded the File.
     *
     * @return the username.
     */
    public String getUser() {
        return user;
    }

    /**
     * Gets the gesture this represents.
     *
     * @return the representing Gesture.
     */
    public Gesture getGesture() {
        return gesture;
    }

    /**
     * Gets the time this sample was recorded.
     *
     * @return the recording time.
     */
    public DateTime getRecordTime() {
        return recordTime;
    }

    /**
     * Gets the actual File this is located in.
     *
     * @return the file this is located in.
     */
    public File getFile() {
        return file;
    }


    /**
     * Loads the Samples on the current Thread.
     * @return the loaded sample.
     */
    private SensorDataBatch loadSample(){
        try{
            byte[] data = FileUtils.readFileToByteArray( file );
            return serializer.deserialize( data );
        }catch ( Throwable exp ){
            exp.printStackTrace();
            return null;
        }
    }

    /**
     * Saves the Data, but does not overwrite existing!
     */
    public boolean saveData(){
        if( !dataPresent() ) return true;
        if( file.exists() ) return true;

        try{
            FileUtils.writeByteArrayToFile( file,  serializer.serialize( batch.get() ) );
            return true;
        }catch ( Throwable exp ){
            exp.printStackTrace();
            return false;
        }
    }


    /**
     * Returns if the Batch is loaded.
     */
    public boolean dataPresent(){
        return this.batch.isLoaded();
    }


    /**
     * Loads the Batch if it's not present yet.
     */
    public SampleProxy loadIfNotPresent(){
        if( !batch.isLoaded() ) batch.get();
        return this;
    }


    /**
     * Gets the Data Batch.
     */
    public SensorDataBatch getBatch(){
        return batch.get();
    }

    /**
     * Puts the batch into the Data.
     * This overwrites all current Data.
     */
    public void putSensorDataBatchIfNotPresent( SensorDataBatch batch ){
        this.batch = Lazy.of( batch );
    }

    /**
     * Maps the data to a new Situation group.
     *
     * @param group to getGesture create this with.
     *
     * @return the generated Sample.
     */
    public TrainingSample toTrainingsSample( SituationGroup group ){
        return new TrainingSample( getBatch(), group, gesture.getName(), new HashMap<>() );
    }

    /**
     * This deletes the Sample!
     * BE AWARE, THAT IT IS GONE FOREVER AFTER THIS!!!
     */
    void delete(){
        FileUtils.deleteQuietly( file );
    }


    @Override
    public boolean equals( Object obj ) {
        if( obj instanceof SampleProxy ){
            //They are same if the file is the same!
            return file.equals( ( (SampleProxy)obj ).file );
        }

        return super.equals(obj);
    }


    @Override
    public int hashCode() {
        return ( user + gesture.getName() ).hashCode();
    }

    /**
     * Creates a File from the passed Parameters.
     *
     * @param user to create for.
     * @param gesture to create for.
     * @param recordTime to create for.
     *
     * @return the determined File place.
     */
    private static final File generateFile( String user, Gesture gesture, DateTime recordTime ){
        return new File( gesture.getSaveDirectory(), user + "_" + recordTime.getMillis() + ".batch" );
    }
}
