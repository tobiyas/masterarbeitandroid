package de.tu_darmstadt.informatik.kom.gesturerec.linker;

import java8.util.stream.RefStreams;

/**
 * Created by Toby on 13.12.2017
 */
public class Limb {

    public static final Limb LEFT_HAND = new Limb("LeftHand");
    public static final Limb RIGHT_HAND = new Limb("RightHand");
    public static final Limb LEFT_FOOT = new Limb("LeftFoot");
    public static final Limb RIGHT_FOOT = new Limb("RightFoot");

    public static final Limb NOT_DEFINED = new Limb("NotDefined");

    public static final Limb[] ALL_LIMBS = new Limb[]{
            LEFT_HAND,
            RIGHT_HAND,
            LEFT_FOOT,
            RIGHT_FOOT
    };


    /**
     * The Name to use.
     */
    private final String name;


    /**
     * Creates a new Limb with the name.
     * @param name to use.
     */
    private Limb(String name){
        this.name = name;
    }

    /**
     * Gets the name of the Limb
     */
    public String getName() {
        return name;
    }


    /**
     * Creates a limb from the name.
     * This is only Cache-Retrieval.
     *
     * @param name to use.
     * @return the found limb.
     */
    public static Limb fromName(String name){
        return RefStreams.of( ALL_LIMBS )
            .filter( l -> l.getName().equals(name) )
            .findFirst()
            .orElse(NOT_DEFINED);
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}