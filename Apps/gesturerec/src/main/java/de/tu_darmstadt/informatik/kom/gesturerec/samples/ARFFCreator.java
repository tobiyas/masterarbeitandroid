package de.tu_darmstadt.informatik.kom.gesturerec.samples;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureSet;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaUtils;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import weka.core.Instances;

/**
 * Created by Tobias on 11.12.2017
 */

public class ARFFCreator {



    /**
     * Loads the Trainingsdata of a SampleBatch into an ARFF file.
     *
     * @param session to use for Features.
     * @param saveFile to save to.
     * @param batch to write
     *
     * @throws Exception if something goes wrong.
     */
    public static void createARFFile(
            GestureRecSession session,
            File saveFile,
            SampleBatch batch ) throws Exception {

        if( saveFile == null || batch == null ) throw new IllegalArgumentException( "May not be null" );
        if( saveFile.exists() ) throw new IllegalArgumentException( "SaveFile already exists." );

        SituationGroup group = new SituationGroup( "TMP", batch.getGestures() );

        //Notify update with Instances:
        WekaFeatureSet set = session.getFeatureFactory().buildFeatures( new ArrayList<>( batch.getGestures() ) );
        Instances arffData = WekaUtils.buildWekaInstanceSet( set, batch.streamLoadSamples( group ), s -> {} );
        String serialized = arffData.toString();
        FileUtils.write( saveFile, serialized, Charset.defaultCharset() );
    }



    /**
     * Loads the Trainingsdata of a stream into an ARFF file.
     *
     * @param session to use for Features.
     * @param saveFile to save to.
     * @param stream to write
     *
     * @throws Exception if something goes wrong.
     */
    public static void createARFFile(
            GestureRecSession session,
            File saveFile,
            SampleFilterStream stream ) throws Exception {

        if( saveFile == null || stream == null ) throw new IllegalArgumentException( "May not be null" );
        createARFFile( session, saveFile, stream.applyFilter() );
    }


}
