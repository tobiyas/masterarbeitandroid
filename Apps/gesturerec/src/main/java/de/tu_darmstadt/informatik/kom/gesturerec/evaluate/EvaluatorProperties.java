package de.tu_darmstadt.informatik.kom.gesturerec.evaluate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;

/**
 * Properties for Evaluations.
 * Created by Toby on 05.11.2017
 */

public class EvaluatorProperties {

    /**
     * The set of users to load samples for.
     */
    private final Collection<String> users = new HashSet<>();

    /**
     * The set of test users to use for evaluation.
     */
    private final Collection<String> testUsers = new HashSet<>();

    /**
     * The Collection of Gestures to use.
     */
    private final Collection<Gesture> gestures = new HashSet<>();

    /**
     * The classifier to use.
     */
    private WekaBaseClassifier classifier = WekaBaseClassifier.RANDOM_FOREST;

    /**
     * If samples should be shuffled.
     */
    private boolean shuffleSamples = false;

    /**
     * The Value to use for generic usage.
     */
    private int value = 66;


    /**
     * Adds new Samples to the system.
     *
     * @param users to use.
     */
    public void addUsers( Collection<String> users ){
        this.users.addAll( users );
    }


    /**
     * Adds new Test users.
     *
     * @param users to add.
     */
    public void addTestUsers( Collection<String> users ){
        this.testUsers.addAll( users );
    }

    /**
     * Gets all current test users.
     *
     * @return all test users.
     */
    public Collection<String> getTestUsers() {
        return new ArrayList<>( testUsers );
    }

    /**
     * Adds the Gestures to the System.
     *
     * @param gestures to add.
     */
    public void addGestures( Collection<Gesture> gestures ){
        this.gestures.addAll( gestures );
    }

    /**
     * Clears all present test users.
     */
    public void clearTestUsers(){
        this.testUsers.clear();
    }

    /**
     * Clears all present Samples.
     */
    public void clearUsers(){
        this.users.clear();
    }

    /**
     * Clears all present gestures.
     */
    public void clearGestures(){
        this.gestures.clear();
    }

    /**
     * Sets the base classifier.
     *
     * @param classifier to set.
     */
    public void setBaseDetector( WekaBaseClassifier classifier ){
        this.classifier = classifier;
    }


    /**
     * Gets the current Base classifier.
     *
     * @return the current base classifier.
     */
    public WekaBaseClassifier getBaseDetector() {
        return classifier;
    }

    /**
     * Gets the Currently active gestures.
     * @return the active gestures.
     */
    public List<Gesture> getGestures() {
        return new ArrayList<>( gestures );
    }

    /**
     * Gets the currently present samples.
     * @return the present samples.
     */
    public List<String> getUsers() {
        return new ArrayList<>( users );
    }


    /**
     * Sets the Value for generic use.
     *
     * @param value to set.
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Gets the generic value.
     *
     * @return to get.
     */
    public int getValue() {
        return value;
    }

    /**
     * Gets if the Samples should be shuffeled.
     * @return true if should be shuffeled.
     */
    public boolean isShuffleSamples() {
        return shuffleSamples;
    }

    /**
     * Sets if the samples should be shuffeled.
     *
     * @param shuffleSamples true if they should beshuffeled.
     */
    public void setShuffleSamples(boolean shuffleSamples) {
        this.shuffleSamples = shuffleSamples;
    }
}
