package de.tu_darmstadt.informatik.kom.gesturerec.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import java8.util.stream.DoubleStreams;
import java8.util.stream.RefStreams;

/**
 * Created by Tobias on 29.12.2017
 */

public class ArrayUtils {


    /**
     * Normalizes all values between 0 and 1.
     *
     * @param value to normalize
     * @return the normalized values.
     */
    public static double[] normalizeValue ( double[] value ){
        double min = DoubleStreams.of( value ).min().orElse( 0 );
        double max = DoubleStreams.of( value ).max().orElse( 1 ) - min;

        double [] normalized = Arrays.copyOf( value, value.length );
        return DoubleStreams.of( normalized )
                .map( v -> v - min )
                .map( v -> v / max )
                .toArray();
    }

    /**
     * does a lowpass filter on the array.
     *
     * @param value to lowpass
     * @param alpha to apply.
     * @return the lowpassed values.
     */
    public static double[] lowpass ( double[] value, double alpha ){
        if( value == null || value.length == 0 ) return new double[ 0 ];

        //Generate the array to produce for output.
        double[] output = new double[ value.length ];
        //Be sure to have data in the 0 array place:
        output[0] = value[0];

        double old = value[0];
        double neu;

        for( int i = 1; i < value.length; i++ ){
            neu = value[i];
            output[i] = old + alpha * ( neu - old);
            old = neu;
        }

        return output;
    }

    /**
     * does a lowpass filter on the array.
     *
     * @param value to lowpass
     * @param alpha to apply.
     * @return the lowpassed values.
     */
    public static double[] lowpassInPlace ( double[] value, double alpha ){
        if( value == null || value.length == 0 ) return new double[ 0 ];
        for( int i = 1; i < value.length; i++ ){
            value[i] = value[i-1] + alpha * ( value[i] - value[i-1] );
        }

        return value;
    }


    /**
     * does a lowpass filter on the array with alpha 0.2.
     *
     * @param value to lowpass
     * @return the lowpassed values.
     */
    public static double[] lowpass_2 ( double[] value ){
        return lowpass( value, 0.2d );
    }

    /**
     * does a lowpass filter on the array with alpha 0.5.
     *
     * @param value to lowpass
     * @return the lowpassed values.
     */
    public static double[] lowpass_5 ( double[] value ){
        return lowpass( value, 0.5d );
    }


    /**
     * This calculates the Extremas of an x-y related Graph, where x = Time and y = amplitude.
     *
     * @param times the array with the Timestamps.
     * @param data the array with the Data.
     * @param sliceSize the size of the slices to compare. lower for more extremas, higher for less.
     * @param ignoreSmallerMax The value of the Max value to ignore below.
     * @param purgeSaddlePoints if true, all Sadlepoints will be purged.
     *
     * @return a list of all extremas. Always contains the first and last element of the provided Arrays.
     */
    public static List<Pair<Long,Double>> extremas( long[] times, double[] data, int sliceSize, double ignoreSmallerMax, boolean purgeSaddlePoints ){
        if( times == null || times.length == 0 ) return new ArrayList<>();

        if( times.length != data.length ) throw new IllegalArgumentException( "Time and Data have to be the same length" );
        if( sliceSize <= 0 || sliceSize >= times.length ) throw new IllegalArgumentException( "SliceSize must be > 0 and < size of arrays" );

        double ignoreBelow = ignoreSmallerMax * DoubleStreams.of( data ).max().orElse( 0 );
        double ignoreAbove = ignoreSmallerMax * DoubleStreams.of( data ).min().orElse( 0 );

        Pair<Long,Double> lastExtreme = new Pair<>( times[0], data[0] );
        List<Pair<Long,Double>> extremas = new ArrayList<>();
        extremas.add( lastExtreme );

        int center = sliceSize / 2;
        double middle,current,max,min;
        for ( int i = center; i < data.length - sliceSize; i ++) {
            middle = data[ i + center ];

            //Check if the distance to the last extreme is high enough:
            if( Math.abs( lastExtreme.getSecond() - middle ) < 0.1 ) continue;

            //Ignore below max:
            if( middle < ignoreBelow && middle > ignoreAbove ) continue;

            //This construct is about 10x faster than streams:
            current = data[i];
            max = current;
            min = current;
            for( int j = i+1; j < i + sliceSize; j++ ){
                current = data[j];

                max = Math.max( max, current );
                min = Math.min( min, current );
            }


            //check if we have an extrema:
            if( Math.abs( middle - max ) <= 0.01
                    || Math.abs( middle - min ) <= 0.01 ) {

                extremas.add( new Pair<>( times[ i + center ], middle ) );
                i+= center;
            }
        }

        extremas.add( new Pair<>( times[ times.length-1 ], data[ data.length-1 ] ) );

        //Remove intermediate Extremas:
        if( purgeSaddlePoints && extremas.size() > 3 ){
            Iterator<Pair<Long,Double>> it = new ArrayList<>( extremas ).iterator();
            Pair<Long,Double> first,currentP,next;

            first = it.next();
            currentP = it.next();
            next = it.next();

            while( it.hasNext() ){
                if( 	( first.getSecond() < currentP.getSecond() && next.getSecond() > currentP.getSecond() )
                        || 	( first.getSecond() > currentP.getSecond() && next.getSecond() < currentP.getSecond() )
                        ){
                    extremas.remove( currentP );
                }

                first = currentP;
                currentP = next;
                next = it.next();
            }
        }

        return extremas;
    }


    /**
     * Aggregates the passed Arrays.
     * @param values to add to each other.
     * @return the the aggregated array.
     */
    public static double[] add( double[]... values ){
        if( values.length == 0 ) return new double[0];

        int max = RefStreams.of( values ).mapToInt( a -> a.length ).max().orElse( 0 );
        double[] result = new double[ max ];

        for( int i = 0; i < max; i++ ){
            for ( double[] value : values) {
                try { result[i] += value[i]; } catch (Throwable exp) {}
            }
        }

        return result;
    }

    /**
     * Aggregates the passed Arrays.
     * @param values to add to each other.
     * @return the the aggregated array.
     */
    public static double[] addABS( double[]... values ){
        if( values.length == 0 ) return new double[0];

        int max = RefStreams.of( values ).mapToInt( a -> a.length ).max().orElse( 0 );
        double[] result = new double[ max ];

        for( int i = 0; i < max; i++ ){
            for ( double[] value : values) {
                try { result[i] += Math.abs( value[i] ); } catch (Throwable exp) {}
            }
        }

        return result;
    }


    /**
     * Normalizes the Data. All Values below a certain value are set to 0.
     *
     * @param data to set.
     * @param defaultOffset to use as border.
     *
     * @return the normalized values.
     */
    public static double[] normaizeWithTreshold( double[] data, double defaultOffset, double defaultValue ){
        defaultOffset = Math.abs( defaultOffset );

        double[] result = new double[ data.length ];
        double min = DoubleStreams.of( data ).min().getAsDouble();
        double max = DoubleStreams.of( data ).max().getAsDouble();

        for( int i = 0; i < data.length; i++ ){
            double point = data[i];
            if( point < defaultOffset && point > -defaultOffset ) result[i] = defaultValue;
            else result[i] = ( point - min ) / ( max - min );
        }

        return result;
    }

}
