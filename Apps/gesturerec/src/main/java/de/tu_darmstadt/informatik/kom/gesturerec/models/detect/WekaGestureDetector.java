package de.tu_darmstadt.informatik.kom.gesturerec.models.detect;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResult;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 09.10.2017
 */

public class WekaGestureDetector extends GestureDetector {


    /**
     * The Executor to run on Async.
     */
    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    /**
     * The session to use.
     */
    private final GestureRecSession session;

    /**
     * The name of the Model.
     */
    private final ModelProxy model;


    private WekaGestureDetector( GestureRecSession session, ModelProxy modelProxy ) throws Exception {
        this.session = session;
        this.model = modelProxy;
    }


    @Override
    public GestureResultWrapper analyseGesture( SensorDataBatch batch ) {
        DetectorResult result = model.getDetector().detect( batch, null );
        Map<String,Double> flattened = result.getAllProbabilities();

        GestureManager gm = session.getGestureManager();
        return new GestureResultWrapper(
                stream( flattened.entrySet() )
                    .map( e -> new Pair<>( gm.createGesture( e.getKey() ), e.getValue() ) )
                    .map( GestureResult::new )
                    .collect( Collectors.toSet() )
        );
    }

    @Override
    public void analyseGestureAsync( SensorDataBatch batch, GestureResultCallback callback ) {
        executor.execute( () -> {
            GestureResultWrapper wrapper;
            try{
                long before = System.currentTimeMillis();
                wrapper = analyseGesture( batch );
                long took = System.currentTimeMillis() - before;
                wrapper.addDebugTime( "Detection", took );
            }catch ( Throwable exp ){
                callback.gotError( batch, exp );
                return;
            }

            callback.gotGestureResults( batch, wrapper );
        });
    }


    @Override
    public String getModelName() {
        return model.getName();
    }


    @Override
    public Collection<Gesture> getSupportedGestures() {
        return new HashSet<>( model.getGestures() );
    }


    /**
     * Generates a Gesture Detector from the Model wanted. Returns null if not possible.
     *
     * @param session to use for assessing assets.
     * @param model the model to use.
     *
     * @return the generated Detector.
     */
    public static WekaGestureDetector generate( GestureRecSession session, ModelProxy model ) {
        try{
            return new WekaGestureDetector( session, model );
        }catch ( Exception exp ){
            exp.printStackTrace();
            return null;
        }
    }


}
