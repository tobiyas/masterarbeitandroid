package de.tu_darmstadt.informatik.kom.gesturerec.file;

import android.os.Environment;

import java.io.File;

/**
 * Created by Toby on 12.12.2017
 */

public class DefaultFileConstants implements FileConstants {


    private static File GESTURE_REC_BASE_FILE = new File( Environment.getExternalStorageDirectory(), "GestureRec" );

    private static final String GESTURE_DATA_PATH = "GestureData";
    private static final String GESTURE_MODEL_PATH = "GestureModel";


    /**
     * The folder for Gestures.
     */
    private final File gestureFolder;

    /**
     * The folder for models
     */
    private final File modelFolder;


    public DefaultFileConstants(){
        this(
                new File( GESTURE_REC_BASE_FILE, GESTURE_DATA_PATH ),
                new File( GESTURE_REC_BASE_FILE, GESTURE_MODEL_PATH )
        );
    }


    public DefaultFileConstants ( File gestureFolder, File modelFolder ){
        this.gestureFolder = gestureFolder;
        this.modelFolder = modelFolder;

        if( !gestureFolder.exists() ) gestureFolder.mkdirs();
        if( !modelFolder.exists() ) modelFolder.mkdirs();
    }


    /**
     * Gets the Base Gesture Folder.
     * @return folder.
     */
    public File getBaseGestureFolder(){
        return new File( GESTURE_REC_BASE_FILE, GESTURE_DATA_PATH );
    }


    /**
     * Gets the Base Model Folder.
     * @return folder.
     */
    public File getBaseModelFolder(){
        return new File( GESTURE_REC_BASE_FILE, GESTURE_MODEL_PATH );
    }

}
