package de.tu_darmstadt.informatik.kom.gesturerec.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleManager;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.StreamUtils;
import java8.util.Optional;
import java8.util.Spliterator;
import java8.util.Spliterators;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 12.12.2017
 */

public class ModelProxyFactory {


    public static final String JSON_NAME_PATH = "name";
    public static final String JSON_GESTURE_PATH = "gestures";
    public static final String JSON_TYPE_PATH = "type";

    public static final String JSON_INCREMENTAL_LEARNER_PATH = "incrementalLearner";
    public static final String JSON_INCREMENTAL_LEARNER_SAMPLES_PATH = "samples";



    /**
     * Loads a Model from the File-System.
     *
     * @param session to use.
     * @param name to use.
     *
     * @return the loaded Model or empty if failed.
     */
    public static Optional<ModelProxy> loadFromFilesystem( GestureRecSession session, String name ){
        File base = session.getFileConstants().getBaseModelFolder();
        File metaFile = new File( base, name + ".meta" );
        if( !metaFile.exists() ) return Optional.empty();

        File modelFile = new File( base, name + ".model" );
        if( !modelFile.exists() ) return Optional.empty();

        //Try to read as JSON.
        try{
            String text = FileUtils.readFileToString( metaFile, Charset.defaultCharset() );
            JsonObject obj = new JsonParser().parse( text ).getAsJsonObject();

            String readName = obj.get( JSON_NAME_PATH ).getAsString();
            List<Gesture> gestures = unwrap( session.getGestureManager(), obj.get( JSON_GESTURE_PATH ).getAsJsonArray() );
            WekaBaseClassifier baseClassifier = WekaBaseClassifier.valueOf( obj.get( JSON_TYPE_PATH ).getAsString() );
            boolean isIncrementalLearner = obj.has(JSON_INCREMENTAL_LEARNER_PATH) && obj.get(JSON_INCREMENTAL_LEARNER_PATH).getAsBoolean();

            //If is an Incremental Learning model -> Create!
            if( isIncrementalLearner ) return createIncrementalLearner( session, readName, gestures, baseClassifier, obj );

            return Optional.of( new ModelProxy( session, baseClassifier, readName, gestures ) );
        }catch ( Throwable exp ){
            exp.printStackTrace();
            return Optional.empty();
        }
    }

    /**
     * Creates an IncrementalLearner for the passed values.
     *
     * @param session to use.
     * @param name to use.
     * @param gestures to use.
     * @param type to use.
     * @param obj to read the Samples from.
     *
     * @return the Proxy if successfull.
     */
    private static Optional<ModelProxy> createIncrementalLearner (
            GestureRecSession session,
            String name,
            List<Gesture> gestures,
            WekaBaseClassifier type,
            JsonObject obj) {

        if( !obj.has( JSON_INCREMENTAL_LEARNER_SAMPLES_PATH ) ) return Optional.empty();

        //Should be a way better implementation than each cast!
        //Note that we so a one-time-lookup here, instead of looking up the sample for EACH long value.
        JsonArray array = obj.get( JSON_INCREMENTAL_LEARNER_SAMPLES_PATH ).getAsJsonArray();
        Collection<Long> times = StreamUtils.stream( array )
                .map( JsonElement::getAsLong )
                .collect( Collectors.toSet() );

        SampleBatch present = timesToProxies( session.getSampleManager(), times );
        if( present.size() != array.size() ) throw new IllegalStateException( "Could not load all Samples for Active Learning Model: " + name);
        return Optional.of( new IncrementalLearningModelProxy( session, type, name, gestures, present ) );
    }



    /**
     * Gets the corresponding sample.
     *
     * @param sampleManager to use.
     * @param times to find.
     *
     * @return the corresponding sample.
     */
    private static SampleBatch timesToProxies( SampleManager sampleManager, Collection<Long> times ) {
        SampleBatch all = sampleManager.query().applyFilter();
        List<SampleProxy> proxies = new ArrayList<>( all.getSamples() );
        Iterator<SampleProxy> it = proxies.iterator();
        while( it.hasNext() ){
            SampleProxy proxy = it.next();
            if( !times.contains( proxy.getRecordTime().getMillis() ) ) it.remove();
        }

        return new SampleBatch( proxies );
    }


    /**
     * Does an unwrap from the Array to Gestures.
     * @param gestureManager to use.
     * @param gestures to unwrap.
     *
     * @return the unwraped Gestures.
     */
    private static List<Gesture> unwrap( GestureManager gestureManager, JsonArray gestures ) {
        return stream( Spliterators.spliteratorUnknownSize( gestures.iterator(), Spliterator.DISTINCT ), false )
                .map( JsonElement::getAsString )
                .map( gestureManager::createGesture )
                .collect( Collectors.toList() );
    }

}
