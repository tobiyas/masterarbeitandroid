package de.tu_darmstadt.informatik.kom.gesturerec.models.detect;

import java.util.ArrayList;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetectorBuilder;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 14.10.2017
 */

public class DetectorCreator {


    /**
     * Creates a new Detector with ALL Test data present!
     *
     * @param session to use for loading.
     * @param classifier to create.
     * @param name to give to the Detector.
     *
     * @return the generated Detector.
     */
    public static WekaDetector create( GestureRecSession session, WekaBaseClassifier classifier, String name ) {
        try{
            SampleBatch batch = session.getSampleManager().query().applyFilter();
            return create( session, classifier, batch, name );
        }catch ( Throwable exp ){
            exp.printStackTrace();
            return null;
        }
    }


    /**
     * Creates a new Detector with ALL Test data present!
     *
     * @param session to use for reading Features.
     * @param classifier to create.
     * @param batch to use for learning.
     * @param name to use for creation.
     *
     * @return the generated Detector.
     */
    public static WekaDetector create( GestureRecSession session,
                                       WekaBaseClassifier classifier,
                                       SampleBatch batch, String name ) {

        try{
            List<String> situations = stream( batch.getGestures() ).map( Gesture::getName ).collect( Collectors.toList() );
            SituationGroup group = new SituationGroup( name, situations );

            return new WekaDetectorBuilder( "de.tu_darmstadt.gestures.Detector", name )
                    .forSituation( group )
                    .withClassifier( classifier )
                    .withFeatures( session.getFeatureFactory().buildFeatures( new ArrayList<>(  batch.getGestures() ) ) )
                    .withPrecondition( new NoPrecondition() )
                    .withSamples( batch.streamLoadSamples( group ) )
                    .build();
        }catch (Throwable exp){
            exp.printStackTrace();
            return null;
        }
    }

}
