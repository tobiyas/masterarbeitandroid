package de.tu_darmstadt.informatik.kom.gesturerec.gestures;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;

import static java8.util.stream.StreamSupport.stream;


/**
 * Created by Tobias on 26.10.2017
 */
public class GestureManager {


    /**
     * The Unknown Gestures.
     */
    private static Gesture UNKNOWN = new Gesture( "UNKNOWN", null );

    public static Gesture UNKNOWN(){
        return UNKNOWN;
    }

    /**
     * The Set of Gestures saved.
     */
    private final Collection<Gesture> gestures = new HashSet<>();

    /**
     * The session to use to access other stuff.
     */
    private final GestureRecSession session;


    public GestureManager(GestureRecSession session) {
        this.session = session;
        UNKNOWN = new Gesture( "UNKNOWN", new File( session.getFileConstants().getBaseGestureFolder(), "UNKNOWN" ) );
    }

    /**
     * This loads all gestures present from the Disc.
     */
    public GestureManager init(){
        this.gestures.clear();

        File base = session.getFileConstants().getBaseGestureFolder();
        if( !base.exists() ) base.mkdirs();

        //Lists all folders in that dir.
        //Every folder is 1 Gesture.

        File[] folders = base.listFiles();
        if( folders != null && folders.length > 0 ) {
            RefStreams.of( folders )
                .filter( File::isDirectory )
                .map( File::getName )
                .forEach( this::createGesture );
        }

        return this;
    }

    /**
     * Creates a new Gesture.
     * @param name to create
     */
    public Gesture createGesture( String name ){
        Gesture cache = getGesture( name );
        if( cache != null ) return cache;

        File folder = new File( session.getFileConstants().getBaseGestureFolder(), name );
        if( !folder.exists() ) folder.mkdirs();

        Gesture gesture = new Gesture( name, folder );
        this.gestures.add( gesture );
        return gesture;
    }


    /**
     * Creates a new Gesture.
     * @param names to create
     */
    public Collection<Gesture> createGestures( String... names ){
        return RefStreams.of( names )
                .map( this::createGesture )
                .collect( Collectors.toSet() );
    }

    /**
     * Deletes the Gestures passed.
     * @param gesture to delete.
     */
    public void deleteGesture( Gesture gesture ){
        if( gesture == null ) return;

        //Remove the Gesture:
        this.gestures.remove( gesture );

        SampleBatch batch = session.getSampleManager().query()
                .addGestures( gesture )
                .applyFilter();

        session.getSampleManager().deleteSamples( batch );

        //Delete the folder with data:
        File folder = gesture.getSaveDirectory();
        try{ FileUtils.deleteDirectory( folder ); }catch ( Throwable exp ) { exp.printStackTrace(); }
    }


    /**
     * Gets a Gesture by name. Returns null if none present.
     * The Name may not contain '/', since it's a control sequence for the File System.
     *
     * @param name to get Gesture for.
     * @return the Gesture by name.
     */
    public Gesture getGesture( String name ) {
        if( name == null || name.isEmpty() || name.contains( "../" ) || name.contains( "/" ) ){
            throw new IllegalArgumentException( "name may not be null, empty or contain ../ or /" );
        }

        //Search for the Gesture:
        return stream( gestures )
                .filter( gesture -> gesture.getName().equalsIgnoreCase( name ) )
                .findFirst()
                .orElse( null );
    }


    /**
     * Gets all Gestures.
     * @return all gestures.
     */
    public Collection<Gesture> getAllWithoutUnknown(){
        return new HashSet<>( gestures );
    }

    /**
     * Gets all Gestures.
     * @return all gestures.
     */
    public List<String> getAllWithoutUnknownAsString(){
        return stream( gestures ).map( Gesture::getName ).collect( Collectors.toList() );
    }

    /**
     * Gets all Gestures.
     * @return all gestures.
     */
    public String[] getAllWithoutUnknownAsStringArray(){
        return stream( gestures ).map( Gesture::getName ).toArray( String[]::new );
    }


    /**
     * Gets all Gestures.
     * @return all gestures.
     */
    public Collection<Gesture> getAllWithUnknown(){
        Collection<Gesture> all = getAllWithoutUnknown();
        all.add( UNKNOWN );
        return all;
    }

}
