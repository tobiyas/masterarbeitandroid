package de.tu_darmstadt.informatik.kom.gesturerec.models;

import com.google.gson.JsonObject;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.utils.SerialVersionUidIgnoringObjectInputStream;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Lazy;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.GestureDetector;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.WekaGestureDetector;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.Collectors2;
import weka.classifiers.Classifier;

import static de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxyFactory.JSON_GESTURE_PATH;
import static de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxyFactory.JSON_INCREMENTAL_LEARNER_PATH;
import static de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxyFactory.JSON_NAME_PATH;
import static de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxyFactory.JSON_TYPE_PATH;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 07.12.2017
 */
public class ModelProxy {

    /**
     * The Type of the model.
     */
    private final WekaBaseClassifier classifier;

    /**
     * The File for the Model
     */
    private final File modelFile;

    /**
     * The File for the Meta data.
     */
    private final File metaFile;

    /**
     * The Classifier.
     */
    protected final Lazy<Classifier> lazyClassifier = Lazy.of( this::lazyLoadClassifier );

    /**
     * A lazy implementation to getGesture the Weka Detector.
     */
    protected final Lazy<WekaDetector> lazyWekaDetector = Lazy.of( this::loadWekaDetector );


    /**
     * A lazy implementation to getGesture the Weka Detector.
     */
    protected final Lazy<GestureDetector> gestureDetector = Lazy.of( this::generateGestureDetector );

    /**
     * The name of the Classifier.
     */
    private final String name;

    /**
     * The collection of used gestures.
     */
    private final List<Gesture> gestures = new ArrayList<>();

    /**
     * The used SituationGroup.
     */
    private final SituationGroup situationGroup;

    /**
     * The session this model belongs to.
     */
    protected final GestureRecSession session;

    /**
     * The last error while loading.
     */
    private Throwable lastLoadingError;


    /**
     * Creates a new Proxy for Models.
     *
     * @param session this model belongs to.
     * @param classifier to use.
     * @param name to use.
     * @param gestures to use.
     */
    public ModelProxy( GestureRecSession session, WekaBaseClassifier classifier, String name, List<Gesture> gestures ) {
        this.session = session;
        this.classifier = classifier;
        this.name = name;
        this.gestures.addAll( gestures );
        Collections.sort( this.gestures );

        this.situationGroup = new SituationGroup( name, stream( gestures ).map( Gesture::getName ).toArray( String[]::new ) );

        File base = session.getFileConstants().getBaseModelFolder();
        this.metaFile = new File( base,name + ".meta" );
        this.modelFile = new File( base,name + ".model" );
    }

    /**
     * Creates a new Proxy for Models.
     *
     * @param session this model belongs to.
     * @param classifier to use.
     * @param name to use.
     * @param gestures to use.
     */
    public ModelProxy( GestureRecSession session, String name, Collection<Gesture> gestures, Classifier classifier ) {
        this.session = session;
        this.lazyClassifier.forceValue( classifier );
        this.classifier = WekaBaseClassifier.fromClassifier( classifier );
        this.name = name;
        this.gestures.addAll( gestures );
        this.situationGroup = new SituationGroup( name, stream( gestures ).map( Gesture::getName ).toArray( String[]::new ) );

        File base = session.getFileConstants().getBaseModelFolder();
        this.metaFile = new File( base,name + ".meta" );
        this.modelFile = new File( base,name + ".model" );
    }


    /**
     * Loads a classifier.
     *
     * @return a classifier.
     */
    private Classifier lazyLoadClassifier() {
        try(
            InputStream fileIn = new FileInputStream( modelFile );
            ObjectInputStream in = new SerialVersionUidIgnoringObjectInputStream( fileIn );
        )
        {
            return (Classifier) in.readObject();
        }catch( Throwable exp ){
            this.lastLoadingError = exp;
            return null;
        }
    }


    /**
     * Wrapper for creating a simple Gesture Detector.
     * @return the generated detector.
     */
    private GestureDetector generateGestureDetector() {
        return WekaGestureDetector.generate( session, this );
    }

    /**
     * Gets a simple Gesture Detector.
     * This implies loading the Klassifier if not loaded yet.
     *
     * @return a gesture detector.
     */
    public GestureDetector getGestureDetector(){
        return gestureDetector.get();
    }


    /**
     * If the Classifier is loaded.
     * @return true if loaded.
     */
    public boolean isLoaded(){
        return lazyClassifier.isLoaded();
    }

    /**
     * Returns true if an Error is present.
     * @return true if an Error is present.
     */
    public boolean hasError(){
        return lastLoadingError != null;
    }

    /**
     * Returns true if this is loadable.
     * @return true if loadable.
     */
    public boolean canLoad(){
        return !lazyClassifier.isLoaded() && lastLoadingError == null;
    }

    /**
     * Returns the last error while loading or null.
     * @return the last error.
     */
    public Throwable getError(){
        return lastLoadingError;
    }


    /**
     * This forces a delete of the Model.
     */
    void delete(){
        FileUtils.deleteQuietly( this.modelFile );
        FileUtils.deleteQuietly( this.metaFile );
    }


    /**
     * Does a save.
     * @return true if worked, false if failed.
     */
    public boolean save(){
        if( !modelFile.exists() ) {
            if( !lazyClassifier.isLoaded() ) return false;
            else if( !saveModel() ) return false;
        }

        if( !metaFile.exists() ){
            if( !saveMeta() ) return false;
        }

        return true;
    }

    /**
     * Saves the Metadata as JSON.
     * @return the Metadata.
     */
    private boolean saveMeta() {
        if( metaFile.exists() ) return true;

        try{
            JsonObject root = generateMeta();
            FileUtils.write( metaFile, root.toString(), Charset.defaultCharset() );
            return true;
        }catch ( IOException exp ){
            exp.printStackTrace();
            return false;
        }
    }


    /**
     * Generates the Metadata for the Model.
     * @return the json Object to save.
     */
    protected JsonObject generateMeta(){
        JsonObject root = new JsonObject();
        root.add( JSON_GESTURE_PATH, stream( gestures ).map( Gesture::getName ).collect( Collectors2.toGsonArray() ) );
        root.addProperty( JSON_NAME_PATH, name );
        root.addProperty( JSON_TYPE_PATH, classifier.name() );
        root.addProperty( JSON_INCREMENTAL_LEARNER_PATH, false );

        return root;
    }


    /**
     * Saves a model. Returns true if worked.
     * @return true if worked.
     */
    private boolean saveModel() {
        if( modelFile.exists() ) return true;
        if( !lazyClassifier.isLoaded() ) return false;

        try (
                ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream( modelFile ) )
            ){
            out.writeObject( lazyClassifier.get() );
            return true;
        }catch ( Throwable exp ){
            exp.printStackTrace();
            return false;
        }
    }


    /**
     * Gets the name of the Model
     * @return the name.
     */
    public String getName() {
        return name;
    }


    /**
     * Gets the present Gestures.
     * @return the present gestures.
     */
    public Collection<Gesture> getGestures() {
        return new HashSet<>( gestures );
    }

    /**
     * Gets the Type.
     * @return the type.
     */
    public WekaBaseClassifier getClassifierType() {
        return classifier;
    }

    /**
     * Gets the Classifier. Returns NULL if there is an error.
     */
    public Classifier getClassifier() {
        return lazyClassifier.get();
    }


    /**
     * Gets the loaded Detector. May be null.
     * @return the loaded detector.
     */
    public WekaDetector getDetector(){
        return lazyWekaDetector.get();
    }

    /**
     * Loading the Weka detector.
     * @return the loaded Detector or null if failed.
     */
    private WekaDetector loadWekaDetector() {
        Classifier classifier = getClassifier();
        if( classifier == null ) return null;

        DetectorDescription description = new DetectorDescription.Builder( new DetectorIdentifier( "defaultModel", name ) )
                .setDescription( "Detector for Gestures" )
                .setSituationGroup( situationGroup )
                .build();

        return new WekaDetector( description,
                session.getFeatureFactory().buildFeatures( new ArrayList<>( getGestures() ) ),
                classifier,
                new NoPrecondition() );
    }

    /**
     * Gets the path the Meta file is saved.
     * @return the path for the Meta file.
     */
    public File getMetaFile() {
        return metaFile;
    }


    /**
     * Gets the path the Model file is saved.
     * @return the path for the Model file.
     */
    public File getModelFile() {
        return modelFile;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if( obj instanceof ModelProxy ){
            return name.equals( ( (ModelProxy) obj ).name );
        }

        return super.equals(obj);
    }


}
