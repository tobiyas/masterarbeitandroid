package de.tu_darmstadt.informatik.kom.gesturerec.recorder;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResult;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.GestureDetector;

/**
 * Created by Tobias on 07.01.2018
 */

public class PermanentDetectorReplay {

    /**
     * The basic batch to use.
     */
    private final SensorDataBatch mainBatch;



    /**
     * The Intervall to detect.
     */
    private final long detectInterval;

    /**
     * The Timeout after a successful detect:
     */
    private final long timeoutAfterDetect;

    /**
     * The amount of detects per second.
     */
    private final int detectsPerSecond;

    /**
     * The minimal security to use for triggering.
     */
    private final double minSecurity;

    /**
     * The actual Detector to use.
     */
    private final GestureDetector detector;

    /**
     * The last top detections.
     */
    private final LinkedList<GestureResult> lastTop = new LinkedList<>() ;


    /**
     * The Set of Gestures to use as Skip-Gestures.
     * They cause a shorter Hault.
     * This is supposed to be for some sort of IDLE stuff.
     */
    private final Collection<Gesture> skipGestures = new HashSet<>();


    /**
     * This creates a new Perm-Detector.
     * WARNING: This may take some time, since the Model is loading in here.
     * WARNING 2: This may throw an Error, if the Loading of the Model fails.
     *
     * @param detectInterval to use for estimating intervals.
     * @param timeoutAfterDetect to apply after detection + interval.
     * @param detectsPerSecond the amount of detects to run per second.
     * @param minSecurity the minimal security to have before triggering ( between 0 and 1 ).
     * @param model to use for detection.
     *
     * @throws Exception May throw this exception if the Model fails to load.
     */
    @SuppressWarnings({"JavaDoc", "WeakerAccess"})
    public PermanentDetectorReplay (
                                    long detectInterval,
                                    long timeoutAfterDetect,
                                    int detectsPerSecond,
                                    double minSecurity,
                                    ModelProxy model,
                                    SensorDataBatch mainBatch ) {

        this.mainBatch = mainBatch;
        this.detectInterval = detectInterval;
        this.timeoutAfterDetect = timeoutAfterDetect;
        this.detectsPerSecond = detectsPerSecond;
        this.minSecurity = minSecurity;

        this.detector = model.getGestureDetector();
    }


    /**
     * These Gestures will be used as Placeholders.
     * When they occur, there will be only a shorter timeout after detection.
     */
    public PermanentDetectorReplay addSkipGestures(Gesture... gestures ){
        this.skipGestures.addAll( Arrays.asList( gestures ) );
        return this;
    }



    /**
     * Starts the Detection.
     * This waits the length specified in the Constructor for the first detection.
     */
    public List<Pair<SensorDataBatch,GestureResultWrapper>> execute() {
        DateTime current = new DateTime( mainBatch.getStartTime() );
        DateTime end = mainBatch.getEndTime();

        List<Pair<SensorDataBatch,GestureResultWrapper>> results = new ArrayList<>();

        //first skip the next DetectionTime:
        current = current.plus( Duration.millis( detectInterval ) );

        //Now start a look of detection:
        while( current.isBefore( end ) ){
            SensorDataBatch batch = get( current );
            GestureResultWrapper result = detector.analyseGesture( batch );

            GestureResult top = result.getTopResult();
            lastTop.add( top );

            if( top.getFirst() != GestureManager.UNKNOWN() && top.getSecond() > minSecurity ) {
                //skip forward and add that we found something:
                results.add( new Pair<>( batch, result ) );

                Duration skipTime = Duration.millis( detectInterval / 2 ).plus( timeoutAfterDetect );
                if( skipGestures.contains( top.getFirst() ) ) skipTime = Duration.millis( timeoutAfterDetect );

                current = current.plus( skipTime );
                continue;
            }

            current = current.plus( Duration.millis( 1000L / detectsPerSecond ) );
        }

        return results;
    }


    /**
     * This is a method that gets ALL Batches in the executions / second time distances.
     * It WILL take some time!
     */
    public List<Pair<SensorDataBatch,GestureResultWrapper>> getAll(  ){
        DateTime current = new DateTime( mainBatch.getStartTime() );
        DateTime end = mainBatch.getEndTime();

        List<Pair<SensorDataBatch,GestureResultWrapper>> results = new ArrayList<>();

        //first skip the next DetectionTime:
        current = current.plus( Duration.millis( detectInterval ) );

        //Now start a look of detection:
        while( current.isBefore( end ) ){
            SensorDataBatch batch = get( current );
            GestureResultWrapper result = detector.analyseGesture( batch );
            results.add( new Pair<>( batch, result ) );

            current = current.plus( Duration.millis( 1000L / detectsPerSecond ) );
        }

        return results;
    }



    /**
     * Gets the interval ending with the passed time.
     *
     * @param formalEnd to get for.
     * @return the generated intervall.
     */
    private SensorDataBatch get( DateTime formalEnd ){
        DateTime end = formalEnd.minus( Duration.millis( 200 ) );
        return mainBatch.extractDataFromTo( end.minus( Duration.millis( detectInterval ) ), end );
    }



}
