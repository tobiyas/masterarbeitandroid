package de.tu_darmstadt.informatik.kom.gesturerec;

import android.content.Context;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextrec.phone.ContextRecSession;
import de.tu_darmstadt.informatik.kom.contextrec.phone.PhoneDataSources;
import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.PermanentIMUScanner;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.gesturerec.features.DefaultFeatureFactory;
import de.tu_darmstadt.informatik.kom.gesturerec.features.FeatureFactory;
import de.tu_darmstadt.informatik.kom.gesturerec.file.DefaultFileConstants;
import de.tu_darmstadt.informatik.kom.gesturerec.file.FileConstants;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.SourceLinker;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelManager;
import de.tu_darmstadt.informatik.kom.gesturerec.models.creator.SimpleModelCreator;
import de.tu_darmstadt.informatik.kom.gesturerec.recorder.DataRecorder;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleManager;
import de.tu_darmstadt.informatik.kom.gesturerec.users.UserManager;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.Dialogs;
import java8.util.stream.RefStreams;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 06.12.2017
 */
public class GestureRecSession implements AutoCloseable {

    /**
     * The used constants to save stuff.
     */
    private final FileConstants fileConstants;

    /**
     * The context rec session to use for collecting data.
     */
    private final ContextRecSession contextRecSession;

    /**
     * The IMU scanner to use for scanning stuff IMUs.
     */
    private final PermanentIMUScanner imuScanner;

    /**
     * The Source linker to use.
     */
    private final SourceLinker linker;

    /**
     * The context to bind stuff to.
     */
    private final Context context;

    /**
     * The Class manageing Users.
     */
    private final UserManager userManager;

    /**
     * The Samplemanager to use for sample stuff.
     */
    private final SampleManager sampleManager;

    /**
     * The used gesture manager.
     */
    private final GestureManager gestureManager;

    /**
     * The Modelmanager to use.
     */
    private final ModelManager modelManager;

    /**
     * The Recorder for Data.
     */
    private final DataRecorder dataRecorder;

    /**
     * The used Feature Factory.
     */
    private FeatureFactory featureFactory;




    /**
     * An option to use Dialogs.
     */
    private final Dialogs Dialogs;

    /**
     * The Creator for simple models.
     */
    private final SimpleModelCreator SimpleModelCreator;


    /**
     * Creates a new GestureRec Session with the Default Feature Factory.
     *
     * @param context to use.
     */
    public GestureRecSession( Context context ) {
        this( context, new DefaultFeatureFactory(), new DefaultFileConstants() );
    }

    /**
     * Creates a new GestureRec Session.
     *
     * @param context to use for creation.
     * @param featureFactory to use for generating Features.
     */
    public GestureRecSession( Context context, FeatureFactory featureFactory, FileConstants fileConstants ){
        //Set up the Basics:
        this.fileConstants = fileConstants;
        this.context = context;
        this.featureFactory = featureFactory;
        this.contextRecSession = new ContextRecSession( context );
        this.imuScanner = new PermanentIMUScanner( context );
        this.userManager = new UserManager( context, this );
        this.linker = new SourceLinker( context, contextRecSession );
        this.gestureManager = new GestureManager( this );
        this.sampleManager = new SampleManager( this );
        this.modelManager = new ModelManager( this );
        this.dataRecorder = new DataRecorder( this );

        //Create simple access Classes:
        this.Dialogs = new Dialogs( this );
        this.SimpleModelCreator = new SimpleModelCreator( this );


        //Config the IMU scanner:
        this.imuScanner.setConsumer( contextRecSession::addDataSource  );

        //Config the session to do remove custom triggers, by using a very long time.:
        this.contextRecSession.switchToCustomTriggering(() -> new DateTime(System.currentTimeMillis() * 2));

        //Reload the Gesture Manager:
        this.gestureManager.init();

        //Let the Linker do his business:
        this.linker.linkCurrent();

        //Load the Sample manager:
        this.sampleManager.reload();

        //Reload the Model Manager.
        this.modelManager.reload();
    }


    /**
     * Starts streaming data to be able to operate on.
     */
    public GestureRecSession startStreaming() {
        if (!contextRecSession.isRunning()){
            this.contextRecSession.start(false);
            this.contextRecSession.startStreaming();
        }

        return this;
    }

    /**
     * Pauses the Streaming of the Data.
     */
    public GestureRecSession stopStreaming(){
        if( contextRecSession.isRunning() ) {
            this.contextRecSession.stop();
            this.contextRecSession.stopStreaming();
        }

        return this;
    }



    @Override
    public void close(){
        if( contextRecSession.isClosed() ) return;

        stopStreaming();

        contextRecSession.close( false );
        imuScanner.stop();
    }






    ///////////////////////////////
    // DataSource Related stuff  //
    ///////////////////////////////

    /**
     * Adds the default Phone sensors.
     */
    public GestureRecSession addPhoneSensorsToSession(){
        contextRecSession.addDataSources( PhoneDataSources.inertialPhoneSensors( context ) );
        return this;
    }


    /**
     * Adds the default Phone sensors.
     */
    public GestureRecSession scanForWearables(){
        PhoneDataSources.wearStreamingConnectors( context, contextRecSession::addDataSources );
        return this;
    }


    /**
     * Adds the default Phone sensors.
     */
    public GestureRecSession scanForIMUs(){
        imuScanner.reset().start();
        return this;
    }

    /**
     * Adds custom sources.
     *
     * @param sources to add
     */
    public GestureRecSession addSources( DataSource... sources ){
        RefStreams.of( sources )
                .filter( this::hasSource )
                .forEach( contextRecSession::addDataSource );

        return this;
    }


    /**
     * Returns if the Source passed is already present.
     * @param source the source to check.
     * @return true if already present.
     */
    private boolean hasSource( DataSource source ){
        String deviceName = source.getDeviceName();
        return stream( contextRecSession.getDataSources() )
            .anyMatch( s -> s.getDeviceName().equals( deviceName ) );
    }



    ///////////////////////////////
    //  Getter for external uses //
    ///////////////////////////////


    /**
     * Gets the file constants to inspect the Folders used by GestureRec.
     * @return the file Constants.
     */
    public FileConstants getFileConstants() {
        return fileConstants;
    }

    /**
     * Gets the used session to use for own Usage.
     * NO GUARANTY IF SOMETHING BREAKS AFTER THIS CALL!
     *
     * @return the session used.
     */
    public ContextRecSession getContextRecSession() {
        return contextRecSession;
    }

    /**
     * Gets the current UserManager.
     *
     * @return the current UserManager.
     */
    public UserManager getUserManager() {
        return userManager;
    }

    /**
     * Gets the SourceLinker to Redefine the Bounds.
     *
     * @return the Linker.
     */
    public SourceLinker getLinker() {
        return linker;
    }

    /**
     * Accesses the Gesture Manages.
     *
     * @return the gesture manager.
     */
    public GestureManager getGestureManager() {
        return gestureManager;
    }


    /**
     * Gets the Sample manager to access and use Samples.
     *
     * @return the Samplemanager for access to samples.
     */
    public SampleManager getSampleManager() {
        return sampleManager;
    }

    /**
     * Gets the Manager for Models.
     *
     * @return the Manager handling Models.
     */
    public ModelManager getModelManager() {
        return modelManager;
    }

    /**
     * Gets the DataRecorder.
     * @return the data Recorder.
     */
    public DataRecorder getDataRecorder(){
        return dataRecorder;
    }

    /**
     * Gets the current Feature Factory.
     * @return the current DefaultFeatureFactory.
     */
    public FeatureFactory getFeatureFactory(){
        return featureFactory;
    }


    //////////////////////
    // Simple Creators: //
    //////////////////////


    /**
     * Gets a simple Dialog creator for Gestures and Users.
     * @return a Dialog Creator.
     */
    public Dialogs Dialogs(){
        return Dialogs;
    }


    /**
     * Gets a Creator for Simple Models.
     * @return the Creator for simple Models.
     */
    public SimpleModelCreator SimpleModelCreator() {
        return SimpleModelCreator;
    }
}
