package de.tu_darmstadt.informatik.kom.gesturerec.features;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureBuilder;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureSet;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.Limb;

import static java8.util.stream.RefStreams.of;

/**
 * This class represents a Feature Factory to generate Features.
 *
 */
public interface FeatureFactory {

    /**
     * Registers a set of Features for the Limb passed.
     *
     * @param registry to register to.
     * @param limb to register to.
     */
    void registerFeatures( WekaFeatureBuilder registry, Limb limb );


    /**
     * Registers a set of Features for a cross Limb detection.
     *
     * @param registry to register to.
     * @param limbs to use for Registering.
     */
    void registerCrossLimbFeatures( WekaFeatureBuilder registry, Collection<Limb> limbs );


    /**
     * This builds ALL Features for every Source present with the passed Gestures.
     *
     * @param gestures to use for classes.
     * @return the Generated Feature Set.
     */
    default WekaFeatureSet buildFeatures( List<Gesture> gestures ) {
        WekaFeatureBuilder builder = buildFeatureBuilder();
        return builder.buildFeatureSet( builder.getFeatureNames(), new SituationGroup( "TMP", gestures ) );
    }


    /**
     * Builds ALL Features for every Source present.
     *
     * @return the generated Features.
     */
    default WekaFeatureBuilder buildFeatureBuilder() {
        WekaFeatureBuilder builder = new WekaFeatureBuilder(false);
        of(Limb.ALL_LIMBS)
                .forEach(limb -> registerFeatures(builder, limb));
        registerCrossLimbFeatures(builder, Arrays.asList(Limb.ALL_LIMBS));

        return builder;
    }

}
