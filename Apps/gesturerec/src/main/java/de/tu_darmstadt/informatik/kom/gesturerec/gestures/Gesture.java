package de.tu_darmstadt.informatik.kom.gesturerec.gestures;

import android.support.annotation.NonNull;

import java.io.File;

/**
 * Created by Toby on 28.08.2017
 */
public class Gesture implements Comparable<Gesture> {

    /**
     * The Name of the Gesture.
     */
    private final String name;

    /**
     * The Directory files are saved for the Gesture.
     */
    private final File saveDirectory;


    public Gesture(String name, File saveDirectory) {
        this.name = name;
        this.saveDirectory = saveDirectory;
    }

    /**
     * Gets the name of the Gesture.
     */
    public String getName() {
        return name;
    }

    public File getSaveDirectory() {
        return saveDirectory;
    }


    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if ( super.equals( obj ) ) return true;
        if( obj == null ) return false;

        return this.toString().equals( obj.toString() );
    }

    @Override
    public int compareTo(@NonNull Gesture gesture) {
        return toString().compareTo( gesture.toString() );
    }
}
