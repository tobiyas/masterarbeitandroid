package de.tu_darmstadt.informatik.kom.gesturerec.file;

import java.io.File;

/**
 * Created by Toby on 09.01.2018
 */
public interface FileConstants {

    /**
     * Gets the Folder for Gesture Data.
     * @return the folder for Gesture-Data.
     */
    File getBaseGestureFolder();


    /**
     * Gets the Folder for Models.
     * @return the folder for models.
     */
    File getBaseModelFolder();


}
