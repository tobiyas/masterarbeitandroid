package de.tu_darmstadt.informatik.kom.gesturerec.features;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureBuilder;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LinearAccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.MagnetometerData;
import de.tu_darmstadt.informatik.kom.contextsense.utils.ArrayInterpolator;
import de.tu_darmstadt.informatik.kom.contextsense.utils.FFT;
import de.tu_darmstadt.informatik.kom.contextsense.utils.MathUtils;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.Limb;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.ArrayUtils;
import java8.util.Optional;
import java8.util.stream.Collectors;
import java8.util.stream.DoubleStreams;
import java8.util.stream.IntStreams;

import static de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureGroup.INERTIAL;

/**
 * Created by Toby on 21.10.2017
 */

public class DefaultFeatureFactory implements FeatureFactory {

    //Nothing more to say to this.
    private static final double GRAVITY_ON_EARTH = 9.81;


    //Config options:

    /**
     * If FFT Should be used as features:
     */
    private static final boolean USE_FFT = false;

    /**
     * The amount of slices for the FFT.
     */
    private static final int FFT_SLICES = 10;


    /**
     * The amount of slices for the Accel to split.
     */
    private static final int ACCEL_SLICED_SLICES = 10;


    /**
     * The amount of slices for the Magnetometer to split.
     */
    private static final int MAGNETO_SLICED_SLICES = 10;


    @Override
    public void registerFeatures( WekaFeatureBuilder registry, Limb limb ) {
        if( registry == null ) return;
        if( limb == null ) return;

        String dataSource = limb.getName();
        final int sliceLength = 200 / ACCEL_SLICED_SLICES;

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Rotation", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .flatMap( data -> data.getT().length > 1 ? Optional.of( data ) : Optional.empty() )
                .map( data -> {
                    List<double[]> orientations = IntStreams
                            .range( 1, data.getT().length )
                            .mapToObj( i -> {
                                double x = data.getX()[i];
                                double y = data.getY()[i];
                                double z = data.getZ()[i];
                                double length = Math.sqrt( x*x + y*y + z*z );
                                return new double[] { x/length, y/length, z/length };
                            }).collect( Collectors.toList() );

                    return IntStreams.range( 1, orientations.size() )
                            .mapToDouble( i -> Math.acos(
                                    orientations.get( i )[0] * orientations.get( i -1 )[0] +
                                    orientations.get( i )[1] * orientations.get( i -1 )[1] +
                                    orientations.get( i )[2] * orientations.get( i -1 )[2] ) )
                            .sum();
                })
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Avg. Acceleration (X)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getAvgNormalizedX )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Avg. Acceleration (Y)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getAvgNormalizedY )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Avg. Acceleration (Z)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getAvgNormalizedZ )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Avg. Linear Acc (X)", sample -> sample
                .getData( LinearAccelerationData.class, dataSource )
                .map( LinearAccelerationData::getX )
                .map( acc -> DoubleStreams.of( acc ).average().getAsDouble() )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Avg. Linear Acc (Y)", sample -> sample
                .getData( LinearAccelerationData.class, dataSource )
                .map( LinearAccelerationData::getY )
                .map( acc -> DoubleStreams.of( acc ).average().getAsDouble() )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Avg. Linear Acc (Z)", sample -> sample
                .getData( LinearAccelerationData.class, dataSource )
                .map( LinearAccelerationData::getZ )
                .map( acc -> DoubleStreams.of( acc ).average().getAsDouble() )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Activity Count (Lin acc x)", sample -> sample
                .getData( LinearAccelerationData.class, dataSource )
                .map( LinearAccelerationData::getX )
                .map( data -> {
                    long samplesAboveThreshold = DoubleStreams.of( data )
                            .filter( d -> (Math.abs( d ) > 0.5 * GRAVITY_ON_EARTH ) )
                            .count();

                    return samplesAboveThreshold / (double)data.length;
                })
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Activity Count (Lin acc y)", sample -> sample
                .getData( LinearAccelerationData.class, dataSource )
                .map( LinearAccelerationData::getY )
                .map( data -> {
                    long samplesAboveThreshold = DoubleStreams.of( data )
                            .filter( d -> (Math.abs( d ) > 0.5 * GRAVITY_ON_EARTH ) )
                            .count();

                    return samplesAboveThreshold / (double)data.length;
                })
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Activity Count (Lin acc z)", sample -> sample
                .getData( LinearAccelerationData.class, dataSource )
                .map( LinearAccelerationData::getZ )
                .map( data -> {
                    long samplesAboveThreshold = DoubleStreams.of( data )
                            .filter( d -> (Math.abs( d ) > 0.5 * GRAVITY_ON_EARTH ) )
                            .count();

                    return samplesAboveThreshold / (double)data.length;
                })
        );


        //Experimental:
        /*registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": extremas x", sample ->
            sample
                .getData( GyroscopeSensorData.class, dataSource )
                .map( data -> extremas( data.getT(), ArrayUtils.normalizeValue( data.getX() ), 6, 0.5d,true ) )
                .map( List::size )
                .map( a -> (double) a )
        );
        registry.registerMultiNumericFeature( INERTIAL, dataSource + ": Gyro X Sliced MiddleValue", ACCEL_SLICED_SLICES, sample -> sample
                .getData( GyroscopeSensorData.class, dataSource )
                .map( GyroscopeSensorData::getX )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, ACCEL_SLICED_SLICES)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i * sliceLength, (i + 1) * sliceLength ) ).average().getAsDouble() )
                        .toArray()
                )
        );
        registry.registerMultiNumericFeature( INERTIAL, dataSource + ": Gyro Y Sliced MiddleValue", ACCEL_SLICED_SLICES, sample -> sample
                .getData( GyroscopeSensorData.class, dataSource )
                .map( GyroscopeSensorData::getY )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, ACCEL_SLICED_SLICES)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i * sliceLength, (i + 1) * sliceLength ) ).average().getAsDouble() )
                        .toArray()
                )
        );
        registry.registerMultiNumericFeature( INERTIAL, dataSource + ": Gyro Z Sliced MiddleValue", ACCEL_SLICED_SLICES, sample -> sample
                .getData( GyroscopeSensorData.class, dataSource )
                .map( GyroscopeSensorData::getZ )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, ACCEL_SLICED_SLICES)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i * sliceLength, (i + 1) * sliceLength ) ).average().getAsDouble() )
                        .toArray()
                )
        );*/








        //Only use FFT if wanted!
        if( USE_FFT ){
            registry.registerMultiNumericFeature( INERTIAL, dataSource + ": Acceleration (X) 0-10Hz", FFT_SLICES, sample -> sample
                    .getData( AccelerationData.class, dataSource )
                    .map( AccelerationData::getX )
                    .map( a -> ArrayInterpolator.interpolateLinear( a, 64) )
                    .map( ArrayUtils::normalizeValue )
                    .map( FFT::fft )
                    .map( data -> Arrays.copyOf( data, FFT_SLICES ) )
            );

            registry.registerMultiNumericFeature( INERTIAL, dataSource + ": Acceleration (Y) 0-10Hz", FFT_SLICES, sample -> sample
                    .getData( AccelerationData.class, dataSource )
                    .map( AccelerationData::getY )
                    .map( a -> ArrayInterpolator.interpolateLinear( a, 64) )
                    .map( ArrayUtils::normalizeValue )
                    .map( FFT::fft )
                    .map( data -> Arrays.copyOf( data, FFT_SLICES ) )
            );

            registry.registerMultiNumericFeature( INERTIAL, dataSource + ": Acceleration (Z) 0-10Hz", FFT_SLICES, sample -> sample
                    .getData( AccelerationData.class, dataSource )
                    .map( AccelerationData::getZ )
                    .map( a -> ArrayInterpolator.interpolateLinear( a, 64) )
                    .map( ArrayUtils::normalizeValue )
                    .map( FFT::fft )
                    .map( data -> Arrays.copyOf( data, FFT_SLICES ) )
            );
        }

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Acceleration Range (X)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getX )
                .map( acc -> {
                    double min = DoubleStreams.of( acc ).min().getAsDouble();
                    double max = DoubleStreams.of( acc ).max().getAsDouble();
                    return max - min;
                } )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Acceleration Range (Y)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getY )
                .map( acc -> {
                    double min = DoubleStreams.of( acc ).min().getAsDouble();
                    double max = DoubleStreams.of( acc ).max().getAsDouble();
                    return max - min;
                } )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Acceleration Range (Z)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getZ )
                .map( acc -> {
                    double min = DoubleStreams.of( acc ).min().getAsDouble();
                    double max = DoubleStreams.of( acc ).max().getAsDouble();
                    return max - min;
                } )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Stdev. Acceleration (X)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getX )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Stdev. Acceleration (Y)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getY )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Stdev. Acceleration (Z)", sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getZ )
                .map( MathUtils::stdev )
        );

        registry.registerMultiNumericFeature( INERTIAL, dataSource + ": Acceleration X Sliced MiddleValue", ACCEL_SLICED_SLICES, sample -> sample
                .getData( AccelerationData.class, dataSource )
                .map( AccelerationData::getX )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, ACCEL_SLICED_SLICES)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i * sliceLength, (i + 1) * sliceLength ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerMultiNumericFeature( INERTIAL, dataSource + ": Acceleration Y Sliced MiddleValue", ACCEL_SLICED_SLICES, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map( AccelerationData::getY )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 10)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i * sliceLength, (i + 1) * sliceLength ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerMultiNumericFeature( INERTIAL, dataSource + ": Acceleration Z Sliced MiddleValue", ACCEL_SLICED_SLICES, sample -> sample
                .getData(AccelerationData.class, dataSource)
                .map( AccelerationData::getZ )
                .map( a -> ArrayInterpolator.interpolateLinear(a, 200) )
                .map( data ->  IntStreams.range(0, 10)
                        .mapToDouble( i -> DoubleStreams.of( Arrays.copyOfRange( data, i * sliceLength, (i + 1) * sliceLength ) ).average().getAsDouble() )
                        .toArray()
                )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Mag. Field stdev (X)", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .map( MagnetometerData::getX )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Mag. Field stdev (Y)", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .map( MagnetometerData::getY )
                .map( MathUtils::stdev )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Mag. Field stdev (Z)", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .map( MagnetometerData::getZ )
                .map( MathUtils::stdev )
        );


        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Mag. Field stdev (X) Sliced", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .map( MagnetometerData::getX )
                .map( values -> MathUtils.stdevSlices( values, MAGNETO_SLICED_SLICES ) )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Mag. Field stdev (Y)", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .map( MagnetometerData::getY )
                .map( values -> MathUtils.stdevSlices( values, MAGNETO_SLICED_SLICES ) )
        );

        registry.registerNumericFeatureWithDefault0( INERTIAL, dataSource + ": Mag. Field stdev (Z)", sample -> sample
                .getData( MagnetometerData.class, dataSource )
                .map( MagnetometerData::getZ )
                .map( values -> MathUtils.stdevSlices( values, MAGNETO_SLICED_SLICES ) )
        );
    }

    @Override
    public void registerCrossLimbFeatures(WekaFeatureBuilder registry, Collection<Limb> limbs) {
        //No Cross-Feed in this version.
    }

}
