package de.tu_darmstadt.informatik.kom.gesturerec.evaluate;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.DetectorCreator;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import java8.util.function.Consumer;
import java8.util.stream.IntStreams;

/**
 * Created by Toby on 05.11.2017
 */

public class SimpleEvaluator {


    /**
     * Builds a Detector + Evaluates it.
     *
     * @param session to use for loading stuff.
     * @param properties to use for creating the evaluator.
     * @param consumer to call for status updates.
     * @param type to use for creation.
     *
     * @return the detector (may be null) and the Results.
     */
    public static Pair<WekaDetector,EvaluationResults> evaluate( GestureRecSession session,
                                                                 EvaluatorProperties properties,
                                                                 Consumer<String> consumer,
                                                                 EvaluationType type ){

        switch ( type ){
            case SPLIT : return buildSplit( session, properties.getGestures(), properties.getUsers(), properties.isShuffleSamples(), properties.getValue(), properties.getBaseDetector(), consumer );
            case CROSS_VALIDATION: return buildParallelCrossValidation( session, properties.getGestures(), properties.getUsers(), properties.isShuffleSamples(), properties.getValue(), properties.getBaseDetector(), consumer );
            case AGAINST_USER_DATA: return buildUserData( session, properties.getGestures(), properties.getUsers(), properties.getTestUsers(), properties.getBaseDetector(), consumer );
            case ALL_USERS_AGAINST_ONE: return buildAllAgainstOneUserData( session, properties.getGestures(), properties.getUsers(), properties.getBaseDetector(), consumer );
        }


        throw new IllegalArgumentException( " Type may not be " + String.valueOf( type ) );
    }


    /**
     * Builds a All against 1 User data.
     * @param session to use.
     * @param gestures gestures to use.
     * @param users to evaluate.
     * @param type to use for evaluation.
     * @param consumer to call when GUI update is possible.
     *
     * @return the generated Result.
     */
    public static Pair<WekaDetector, EvaluationResults> buildAllAgainstOneUserData( GestureRecSession session,
                                                                                     Collection<Gesture> gestures,
                                                                                     Collection<String> users,
                                                                                     WekaBaseClassifier type,
                                                                                     Consumer<String> consumer) {
        EvaluationResults totalResults = new EvaluationResults();
        WekaDetector tmp = null;

        int max = users.size();
        int current = 1;
        for( String user : users ){
            consumer.accept( "Doing " + user + " ("+current+"/"+max+")" );

            Collection<String> others = new HashSet<>( users );
            others.remove( user );

            SampleBatch otherBatch = session.getSampleManager().query().addUsers( others ).addGestures( gestures ).applyFilter();
            SampleBatch ownBatch = session.getSampleManager().query().addUsers( user ).addGestures( gestures ).applyFilter();

            WekaDetector model = build( session,
                    type,
                    otherBatch );

            tmp = model;
            totalResults.mergeResults( test( session, model, ownBatch ) );

            current++;
        }

        return new Pair<>( tmp, totalResults );
    }

    /**
     * Builds a All against 1 Batch data.
     *
     * @param type to use for building.
     * @param batches to use for x-Folds.
     * @param consumer to call when GUI update is possible.
     *
     * @return the generated Result.
     */
    public static Pair<WekaDetector, EvaluationResults> buildAllBatchesAgainstEachOther( GestureRecSession session,
                                                                                    WekaBaseClassifier type,
                                                                                    Collection<SampleBatch> batches,
                                                                                    Consumer<String> consumer) {
        EvaluationResults totalResults = new EvaluationResults();
        WekaDetector tmp = null;

        int max = batches.size();
        int current = 1;
        for( SampleBatch batch : batches ){
            consumer.accept( "Doing "+current+"/"+max+"" );

            Collection<SampleBatch> againstBatches = new ArrayList<>( batches );
            againstBatches.remove( batch );

            SampleBatch otherBatch = new SampleBatch().addBatches( againstBatches );
            WekaDetector model = build( session,
                    type,
                    otherBatch );

            tmp = model;
            totalResults.mergeResults( test( session, model, batch ) );

            current++;
        }

        return new Pair<>( tmp, totalResults );
    }


    /**
     * Creates testing against userdata of someone else.
     *
     * @param session to use for loading.
     * @param gestures to use for the Evaluation.
     * @param user1 first user to use.
     * @param user2 first user to use.
     * @param type the type of classifier to use.
     * @param consumer to call for status updates.
     *
     * @return the generated Detector + the Results.
     */
    public static Pair<WekaDetector, EvaluationResults> buildUserData( GestureRecSession session,
                                                                        Collection<Gesture> gestures,
                                                                        Collection<String> user1,
                                                                        Collection<String> user2,
                                                                        WekaBaseClassifier type,
                                                                        Consumer<String> consumer ) {

        consumer.accept( "Loading Samples..." );
        SampleBatch trainingSamples = session.getSampleManager().query().addUsers( user1 ).addGestures( gestures ).applyFilter();
        SampleBatch testSamples = session.getSampleManager().query().addUsers( user2 ).addGestures( gestures ).applyFilter();

        consumer.accept( "Training..." );
        WekaDetector detector =  build( session, type, trainingSamples );

        return new Pair<>( detector, test( session, detector, testSamples ) );
    }


    /**
     * Creates testing of a existing model against some other data.
     *
     * @param session to use for loading.
     * @param proxy to evaluate.
     * @param batch to use for testing.
     *
     * @return the generated Detector + the Results.
     */
    public static EvaluationResults buildModelEval( GestureRecSession session,
                                                        ModelProxy proxy,
                                                        SampleBatch batch ) {
        return test( session, proxy.getDetector(), batch );
    }



    /**
     * Creates a CrossValidation Result.
     *
     * @param session to use.
     * @param gestures to use
     * @param users to use.
     * @param shuffled if the data should be shuffled
     * @param type  the type of classifier to use.
     * @param consumer to use.
     *
     * @return the result of the Cross validation.
     */
    public static Pair<WekaDetector, EvaluationResults> buildParallelCrossValidation( GestureRecSession session,
                                                                                      Collection<Gesture> gestures,
                                                                                      Collection<String> users,
                                                                                      boolean shuffled,
                                                                                      int folds,
                                                                                      WekaBaseClassifier type,
                                                                                      Consumer<String> consumer ) {

        EvaluationResults totalResults = new EvaluationResults();

        consumer.accept( "Loading samples..." );
        SampleBatch allSamples = session.getSampleManager().query()
                .addUsers( users )
                .addGestures( gestures )
                .setShuffled( shuffled )
                .applyFilter();

        AtomicInteger counter = new AtomicInteger( 0 );
        consumer.accept( "Starting Splits parallel..." );
        IntStreams.range(0,folds)
                .parallel()
                .forEach( i -> {
                    Pair<SampleBatch,SampleBatch> split = splitForCrossSamples( allSamples, folds, i );
                    WekaDetector detector = build( session, type, split.getFirst() );
                    totalResults.mergeResults( test( session, detector, split.getSecond() ) );

                    int newValue = counter.addAndGet( 1 );
                    consumer.accept( "Splits done: " + newValue + "/" + folds + " ..." );
                } );

        return new Pair<>( null, totalResults );
    }


    /**
     * Splits the Data into CrossValidataion Samples.
     *
     * @param batch to split
     * @param splits the amount of splits.
     * @param slot the slot to calculate.
     *
     * @return the split list in Trainings and Test Samples.
     */
    private static Pair<SampleBatch, SampleBatch> splitForCrossSamples (
                        SampleBatch batch,
                        int splits,
                        int slot ) {

        int amountTestData = batch.size() / splits;
        int start = amountTestData * slot;
        int end = Math.min( start + amountTestData, batch.size() );

        List<SampleProxy> data = new ArrayList<>( batch.getSamples() );
        List<SampleProxy> testData = data.subList( start, end );
        List<SampleProxy> trainingData = new ArrayList<>( data );
        trainingData.removeAll( testData );

        return new Pair<>(
                new SampleBatch( trainingData, batch.getGestures() ),
                new SampleBatch( testData, batch.getGestures() )
        );
    }


    /**
     * Builds the Split variant.
     *
     * @param session to use for loading
     * @param gestures to use
     * @param users to use.
     * @param shuffled if the data should be shuffled
     * @param splitPoint the point where to split (0-100)
     * @param type  the type of classifier to use.
     * @param updateMessagesCallback to call when status changed.
     *
     * @return the generated Detector + Results.
     */
    public static Pair<WekaDetector, EvaluationResults> buildSplit ( GestureRecSession session,
                                                                     Collection<Gesture> gestures,
                                                                     Collection<String> users,
                                                                     boolean shuffled,
                                                                     double splitPoint,
                                                                     WekaBaseClassifier type,
                                                                     Consumer<String> updateMessagesCallback ) {
        
        if( users == null || users.isEmpty() ) throw new IllegalStateException( "No Users!" );

        if(updateMessagesCallback != null) updateMessagesCallback.accept( "Loading Samples" );
        SampleBatch batch = session.getSampleManager().query()
                .addUsers( users )
                .addGestures( gestures )
                .applyFilter();

        if( batch.isEmpty() ) throw new IllegalStateException( "No samples!" );

        //Split the Samples:
        Pair<SampleBatch,SampleBatch> split = splitBySplit( batch, splitPoint, shuffled );

        //Build the Detector:
        if(updateMessagesCallback != null) updateMessagesCallback.accept( "Creating Detector..." );
        WekaDetector detector = build( session, type, split.getFirst() );

        //Test it:
        if(updateMessagesCallback != null) updateMessagesCallback.accept( "Testing..." );
        return new Pair<>( detector, test( session, detector, split.getSecond() ) );
    }


    /**
     * This does a simple Sample - Vs - Sample Test.
     * @param session to use for creating the Detector.
     * @param trainingsData to use.
     * @param testData to use.
     * @param classifier to use.
     *
     * @return the created Detector and the results.
     */
    public static Pair<WekaDetector,EvaluationResults> buildBatchAgainstBatch(
            GestureRecSession session,
            SampleBatch trainingsData,
            SampleBatch testData,
            WekaBaseClassifier classifier ){

        WekaDetector detector = build( session, classifier, trainingsData );
        return new Pair<>( detector, test( session, detector, testData ) );
    }


    /**
     * Creates a new Weka Detector from Samples + Properties.
     *
     * @param session to use.
     * @param type to use.
     * @param batch to use.
     *
     * @return the generated Detector.
     */
    private static WekaDetector build(
            GestureRecSession session,
            WekaBaseClassifier type,
            SampleBatch batch ){

        return DetectorCreator.create(
                session,
                type,
                batch,
                type.name() +"_TMP_" + System.currentTimeMillis()
        );
    }


    /**
     * Does a test with the Detector.
     * This can take some time dependent on the amount of test data.
     *
     * @param session to use to retrieve stuff
     * @param detector to use for testing.
     * @param testSamples to use for testing.
     *
     * @return the results of the test.
     */
    private static EvaluationResults test( GestureRecSession session, WekaDetector detector, SampleBatch testSamples ) {
        if( testSamples.isEmpty() ) return new EvaluationResults();
        if( detector == null ) return null;

        EvaluationResults results = new EvaluationResults();
        GestureManager gm = session.getGestureManager();

        for( TrainingSample sample : testSamples.loadSamplesWithRandomName() ) {
            DetectorResult result = detector.detect( sample.getData(), new DetectionResult( DateTime.now() ));
            Gesture gesture = result.getLabelWithHighestProbability()
                    .map( gm::getGesture )
                    .orElse( GestureManager.UNKNOWN() );

            results.addResult( gm.getGesture( sample.getCorrectSituation() ), gesture );
        }

        return results;
    }


    /**
     * Splits the Samples in 2 Lists.
     * @param samples to split
     * @param splitRange the range to split 0-100 in percent.
     * @return the pair of split data.
     */
    private static Pair<SampleBatch,SampleBatch> splitBySplit( SampleBatch samples, double splitRange, boolean randomize ){
        List<SampleProxy> copySamples = new ArrayList<>( samples.getSamples() );
        if( randomize ) Collections.shuffle( copySamples );

        splitRange /= 100d;
        int splitPoint = (int) Math.floor( splitRange * (double)copySamples.size() );
        SampleBatch batch1 = new SampleBatch( copySamples.subList(0, splitPoint ), samples.getGestures() );
        SampleBatch batch2 = new SampleBatch( copySamples.subList( splitPoint, copySamples.size() ), samples.getGestures() );

        return new Pair<>( batch1, batch2 );
    }


}
