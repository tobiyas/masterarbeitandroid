package de.tu_darmstadt.informatik.kom.gesturerec.models;

import android.util.Log;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import java8.util.Optional;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;
import weka.classifiers.Classifier;

import static java8.util.stream.StreamSupport.stream;

/**
 *
 * Created by Toby on 07.12.2017
 */

public class ModelManager {


    /**
     * A set of all Models present.
     */
    private final Collection<ModelProxy> models = new HashSet<>();

    /**
     * The session to use.
     */
    private final GestureRecSession session;

    /**
     * Loads the Model Manager with the Gesture Manager.
     *
     * @param session to use.
     */
    public ModelManager( GestureRecSession session ) {
        this.session = session;
    }


    /**
     * Reloads the Models as Proxies.
     */
    public void reload(){
        models.clear();

        File[] files = session.getFileConstants().getBaseModelFolder().listFiles();
        if( files == null || files.length == 0 ) return;

        RefStreams.of( files )
                .filter( f -> f.getName().endsWith( ".model" ) )
                .map( File::getName )
                .map( n -> n.replace( ".model", "" ) )
                .map( n -> ModelProxyFactory.loadFromFilesystem( session, n ) )
                .filter( Optional::isPresent )
                .map( Optional::get )
                .forEach( models::add );
    }


    /**
     * Gets all models present.
     * @return all models.
     */
    public Collection<ModelProxy> getAllModels(){
        return new HashSet<>( models );
    }

    /**
     * Gets all models present as String.
     * @return all models.
     */
    public Collection<String> getAllModelNames(){
        return stream( models )
                .map( ModelProxy::getName )
                .collect( Collectors.toSet() );
    }


    /**
     * Gets the Model for the Name passed.
     *
     * @param name to getGesture for.
     * @return the model for the Name or null.
     */
    public ModelProxy getForName( String name ){
        return stream( models )
                .filter( m -> m.getName().equalsIgnoreCase( name ) )
                .findFirst()
                .orElse( null );
    }


    /**
     * Returns true if a Model with that name is already present.
     * @param name to search.
     * @return true if any present.
     */
    public boolean presentForName( String name ){
        return getForName( name ) != null;
    }



    /**
     * Adds a new Model and saves it.
     *
     * @return  the generated Model, null if failed..
     */
    public ModelProxy addModel( String name, Collection<Gesture> gestures, Classifier classifier ){
        //already present:
        if( presentForName( name ) ) return null;

        //Create and save:
        ModelProxy proxy = new ModelProxy( session, name, gestures, classifier );
        if( proxy.save() ) models.add( proxy );
        else return null;

        return proxy;
    }


    /**
     * Adds a new Model and saves it.
     *
     * @return  the generated Model, null if failed..
     */
    public IncrementalLearningModelProxy addModelIncrementalLearner(String name,
                                                                    List<Gesture> gestures,
                                                                    Classifier classifier,
                                                                    SampleBatch batch ){
        //already present:
        if( presentForName( name ) ) return null;

        //Create and save:
        IncrementalLearningModelProxy proxy = new IncrementalLearningModelProxy( session, name, gestures, classifier, batch );
        if( proxy.save() ) models.add( proxy );
        else return null;

        return proxy;
    }


    /**
     * Trains a new Model and does NOT add it to the System!
     *
     * @param name to use.
     * @param classifier to use.
     * @param batch to use.
     *
     * @return the created model.
     */
    public ModelProxy trainNewTMPModel( String name, WekaBaseClassifier classifier, SampleBatch batch ) {
        return session.SimpleModelCreator().createNewModel( name, classifier, batch, false );
    }

    /**
     * Traina a new Model and does NOT add it to the System!
     *
     * @param name to use.
     * @param classifier to use.
     * @param batch to use.
     *
     * @return the created model.
     */
    public IncrementalLearningModelProxy trainNewTMPIncrementalLearnerModel(String name, WekaBaseClassifier classifier, SampleBatch batch ) {
        //Save cast:
        return (IncrementalLearningModelProxy) session.SimpleModelCreator().createNewModel( name, classifier, batch, true );
    }


    /**
     * Traina a new Model and does add it to the System!
     *
     * @param name to use.
     * @param classifier to use.
     * @param batch to use.
     *
     * @return the created model.
     */
    public ModelProxy trainNewModel( String name, WekaBaseClassifier classifier, SampleBatch batch ) {
        ModelProxy proxy = trainNewTMPModel( name, classifier, batch );
        if( proxy != null ) this.models.add( proxy );
        return proxy;
    }


    /**
     * Traina a new Model and does add it to the System!
     *
     * @param name to use.
     * @param classifier to use.
     * @param batch to use.
     *
     * @return the created model.
     */
    public IncrementalLearningModelProxy trainNewModelIncrementalLearner( String name, WekaBaseClassifier classifier, SampleBatch batch ) {
        IncrementalLearningModelProxy proxy = trainNewTMPIncrementalLearnerModel( name, classifier, batch );
        if( proxy != null ) this.models.add( proxy );
        return proxy;
    }


    /**
     * Deletes the passed model.
     * @param proxy to use.
     */
    public void deleteModel( ModelProxy proxy ) {
        if( proxy == null ) return;

        this.models.remove( proxy );
        proxy.delete();
    }
}
