package de.tu_darmstadt.informatik.kom.gesturerec.evaluate;

/**
 * Created by Toby on 05.11.2017
 */

public enum EvaluationType {

    SPLIT,
    CROSS_VALIDATION,
    AGAINST_USER_DATA,
    ALL_USERS_AGAINST_ONE

}
