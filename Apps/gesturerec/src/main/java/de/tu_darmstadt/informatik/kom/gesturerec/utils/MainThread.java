package de.tu_darmstadt.informatik.kom.gesturerec.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import java8.util.function.Consumer;
import java8.util.function.Supplier;

/**
 * Created by Toby on 14.10.2017
 */

public class MainThread {

    private static final Executor cachedExecutor = Executors.newFixedThreadPool( 4 );

    /**
     * Lets something run on the MT now.
     * @param run to run on.
     */
    public static void runOnMT( Runnable run ){
        MTHandler().post( run );
    }

    /**
     * Lets something run on the MT later.
     * @param run to run later.
     * @param delay in MS to wait.
     */
    public static void runOnMTLater( Runnable run, long delay ){
        MTHandler().postDelayed( run, delay );
    }

    /**
     * Runs the Runnable async.
     * @param run to run.
     */
    public static void runAsync( Runnable run ){
        cachedExecutor.execute( run );
    }

    /**
     * Runs the Runnable async.
     * @param run to run.
     * @param finished to run when done.
     */
    public static void runAsync( Runnable run, Runnable finished ){
        cachedExecutor.execute( () -> {
            run.run();
            finished.run();
        } );
    }

    /**
     * Runs the Runnable async.
     * @param run to run.
     * @param finished to run when done.
     */
    public static void runAsyncCallbackMT( Runnable run, Runnable finished ){
        runAsync( run, () -> runOnMT( finished ) );
    }

    /**
     * Runs the Runnable async.
     * @param run to run.
     * @param finished to run when done.
     */
    public static <T> void runAsyncResultMT( Supplier<T> run, Consumer<T> finished ){
        runAsync( () -> {
            T result = run.get();
            runOnMT( () -> finished.accept( result ) );
        } );
    }

    /**
     * Displays a long Toast on the MT.
     *
     * @param context to use.
     * @param message to show.
     */
    public static void ToastLong( Context context, String message ){
        runOnMT( () -> Toast.makeText( context, message, Toast.LENGTH_LONG ).show() );
    }

    /**
     * Displays a short Toast on the MT.
     *
     * @param context to use.
     * @param message to show.
     */
    public static void ToastShort( Context context, String message ){
        runOnMT( () -> Toast.makeText( context, message, Toast.LENGTH_SHORT ).show() );
    }


    /**
     * Gets a handler for the Main Thread.
     * @return the handler for the MT.
     */
    public static Handler MTHandler(){
        return new Handler( Looper.getMainLooper() );
    }

}
