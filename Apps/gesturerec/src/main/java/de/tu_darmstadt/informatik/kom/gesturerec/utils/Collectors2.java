package de.tu_darmstadt.informatik.kom.gesturerec.utils;

import com.google.gson.JsonArray;

import org.json.JSONArray;

import java.util.EnumSet;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import java8.util.function.BiConsumer;
import java8.util.function.BinaryOperator;
import java8.util.function.Function;
import java8.util.function.Functions;
import java8.util.function.Supplier;
import java8.util.stream.Collector;

/**
 * Created by Toby on 12.09.2017
 */

public class Collectors2 {

    /**
     * This is a simple Collector for a JSON Array.
     * @return the generated array.
     */
    public static Collector<Object, JSONArray, JSONArray> toJsonArray() {

        return new Collector<Object, JSONArray, JSONArray>() {

            @Override
            public Supplier<JSONArray> supplier() {
                return JSONArray::new;
            }

            @Override
            public BiConsumer<JSONArray, Object> accumulator() {
                return JSONArray::put;
            }

            @Override
            public BinaryOperator<JSONArray> combiner() {
                return (a,b) -> {
                    for(int i = 0; i < b.length(); i++) {
                        try{ a.put(b.get(i)); }catch(Throwable ignored){}
                    }

                    return a;
                };
            }

            @Override
            public Function<JSONArray, JSONArray> finisher() {
                return a -> a;
            }

            @Override
            public Set<Characteristics> characteristics() {
                return EnumSet.of(Characteristics.IDENTITY_FINISH);
            }
        };
    }

    /**
     * This is a simple Collector for a JSON Array from Gson.
     * @return the generated array.
     */
    public static <T> Collector<T, JsonArray, JsonArray> toGsonArray() {

        return new Collector<T, JsonArray, JsonArray>() {

            @Override
            public Supplier<JsonArray> supplier() {
                return JsonArray::new;
            }

            @Override
            public BiConsumer<JsonArray, T> accumulator() {
                return (a,b) -> a.add( b.toString() );
            }

            @Override
            public BinaryOperator<JsonArray> combiner() {
                return (a,b) -> {
                    for(int i = 0; i < b.size(); i++) {
                        try{ a.add(b.get(i)); }catch(Throwable ignored){}
                    }

                    return a;
                };
            }

            @Override
            public Function<JsonArray, JsonArray> finisher() {
                return a -> a;
            }

            @Override
            public Set<Characteristics> characteristics() {
                return EnumSet.of(Characteristics.IDENTITY_FINISH);
            }
        };
    }


    public static Collector<SampleProxy, SampleBatch, SampleBatch> toSampleBatch() {

        return new Collector<SampleProxy, SampleBatch, SampleBatch>() {

            @Override
            public Supplier<SampleBatch> supplier() {
                return SampleBatch::new;
            }

            @Override
            public BiConsumer<SampleBatch, SampleProxy> accumulator() {
                return SampleBatch::addSample;
            }

            @Override
            public BinaryOperator<SampleBatch> combiner() {
                return SampleBatch::addBatch;
            }

            @Override
            public Function<SampleBatch, SampleBatch> finisher() {
                return Functions.identity();
            }

            @Override
            public Set<Characteristics> characteristics() {
                return EnumSet.of( Characteristics.IDENTITY_FINISH );
            }
        };
    }

}
