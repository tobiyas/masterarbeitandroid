package de.tu_darmstadt.informatik.gesturereader.gesturerec.gestures;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;

import static de.tu_darmstadt.informatik.gesturereader.gesturerec.utils.FileConstants.getBaseGestureFolder;
import static java8.util.stream.StreamSupport.stream;


/**
 * Created by Tobias on 26.10.2017
 */
public class GestureManager {

    /**
     * The instance to use.
     */
    private static GestureManager instance;


    /**
     * The Set of Gestures saved.
     */
    private final Collection<Gesture> gestures = new HashSet<>();

    /**
     * The Unknown Gestures.
     */
    public static final Gesture UNKNOWN = new Gesture( "UNKNOWN", new File( getBaseGestureFolder(), "UNKNOWN" ) );



    /**
     * This loads all gestures present from the Disc.
     */
    public GestureManager init(){
        this.gestures.clear();

        File base = getBaseGestureFolder();
        if( !base.exists() ) base.mkdirs();

        //Lists all folders in that dir.
        //Every folder is 1 Gesture.

        File[] folders = base.listFiles();
        if( folders != null && folders.length > 0 ) {
            RefStreams.of( folders )
                .filter( File::isDirectory )
                .map( File::getName )
                .forEach( this::createGesture );
        }

        return this;
    }

    /**
     * Creates a new Gesture.
     * @param name to create
     */
    public Gesture createGesture( String name ){
        Gesture cache = get( name );
        if( cache != null ) return cache;

        File folder = new File( getBaseGestureFolder(), name );
        if( !folder.exists() ) folder.mkdirs();

        Gesture gesture = new Gesture( name, folder );
        this.gestures.add( gesture );
        return gesture;
    }

    /**
     * Deletes the Gestures passed.
     * @param gesture to delete.
     */
    public void deleteGesture( Gesture gesture ){
        if( gesture == null ) return;

        //Remove the Gesture:
        this.gestures.remove( gesture );

        //Delete the folder with data:
        File folder = gesture.getSaveDirectory();
        try{ FileUtils.deleteDirectory( folder ); }catch ( Throwable exp ) { exp.printStackTrace(); }
    }


    /**
     * Gets a Gesture by name. Returns null if none present.
     * @param name to getGesture for.
     * @return the Gesture by name.
     */
    public Gesture get( String name ){
        if( name == null || name.isEmpty() || name.contains( "../" ) ){
            throw new IllegalArgumentException( "name may not be null, empty or contain ../" );
        }

        //Search for the Gesture:
        return stream( gestures )
                .filter( gesture -> gesture.getName().equalsIgnoreCase( name ) )
                .findFirst()
                .orElse( null );
    }


    /**
     * Gets all Gestures.
     * @return all gestures.
     */
    public Collection<Gesture> getAllWithoutUnknown(){
        return new HashSet<>( gestures );
    }

    /**
     * Gets all Gestures.
     * @return all gestures.
     */
    public List<String> getAllWithoutUnknownAsString(){
        return stream( gestures ).map( Gesture::getName ).collect( Collectors.toList() );
    }

    /**
     * Gets all Gestures.
     * @return all gestures.
     */
    public String[] getAllWithoutUnknownAsStringArray(){
        return stream( gestures ).map( Gesture::getName ).toArray( String[]::new );
    }


    /**
     * Gets all Gestures.
     * @return all gestures.
     */
    public Collection<Gesture> getAllWithUnknown(){
        Collection<Gesture> all = getAllWithoutUnknown();
        all.add( UNKNOWN );
        return all;
    }


    /**
     * Gets the Instance.
     * @return the instance.
     */
    public static GestureManager instance(){
        return instance == null ? instance = new GestureManager().init() : instance;
    }

}
