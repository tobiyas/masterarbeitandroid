package de.tu_darmstadt.informatik.gesturereader.gesturerec.gestures;

import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;

import static de.tu_darmstadt.informatik.gesturereader.gesturerec.gestures.GestureManager.UNKNOWN;

/**
 * This is a Pair representation to simplify Gesture Results.
 * Created by Toby on 09.10.2017
 */
public class GestureResult extends Pair<Gesture,Double> {

    public GestureResult( Map.Entry<String,Double> entry ){
        this( GestureManager.instance().createGesture( entry.getKey() ), entry.getValue());
    }

    public GestureResult(Gesture first, Double second) {
        super(first, second);
    }


    /**
     * Creates a new Empty Element.
     * @return an Empty element.
     */
    public static GestureResult empty(){
        return new GestureResult( UNKNOWN, 1d );
    }

    /**
     * Creates a new Empty Sequence.
     * @return empty sequence
     */
    public static Set<GestureResult> emptySet(){
        return RefStreams.of( empty() ).collect( Collectors.toSet() );
    }
}
