package de.tu_darmstadt.informatik.gesturereader.gesturerec.evaluate;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.DetectorCreator;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;

/**
 * Created by Toby on 15.10.2017
 */

public class WekaDetectorEvaluator {

    /**
     * The classifier to use.
     */
    private final WekaBaseClassifier classifier;

    /**
     * The Samples to use for Training.
     */
    private final SampleBatch trainingSamples = new SampleBatch();

    /**
     * The Samples to use for Testing.
     */
    private final SampleBatch testSamples = new SampleBatch();


    /**
     * The build classifier.
     */
    private WekaDetector detector;

    /**
     * Builds an Evaluator.
     * @param classifier to use.
     */
    public WekaDetectorEvaluator( WekaBaseClassifier classifier ) {
        this.classifier = classifier;
    }

    /**
     * Builds an Evaluator.
     * @param predefinedDetector to use.
     */
    public WekaDetectorEvaluator( WekaDetector predefinedDetector ) {
        this.classifier = WekaBaseClassifier.fromClassifier( predefinedDetector.getModel() );
        this.detector = predefinedDetector;
    }


    /**
     * Adds Training Samples.
     * @param batch to add.
     */
    public WekaDetectorEvaluator addTrainingSamples( SampleBatch batch ){
        this.trainingSamples.addBatch( batch );
        return this;
    }


    /**
     * Add some Test samples.
     * @param batch to add.
     */
    public WekaDetectorEvaluator addTestSamples( SampleBatch batch ){
        this.testSamples.addBatch( batch );
        return this;
    }

    /**
     * Add some Test samples.
     * @param batch to add.
     * @param splitPoint the point to split (0-100)%
     *
     * @return this for chaining.
     */
    public WekaDetectorEvaluator addSamplesWithSplit( SampleBatch batch, double splitPoint ) {
        return addSamplesWithSplit( batch, splitPoint, true);
    }

    /**
     * Add some Test samples.
     * @param samples to add.
     * @param splitPoint the point to split (0-100)%
     * @param shuffle if the Data should be shuffeled before.
     *
     * @return this for chaining.
     */
    public WekaDetectorEvaluator addSamplesWithSplit( SampleBatch samples, double splitPoint, boolean shuffle ){
        Pair<SampleBatch,SampleBatch> pair = splitBySplit( samples, splitPoint, shuffle );

        this.trainingSamples.addBatch( pair.getFirst() );
        this.testSamples.addBatch( pair.getSecond() );
        return this;
    }

    /**
     * Builds the Detector.
     * This takes a while!
     */
    public WekaDetectorEvaluator build(){
        this.detector = null;

        try{
            this.detector = DetectorCreator.create( this.classifier, trainingSamples, classifier.name() +"_TMP" );
        }catch ( Throwable exp ){
            exp.printStackTrace();
        }

        return this;
    }

    /**
     * Gets if the Detector is present.
     * @return if the detector is present.
     */
    public boolean hasDetector(){
        return detector != null;
    }


    /**
     * This starts a Training run. This may take a while!
     */
    public WekaDetectorEvaluator train(){
        if( detector == null ) return this;

        try{
            this.detector.trainBatch( trainingSamples.streamLoadSamplesWithRandomName() );
        }catch ( Throwable exp ){
            exp.printStackTrace();
        }

        return this;
    }

    /**
     * Gets the used classifier.
     * @return the used classifier.
     */
    public WekaBaseClassifier getClassifier() {
        return classifier;
    }


    /**
     * Splits the Samples in 2 Lists.
     * @param samples to split
     * @param splitRange the range to split 0-100 in percent.
     * @return the pair of split data.
     */
    private static Pair<SampleBatch,SampleBatch> splitBySplit( SampleBatch samples, double splitRange, boolean randomize ){
        List<SampleProxy> copySamples = new ArrayList<>( samples.getSamples() );
        if( randomize ) Collections.shuffle( copySamples );

        splitRange /= 100d;
        int splitPoint = (int) Math.floor( splitRange * (double)copySamples.size() );
        SampleBatch batch1 = new SampleBatch( copySamples.subList(0, splitPoint ), samples.getGestures() );
        SampleBatch batch2 = new SampleBatch( copySamples.subList( splitPoint, copySamples.size() ), samples.getGestures() );

        return new Pair<>( batch1, batch2 );
    }


    /**
     * Does a test with the Detector.
     * This can take some time dependent on the amount of test data.
     *
     * @return the results of the test.
     */
    public EvaluationResults test() {
        if( testSamples.isEmpty() ) return null;
        if( detector == null ) return null;

        EvaluationResults results = new EvaluationResults();
        GestureManager gm = GestureManager.instance();

        for( TrainingSample sample : testSamples.loadSamplesWithRandomName() ) {
            DetectorResult result = detector.detect( sample.getData(), new DetectionResult( DateTime.now() ));
            Gesture gesture = result.getLabelWithHighestProbability()
                    .map( gm::get )
                    .orElse( GestureManager.UNKNOWN );

            results.addResult( gm.get( sample.getCorrectSituation() ), gesture );
        }

        return results;
    }


}
