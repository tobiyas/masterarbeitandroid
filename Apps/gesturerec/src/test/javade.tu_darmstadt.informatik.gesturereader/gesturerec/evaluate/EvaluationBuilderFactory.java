package de.tu_darmstadt.informatik.gesturereader.gesturerec.evaluate;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectionResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.DetectorResult;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.DetectorCreator;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import java8.util.function.Consumer;

/**
 * Created by Toby on 05.11.2017
 */

public class EvaluationBuilderFactory {


    /**
     * Builds a Detector + Evaluates it.
     *
     * @param session to use for loading stuff.
     * @param properties to use for creating the evaluator.
     * @param consumer to call for status updates.
     * @param type to use for creation.
     *
     * @return the detector (may be null) and the Results.
     */
    public static Pair<WekaDetector,EvaluationResults> evaluate( GestureRecSession session,
                                                                 EvaluatorProperties properties,
                                                                 Consumer<String> consumer,
                                                                 EvaluationType type ){

        switch ( type ){
            case SPLIT : return buildSplit( session, properties, consumer );
            case CROSS_VALIDATION: return buildCrossValidation( session, properties, consumer );
            case AGAINST_USER_DATA: return buildUserData( session, properties, consumer );
        }


        throw new IllegalArgumentException( " Type may not be " + String.valueOf( type ) );
    }

    /**
     * Creates testing against userdata of someone else.
     *
     * @param session to use for loading.
     * @param properties to use for creating.
     * @param consumer to call for status updates.
     *
     * @return the generated Detector + the Results.
     */
    private static Pair<WekaDetector, EvaluationResults> buildUserData( GestureRecSession session,
                                                                        EvaluatorProperties properties,
                                                                        Consumer<String> consumer ) {

        consumer.accept( "Loading Samples..." );
        SampleBatch trainingSamples = session.getSampleManager().query().addUsers( properties.getUsers() ).applyFilter();
        SampleBatch testSamples = session.getSampleManager().query().addUsers( properties.getTestUsers() ).applyFilter();

        consumer.accept( "Training..." );
        WekaDetector detector =  build( properties, trainingSamples );

        return new Pair<>( detector, test( detector, testSamples ) );
    }


    /**
     * Creates a CrossValidation Result.
     *
     * @param session to use.
     * @param properties to use.
     * @param consumer to use.
     *
     * @return the result of the Cross validation.
     */
    private static Pair<WekaDetector, EvaluationResults> buildCrossValidation( GestureRecSession session,
                                                                               EvaluatorProperties properties,
                                                                               Consumer<String> consumer ) {

        EvaluationResults totalResults = new EvaluationResults();

        consumer.accept( "Loading samples..." );
        SampleBatch allSamples = session.getSampleManager().query()
                .setShuffled( properties.isShuffleSamples() )
                .applyFilter();

        int splits = properties.getValue();
        for( int i = 0; i < splits; i++ ){
            consumer.accept( "Starting split " + (i+1) + "/" + splits + " ..." );

            Pair<SampleBatch,SampleBatch> split = splitForCrossSamples( allSamples, splits, i );
            WekaDetector detector = build(  properties, split.getFirst() );
            totalResults.mergeResults( test( detector, split.getSecond() ) );
        }

        return new Pair<>( null, totalResults );
    }


    /**
     * Splits the Data into CrossValidataion Samples.
     *
     * @param batch to split
     * @param splits the amount of splits.
     * @param slot the slot to calculate.
     *
     * @return the split list in Trainings and Test Samples.
     */
    private static Pair<SampleBatch, SampleBatch> splitForCrossSamples(
                        SampleBatch batch,
                        int splits,
                        int slot ) {

        int amountTestData = batch.size() / splits;
        int start = amountTestData * slot;
        int end = Math.min( start + amountTestData, batch.size() );

        List<SampleProxy> data = new ArrayList<>( batch.getSamples() );
        List<SampleProxy> testData = data.subList( start, end );
        List<SampleProxy> trainingData = new ArrayList<>( data );
        trainingData.removeAll( testData );

        return new Pair<>(
                new SampleBatch( trainingData, batch.getGestures() ),
                new SampleBatch( testData, batch.getGestures() )
        );
    }


    /**
     * Builds the Split variant.
     *
     * @param session to use for loading
     * @param properties to use for creating.
     * @param updateMessagesCallback to call when status changed.
     *
     * @return the generated Detector + Results.
     */
    private static Pair<WekaDetector, EvaluationResults> buildSplit( GestureRecSession session,
                                                                     EvaluatorProperties properties,
                                                                     Consumer<String> updateMessagesCallback ) {
        
        if( properties.getUsers().isEmpty() ) throw new IllegalStateException( "No Users!" );

        if(updateMessagesCallback != null) updateMessagesCallback.accept( "Loading Samples" );
        SampleBatch batch = session.getSampleManager().query()
                .addUsers( properties.getUsers() )
                .addGestures( properties.getGestures() )
                .applyFilter();

        if( batch.isEmpty() ) throw new IllegalStateException( "No samples!" );

        //Split the Samples:
        Pair<SampleBatch,SampleBatch> split = splitBySplit( batch, properties.getValue(), properties.isShuffleSamples() );

        //Build the Detector:
        if(updateMessagesCallback != null) updateMessagesCallback.accept( "Creating Detector..." );
        WekaDetector detector = build( properties, split.getFirst() );

        //Test it:
        if(updateMessagesCallback != null) updateMessagesCallback.accept( "Testing..." );
        return new Pair<>( detector, test( detector, split.getSecond() ) );
    }


    /**
     * Creates a new Weka Detector from Samples + Properties.
     *
     * @param properties to use.
     * @param batch to use.
     *
     * @return the generated Detector.
     */
    private static WekaDetector build( EvaluatorProperties properties,
                                      SampleBatch batch){

        return DetectorCreator.create(
                properties.getBaseDetector(),
                batch,
                properties.getBaseDetector().name() +"_TMP_" + System.currentTimeMillis()
        );
    }


    /**
     * Does a test with the Detector.
     * This can take some time dependent on the amount of test data.
     *
     * @param detector to use for testing.
     * @param testSamples to use for testing.
     *
     * @return the results of the test.
     */
    private static EvaluationResults test( WekaDetector detector, SampleBatch testSamples ) {
        if( testSamples.isEmpty() ) return null;
        if( detector == null ) return null;

        EvaluationResults results = new EvaluationResults();
        GestureManager gm = GestureManager.instance();

        for( TrainingSample sample : testSamples.loadSamplesWithRandomName() ) {
            DetectorResult result = detector.detect( sample.getData(), new DetectionResult( DateTime.now() ));
            Gesture gesture = result.getLabelWithHighestProbability()
                    .map( gm::get )
                    .orElse( GestureManager.UNKNOWN );

            results.addResult( gm.get( sample.getCorrectSituation() ), gesture );
        }

        return results;
    }


    /**
     * Splits the Samples in 2 Lists.
     * @param samples to split
     * @param splitRange the range to split 0-100 in percent.
     * @return the pair of split data.
     */
    private static Pair<SampleBatch,SampleBatch> splitBySplit( SampleBatch samples, double splitRange, boolean randomize ){
        List<SampleProxy> copySamples = new ArrayList<>( samples.getSamples() );
        if( randomize ) Collections.shuffle( copySamples );

        splitRange /= 100d;
        int splitPoint = (int) Math.floor( splitRange * (double)copySamples.size() );
        SampleBatch batch1 = new SampleBatch( copySamples.subList(0, splitPoint ), samples.getGestures() );
        SampleBatch batch2 = new SampleBatch( copySamples.subList( splitPoint, copySamples.size() ), samples.getGestures() );

        return new Pair<>( batch1, batch2 );
    }


}
