package de.tu_darmstadt.informatik.gesturereader.gesturerec.evaluate;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 15.10.2017
 */

public class EvaluationResults {

    /**
     * The map of Result -> amount.
     */
    private final Set<ResultEntry> results = new HashSet<>();



    /**
     * Gets all present gestures.
     * @return all present gestures.
     */
    private Set<Gesture> getPresentGestures(){
        return stream( results )
                .map( ResultEntry::getGot )
                .collect( Collectors.toSet() );
    }


    /**
     * The gesture to add.
     * @param gesture to add.
     */
    private void addGesture( Gesture gesture ){
        Set<Gesture> present = getPresentGestures();

        //Add incorrect classifications:
        for( Gesture p : present ){
            results.add( new ResultEntry( p, gesture ) );
            results.add( new ResultEntry( gesture, p ) );
        }

        //Add the correct classification:
        results.add( new ResultEntry( gesture, gesture ) );
    }



    /**
     * Adds this result to the result batch.
     * @param wanted the wanted Gesture
     * @param got the got Gesture
     */
    public void addResult( Gesture wanted, Gesture got ) {
        if( wanted == null || got == null ) return;

        //First check if one of the Gestures is new:
        Set<Gesture> present = getPresentGestures();
        if( !present.contains( wanted ) ) addGesture( wanted );
        if( wanted != got && !present.contains( got ) ) addGesture( got );

        //Now add the Result:
        stream( results )
            .filter( r -> r.is( wanted, got ) )
            .forEach( ResultEntry::add1 );
    }


    /**
     * Gets the amount of Data for the Gesture passed is in Wanted.
     *
     * @param gesture to check.
     * @return the amount of Data with the passed Gestures.
     */
    public int getWantedEntries(Gesture gesture){
        return stream( results )
                .filter( r -> r.getWanted() == gesture )
                .mapToInt( ResultEntry::getAmount )
                .sum();
    }


    /**
     * Gets the amount of Data for the Gesture passed is in Got.
     *
     * @param gesture to check.
     * @return the amount of Data with the passed Gestures.
     */
    public int getGotEntries(Gesture gesture){
        return stream( results )
                .filter( r -> r.getGot() == gesture )
                .mapToInt( ResultEntry::getAmount )
                .sum();
    }

    /**
     * Gets the Entry for the Results.
     * @param wanted the Gesture wanted
     * @param got the Gesture got.
     * @return the result.
     */
    public int getEntry(Gesture wanted, Gesture got){
        return stream( results )
                .filter( r -> r.is( wanted, got) )
                .mapToInt( ResultEntry::getAmount )
                .findFirst().orElse( 0 );
    }


    /**
     * Prints the Results.
     * @param stream to print in.
     */
    public void print( PrintStream stream ) {
        List<Gesture> present = new ArrayList<>( getPresentGestures() );

        //Print first matrix:
        stream.print("      ");
        for( Gesture gesture : present ){
            stream.print("  |   ");
            stream.print(gesture);
        }

        stream.println();

        for( Gesture wanted : present ){
            stream.print( wanted );

            for( Gesture got : present ){
                stream.print( " |   " );
                stream.print( getEntry( wanted, got ) );
            }

            stream.println();
        }
    }


    /**
     * Gets the total amount of tested Data.
     * @return the amount of tested data.
     */
    public int getWantedEntries(){
        return stream( results )
            .mapToInt( ResultEntry::getAmount )
            .sum();
    }


    /**
     * Gets the total amount of correct classified test-samples.
     * @param gesture to get for.
     * @return the amount of correct classified samples.
     */
    public int getCorrectClassified(Gesture gesture) {
        return stream( results )
                .filter( r -> r.is( gesture, gesture ) )
                .mapToInt( ResultEntry::getAmount )
                .sum();
    }

    /**
     * Gets the total amount of wrong classified test-samples.
     * @param gesture to get for.
     * @return the amount of wrong classified samples.
     */
    public int getWrongClassified(Gesture gesture){
        return stream( results )
                .filter( r -> r.getWanted() == gesture)
                .filter( ResultEntry::isIncorrect )
                .mapToInt( ResultEntry::getAmount )
                .sum();
    }

    /**
     * Gets the total amount of classified Samples.
     * @return the amount.
     */
    public int getCorrectClassifiedTotal() {
        return stream( getPresentGestures() )
                .mapToInt( this::getCorrectClassified )
                .sum();
    }

    /**
     * Gets the total amount of wrong classified Samples.
     * @return the amount of wrong classified samples.
     */
    public int getWrongClassifiedTotal() {
        return stream( getPresentGestures() )
                .mapToInt( this::getWrongClassified )
                .sum();
    }

    /**
     * Gets the Accuracy of the Test data.
     * The value is between 0 and 1.
     *
     * @return the accuracy.
     */
    public double getAccuracy() {
        return (double) getCorrectClassifiedTotal() / (double) getWantedEntries();
    }

    /**
     * Gets the error of the Data.
     *
     * @return the error ( 1 - accuracy).
     */
    public double getError(){
        return 1d - getAccuracy();
    }

    /**
     * Gets the average Precision of the Test data.
     * The value is between 0 and 1.
     *
     * @return the accuracy.
     */
    public double getAveragePrecision() {
        return stream( getPresentGestures() )
                .mapToDouble( this::getPrecision )
                .average()
                .orElse( 0d );
    }

    /**
     * Gets the precision of the Test data for the Gesture..
     * @param gesture to get for.
     * @return the precision of  the Gesture.
     */
    public double getPrecision( Gesture gesture ){
        return (double) getCorrectClassified( gesture ) / (double) getWantedEntries( gesture );
    }

    /**
     * Gets the Recall of the Gesture.
     *
     * @param gesture to check
     * @return the recall.
     */
    public double getRecall( Gesture gesture ){
        return (double) getCorrectClassified( gesture ) / (double) getGotEntries( gesture );
    }


    /**
     * Gets the Average Recall for the Results.
     *
     * @return the average Recall.
     */
    public double getAverageRecall(){
        return stream( getPresentGestures() )
                .mapToDouble( this::getRecall )
                .average().orElse( 0d );
    }


    /**
     * Gets a Map for the Precision: Gesture -> Precision.
     */
    public Map<Gesture,Double> getPrecisionPerGesture(){
        Map<Gesture,Double> precisionMap = new HashMap<>();
        for( Gesture gesture : getPresentGestures() ){
            precisionMap.put( gesture, getPrecision( gesture ) );
        }

        return precisionMap;
    }

    /**
     * Merges the Results of the Other results to this one.
     *
     * @param other to add.
     */
    public void mergeResults( EvaluationResults other ) {
        for( ResultEntry otherEntry : other.results ){
            stream( new HashSet<>( results ) )
                .filter( r -> r.is( otherEntry.getWanted(), otherEntry.getGot() ) )
                .findFirst()
                .ifPresentOrElse(
                    s -> s.add( otherEntry.getAmount() ),
                    () -> results.add( otherEntry.clone() )
                );
        }
    }


    private class ResultEntry implements Cloneable{
        private final Gesture wanted;
        private final Gesture got;

        private int amount = 0;

        ResultEntry( Gesture wanted, Gesture got ){
            this.wanted = wanted;
            this.got = got;
        }

        ResultEntry( Gesture wanted, Gesture got, int amount ){
            this( wanted, got );

            this.amount = amount;
        }

        Gesture getWanted() {
            return wanted;
        }

        Gesture getGot() {
            return got;
        }

        int getAmount() {
            return amount;
        }

        int add1(){
            return amount++;
        }

        int add( int amount ) {
            return this.amount += amount;
        }

        public boolean is( Gesture wanted, Gesture got ){
            return this.wanted == wanted && this.got == got;
        }

        boolean isCorrect(){
            return wanted == got;
        }

        boolean isIncorrect(){
            return !isCorrect();
        }

        @Override
        public ResultEntry clone(){
            return new ResultEntry( wanted, got, amount );
        }

        @Override
        public int hashCode() {
            return (wanted.toString() + got.toString()).hashCode();
        }

        @Override
        public boolean equals( Object object ) {
            if( object instanceof ResultEntry ){
                ResultEntry other = (ResultEntry) object;
                return other.wanted == wanted && other.got == got;
            }

            return super.equals( object );
        }

        @Override
        public String toString() {
            return getWanted().getName() + " -> " + getGot().getName() + " = " + amount;
        }
    }
}
