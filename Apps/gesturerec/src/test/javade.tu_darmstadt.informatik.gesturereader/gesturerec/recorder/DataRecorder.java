package de.tu_darmstadt.informatik.gesturereader.gesturerec.recorder;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import java8.util.function.Consumer;

/**
 * Created by Toby on 09.10.2017
 */

public class DataRecorder {

    /**
     * The Executor to use for Scheduling.
     */
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    /**
     * The Session to observe.
     */
    private final GestureRecSession linkedSession;


    /**
     * Creates a Data Recorder.
     * @param session to use.
     */
    public DataRecorder( GestureRecSession session ){
        this.linkedSession = session;
    }


    /**
     * Gets the Data from start to end.
     * This does NOT look into the Future!!!
     *
     * @param start from when to get the data.
     * @param end to when to get the data.
     *
     * @return the collected data in the interval.
     */
    public SensorDataBatch getDataNow( DateTime start, DateTime end ){
        return linkedSession.getContextRecSession().collectStreamingData( start, end ).orElse( null );
    }



    /**
     * This starts collecting data for the duration specified.
     *
     * @param start when to start.
     * @param end when to end
     * @param waitTimeAfter the time to wait after to be sure all data is there.
     * @param consumer to call when done.
     * @param handler to use for the callback
     */
    public void startCollectingData( DateTime start, DateTime end, Duration waitTimeAfter, Consumer<SensorDataBatch> consumer, Handler handler ) {
        //No consumer -> No need to collect!
        if( consumer == null ) return;

        Duration waitingTime = new Duration( DateTime.now(), end ).plus( waitTimeAfter );
        final Handler finalHandler = handler == null ? new Handler( Looper.getMainLooper() ) : handler;

        executor.schedule(
                () -> {
                    SensorDataBatch batch = getDataNow( start, end );
                    finalHandler.post( () -> consumer.accept( batch ) );
                },
                waitingTime.getMillis(),
                TimeUnit.MILLISECONDS
        );
    }


    /**
     * This starts collecting data for the duration specified.
     *
     * @param collectingDuration the duration to collect
     * @param waitTimeAfter the time to wait after to be sure all data is there.
     * @param consumer to call when done.
     * @param handler to use for the callback
     */
    public void startCollectingData( Duration collectingDuration, Duration waitTimeAfter, Consumer<SensorDataBatch> consumer, Handler handler ) {
        DateTime start = DateTime.now();
        DateTime end = start.plus( collectingDuration );
        startCollectingData( start, end, waitTimeAfter, consumer, handler );
    }


    /**
     * This starts collecting data for the duration specified.
     * Callback is on Main Thread!
     *
     * @param collectingDuration the duration to collect
     * @param waitTimeAfter the time to wait after to be sure all data is there.
     * @param consumer to call when done.
     */
    public void startCollectingData( Duration collectingDuration, Duration waitTimeAfter, Consumer<SensorDataBatch> consumer ) {
        this.startCollectingData( collectingDuration, waitTimeAfter, consumer, new Handler( Looper.getMainLooper() ));
    }


    /**
     * This starts collecting data for the duration specified.
     *
     * @param start when to start.
     * @param end when to end
     * @param waitTimeAfter the time to wait after to be sure all data is there.
     * @param consumer to call when done.
     * @param handler to use for the callback
     */
    public void startCollectingDataAndSaveAsSample(DateTime start, DateTime end, Duration waitTimeAfter, @Nullable Consumer<SampleProxy> consumer, Gesture gesture, Handler handler ) {
        Duration waitingTime = new Duration( DateTime.now(), end ).plus( waitTimeAfter );
        final Handler finalHandler = handler == null ? new Handler( Looper.getMainLooper() ) : handler;

        executor.schedule(
                () -> {
                    SensorDataBatch batch = getDataNow( start, end );
                    finalHandler.post( () -> {
                        SampleProxy proxy = linkedSession.getSampleManager().addSampleCurrentUser( gesture, start, batch );
                        if( consumer != null ) consumer.accept( proxy );
                    } );
                },
                waitingTime.getMillis(),
                TimeUnit.MILLISECONDS
        );
    }


    /**
     * This starts collecting data for the duration specified.
     *
     * @param collectingDuration the duration to collect
     * @param waitTimeAfter the time to wait after to be sure all data is there.
     * @param consumer to call when done.
     * @param handler to use for the callback
     */
    public void startCollectingDataAndSaveAsSample( Duration collectingDuration, Duration waitTimeAfter, @Nullable Consumer<SampleProxy> consumer, Gesture gesture, Handler handler ) {
        DateTime start = DateTime.now();
        DateTime end = start.plus( collectingDuration );
        startCollectingDataAndSaveAsSample( start, end, waitTimeAfter, consumer, gesture, handler );
    }


    /**
     * This starts collecting data for the duration specified.
     * Callback is on Main Thread!
     *
     * @param collectingDuration the duration to collect
     * @param waitTimeAfter the time to wait after to be sure all data is there.
     * @param consumer to call when done.
     * @param gesture to save as.
     */
    public void startCollectingDataAndSaveAsSample( Duration collectingDuration, Duration waitTimeAfter, @Nullable Consumer<SampleProxy> consumer, Gesture gesture ) {
        this.startCollectingDataAndSaveAsSample( collectingDuration, waitTimeAfter, consumer, gesture, new Handler( Looper.getMainLooper() ));
    }


}
