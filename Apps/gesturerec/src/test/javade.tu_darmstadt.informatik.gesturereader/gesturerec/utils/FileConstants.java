package de.tu_darmstadt.informatik.gesturereader.gesturerec.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by Toby on 12.12.2017
 */

public class FileConstants {


    private static File GESTURE_REC_BASE_FILE = new File( Environment.getExternalStorageDirectory(), "GestureRec" );
    private static final String GESTURE_DATA_PATH = "GestureData";
    private static final String GESTURE_MODEL_PATH = "GestureModel";


    /**
     * Gets the Base Gesture Folder.
     * @return folder.
     */
    public static File getBaseGestureFolder(){
        return new File( GESTURE_REC_BASE_FILE, GESTURE_DATA_PATH );
    }


    /**
     * Gets the Base Model Folder.
     * @return folder.
     */
    public static File getBaseModelFolder(){
        return new File( GESTURE_REC_BASE_FILE, GESTURE_MODEL_PATH );
    }


    /**
     * Changes the Base Folder to the desired path.
     * PLEASE CHANGE THIS BEFORE CREATING ANY GESTURE_REC SESSIONS!
     *
     * @param baseFolder to set.
     */
    public static void changeBaseFolder( File baseFolder ){
        if( baseFolder == null ) return;
        GESTURE_REC_BASE_FILE = baseFolder;
    }

}
