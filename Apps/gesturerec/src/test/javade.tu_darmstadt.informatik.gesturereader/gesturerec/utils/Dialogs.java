package de.tu_darmstadt.informatik.gesturereader.gesturerec.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import java8.util.function.BiConsumer;
import java8.util.function.Consumer;
import java8.util.stream.Collectors;
import java8.util.stream.IntStreams;
import java8.util.stream.RefStreams;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 26.10.2017
 */

public class Dialogs {


    /**
     * A linked session.
     */
    private final GestureRecSession session;


    /**
     * Creates a new Dialog Creator.
     * @param session to use.
     */
    public Dialogs( GestureRecSession session ) {
        this.session = session;
    }

    /**
     * opens a Multi-Selection Dialog with all Selection options for Gestures.
     * @param context to use for showing.
     * @param preselected to pre select.
     * @param consumer to call when positive button is pressed.
     */
    public void OpenGestureMultiSelectionDialog( Context context, Collection<Gesture> preselected, Consumer<List<Gesture>> consumer ){
        if( preselected == null ) preselected = new HashSet<>();

        final Collection<String> preselectedString = stream( preselected ).map( Gesture::getName ).collect( Collectors.toSet() );
        final String[] gestures = stream( session.getGestureManager().getAllWithoutUnknownAsString() )
                .sorted()
                .toArray(String[]::new);

        final boolean[] checked = new boolean[gestures.length];
        IntStreams.range( 0, gestures.length ).forEach( i -> checked[i] = preselectedString.contains( gestures[i] ) );

        GestureManager gestureManager = session.getGestureManager();
        DialogInterface.OnMultiChoiceClickListener listener = (d, s, b) -> checked[s] = b;
        new AlertDialog.Builder( context )
                .setTitle("Select Gestures:")
                .setMultiChoiceItems(gestures, checked, listener)
                .setPositiveButton("Apply", (s, _u) -> {
                    List<Gesture> used = new ArrayList<>();
                    for (int i = 0; i < checked.length; i++) {
                        if ( checked[i] ) {
                            used.add( gestureManager.get( gestures[i] ) );
                        }
                    }

                    s.dismiss();
                    consumer.accept( used );
                })
                .setNegativeButton( "Cancel", (v,a) -> v.dismiss() )
                .create().show();
    }

    /**
     * opens a Multi-Selection Dialog with all Selection options for Gestures.
     * @param context to use for showing.
     * @param consumer to call when positive button is pressed.
     */
    public void OpenGestureMultiSelectionDialog( Context context, Consumer<List<Gesture>> consumer ){
        OpenGestureMultiSelectionDialog( context, null, consumer );
    }



    /**
     * opens a Multi-Selection Dialog with all Selection options for Gestures.
     * @param context to use for showing.
     * @param consumer to call when both positive button is pressed.
     */
    public void OpenGestureMultiSelectionAndUsersDialog( Context context, BiConsumer<List<Gesture>,List<String>> consumer ){
        OpenGestureMultiSelectionDialog(
                context,
                gestures -> OpenStringMultiSelectionDialog(
                        context,
                        "Users",
                        session.getUserManager(). readAllPresentUsersOrDefault(),
                        users -> consumer.accept( gestures, users )
                )
        );
    }



    /**
     * opens a Multi-Selection Dialog with all Selection options for Gestures.
     * @param context to use for showing.
     * @param values the values to display.
     * @param preselected the values that are preselected.
     * @param consumer the callback to call when positive button is pressed.
     */
    public void OpenStringMultiSelectionDialog( Context context, String nameToDisplay, String[] values, Collection<String> preselected, Consumer<List<String>> consumer ){
        final Collection<String> finalPreselected = preselected == null ? new HashSet<>() : preselected;
        final boolean[] checked = new boolean[values.length];
        IntStreams.range( 0, values.length ).forEach( i -> checked[i] = finalPreselected.contains( values[i] ) );


        DialogInterface.OnMultiChoiceClickListener listener = (d, s, b) -> checked[s] = b;
        new AlertDialog.Builder( context )
                .setTitle("Select " + nameToDisplay + ":")
                .setMultiChoiceItems(values, checked, listener)
                .setPositiveButton("Apply", (s, _u) -> {
                    List<String> used = new ArrayList<>();
                    for (int i = 0; i < checked.length; i++) {
                        if( checked[i] ) used.add(values[i]);
                    }

                    s.dismiss();
                    consumer.accept( used );
                })
                .setNegativeButton( "Cancel", (v,a) -> v.dismiss() )
                .create().show();
    }


    /**
     * opens a Multi-Selection Dialog with all Selection options for Gestures.
     * @param context to use for showing.
     * @param values the values to display.
     * @param consumer the callback to call when positive button is pressed.
     */
    public void OpenStringMultiSelectionDialog( Context context, String nameToDisplay, String[] values, Consumer<List<String>> consumer ){
        OpenStringMultiSelectionDialog( context, nameToDisplay, values, null, consumer);
    }

    /**
     * opens a Multi-Selection Dialog with all Selection options for Gestures.
     * @param context to use for showing.
     * @param consumer to call when positive button is pressed.
     */
    public void OpenGestureMultiSelectionDialogWithSampleAmount( Context context, Consumer<List<Gesture>> consumer ){
        final String[] gestures = stream( session.getGestureManager().getAllWithoutUnknownAsString() )
                .sorted().toArray(String[]::new);
        String[] adapted = RefStreams.of( gestures ).map( g -> g + " " + getAmountSamples( g ) ).toArray( String[]::new );

        final boolean[] checked = new boolean[gestures.length];

        DialogInterface.OnMultiChoiceClickListener listener = (d, s, b) -> checked[s] = b;
        new AlertDialog.Builder( context )
                .setTitle("Select Gestures:")
                .setMultiChoiceItems( adapted, checked, listener )
                .setPositiveButton("Apply", (s, _u) -> {
                    List<Gesture> used = new ArrayList<>();
                    for (int i = 0; i < checked.length; i++) {
                        if (checked[i]) {
                            used.add( session.getGestureManager().get( gestures[i] ) );
                        }
                    }

                    s.dismiss();
                    consumer.accept( used );
                })
                .setNegativeButton( "Cancel", (v,a) -> v.dismiss() )
                .create().show();
    }


    /**
     * Posts the amount of samples.
     * @param gestureName to use.
     */
    private int getAmountSamples( String gestureName){
        Gesture gesture = session.getGestureManager().get( gestureName );
        File folder = gesture.getSaveDirectory();
        String[] list = folder.list();

        return list == null
            ? 0
            : (int) RefStreams.of( list )
                .filter( n -> n.endsWith( ".batch" ) )
                .count();
    }


    /**
     * opens a Single-Selection Dialog with all Selection options for Gestures.
     * @param context to use for showing.
     * @param consumer to call when positive button is pressed.
     */
    public void OpenGestureSingleSelectionDialog( Context context, Consumer<Gesture> consumer ){
        final String[] gestures = stream( session.getGestureManager().getAllWithoutUnknownAsString() )
                .sorted()
                .toArray(String[]::new);

        DialogInterface.OnClickListener listener = (d, s) -> {
            consumer.accept( session.getGestureManager().get( gestures[s] ) );
            d.dismiss();
        };

        new AlertDialog.Builder( context )
                .setTitle("Select Gestures:")
                .setItems( gestures, listener )
                .setNegativeButton( "Cancel", (v,a) -> v.dismiss() )
                .create().show();
    }

    /**
     * opens a Single-Selection Dialog with all Selection options for Gestures.
     * @param context to use for showing.
     * @param elements to show
     * @param consumer to call when positive button is pressed.
     */
    public void OpenStringSingleSelectionDialog( Context context, String[] elements, Consumer<String> consumer ){
        new AlertDialog.Builder( context )
                .setTitle("Select Item:")
                .setItems( elements, (v,i) -> consumer.accept( elements[i] ) )
                .setNegativeButton( "Cancel", (v,a) -> v.dismiss() )
                .create().show();
    }




}
