package de.tu_darmstadt.informatik.gesturereader.gesturerec.linker;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.kom.contextrec.phone.ContextRecSession;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.StreamingDataSource;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 25.09.2017
 */

public class RestartSourceDialog extends Dialog {

    private ArrayAdapter<String> adapter;


    public RestartSourceDialog(@NonNull Context context) {
        super(context);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.restart_sources_dialog );

        ContextRecSession session = MainApplication.fromContext( this.getContext() ).getSession().getContextRecSession();

        adapter = new ArrayAdapter<>( this.getContext(), android.R.layout.simple_list_item_1,
            stream( session.getDataSources() )
                .map( DataSource::getDeviceName )
                .sorted()
                .toArray( String[]::new )
        );

        ListView list = findViewById(R.id.list);
        list.setAdapter( adapter );
        list.setOnItemClickListener( this::selected );

        findViewById( R.id.exit ).setOnClickListener( this::back );
    }

    private void selected(AdapterView<?> adapterView, View view, int index, long l) {
        Optional<DataSource> source = stream( MainApplication.fromContext( this.getContext() ).getSession().getContextRecSession().getDataSources() )
                .filter( s -> s.getDeviceName().equalsIgnoreCase( adapter.getItem( index ) ) )
                .findFirst();

        source.map( StreamingDataSource.class::cast ).ifPresent( s -> { s.stopStreaming(); s.startStreaming(); } );
        Toast.makeText( this.getContext(), source.isPresent() ? ("Source: " + source.get().getDeviceName() + " Restartet.") : "FAIL!", Toast.LENGTH_LONG ).show();
    }


    public void back(View unused){
        this.dismiss();
    }
}
