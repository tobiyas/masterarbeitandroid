package de.tu_darmstadt.informatik.gesturereader.gesturerec.linker;

import android.content.Context;

import org.apache.commons.io.FileUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import de.tu_darmstadt.informatik.kom.contextrec.phone.ContextRecSession;
import de.tu_darmstadt.informatik.kom.contextrec.phone.SourcesChangedListener;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import java8.util.Optional;
import java8.util.stream.RefStreams;

import static java8.util.stream.StreamSupport.stream;

/**
 * This class links the DataSource to the Limb.
 *
 * Created by Toby on 28.08.2017
 */

public class SourceLinker implements SourcesChangedListener {

    /**
     * The singleton instance.
     */
    private static SourceLinker singleton;

    /**
     * The Session to use.
     */
    private final ContextRecSession session;

    /**
     * The Context to use.
     */
    private final Context context;

    /**
     * The Source to limb map.
     */
    private final Map<String,Limb> sourceLimbMap = new HashMap<>();



    public SourceLinker( Context context, ContextRecSession session ){
        this.context = context;
        this.session = session;

        this.session.registerSourcesChangedListener( this );

        readFromFile();
        linkCurrent();
    }

    /**
     * Reads the Limbs from the Save-File.
     */
    private void readFromFile(){
        File file = getFile();
        sourceLimbMap.clear();

        //Preload all Sources to NOT_DEFINED.
        stream( session.getDataSources() ).forEach( s -> sourceLimbMap.put( s.getDeviceName(), Limb.NOT_DEFINED) );

        if(file.exists()){
            try {
                List<String> lines = FileUtils.readLines( file, Charset.defaultCharset() );
                for(String line : lines){
                    String[] split = line.split(Pattern.quote("="));
                    if(split.length != 2) continue;

                    String deviceName = split[0];
                    String limbName = split[1];
                    Limb = Limb.fromName(limbName);

                    sourceLimbMap.put( deviceName, limb );
                }
            }catch (Throwable exp){
                exp.printStackTrace();
            }
        }
    }



    /**
     * Get the file to save to.
     */
    private File getFile(){
        return new File(context.getFilesDir(), "limb_assoc");
    }


    /**
     * Saves the Limbs from the Save-File.
     */
    private void saveToFile(){
        File file = getFile();

        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))){
            for(Map.Entry<String,Limb> entry : this.sourceLimbMap.entrySet()){
                writer.append(entry.getKey())
                        .append("=")
                        .append(entry.getValue().getName());
                writer.newLine();
            }

            writer.flush();
        }catch (Throwable exp){
            exp.printStackTrace();
        }
    }


    /**
     * Links the current settings.
     */
    public void linkCurrent(){
        for( DataSource source : session.getDataSources() ){
            Limb = sourceLimbMap.get(source.getDeviceName());
            source.changeName( limb == null ? Limb.NOT_DEFINED.getName() : limb.getName() );
        }
    }


    @Override
    public void sourceAdded(DataSource source) {
        String device = source.getDeviceName();
        if( !this.sourceLimbMap.containsKey( device ) ) this.sourceLimbMap.put( device, Limb.NOT_DEFINED );
        else source.changeName( sourceLimbMap.get( device ).getName() );
    }


    @Override
    public void sourceRemoved(DataSource source) {
        //Not needed!
    }


    /**
     * Returns the first found datasource for the Limb.
     * @param limb to use.
     * @return the first found datasource.
     */
    public DataSource getFirst(Limb) {
        Optional<String> name = stream( this.sourceLimbMap.entrySet() )
                .filter( e -> e.getValue() == limb )
                .findFirst()
                .map( Map.Entry::getKey );

        if( !name.isPresent() ) return null;
        return stream( this.session.getDataSources() )
                .filter( s -> s.getDeviceName().equals( name.get() ) )
                .findFirst()
                .orElse( null );
    }


    /**
     * Changes the DataSource to a specific limb.
     * @param dataSource to use.
     * @param limb to set.
     */
    public void change(String dataSource, Limb) {
        //Remove all Items with that limb.
        for(Map.Entry<String,Limb> entry : sourceLimbMap.entrySet()){
            if(entry.getValue() == limb) sourceLimbMap.put(entry.getKey(), Limb.NOT_DEFINED);
        }

        this.sourceLimbMap.put( dataSource, limb );
        saveToFile();
        linkCurrent();
    }


    public static class Limb{

        public static final Limb LEFT_HAND = new Limb("LeftHand");
        public static final Limb RIGHT_HAND = new Limb("RightHand");
        public static final Limb LEFT_FOOT = new Limb("LeftFoot");
        public static final Limb RIGHT_FOOT = new Limb("RightFoot");

        public static final Limb NOT_DEFINED = new Limb("NotDefined");

        public static final Limb[] ALL_LIMBS = new Limb[]{LEFT_HAND,RIGHT_HAND,LEFT_FOOT,RIGHT_FOOT};


        /**
         * The Name to use.
         */
        private final String name;


        /**
         * Creates a new Limb with the name.
         * @param name to use.
         */
        private Limb(String name){
            this.name = name;
        }

        /**
         * Gets the name of the Limb
         */
        public String getName() {
            return name;
        }


        /**
         * Creates a limb from the name.
         * This is only Cache-Retrieval.
         *
         * @param name to use.
         * @return the found limb.
         */
        public static Limb fromName(String name){
            return RefStreams.of( ALL_LIMBS )
                .filter( l -> l.getName().equals(name) )
                .findFirst()
                .orElse(NOT_DEFINED);
        }

        @Override
        public String toString() {
            return getName();
        }

        @Override
        public int hashCode() {
            return getName().hashCode();
        }
    }

    /**
     * Creates a new linker.
     * @param context to use.
     * @param session to use.
     * @return the singleton.
     */
    public static SourceLinker create( Context context, ContextRecSession session ){
        return singleton == null ? singleton = new SourceLinker( context, session ) : singleton;
    }


}
