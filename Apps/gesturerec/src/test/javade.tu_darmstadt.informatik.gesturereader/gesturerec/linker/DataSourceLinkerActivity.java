package de.tu_darmstadt.informatik.gesturereader.gesturerec.linker;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Locale;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.kom.contextrec.phone.ContextRecSession;
import de.tu_darmstadt.informatik.kom.contextrec.phone.PhoneDataSources;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.StreamingDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.Vibrateable;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 29.08.2017
 */

public class DataSourceLinkerActivity extends AppCompatActivity {

    /**
     * The Threshold of error to when show movement.
     */
    private static final double MOVEMENT_DETECTION_THRESHOLD = 3;

    /**
     * The amount of seconds to search for Frequency.
     */
    private static final double FREQ_READ_TIME_IN_SEC = 3;


    /**
     * The Linker to use.
     */
    private SourceLinker linker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linker);

        this.linker = MainApplication.fromContext( this ).getSession().getLinker();

        //Set click listener:
        findViewById(R.id.left_arm).setOnClickListener( v -> clicked(SourceLinker.Limb.LEFT_HAND) );
        findViewById(R.id.right_arm).setOnClickListener( v -> clicked(SourceLinker.Limb.RIGHT_HAND) );
        findViewById(R.id.left_foot).setOnClickListener( v -> clicked(SourceLinker.Limb.LEFT_FOOT) );
        findViewById(R.id.right_foot).setOnClickListener( v -> clicked(SourceLinker.Limb.RIGHT_FOOT) );

        //Set long press listener
        findViewById(R.id.left_arm).setOnLongClickListener( v -> longClicked(SourceLinker.Limb.LEFT_HAND) );
        findViewById(R.id.right_arm).setOnLongClickListener( v -> longClicked(SourceLinker.Limb.RIGHT_HAND) );
        findViewById(R.id.left_foot).setOnLongClickListener( v -> longClicked(SourceLinker.Limb.LEFT_FOOT) );
        findViewById(R.id.right_foot).setOnLongClickListener( v -> longClicked(SourceLinker.Limb.RIGHT_FOOT) );

        //Schedule Movement Detection:
        findViewById(R.id.left_arm).postDelayed( this::movementDetection ,500 );

        //Other:
        findViewById( R.id.restart_dialog ).setOnLongClickListener( this::openVibrateView );
        findViewById( R.id.calibrate ).setOnLongClickListener( this::rescanWear );
    }

    private boolean rescanWear(View view) {
        Toast.makeText( this, "Rescaning Wear...", Toast.LENGTH_LONG ).show();

        ContextRecSession session = MainApplication.fromContext( this ).getSession().getContextRecSession();
        PhoneDataSources.wearStreamingConnectors( this, s -> {
            List<DataSource> present = session.getDataSources();

            for( DataSource other : s ){
                boolean found = false;
                for( DataSource source : present ){
                    if( source.getDeviceName().equalsIgnoreCase( other.getDeviceName() ) ){
                        found = true;
                        break;
                    }
                }

                if( !found ) session.addDataSource( other );
            }

        } );
        return true;
    }

    private void movementDetection() {
        movementDetection(SourceLinker.Limb.LEFT_HAND, R.id.left_arm);
        movementDetection(SourceLinker.Limb.RIGHT_HAND, R.id.right_arm);
        movementDetection(SourceLinker.Limb.LEFT_FOOT, R.id.left_foot);
        movementDetection(SourceLinker.Limb.RIGHT_FOOT, R.id.right_foot);

        findViewById(R.id.right_foot).postDelayed( this::movementDetection, 500 );
    }


    private void movementDetection(SourceLinker.Limb, int id){
        DataSource source = linker.getFirst(limb);
        View view = findViewById(id);
        if(source != null){
            double motion = getMotionMSE( source );
            view.setBackgroundColor(
                    motion <= 0 ?
                            Color.BLUE :
                            ( motion > MOVEMENT_DETECTION_THRESHOLD ?
                                    Color.RED :
                                    Color.GREEN
                            )
            );
        }else{
            view.setBackgroundColor( Color.WHITE );
        }
    }

    private void clicked(SourceLinker.Limb){
        new DataSourceSelectDialog( this )
            .set( linker.getFirst(limb), limb )
            .setCallback( this::gotChange )
            .show();
    }

    private boolean longClicked(SourceLinker.Limb){
        DataSource source = linker.getFirst( limb );
        String name = source == null ? "NONE" : source.getDeviceName();

        double accelFreq = getAccelFreq( source );
        Toast.makeText( this, String.format(Locale.getDefault(), "Device: '%s' Freq: '%.1f'", name, accelFreq ), Toast.LENGTH_LONG).show();
        return true;
    }


    public boolean openVibrateView( View unused ){
        final String[] vibrateable = stream( MainApplication.fromContext( this ).getSession().getContextRecSession().getDataSources() )
                .filter( s -> s instanceof Vibrateable)
                .map( DataSource::getDeviceName )
                .toArray( String[]::new );

        if( vibrateable.length <= 0 ) {
            Toast.makeText( this, "No vibrateable Sources", Toast.LENGTH_LONG ).show();
            return true;
        }

        //Show dialog.
        new AlertDialog.Builder( this )
            .setItems( vibrateable, (d,w) -> vibrateSource( vibrateable[w] ) )
            .create()
            .show();

        return true;
    }

    private void vibrateSource( String sourceName ){
        Log.d( "Linker", "Vibrating: " + sourceName );

        Optional<Vibrateable> source = stream( MainApplication.fromContext( this ).getSession().getContextRecSession().getDataSources() )
                .filter( s -> s.getDeviceName().equals( sourceName ) )
                .filter( s -> s instanceof Vibrateable )
                .map( Vibrateable.class::cast )
                .findFirst();

        //Vibrate or die!
        source.ifPresentOrElse(
                Vibrateable::vibrate,
                () -> Toast.makeText( this, "Can not vibrate.", Toast.LENGTH_LONG ).show()
        );
    }

    private double getAccelFreq( DataSource source ){
        if( source == null ) return -2;

        if( source instanceof StreamingDataSource ){
            StreamingDataSource sSource = (StreamingDataSource) source;
            Optional<SensorDataBatch> batch = sSource.collectStreamedData( new DateTime().minusMillis( (int)FREQ_READ_TIME_IN_SEC * 1000 + 200 ), new DateTime().minus(200) );
            if(!batch.isPresent()) return 0;

            Optional<AccelerationData> data = batch.get().getData( AccelerationData.class );
            if(!batch.isPresent()) return -1;

            return (double)data.get().getT().length / FREQ_READ_TIME_IN_SEC;
        }

        return 0;
    }

    private void gotChange(String dataSource, SourceLinker.Limb) {
        linker.change( dataSource, limb);
    }

    public void startCalibration(View unused){
        MainApplication.fromContext( this ).getSession().getContextRecSession().doCalibration();
        Toast.makeText( this, "Starting Calibration", Toast.LENGTH_LONG ).show();
    }


    /**
     * Open a view that can restart a streaming device!
     */
    public void restart_gui(View unused){
        new RestartSourceDialog( this ).show();
    }


    /**
     * Gets the Motion of the Source linked.
     *
     * @return Value :
     *      < 0 if the source does not seem to have a motion element.
     *      > 0 if the source has a motion component and it can be evaluated.
     */
    private double getMotionMSE(DataSource source){
        if(! ( source instanceof StreamingDataSource ) ) return -1;

        StreamingDataSource streamingDataSource = (StreamingDataSource) source;
        Optional<SensorDataBatch> oBatch = streamingDataSource.collectStreamedData( DateTime.now().minusMillis(500), DateTime.now() );
        if(!oBatch.isPresent()) return -1;

        SensorDataBatch batch = oBatch.get();
        Optional<AccelerationData> oData = batch.getData(AccelerationData.class);
        if(!oData.isPresent()) return -1;

        AccelerationData data = oData.get();
        double mse = data.getMeanSquareError();

        //mse with less values are pretty bad, so we need to up them a bit.
        double values = data.getT().length;
        if(values <= 10) mse *= 20 - values;

        return mse;
    }


}
