package de.tu_darmstadt.informatik.gesturereader.gesturerec.linker;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.kom.contextrec.phone.ContextRecSession;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import java8.util.function.BiConsumer;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 29.08.2017
 */
public class DataSourceSelectDialog extends Dialog {

    /**
     * The consumer to call when done.
     */
    private BiConsumer<String,SourceLinker.Limb> consumer;

    /**
     * The simple Adapter to use.
     */
    private ArrayAdapter<String> adapter;

    /**
     * The limb to change.
     */
    private SourceLinker.Limb;

    /**
     * The current selection.
     */
    private String selected;


    public DataSourceSelectDialog(@NonNull Context context) {
        super(context);
    }


    public DataSourceSelectDialog setCallback(BiConsumer<String,SourceLinker.Limb> callback ){
        this.consumer = callback;
        return this;
    }

    public DataSourceSelectDialog set( @Nullable  DataSource source,  SourceLinker.Limb ){
        if(source != null && adapter != null){
            ListView list = findViewById(R.id.list);
            if(list != null) {
                for( int i = 0; i < adapter.getCount(); i++ ){
                    if(Objects.equals(adapter.getItem(i), source.getDeviceName())){
                        list.setSelection( i );
                        selected = source.getDeviceName();
                    }
                }
            }
        }

        TextView view = findViewById(de.tu_darmstadt.informatik.gesturereader.R.id.text);
        if(view != null) view.setText( limb.getName() );

        this.limb = limb;
        return this;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_link);

        ContextRecSession session = MainApplication.fromContext( getContext() ).getSession().getContextRecSession();

        List<String> array = stream( session.getDataSources() )
                .map( DataSource::getDeviceName )
                .sorted()
                .collect( Collectors.toList() );
        adapter = new ArrayAdapter<>( getContext(), android.R.layout.simple_list_item_single_choice, array );

        ListView view = findViewById(R.id.list);
        view.setAdapter(adapter);
        view.setOnItemClickListener(this::itemClicked);

        findViewById(R.id.cancel).setOnClickListener(this::dismiss);
        findViewById(R.id.apply).setOnClickListener(this::apply);
    }

    private void itemClicked(AdapterView<?> adapterView, View view, int index, long l) {
        this.selected = adapter.getItem(index);
    }


    private void apply(View view) {
        if(selected != null && !selected.isEmpty() && consumer != null) consumer.accept(selected, limb);
        this.dismiss();
    }


    private void dismiss(View view) {
        this.dismiss();
    }
}
