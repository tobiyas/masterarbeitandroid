package de.tu_darmstadt.informatik.gesturereader.gesturerec.models.creator;

import java.util.Collection;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetectorBuilder;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelManager;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleFilterStream;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleManager;

import static de.tu_darmstadt.informatik.gesturereader.gesturerec.features.FeatureFactory.buildFeatures;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 09.12.2017
 */

public class SimpleModelCreator {


    /**
     * The GestureManager to use.
     */
    private final GestureManager gestureManager;

    /**
     * The SampleManager to use.
     */
    private final SampleManager sampleManager;

    /**
     * The ModelManager used.
     */
    private final ModelManager modelManager;


    public SimpleModelCreator( GestureManager gestureManager,
                               SampleManager sampleManager,
                               ModelManager modelManager ) {

        this.gestureManager = gestureManager;
        this.sampleManager = sampleManager;
        this.modelManager = modelManager;
    }


    /**
     * Creates a simple new Model with the passed Type + Gestures.
     * All User data will be taken for the creation.
     *
     * @param name to use.
     * @param type to create.
     * @param gestures to use.
     *
     * @return the created proxy or throws exception if failed.
     *
     * @throws IllegalArgumentException if arguments are invalid.
     * @throws RuntimeException if Creation itself failed.
     */
    public ModelProxy createNewModel( String name,
                                      WekaBaseClassifier type,
                                      Collection<Gesture> gestures ){

        if( gestures == null || gestures.isEmpty() ) throw new IllegalArgumentException( "Gestures is empty" );

        //Get samples and cover them:
        SampleBatch samples = sampleManager.query()
                .addGestures( gestures )
                .applyFilter();

        return createNewModel( name, type, samples );
    }


    /**
     * Creates a simple new Model with the passed Type + Batch.
     *
     * @param name to use.
     * @param type to create.
     * @param batch to use.
     *
     * @return the created proxy or throws exception if failed.
     *
     * @throws IllegalArgumentException if arguments are invalid.
     * @throws RuntimeException if Creation itself failed.
     */
    public ModelProxy createNewModel( String name,
                                      WekaBaseClassifier type,
                                      SampleBatch batch ){

        if( modelManager.presentForName( name ) ) throw new IllegalArgumentException( "Name already taken" );
        if( batch == null || batch.getGestures().isEmpty() ) throw new IllegalArgumentException( "Gestures is empty" );

        if( batch.size() == 0 ) throw new IllegalArgumentException( "No Samples covered" );

        //Create a situationGroup to create.
        SituationGroup situationGroup = new SituationGroup( name, batch.getGestures() );

        //Load all samples:
        List<TrainingSample> trainingSamples = batch.loadSamples( situationGroup );

        try{
            WekaDetector detector = new WekaDetectorBuilder( "de.tu_darmstadt.gestures.Detector", name )
                    .forSituation( situationGroup )
                    .withClassifier( type )
                    .withFeatures( buildFeatures( situationGroup ) )
                    .withPrecondition( new NoPrecondition() )
                    .withSamples( stream( trainingSamples ) )
                    .build();

            return modelManager.addModel( name, batch.getGestures(), detector.getModel() );
        }catch ( Throwable exp ){
            throw new RuntimeException( "Error on creation", exp );
        }
    }


    /**
     * Creates a simple new Model with the passed Type + Filters.
     *
     * @param name to use.
     * @param type to create.
     * @param filterStream to use.
     *
     * @return the created proxy or throws exception if failed.
     *
     * @throws IllegalArgumentException if arguments are invalid.
     * @throws RuntimeException if Creation itself failed.
     */
    public ModelProxy createNewModel( String name, WekaBaseClassifier type, SampleFilterStream filterStream ) {
        return createNewModel( name, type, filterStream.applyFilter() );
    }



}
