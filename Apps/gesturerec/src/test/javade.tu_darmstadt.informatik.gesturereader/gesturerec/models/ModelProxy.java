package de.tu_darmstadt.informatik.gesturereader.gesturerec.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashSet;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorDescription;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.DetectorIdentifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.preconditions.NoPrecondition;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.utils.SerialVersionUidIgnoringObjectInputStream;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Lazy;
import de.tu_darmstadt.informatik.kom.gesturerec.features.FeatureFactory;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.Collectors2;
import java8.util.Optional;
import java8.util.Spliterator;
import java8.util.Spliterators;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;
import weka.classifiers.Classifier;

import static de.tu_darmstadt.informatik.gesturereader.gesturerec.utils.FileConstants.getBaseModelFolder;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 07.12.2017.
 */

public class ModelProxy {

    /**
     * The Type of the model.
     */
    private final WekaBaseClassifier classifier;

    /**
     * The File for the Model
     */
    private final File modelFile;

    /**
     * The File for the Meta data.
     */
    private final File metaFile;

    /**
     * The Classifier.
     */
    private final Lazy<Classifier> lazyClassifier;

    private final Lazy<WekaDetector> lazyWekaDetector = Lazy.of( this::loadWekaDetector );

    /**
     * The name of the Classifier.
     */
    private final String name;

    /**
     * The collection of used gestures.
     */
    private final Collection<Gesture> gestures = new HashSet<>();

    /**
     * The used SituationGroup.
     */
    private final SituationGroup situationGroup;

    /**
     * The last error while loading.
     */
    private Throwable lastLoadingError;


    /**
     * Creates a new Proxy for Models.
     *
     * @param classifier to use.
     * @param name to use.
     * @param gestures to use.
     */
    public ModelProxy( WekaBaseClassifier classifier, String name, Collection<Gesture> gestures ) {
        this.classifier = classifier;
        this.name = name;
        this.gestures.addAll( gestures );
        this.situationGroup = new SituationGroup( name, stream( gestures ).map( Gesture::getName ).toArray( String[]::new ) );

        File base = getBaseModelFolder();
        this.metaFile = new File( base,name + ".meta" );
        this.modelFile = new File( base,name + ".model" );

        lazyClassifier = Lazy.of( this::lazyLoadClassifier );
    }

    /**
     * Creates a new Proxy for Models.
     *
     * @param classifier to use.
     * @param name to use.
     * @param gestures to use.
     */
    public ModelProxy( String name, Collection<Gesture> gestures, Classifier classifier ) {
        this.lazyClassifier = Lazy.of( classifier );
        this.classifier = WekaBaseClassifier.fromClassifier( classifier );
        this.name = name;
        this.gestures.addAll( gestures );
        this.situationGroup = new SituationGroup( name, stream( gestures ).map( Gesture::getName ).toArray( String[]::new ) );

        File base = getBaseModelFolder();
        this.metaFile = new File( base,name + ".meta" );
        this.modelFile = new File( base,name + ".model" );
    }


    /**
     * Loads a classifier.
     *
     * @return a classifier.
     */
    private Classifier lazyLoadClassifier() {
        try(
            InputStream fileIn = new FileInputStream( modelFile );
            ObjectInputStream in = new SerialVersionUidIgnoringObjectInputStream( fileIn );
        )
        {
            return (Classifier) in.readObject();
        }catch( Throwable exp ){
            this.lastLoadingError = exp;
            return null;
        }
    }


    /**
     * If the Classifier is loaded.
     * @return true if loaded.
     */
    public boolean isLoaded(){
        return lazyClassifier.isLoaded();
    }

    /**
     * Returns true if an Error is present.
     * @return true if an Error is present.
     */
    public boolean hasError(){
        return lastLoadingError != null;
    }

    /**
     * Returns true if this is loadable.
     * @return true if loadable.
     */
    public boolean canLoad(){
        return !lazyClassifier.isLoaded() && lastLoadingError == null;
    }

    /**
     * Returns the last error while loading or null.
     * @return the last error.
     */
    public Throwable getError(){
        return lastLoadingError;
    }


    /**
     * This forces a delete of the Model.
     */
    public void delete(){
        FileUtils.deleteQuietly( this.modelFile );
        FileUtils.deleteQuietly( this.metaFile );
    }


    /**
     * Does a save.
     * @return true if worked, false if failed.
     */
    public boolean save(){
        if( !modelFile.exists() ) {
            if( !lazyClassifier.isLoaded() ) return false;
            else if( !saveModel() ) return false;
        }

        if( !metaFile.exists() ){
            if( !saveMeta() ) return false;
        }

        return true;
    }

    /**
     * Saves the Metadata as JSON.
     * @return the Metadata.
     */
    private boolean saveMeta() {
        if( metaFile.exists() ) return true;

        try{
            JsonObject root = new JsonObject();
            root.add( "gestures", stream( gestures ).map( Gesture::getName ).collect( Collectors2.toGsonArray() ) );
            root.addProperty( "name", name );
            root.addProperty( "type", classifier.name() );

            FileUtils.write( metaFile, root.toString(), Charset.defaultCharset() );
            return true;
        }catch ( IOException exp ){
            exp.printStackTrace();
            return false;
        }
    }

    /**
     * Saves a model. Returns true if worked.
     * @return true if worked.
     */
    private boolean saveModel() {
        if( modelFile.exists() ) return true;
        if( !lazyClassifier.isLoaded() ) return false;

        try (
                ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream( modelFile ) )
            ){
            out.writeObject( lazyClassifier.get() );
            return true;
        }catch ( Throwable exp ){
            exp.printStackTrace();
            return false;
        }
    }


    /**
     * Gets the name of the Model
     * @return the name.
     */
    public String getName() {
        return name;
    }


    /**
     * Gets the present Gestures.
     * @return the present gestures.
     */
    public Collection<Gesture> getGestures() {
        return new HashSet<>( gestures );
    }

    /**
     * Gets the Type.
     * @return the type.
     */
    public WekaBaseClassifier getClassifierType() {
        return classifier;
    }

    /**
     * Gets the Classifier. Returns NULL if there is an error.
     */
    public Classifier getClassifier() {
        return lazyClassifier.get();
    }


    /**
     * Gets the loaded Detector. May be null.
     * @return the loaded detector.
     */
    public WekaDetector getDetector(){
        return lazyWekaDetector.get();
    }

    /**
     * Loading the Weka detector.
     * @return the loaded Detector or null if failed.
     */
    private WekaDetector loadWekaDetector() {
        Classifier classifier = getClassifier();
        if( classifier == null ) return null;

        DetectorDescription description = new DetectorDescription.Builder( new DetectorIdentifier( "defaultModel", name ) )
                .setDescription( "Detector for Gestures" )
                .setSituationGroup( situationGroup )
                .build();

        return new WekaDetector( description,
                FeatureFactory.buildFeatures( situationGroup ),
                classifier,
                new NoPrecondition() );
    }


    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if( obj instanceof ModelProxy ){
            return name.equals( ( (ModelProxy) obj ).name );
        }

        return super.equals(obj);
    }


    /**
     * Loads a Model from the File-System.
     *
     * @param gestureManager to use.
     * @param name to use.
     *
     * @return the loaded Model or empty if failed.
     */
    public static Optional<ModelProxy> loadFromFilesystem( GestureManager gestureManager, String name ){
        File base = getBaseModelFolder();
        File metaFile = new File( base, name + ".meta" );
        if( !metaFile.exists() ) return Optional.empty();

        File modelFile = new File( base, name + ".model" );
        if( !modelFile.exists() ) return Optional.empty();

        //Try to read as JSON.
        try{
            String text = FileUtils.readFileToString( metaFile, Charset.defaultCharset() );
            JsonObject obj = new JsonParser().parse( text ).getAsJsonObject();

            String readName = obj.get( "name" ).getAsString();
            Collection<Gesture> gestures = unwrap( gestureManager, obj.get( "gestures" ).getAsJsonArray() );
            WekaBaseClassifier baseClassifier = WekaBaseClassifier.valueOf( obj.get( "type" ).getAsString() );

            return Optional.of( new ModelProxy( baseClassifier, readName, gestures ) );
        }catch ( Throwable exp ){
            exp.printStackTrace();
            return Optional.empty();
        }
    }

    /**
     * Does an unwrap from the Array to Gestures.
     * @param gestureManager to use.
     * @param gestures to unwrap.
     *
     * @return the unwraped Gestures.
     */
    private static Collection<Gesture> unwrap( GestureManager gestureManager, JsonArray gestures ) {
        return StreamSupport.stream( Spliterators.spliteratorUnknownSize( gestures.iterator(), Spliterator.DISTINCT ), false )
                .map( JsonElement::getAsString )
                .map( gestureManager::createGesture )
                .collect( Collectors.toSet() );
    }

}
