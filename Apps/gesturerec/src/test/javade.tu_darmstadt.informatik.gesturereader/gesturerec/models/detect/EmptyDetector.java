package de.tu_darmstadt.informatik.gesturereader.gesturerec.models.detect;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;

/**
 * Created by Toby on 14.10.2017
 */

public class EmptyDetector extends GestureDetector {


    private static EmptyDetector instance;


    /**
     * Gets the Singleton for this Detector.
     */
    public static EmptyDetector get(){
        return instance == null ? instance = new EmptyDetector() : instance;
    }


    // Private constructor for Singleton.
    private EmptyDetector (){}



    @Override
    public GestureResultWrapper analyseGesture(SensorDataBatch batch) {
        return GestureResultWrapper.empty();
    }


    @Override
    public void analyseGestureAsync(SensorDataBatch batch, GestureResultCallback callback) {
        callback.gotGestureResults( batch, analyseGesture( batch ) );
    }


    @Override
    public String getModelName() {
        return "EMPTY";
    }
}
