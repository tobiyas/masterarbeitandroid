package de.tu_darmstadt.informatik.gesturereader.gesturerec.models.detect;

import android.os.Handler;
import android.os.Looper;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;
import java8.util.function.BiConsumer;

/**
 * This is an Interface for different Detectors.
 * <br/>The Detectors all have in common, that they work on the {@link de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch}
 * provided by the ContextRec Library.
 */

public abstract class GestureDetector {


    /**
     * This does an analysis on the Raw Data. It returns the Gesture wanted.
     * @param batch to analyse.
     * @return a set of Gesture Results.
     */
    public abstract GestureResultWrapper analyseGesture( SensorDataBatch batch );


    /**
     * Does an Async Analysis. This does not block the Main Thread.
     * The Callback is called, when the Batch is analysed and the Results are in.
     *
     * @param batch to analysis.
     * @param callback to call when done.
     */
    public abstract void analyseGestureAsync( SensorDataBatch batch, GestureResultCallback callback );

    /**
     * Does an Async Analysis. This does not block the Main Thread.
     * The Callback is called, when the Batch is analysed and the Results are in.
     *
     * @param batch to analysis.
     * @param callbackSuccess to call when worked.
     * @param callbackError to call when failed.
     */
    public void analyseGestureAsync( SensorDataBatch batch,
                                               BiConsumer<SensorDataBatch,GestureResultWrapper> callbackSuccess,
                                               BiConsumer<SensorDataBatch,Throwable> callbackError
                                    ){
        GestureResultCallback callback = new GestureResultCallback() {
            @Override
            public void gotGestureResults(SensorDataBatch batch, GestureResultWrapper results) {
                callbackSuccess.accept( batch, results );
            }

            @Override
            public void gotError(SensorDataBatch batch, Throwable exp) {
                callbackError.accept( batch, exp );
            }
        };

        analyseGestureAsync( batch, callback );
    }


    /**
     * Does an Async Analysis. This does not block the Main Thread.
     * The Callback is called, when the Batch is analysed and the Results are in.
     * The Callback is called from the MT
     *
     * @param batch to analysis.
     * @param callbackSuccess to call when worked.
     * @param callbackError to call when failed.
     */
    public void analyseGestureAsyncMT( SensorDataBatch batch,
                                     BiConsumer<SensorDataBatch,GestureResultWrapper> callbackSuccess,
                                     BiConsumer<SensorDataBatch,Throwable> callbackError
                                 ){
        Handler handler = new Handler( Looper.getMainLooper() );
        GestureResultCallback callback = new GestureResultCallback() {
            @Override
            public void gotGestureResults(SensorDataBatch batch, GestureResultWrapper results) {
                handler.post( () -> callbackSuccess.accept( batch, results ) );
            }

            @Override
            public void gotError(SensorDataBatch batch, Throwable exp) {
                handler.post( () -> callbackError.accept( batch, exp ) );
            }
        };

        analyseGestureAsync( batch, callback );
    }


    /**
     * Gets the Name of the Detector.
     * @return name of the detector.
     */
    public abstract String getModelName();



    public interface GestureResultCallback{

        /**
         * This is called, when the Results of the Gesture Analysis is done.
         * @param batch that was used.
         * @param results that have been evaluated.
         */
        void gotGestureResults(SensorDataBatch batch, GestureResultWrapper results);

        /**
         * This indicates, that we got an error while executing the Detector.
         * @param batch that was used.
         * @param exp that was thrown.
         */
        void gotError(SensorDataBatch batch, Throwable exp);
    }

}
