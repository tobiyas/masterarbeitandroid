package de.tu_darmstadt.informatik.gesturereader.gesturerec.samples;


import org.joda.time.DateTime;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;

import static de.tu_darmstadt.informatik.gesturereader.gesturerec.utils.FileConstants.getBaseGestureFolder;

/**
 * Created by Tobias on 06.12.2017
 */

public class SampleManager {


    /**
     * The set of samples to query for stuff.
     */
    private final Set<SampleProxy> samples = new HashSet<>();

    /**
     * The used GestureManager.
     */
    private final GestureRecSession session;


    /**
     * Creates a new SampleManager.
     *
     * @param session to use for getting stuff.
     */
    public SampleManager( GestureRecSession session){
        this.session = session;
    }


    /**
     * This does a complete Reload of all proxies on disc!
     */
    public void reload(){
        this.samples.clear();

        File base = getBaseGestureFolder();
        File[] gestureFolders = base.listFiles();
        if( gestureFolders == null || gestureFolders.length == 0 ) return;

        for( File gestureFolder : gestureFolders ){
            File[] files = gestureFolder.listFiles();
            if( files == null || files.length == 0 ) continue;

            Gesture gesture = session.getGestureManager().createGesture( gestureFolder.getName() );
            for( File file : files ){
                String name = file.getName().replace( ".batch", "" );

                String[] split = name.split( Pattern.quote( "_" ) );
                if( split.length != 2 ) continue;

                String user = split[0];
                String dateString = split[1];

                try{
                    long time = Long.parseLong( dateString );
                    SampleProxy proxy = new SampleProxy( user, gesture, new DateTime( time ), file );
                    samples.add( proxy );
                }catch ( NumberFormatException exp ){
                    exp.printStackTrace();
                }
            }
        }
    }


    /**
     * Does a new Query on the samples.
     * @return the query.
     */
    public SampleFilterStream query(){
        return new SampleFilterStream( this.session.getGestureManager(), this.samples );
    }


    /**
     * Adds AND saves new Sample to the Collection.
     *
     * @return true if worked, false if not.
     */
    public SampleProxy addSample( Gesture gesture, DateTime recordTime, String user, SensorDataBatch batch ){
        SampleProxy proxy = new SampleProxy( user, gesture, recordTime, batch );
        if( samples.contains( proxy ) ) return null;

        if( proxy.saveData() ) {
            samples.add( proxy );
            return proxy;
        }

        return null;
    }


    /**
     * Adds AND saves new Sample to the Collection.
     *
     * @param gesture to save.
     * @param recordTime to use for saving.
     * @param batch to save.
     *
     * @return the Proxy if worked, null if not.
     */
    public SampleProxy addSampleCurrentUser( Gesture gesture, DateTime recordTime, SensorDataBatch batch ){
        String currentUser = session.getUserManager().getCurrentUserName();
        return addSample( gesture, recordTime, currentUser, batch );
    }



}
