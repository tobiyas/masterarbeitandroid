package de.tu_darmstadt.informatik.gesturereader.gesturerec.samples;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.TrainingSample;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import java8.util.stream.Collectors;
import java8.util.stream.Stream;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 06.12.2017
 */

public class SampleBatch {

    /**
     * The samples to work on.
     */
    private final Collection<SampleProxy> samples = new HashSet<>();

    /**
     * All gestures present.
     */
    private final Collection<Gesture> gestures = new HashSet<>();


    public SampleBatch( Collection<SampleProxy> samples, Collection<Gesture> gestures ) {
        if( samples != null ) this.samples.addAll( samples );
        if( gestures != null ) this.gestures.addAll( gestures );
    }


    /**
     * Creates an empty Sample Batch.
     */
    public SampleBatch(){}


    /**
     * This physically loads the Trainingssamples.
     *
     * @param group to load for.
     *
     * @return the loaded Samples.
     */
    public List<TrainingSample> loadSamples( SituationGroup group ){
        return stream( samples )
                .map( SampleProxy::loadIfNotPresent )
                .map( sample -> sample.toTrainingsSample( group ) )
                .collect( Collectors.toList() );
    }

    /**
     * This physically loads the Trainingssamples.
     *
     * @param situationName to load with.
     *
     * @return the loaded Samples.
     */
    public List<TrainingSample> loadSamplesWithName( String situationName ){
        return loadSamples( new SituationGroup( situationName, getGestures() ) );
    }

    /**
     * This physically loads the Trainingssamples.
     *
     * @return the loaded Samples.
     */
    public List<TrainingSample> loadSamplesWithRandomName(){
        return loadSamplesWithName( String.valueOf( System.currentTimeMillis() ) );
    }


    /**
     * This physically loads the Trainingssamples.
     *
     * @param group to load for.
     *
     * @return the loaded Samples.
     */
    public Stream<TrainingSample> streamLoadSamples( SituationGroup group ){
        return stream( samples )
                .map( SampleProxy::loadIfNotPresent )
                .map( sample -> sample.toTrainingsSample( group ) );
    }


    /**
     * This physically loads the Trainingssamples.
     *
     * @param name to load for.
     *
     * @return the loaded Samples.
     */
    public Stream<TrainingSample> streamLoadSamplesWithName( String name ){
        return streamLoadSamples( new SituationGroup( name, getGestures() ) );
    }

    /**
     * This physically loads the Trainingssamples.
     *
     * @return the loaded Samples.
     */
    public Stream<TrainingSample> streamLoadSamplesWithRandomName( ){
        return streamLoadSamplesWithName( String.valueOf( System.currentTimeMillis() ) );
    }



    /**
     * Gets the Size of the Samples present.
     */
    public int size(){
        return samples.size();
    }


    /**
     * Returns true when empty.
     * @return true when empty.
     */
    public boolean isEmpty(){
        return size() <= 0;
    }

    /**
     * Simply gets the Samples.
     * @return the samples.
     */
    public Collection<SampleProxy> getSamples(){
        return samples;
    }

    /**
     * Gets all Gestures present.
     *
     * @return the present gestures.
     */
    public Collection<Gesture> getGestures() {
        return gestures;
    }


    /**
     * Adds all Data from the other batch to this one.
     * @param batch to add.
     */
    public void addBatch( SampleBatch batch ){
        if( batch == null ) return;

        this.samples.addAll( batch.getSamples() );
        this.gestures.addAll( batch.getGestures() );
    }


    /**
     * Adds the Sample passed plus it's Gesture if not prsent..
     * @param proxy to add.
     */
    public void addSample( SampleProxy proxy ){
        if( proxy == null ) return;

        this.samples.add( proxy );
        this.gestures.add( proxy.getGesture() );
    }

    /**
     * Clears the Batch and it's gestures.
     */
    public void clear(){

    }


    /**
     * Saves this batch as ARFF File.
     * This simply redirects to {@link ARFFCreator}
     *
     * @param file to save to.
     */
    public void saveToARFFFile( File file ) throws Exception {
        ARFFCreator.createARFFile( file, this );
    }

}
