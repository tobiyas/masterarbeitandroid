package de.tu_darmstadt.informatik.gesturereader.gesturerec.samples;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.base.SituationGroup;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureSet;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaUtils;
import weka.core.Instances;

import static de.tu_darmstadt.informatik.gesturereader.gesturerec.features.FeatureFactory.buildFeatures;

/**
 * Created by Tobias on 11.12.2017
 */

public class ARFFCreator {



    /**
     * Loads the Trainingsdata of a SampleBatch into an ARFF file.
     *
     * @param saveFile to save to.
     * @param batch to write
     *
     * @throws Exception if something goes wrong.
     */
    public static void createARFFile(
            File saveFile,
            SampleBatch batch ) throws Exception {

        if( saveFile == null || batch == null ) throw new IllegalArgumentException( "May not be null" );
        if( saveFile.exists() ) throw new IllegalArgumentException( "SaveFile already exists." );

        SituationGroup group = new SituationGroup( "TMP", batch.getGestures() );

        //Notify update with Instances:
        WekaFeatureSet set = buildFeatures( group );
        Instances arffData = WekaUtils.buildWekaInstanceSet( set, batch.streamLoadSamples( group ), s -> {} );
        String serialized = arffData.toString();
        FileUtils.write( saveFile, serialized, Charset.defaultCharset() );
    }



    /**
     * Loads the Trainingsdata of a stream into an ARFF file.
     *
     * @param saveFile to save to.
     * @param stream to write
     *
     * @throws Exception if something goes wrong.
     */
    public static void createARFFile(
            File saveFile,
            SampleFilterStream stream ) throws Exception {

        if( saveFile == null || stream == null ) throw new IllegalArgumentException( "May not be null" );
        createARFFile( saveFile, stream.applyFilter() );
    }


}
