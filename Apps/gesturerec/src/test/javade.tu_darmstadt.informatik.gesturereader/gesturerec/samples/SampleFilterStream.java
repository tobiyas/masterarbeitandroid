package de.tu_darmstadt.informatik.gesturereader.gesturerec.samples;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 06.12.2017
 */

public class SampleFilterStream {

    /**
     * The users to filter by. Null -> Use all!
     */
    private Collection<String> users = null;

    /**
     * The gestures to filter by. Null -> Use all!
     */
    private Collection<Gesture> gestures = null;

    /**
     * The time from when to filter.
     */
    private DateTime from;

    /**
     * The time to when to filter.
     */
    private DateTime to;

    /**
     * if the Data should be shuffeled.
     */
    private boolean shuffle;

    /**
     * The set of proxied Samples.
     */
    private final Set<SampleProxy> proxySet = new HashSet<>();

    /**
     * The manager to use for handling Gestures.
     */
    private final GestureManager gestureManager;


    /**
     * This creates a new Stream on the Proxied datas.
     * @param gestureManager to use for getting all Gestures.
     * @param proxies to use.
     */
    public SampleFilterStream(GestureManager gestureManager, Collection<SampleProxy> proxies ) {
        this.proxySet.addAll( proxies );
        this.gestureManager = gestureManager;
    }

    /**
     * Adds users to the Stream.
     *
     * @param users to add.
     * @return this for chaining.
     */
    public SampleFilterStream addUsers( String... users ) {
        if( users == null || users.length == 0 ) return this;

        return addUsers( Arrays.asList( users ) );
    }


    /**
     * Adds users to the Stream.
     *
     * @param users to add.
     * @return this for chaining.
     */
    public SampleFilterStream addUsers( Collection<String> users ){
        if( users == null || users.size() == 0 ) return this;

        if( this.users == null ) this.users = new HashSet<>( users );
        else this.users.addAll( users );

        return this;
    }


    /**
     * Adds gestures to the Stream.
     *
     * @param gestures to add.
     * @return this for chaining.
     */
    public SampleFilterStream addGestures( Gesture... gestures ) {
        if( gestures == null || gestures.length == 0 ) return this;

        return addGestures( Arrays.asList( gestures ) );
    }


    /**
     * Adds gestures to the Stream.
     *
     * @param gestures to add.
     * @return this for chaining.
     */
    public SampleFilterStream addGestures( Collection<Gesture> gestures ){
        if( gestures == null || gestures.isEmpty() ) return this;

        if( this.gestures == null ) this.gestures= new HashSet<>( gestures );
        else this.gestures.addAll( gestures );

        return this;
    }


    /**
     * Sets from when to filter.
     * @param from when.
     */
    public SampleFilterStream setFrom(DateTime from) {
        this.from = from;
        return this;
    }

    /**
     * Clears the From filter.
     */
    public SampleFilterStream clearFrom(){
        this.from = null;
        return this;
    }

    /**
     * Sets to when to filter.
     * @param to when.
     */
    public SampleFilterStream setTo(DateTime to) {
        this.to = to;
        return this;
    }

    /**
     * Clears the to filter.
     */
    public SampleFilterStream clearTo(){
        this.to = null;
        return this;
    }

    /**
     * Clears all Users.
     */
    public SampleFilterStream clearUsers(){
        this.users = null;
        return this;
    }


    /**
     * Clears all gestures.
     */
    public SampleFilterStream clearGestures(){
        this.gestures = null;
        return this;
    }


    /**
     * Sets if the Set should be shuffeled.
     */
    public SampleFilterStream setShuffled( boolean shuffle ){
        this.shuffle = shuffle;
        return this;
    }

    /**
     * Applies the current filter.
     *
     * @return the unfiltered data.
     */
    public SampleBatch applyFilter(){
        Collection<Gesture> usedGestures = gestures == null
                ? gestureManager.getAllWithoutUnknown()
                : gestures;

        List<SampleProxy> filtered =
            stream( proxySet )

                //Filter User + Gestures:
                .filter( s -> users == null || users.contains( s.getUser() ) )
                .filter( s -> usedGestures.contains( s.getGesture() ) )

                //Filter time:
                .filter( s -> from == null || from.isBefore( s.getRecordTime() ) )
                .filter( s -> to == null || to.isAfter( s.getRecordTime() ) )

                //Collect:
                .collect( Collectors.toList() );

        if( shuffle ) Collections.shuffle( filtered );
        return new SampleBatch( filtered, usedGestures );
    }

}
