package de.tu_darmstadt.informatik.gesturereader.gesturerec;

import android.content.Context;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.kom.contextrec.phone.ContextRecSession;
import de.tu_darmstadt.informatik.kom.contextrec.phone.PhoneDataSources;
import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.PermanentIMUScanner;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.SourceLinker;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelManager;
import de.tu_darmstadt.informatik.kom.gesturerec.models.creator.SimpleModelCreator;
import de.tu_darmstadt.informatik.kom.gesturerec.recorder.DataRecorder;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleManager;
import de.tu_darmstadt.informatik.kom.gesturerec.users.UserManager;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.Dialogs;

/**
 * Created by Tobias on 06.12.2017
 */
public class GestureRecSession implements AutoCloseable {

    /**
     * The context rec session to use for collecting data.
     */
    private final ContextRecSession contextRecSession;

    /**
     * The IMU scanner to use for scanning stuff IMUs.
     */
    private final PermanentIMUScanner imuScanner;

    /**
     * The Source linker to use.
     */
    private final SourceLinker linker;

    /**
     * The context to bind stuff to.
     */
    private final Context context;

    /**
     * The Class manageing Users.
     */
    private final UserManager userManager;

    /**
     * The Samplemanager to use for sample stuff.
     */
    private final SampleManager sampleManager;

    /**
     * The used gesture manager.
     */
    private final GestureManager gestureManager;

    /**
     * The Modelmanager to use.
     */
    private final ModelManager modelManager;

    /**
     * The Recorder for Data.
     */
    private final DataRecorder dataRecorder;




    /**
     * An option to use Dialogs.
     */
    private final Dialogs Dialogs;

    /**
     * The Creator for simple models.
     */
    private final SimpleModelCreator SimpleModelCreator;


    /**
     * Creates a new GestureRec Session.
     *
     * @param context to use for creation.
     */
    public GestureRecSession( Context context ){
        //Set up the Basics:
        this.context = context;
        this.contextRecSession = new ContextRecSession( context );
        this.imuScanner = new PermanentIMUScanner( context );
        this.userManager = new UserManager( context );
        this.linker = new SourceLinker( context, contextRecSession );
        this.gestureManager = new GestureManager();
        this.modelManager = new ModelManager( this );
        this.sampleManager = new SampleManager( this );
        this.dataRecorder = new DataRecorder( this );


        //Create simple access Classes:
        this.Dialogs = new Dialogs( this );
        this.SimpleModelCreator = new SimpleModelCreator( gestureManager, sampleManager, modelManager );


        //Config the IMU scanner:
        this.imuScanner.setConsumer( contextRecSession::addDataSource  );

        //Config the session to do remove custom triggers, by using a very long time.:
        this.contextRecSession.switchToCustomTriggering(() -> new DateTime(System.currentTimeMillis() * 2));

        //Reload the Gesture Manager:
        this.gestureManager.init();

        //Let the Linker do his business:
        this.linker.linkCurrent();

        //Load the Sample manager:
        this.sampleManager.reload();

        //Reload the Model Manager.
        this.modelManager.reload();
    }


    /**
     * Starts streaming data to be able to operate on.
     */
    public GestureRecSession startStreaming() {
        if (!contextRecSession.isRunning()){
            this.contextRecSession.start(false);
            this.contextRecSession.startStreaming();
        }

        return this;
    }

    /**
     * Pauses the Streaming of the Data.
     */
    public GestureRecSession stopStreaming(){
        if( contextRecSession.isRunning() ) {
            this.contextRecSession.stop();
            this.contextRecSession.stopStreaming();
        }

        return this;
    }



    @Override
    public void close(){
        if( contextRecSession.isClosed() ) return;

        stopStreaming();

        contextRecSession.close( false );
        imuScanner.stop();
    }






    ///////////////////////////////
    // DataSource Related stuff  //
    ///////////////////////////////

    /**
     * Adds the default Phone sensors.
     */
    public GestureRecSession addPhoneSensorsToSession(){
        contextRecSession.addDataSources( PhoneDataSources.inertialPhoneSensors( context ) );
        return this;
    }


    /**
     * Adds the default Phone sensors.
     */
    public GestureRecSession scanForWearables(){
        PhoneDataSources.wearStreamingConnectors( context, contextRecSession::addDataSources );
        return this;
    }


    /**
     * Adds the default Phone sensors.
     */
    public GestureRecSession scanForIMUs(){
        imuScanner.start();
        return this;
    }

    /**
     * Adds custom sources.
     *
     * @param sources to add
     */
    public GestureRecSession addSources( DataSource... sources ){
        this.contextRecSession.addDataSources( sources );
        return this;
    }



    ///////////////////////////////
    //  Getter for external uses //
    ///////////////////////////////

    /**
     * Gets the used session to use for own Usage.
     * NO GUARANTY IF SOMETHING BREAKS AFTER THIS CALL!
     *
     * @return the session used.
     */
    public ContextRecSession getContextRecSession() {
        return contextRecSession;
    }

    /**
     * Gets the current UserManager.
     *
     * @return the current UserManager.
     */
    public UserManager getUserManager() {
        return userManager;
    }

    /**
     * Gets the SourceLinker to Redefine the Bounds.
     *
     * @return the Linker.
     */
    public SourceLinker getLinker() {
        return linker;
    }

    /**
     * Accesses the Gesture Manages.
     *
     * @return the gesture manager.
     */
    public GestureManager getGestureManager() {
        return gestureManager;
    }


    /**
     * Gets the Sample manager to access and use Samples.
     *
     * @return the Samplemanager for access to samples.
     */
    public SampleManager getSampleManager() {
        return sampleManager;
    }

    /**
     * Gets the Manager for Models.
     *
     * @return the Manager handling Models.
     */
    public ModelManager getModelManager() {
        return modelManager;
    }

    /**
     * Gets the DataRecorder.
     * @return the data Recorder.
     */
    public DataRecorder getDataRecorder(){
        return dataRecorder;
    }


    //////////////////////
    // Simple Creators: //
    //////////////////////


    /**
     * Gets a simple Dialog creator for Gestures and Users.
     * @return a Dialog Creator.
     */
    public Dialogs Dialogs(){
        return Dialogs;
    }


    /**
     * Gets a Creator for Simple Models.
     * @return the Creator for simple Models.
     */
    public SimpleModelCreator SimpleModelCreator() {
        return SimpleModelCreator;
    }
}
