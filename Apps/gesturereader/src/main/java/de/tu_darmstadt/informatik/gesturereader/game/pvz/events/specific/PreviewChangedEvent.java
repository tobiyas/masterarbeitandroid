package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;

/**
 * Created by Tobias on 22.10.2017
 */

public class PreviewChangedEvent extends Event {

    /**
     * The new Preview plant to display.
     */
    private final Plant newPreview;


    public PreviewChangedEvent(Plant newPreview) {
        this.newPreview = newPreview;
    }


    public Plant getNewPreview() {
        return newPreview;
    }
}
