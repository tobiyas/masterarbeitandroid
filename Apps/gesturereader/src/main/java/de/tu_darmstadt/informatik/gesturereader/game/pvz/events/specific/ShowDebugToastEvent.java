package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Created by Toby on 24.10.2017.
 */

public class ShowDebugToastEvent extends Event {


    /**
     * The Text to show.
     */
    private final String text;

    /**
     * The length. LONG or SHORT
     */
    private final int length;


    /**
     * Shows a debug toast.
     * This does nothing during production!
     * Can be fired from Anywhere!!!
     *
     * @param text to show.
     * @param length Toast.LONG or Toast.SHORT.
     */
    public ShowDebugToastEvent(String text, int length) {
        this.text = text;
        this.length = length;
    }


    public String getText() {
        return text;
    }

    public int getLength() {
        return length;
    }
}
