package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Created by Tobias on 09.11.2017
 */

public class RemoveResourceEvent extends Event {

    /**
     * The amount of resources to remove.
     */
    private final int amount;


    /**
     * Removes the Resources passed to the system.
     * This is followed by an Update event.
     *
     * @param amount to add.
     */
    public RemoveResourceEvent( int amount ) {
        this.amount = amount;
    }

    /**
     * The Amount remove.
     *
     * @return the amount remove.
     */
    public int getAmount() {
        return amount;
    }
}
