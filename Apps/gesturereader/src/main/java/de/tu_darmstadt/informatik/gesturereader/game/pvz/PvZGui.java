package de.tu_darmstadt.informatik.gesturereader.game.pvz;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.config.GestureKeyConfig;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ContinueMovementDetectionEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PauseMovementDetectionEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.base.PvZBootstrap;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.gui.ViewProvider;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnActivity;

/**
 * Created by Tobias on 21.10.2017
 */
public class PvZGui extends OwnActivity implements ViewProvider {

    /**
     * The current Game field.
     */
    private PvZBootstrap bootstrap;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pvz_activity);

        //Init the Bootstrap to build the System:
        bootstrap = new PvZBootstrap( this ).buildBase();


        //Feature + Debug toggle:
        boolean debugMode = true;

        if( debugMode ) bootstrap.buildDebugControls().enableDebugControls();
        else bootstrap.disableDebugControls();

        String model = getIntent().getStringExtra( "model" );

        GestureKeyConfig config = new GestureKeyConfig();
        //TODO Fill with links!
        bootstrap.buildMovementControls( model, config );
    }


    @Override
    public View provideView(int viewId) {
        return findViewById( viewId );
    }

    @Override
    public Context getContext() {
        return this;
    }


    @Override
    protected void onStop() {
        super.onStop();

        if( bootstrap != null ) bootstrap.teardown();
    }

    //This is only for debugging purposes.
    public void toggleDebug( View view ) {
        CheckBox checkBox = (CheckBox) view;
        findViewById( R.id.debug_nav ).setVisibility( checkBox.isChecked() ? View.VISIBLE : View.GONE );
    }

    /**
     * This is called when the Checkbox for Disabling movement is touched.
     */
    public void toggleMovement( View view ) {
        CheckBox check = (CheckBox) view;
        boolean enable = check.isChecked();

        Event event = enable ? new ContinueMovementDetectionEvent() : new PauseMovementDetectionEvent();
        this.bootstrap.getEventSystem().fireEvent( event );
    }
}
