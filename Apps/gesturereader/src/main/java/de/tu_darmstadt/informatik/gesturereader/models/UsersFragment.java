package de.tu_darmstadt.informatik.gesturereader.models;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;
import java.util.Map;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.evaluation.VerifySamplesDialog;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import java8.util.stream.RefStreams;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 08.01.2018
 */
public class UsersFragment extends Fragment {

    private ListView list;
    private GestureRecSession session;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        list = ( ListView ) inflater.inflate( R.layout.fragment_users, container, false );
        session = MainApplication.fromContext( this.getContext() ).getSession();

        updateAdapter();

        return list;
    }


    private void updateAdapter() {
        Context context = this.getContext();
        if( context == null ) return;

        String[] users = RefStreams.of( session.getUserManager().readAllPresentUsers() )
                .map( u -> u + " - " + session.getSampleManager().query().addUsers( u ).applyFilter().size() )
                .toArray( String[]::new );
        list.setChoiceMode( ListView.CHOICE_MODE_SINGLE );

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                context,
                android.R.layout.simple_list_item_1,
                users
        );

        list.setAdapter( adapter );
        list.setOnItemClickListener( this::selectUser );
        adapter.notifyDataSetChanged();

        list.setOnItemLongClickListener( this::askDelete );
    }


    private void selectUser(AdapterView<?> adapterView, View view, int slot, long unused) {
        String user = (String) adapterView.getItemAtPosition( slot );
        user = user.split( " - " )[0];

        Context context = this.getContext();
        if( context == null ) return;

        SampleBatch batch = session.getSampleManager().query().addUsers( user ).applyFilter();
        Map<Gesture,Long> accurance = new HashMap<>();
        for( Gesture gesture : batch.getGestures() ){
            accurance.put( gesture, stream( batch.getSamples() ).filter( s -> s.getGesture() == gesture ).count() );
        }

        String[] items = stream( accurance.entrySet() )
                .map( e -> e.getKey().getName() + " - " + e.getValue() )
                .sorted()
                .toArray( String[]::new );

        new AlertDialog.Builder( context )
                .setTitle( user + "'s Data" )
                .setItems( items, (i,b) -> {} )
                .create()
                .show();
    }


    private boolean askDelete( AdapterView<?> adapterView, View unused1, int slot, long unused2 ) {
        String user = (String) adapterView.getItemAtPosition( slot );
        final String fuser = user.split( " - " )[0];

        Context context = this.getContext();
        if( context == null ) return true;

        int amount = session.getSampleManager().query().addUsers( fuser ).applyFilter().size();
        new AlertDialog.Builder( context )
                .setTitle( "DELETE " + amount + " " + user + "'s Samples?" )
                .setPositiveButton( "YES", (a,b) -> deleteUser( fuser ) )
                .setNegativeButton( "NO", (a,b) -> a.dismiss() )
                .create()
                .show();

        return true;
    }


    /**
     * Deletes the User by deleting his content.
     * @param user to delete-
     */
    private void deleteUser( String user ) {
        SampleBatch batch = session.getSampleManager().query().addUsers( user ).applyFilter();
        session.getSampleManager().deleteSamples( batch );

        //On delete -> Update!
        updateAdapter();
    }

}
