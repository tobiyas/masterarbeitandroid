package de.tu_darmstadt.informatik.gesturereader.game.fight.gui;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameModel;

/**
 * Created by Tobias on 02.01.2018
 */

public class AfterDoneAnimator implements FightGameModel.WinLooseCallback {

    /**
     * The view of one self.
     */
    private final ImageView self;

    /**
     * The view of the Enemy.
     */
    private final ImageView enemy;

    /**
     * The size to move for the looser.
     */
    private final int looseMoveWidth;


    public AfterDoneAnimator(ImageView self, ImageView enemy, Activity activity ) {
        this.self = self;
        this.enemy = enemy;

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics( metrics );
        looseMoveWidth = metrics.widthPixels / 2;
    }


    @Override
    public void playerWon() {
        replaceCheer( self );
        replaceSad( enemy, looseMoveWidth );
    }


    @Override
    public void playerLost() {
        replaceCheer( enemy );
        replaceSad( self, -looseMoveWidth );
    }


    /**
     * Replaces the Image view with a winne pose + makes it jump.
     *
     * @param winner to use.
     */
    private void replaceCheer( ImageView winner ){
        winner.setImageResource( R.drawable.stick_cheer );

        Animation bounce = AnimationUtils.loadAnimation( winner.getContext(), R.anim.bounce );
        winner.postDelayed( () -> winner.startAnimation( bounce ), 200 );
    }

    /**
     * Replaces the Image with the looser pose and makes it move to the side and sets it to white afterwards.
     *
     * @param sad the image to remove.
     * @param toMove the amount to move it.
     */
    private void replaceSad( ImageView sad, int toMove  ){
        sad.setImageResource( R.drawable.stick_sad );

        int time = 3000;
        Animation animation = new TranslateAnimation(0, toMove, 0, 0);
        animation.setDuration( time );
        animation.setFillAfter( true );

        sad.postDelayed( () -> sad.startAnimation( animation ), 200 );
        sad.postDelayed( () -> sad.setImageResource( R.drawable.stick_gone ), time );
    }

}
