package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;

/**
 * Provides a Plant at the Position.
 * Created by Toby on 24.10.2017
 */

public interface PlantProvider {

    /**
     * Gets the plant at the wanted position.
     * @param x to get
     * @param y to get.
     * @return the plant or null if none at this point.
     */
    Plant getPlantAt(int x, int y );

}
