package de.tu_darmstadt.informatik.gesturereader.game.fight.level;

import android.content.Intent;

import static de.tu_darmstadt.informatik.gesturereader.utils.Bundles.getExtraString;

/**
 * Created by Toby on 04.01.2018
 */
public class Level {

    protected static final String HEALTH_SELF_PATH = "healthSelf";
    protected static final String HEALTH_ENEMY_PATH = "healthEnemy";
    protected static final String DAMAGE_SELF_PATH = "damageSelf";
    protected static final String DAMAGE_ENEMY_PATH = "damageEnemy";
    protected static final String ROUND_PATH = "round";
    protected static final String HAS_SKIP_BUTTON_PATH = "hasSkipButton";
    protected static final String USE_RETRY_GESTURE_PROVIDER_PATH = "useRetryGestureProvicder";
    protected static final String RECORDING_ON_PATH = "recordingOn";
    protected static final String PLAY_SOUNDS_PATH = "playSounds";
    protected static final String SINGLE_GESTURE_PATH = "singleGesture";


    protected static final int HEALTH_SELF_DEFAULT = 100;
    protected static final int HEALTH_ENEMY_DEFAULT = 100;
    protected static final int DAMAGE_SELF_DEFAULT = 10;
    protected static final int DAMAGE_ENEMY_DEFAULT = 10;
    protected static final String ROUND_DEFAULT = "Training";
    protected static final boolean HAS_SKIP_BUTTON_DEFAULT = false;
    protected static final boolean USE_RETRY_GESTURE_PROVIDER_DEFAULT = false;
    protected static final boolean RECORDING_ON_DEFAULT = false;
    protected static final boolean PLAY_SOUNDS_DEFAULT = true;
    protected static final String SINGLE_GESTURE_DEFAULT = "";


    private int healthSelf = HEALTH_SELF_DEFAULT;
    private int healthEnemy = HEALTH_ENEMY_DEFAULT;
    private int damageSelf = DAMAGE_SELF_DEFAULT;
    private int damageEnemy = DAMAGE_ENEMY_DEFAULT;
    private String round = ROUND_DEFAULT;
    private boolean hasSkipButton = HAS_SKIP_BUTTON_DEFAULT;
    private boolean useRetryGestureProvider = USE_RETRY_GESTURE_PROVIDER_DEFAULT;
    private boolean recordingOn = RECORDING_ON_DEFAULT;
    private boolean playSounds = PLAY_SOUNDS_DEFAULT;
    private String singleGesture = SINGLE_GESTURE_DEFAULT;


    /**
     * Loads a default Level.
     */
    public Level(){}


    /**
     * Loads from an Intent.
     * @param loadFrom to load from.
     */
    public Level( Intent loadFrom ){
        this.healthSelf = loadFrom.getIntExtra( HEALTH_SELF_PATH, HEALTH_SELF_DEFAULT );
        this.healthEnemy = loadFrom.getIntExtra( HEALTH_ENEMY_PATH, HEALTH_ENEMY_DEFAULT );

        this.damageSelf = loadFrom.getIntExtra( DAMAGE_SELF_PATH, DAMAGE_SELF_DEFAULT );
        this.damageEnemy = loadFrom.getIntExtra( DAMAGE_ENEMY_PATH, DAMAGE_ENEMY_DEFAULT );

        this.round = getExtraString( loadFrom, ROUND_PATH, ROUND_DEFAULT );

        this.hasSkipButton = loadFrom.getBooleanExtra( HAS_SKIP_BUTTON_PATH, HAS_SKIP_BUTTON_DEFAULT );
        this.useRetryGestureProvider = loadFrom.getBooleanExtra( USE_RETRY_GESTURE_PROVIDER_PATH, USE_RETRY_GESTURE_PROVIDER_DEFAULT);
        this.singleGesture = getExtraString( loadFrom, SINGLE_GESTURE_PATH, SINGLE_GESTURE_DEFAULT );
        this.playSounds = loadFrom.getBooleanExtra( PLAY_SOUNDS_PATH, PLAY_SOUNDS_DEFAULT );
        this.recordingOn = loadFrom.getBooleanExtra(RECORDING_ON_PATH, RECORDING_ON_DEFAULT);
    }


    /**
     * Saves the current Level to the Bundle.
     * @param intent to use.
     */
    public void saveTo( Intent intent ){
        if( intent == null ) return;

        intent.putExtra( HEALTH_SELF_PATH, healthSelf );
        intent.putExtra( HEALTH_ENEMY_PATH, healthEnemy );
        intent.putExtra( DAMAGE_SELF_PATH, damageSelf );
        intent.putExtra( DAMAGE_ENEMY_PATH, damageEnemy );
        intent.putExtra( ROUND_PATH, round );
        intent.putExtra( HAS_SKIP_BUTTON_PATH, hasSkipButton );
        intent.putExtra( USE_RETRY_GESTURE_PROVIDER_PATH, useRetryGestureProvider );
        intent.putExtra( SINGLE_GESTURE_PATH, singleGesture );
        intent.putExtra( RECORDING_ON_PATH, recordingOn );
        intent.putExtra( PLAY_SOUNDS_PATH, playSounds );
    }



    public int getHealthSelf() {
        return healthSelf;
    }

    public Level setHealthSelf(int healthSelf) {
        this.healthSelf = healthSelf;
        return this;
    }

    public int getHealthEnemy() {
        return healthEnemy;
    }

    public Level setHealthEnemy(int healthEnemy) {
        this.healthEnemy = healthEnemy;
        return this;
    }

    public int getDamageSelf() {
        return damageSelf;
    }

    public Level setDamageSelf(int damageSelf) {
        this.damageSelf = damageSelf;
        return this;
    }

    public int getDamageEnemy() {
        return damageEnemy;
    }

    public Level setDamageEnemy(int damageEnemy) {
        this.damageEnemy = damageEnemy;
        return this;
    }

    public String getRound() {
        return round;
    }

    public Level setRound(String round) {
        this.round = round;
        return this;
    }

    public boolean isHasSkipButton() {
        return hasSkipButton;
    }

    public Level setHasSkipButton(boolean hasSkipButton) {
        this.hasSkipButton = hasSkipButton;
        return this;
    }

    public Level setUseRetryGestureProvider(boolean useRetryGestureProvider) {
        this.useRetryGestureProvider = useRetryGestureProvider;
        return this;
    }

    public boolean isUseRetryGestureProvider() {
        return useRetryGestureProvider;
    }

    public String getSingleGesture() {
        return singleGesture;
    }

    public Level setSingleGesture(String singleGesture) {
        this.singleGesture = singleGesture;
        return this;
    }

    public boolean isPlaySounds() {
        return playSounds;
    }

    public Level setPlaySounds(boolean playSounds) {
        this.playSounds = playSounds;
        return this;
    }

    public boolean isRecordingOn() {
        return recordingOn;
    }

    public Level setRecordingOn(boolean recordingOn) {
        this.recordingOn = recordingOn;
        return this;
    }
}
