package de.tu_darmstadt.informatik.gesturereader.game.fight.gui;

import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameModel;
import de.tu_darmstadt.informatik.gesturereader.utils.Animations;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;

/**
 * Created by Toby on 03.01.2018
 */
public class ExecutePlayerAnimation implements FightGameModel.GestureExecutedListener {

    /**
     * The session to use.
     */
    private final GestureRecSession session;

    /**
     * The view for self.
     */
    private final ImageView self, selfItem;

    /**
     * The view for the enemy.
     */
    private final ImageView enemy, enemyItem;

    /**
     * Shake animation.
     */
    private final Animation shake;

    /**
     * The map of Gesture -> Animation.
     */
    private final Map<String,Animation> gestureAnimationMap = new HashMap<>();

    /**
     * The map of Gesture -> Animation.
     */
    private final Map<String,Integer> itemImageMap = new HashMap<>();



    public ExecutePlayerAnimation( GestureRecSession session,
                                   ImageView self, ImageView enemy,
                                   ImageView selfItem, ImageView enemyItem
                                   ) {

        this.session = session;
        this.self = self;
        this.selfItem = selfItem;

        this.enemy = enemy;
        this.enemyItem = enemyItem;

        this.shake = AnimationUtils.loadAnimation( self.getContext(), R.anim.shake );


        //Register Animations:
        this.gestureAnimationMap.put( "jump", Animations.up( 200, 500 ) );
        this.gestureAnimationMap.put( "duck", Animations.scaleY( 0.5, 500 ) );
        this.gestureAnimationMap.put( "kick_left", Animations.rotateLeftOnce( 500 ) );
        this.gestureAnimationMap.put( "kick_right", Animations.rotateRightOnce( 500 ) );

        this.gestureAnimationMap.put( "idle", null );


        //Register Item images:
        this.itemImageMap.put( "attack", R.drawable.sword );
        this.itemImageMap.put( "block", R.drawable.shield );
    }

    @Override
    public void gestureExecuted( DateTime start, DateTime end, Gesture wanted, Gesture got ) {
        boolean correct = wanted != null && got != null && wanted.equals( got );
        Gesture counter = getCounter( wanted );

        MainThread.runOnMT( () -> execute( self, selfItem, got, !correct ) );
        MainThread.runOnMT( () -> execute( enemy, enemyItem, counter, correct ) );
    }


    /**
     * Do an Play execution!
     *
     * @param imagePerson to use.
     * @param gesture to execute.
     * @param damageAnimation if we should show damage.
     */
    private void execute( ImageView imagePerson, ImageView imageItem, Gesture gesture, boolean damageAnimation ) {
        AnimationSet animationSet = new AnimationSet( false );
        if( damageAnimation ) animationSet.addAnimation( shake );

        //Get the animation for the Gesture:
        String name = gesture == null ? "" : gesture.getName();
        Animation toUse = this.gestureAnimationMap.get( name );
        if( toUse != null ) animationSet.addAnimation( toUse );

        //Clear the Image or set the correct one:
        int id = itemImageMap.containsKey( name ) ? itemImageMap.get( name ) : 0;
        imageItem.setImageResource( id );

        //Reset if present:
        if( id != 0) imageItem.postDelayed( () -> imageItem.setImageResource( 0 ), 500L );

        //Only execute if we have any:
        if( !animationSet.getAnimations().isEmpty() ) {
            imagePerson.startAnimation( animationSet );
            if( id != 0 ) imageItem.startAnimation( animationSet );
        }
    }


    private Gesture getCounter( Gesture gesture ){
        String g = gesture.getName();
        GestureManager gm = session.getGestureManager();
        if( g.equalsIgnoreCase( "attack" ) ) return gm.getGesture( "block" );
        if( g.equalsIgnoreCase( "block" ) ) return gm.getGesture( "attack" );

        if( g.equalsIgnoreCase( "kick_left" ) ) return gm.getGesture( "kick_right" );
        if( g.equalsIgnoreCase( "kick_right" ) ) return gm.getGesture( "kick_left" );

        if( g.equalsIgnoreCase( "jump" ) ) return gm.getGesture( "duck" );
        if( g.equalsIgnoreCase( "duck" ) ) return gm.getGesture( "jump" );

        if( g.equalsIgnoreCase( "ide" ) ) return gm.getGesture( "idle" );

        return GestureManager.UNKNOWN();
    }

}
