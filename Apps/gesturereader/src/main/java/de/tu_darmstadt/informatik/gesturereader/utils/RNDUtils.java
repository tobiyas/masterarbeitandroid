package de.tu_darmstadt.informatik.gesturereader.utils;

import java.util.Collection;
import java.util.Random;

import static java8.util.stream.StreamSupport.stream;


public class RNDUtils {

    private static final Random rand = new Random();

    public static <T> T rand( Collection<T> elements ){
        if( elements == null || elements.isEmpty() ) return null;

        return stream( elements )
                .skip( rand.nextInt( elements.size() - 1 ) )
                .findFirst()
                .orElse( null );
    }

}
