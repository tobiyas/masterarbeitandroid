package de.tu_darmstadt.informatik.gesturereader.models;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.utils.ViewUtils;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import java8.util.stream.RefStreams;

/**
 * Created by Toby on 08.01.2018
 */
public class GestureFragment extends Fragment {

    private GestureRecSession session;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState ) {
        View root = inflater.inflate( R.layout.fragment_gestures, container, false );

        this.session = MainApplication.fromContext( this.getContext() ).getSession();

        ImageView create = root.findViewById( R.id.add_new );
        create.setOnClickListener( v -> createNew( root ) );
        updateList( root );

        return root;
    }

    /**
     * Creates a new Model
     * @param rootView to use for recreating
     */
    private void createNew( View rootView ) {
        ViewUtils.buildSaveInputDialog(
                this.getContext(),
                "New Gesture?",
                "Gesture",
                new ViewUtils.InputDialogCallback() {
                    @Override
                    public void acceptClicked(DialogInterface dialog, String name ) {
                        Gesture gesture = session.getGestureManager().createGesture( name );
                        Toast.makeText( getContext(),
                                "Gesture " + gesture.getName() + " created.",
                                Toast.LENGTH_LONG )
                                .show();

                        updateList( rootView );
                    }

                    //Not needed:
                    @Override public void cancelClicked(DialogInterface dialog) {}
                }).show();

    }


    private void updateList( View root ){
        ListView list = root.findViewById( R.id.list);
        if( list == null ) return;

        Context context = this.getContext();
        if( context == null ) return;


        String[] gestures = RefStreams.of( session.getGestureManager().getAllWithoutUnknownAsStringArray() ).sorted().toArray( String[]::new );
        String[] adapted = RefStreams.of( gestures ).map( g -> g + " " + getAmountSamples( g ) ).toArray( String[]::new );

        list.setAdapter( new ArrayAdapter<>( context, android.R.layout.simple_list_item_1, adapted ) );

        final GestureManager gm = session.getGestureManager();
        list.setOnItemLongClickListener( (a,s,i,_u) -> {
            Gesture gesture = gm.getGesture( gestures[i] );
            new AlertDialog.Builder( this.getContext() )
                    .setTitle( "Delete " + gesture.getName() )
                    .setPositiveButton( "YES", (v,_u2) -> { gm.deleteGesture( gesture ); updateList( root ); } )
                    .setNegativeButton( "NO", (v,_u2) -> v.dismiss() )
                    .create()
                    .show();
            return true;
        } );
    }


    /**
     * Posts the amount of samples.
     * @param gestureName to use.
     */
    private int getAmountSamples( String gestureName ){
        Gesture gesture = session.getGestureManager().getGesture( gestureName );
        File folder = gesture.getSaveDirectory();
        String[] list = folder.list();

        return list == null ? 0 : list.length;
    }

}
