package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Created by Tobias on 22.10.2017
 */

public class ResourcesChangedEvent extends Event {

    /**
     * The new amount of resources.
     */
    private final int newResources;


    public ResourcesChangedEvent(int newResources) {
        this.newResources = newResources;
    }


    public int getNewResources() {
        return newResources;
    }
}
