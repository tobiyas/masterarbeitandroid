package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Created by Toby on 24.10.2017
 */

public class GameSpeedUpEvent extends Event {

    /**
     * The value to increase the Gamespeed.
     */
    private final double increase;


    public GameSpeedUpEvent(double increase) {
        this.increase = increase;
    }


    public double getIncrease() {
        return increase;
    }
}
