package de.tu_darmstadt.informatik.gesturereader.game.fight.detect;

import android.widget.ImageView;
import android.widget.ProgressBar;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.List;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.utils.QuadConsumer;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.recorder.PermanentDetector;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 02.01.2018
 */

public class FightGesturePermanentDetector extends FightGestureDetector {


    /**
     * The perm detector.
     */
    private final PermanentDetector permDetector;



    public FightGesturePermanentDetector( ProgressBar progressBar, ImageView nextGestureView,
                                         ModelProxy model, GestureRecSession session,
                                         EvalSaver saver ) {
        super( progressBar, nextGestureView, model, session, saver );

        this.permDetector = new PermanentDetector( session, 2_000L, 500L, 5, 0.5d, model );
        this.permDetector.addCallback( (b,r) -> this.resultList.add( new Pair<>( b, r ) ) );
        this.permDetector.start();
    }


    private final List<Pair<SensorDataBatch,GestureResultWrapper>> resultList = new ArrayList<>();

    /**
     * Starts recording a new Gesture NOW!
     */
    private void startRecording( QuadConsumer<DateTime,DateTime,Gesture,Gesture> callback, Gesture gesture ) {
        mt.postDelayed( resultList::clear, FADE_TIME + 500 );

        //Start the recording:
        Duration tillEnd = Duration.ZERO.plus( Duration.millis( FADE_TIME + 1_000 ) ).plus( GESTURE_LENGTH );
        mt.postDelayed( () -> evalEnd( callback, gesture ) , tillEnd.getMillis() );

        //Init the timer for the progress bar:
        for( int i = 0; i <= 20; i++ ) {
            int timeOffset = i * 10;
            mt.postDelayed( () -> this.progressBar.setProgress( timeOffset ), ( timeOffset * 10 ) + FADE_TIME + 1_000 );
        }

        //Set the picture to do.
        mt.post( () -> setToPreviewGesture( gesture ) );
    }


    private void evalEnd( QuadConsumer<DateTime, DateTime, Gesture, Gesture> callback, Gesture wanted ) {
        List<Pair<SensorDataBatch,GestureResultWrapper>> copy = new ArrayList<>( resultList );
        Pair<SensorDataBatch,GestureResultWrapper> result = stream( copy )
                .filter( e -> wanted.getName().equalsIgnoreCase( "idle" ) || !e.getSecond().getTopResult().getFirst().getName().equalsIgnoreCase( "idle" ) )
                .max( (a,b) -> Double.compare( a.getSecond().getTopResult().getSecond(), b.getSecond().getTopResult().getSecond() ) )
                .orElse( null );


        //If empty -> This is bad!
        if( result == null ) {
            mt.post( () -> callback.accept( DateTime.now().minus( 2_000 ), DateTime.now(), wanted, GestureManager.UNKNOWN() ) );
            mt.post( () -> removePreview( false ) );
            return;
        }

        SensorDataBatch batch = result.getFirst();
        GestureResultWrapper wrapper = result.getSecond();
        mt.post( () -> callback.accept( batch.getStartTime(), batch.getEndTime(), wanted, wrapper.getTopResult().getFirst() ) );

        //Removes the preview:
        mt.post( () -> removePreview( wrapper.getTopResult().getFirst().equals( wanted ) ) );

        //Save the stuff if wanted:
        if( saver != null ) saver.save( batch, wanted );
    }


    /**
     * Sets the Gesture image passed to the Gui.
     *
     * @param gesture to set.
     */
    private void setToPreviewGesture( Gesture gesture ) {
        nextGestureView.setImageResource( gestureToId( gesture ) );
        nextGestureView.setImageAlpha( 0 );

        nextGestureView.startAnimation( fadeInAnimation );
        nextGestureView.postDelayed( () -> nextGestureView.setImageAlpha( 255 ),FADE_TIME );
    }


    /**
     * Tries to resolve picture IDs by the Name of the Gestures.
     *
     * @param gesture to resolve.
     * @return the used Gesture.
     */
    private int gestureToId( Gesture gesture ){
        if( gesture == null ) return R.drawable.cancel;

        String name = gesture.getName();
        if( name.equalsIgnoreCase( "kick_left" ) ) return R.drawable.kick_left;
        if( name.equalsIgnoreCase( "kick_right" ) ) return R.drawable.kick_right;

        if( name.equalsIgnoreCase( "attack" ) ) return R.drawable.attack;
        if( name.equalsIgnoreCase( "block" ) ) return R.drawable.block;

        if( name.equalsIgnoreCase( "jump" ) ) return R.drawable.jump;
        if( name.equalsIgnoreCase( "duck" ) ) return R.drawable.duck;

        if( name.equalsIgnoreCase( "idle" ) ) return R.drawable.idle;

        return R.drawable.cancel;
    }

    @Override
    public void stop(){
        this.permDetector.stop();
        this.permDetector.close();
    }


    /**
     * Removes the Preview.
     * @param wasCorrect if it was correct.
     */
    private void removePreview( boolean wasCorrect ) {
        this.nextGestureView.startAnimation( fadeOutAnimation );
        this.nextGestureView.setImageAlpha( 255 );

        mt.postDelayed( () -> this.nextGestureView.setImageAlpha( 0 ), FADE_TIME );
    }


    @Override
    public void startDetectingGesture(QuadConsumer<DateTime,DateTime,Gesture,Gesture> callback, Gesture gesture ) {
        startRecording( callback, gesture );
    }

}
