package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

/**
 * Event fired, when a Zombie reaches the End of the lane.
 * Created by Toby on 24.10.2017
 */

public class ZombieReachedEndOfLaneEvent extends Event {


    /**
     * The Zombie reaching the End.
     */
    private final Zombie zombie;

    /**
     * The lane the end was reached.
     */
    private final int lane;


    public ZombieReachedEndOfLaneEvent(Zombie zombie, int lane) {
        this.zombie = zombie;
        this.lane = lane;
    }


    public int getLane() {
        return lane;
    }

    public Zombie getZombie() {
        return zombie;
    }
}
