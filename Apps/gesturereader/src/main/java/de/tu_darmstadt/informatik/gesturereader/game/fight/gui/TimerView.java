package de.tu_darmstadt.informatik.gesturereader.game.fight.gui;

import android.content.Context;
import android.util.AttributeSet;

import de.tu_darmstadt.informatik.gesturereader.R;

/**
 * Created by Tobias on 01.01.2018
 */

public class TimerView extends android.support.v7.widget.AppCompatTextView {


    /**
     * The amount of seconds passed.
     */
    private int secondsPassed = 0;


    /**
     * The callback to use.
     */
    private final Runnable callback = this::tick;


    public TimerView(Context context) {
        super(context);

        this.setText( R.string.timer_default );
    }

    public TimerView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setText( R.string.timer_default );
    }

    public TimerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.setText( R.string.timer_default );
    }




    /**
     * Starts / Resumes the timer.
     */
    public void start(){
        this.postDelayed( callback, 1000 );
    }


    /**
     * Ticks the timer 1 second.
     */
    private void tick(){
        secondsPassed++;
        updateView();

        this.postDelayed( callback, 1000 );
    }

    /**
     * Pauses the timer.
     */
    public void pause(){
        this.removeCallbacks( callback );
    }

    /**
     * Resets the Timer to 0.
     */
    public void reset(){
        secondsPassed = 0;
        post( this::updateView );
    }


    /**
     * Does a simple update for the View.
     */
    private void updateView(){
        int seconds = secondsPassed % 60;
        int minutes = secondsPassed / 60;

        StringBuilder text = new StringBuilder();
        if( minutes < 10 ) text.append( "0" );
        text.append( minutes ).append( ":" );
        if( seconds < 10 ) text.append( "0" );
        text.append(seconds);

        this.setText( text );
    }


    /**
     * Gets the already passed seconds.
     * @return the passed seconds
     */
    public int getSecondsPassed(){
        return secondsPassed;
    }

}
