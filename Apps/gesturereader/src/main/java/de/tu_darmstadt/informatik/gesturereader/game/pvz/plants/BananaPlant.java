package de.tu_darmstadt.informatik.gesturereader.game.pvz.plants;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;

import static de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem.TICKS_PER_SECOND;

/**
 * Created by Tobias on 22.10.2017
 */

public class BananaPlant extends Plant {


    public BananaPlant( int lane, int slot ) {
        super( lane, slot, "Banana", R.drawable.banana, 1, 500, TICKS_PER_SECOND, 200, 10);
    }

    public BananaPlant(){
        this(-1,-1);
    }


    @Override
    protected void tickIntern( EventSystem eventSystem ) {
        //Not needed for now!
    }

}
