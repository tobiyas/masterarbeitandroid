package de.tu_darmstadt.informatik.gesturereader.game.fight.level;

/**
 * Created by Toby on 04.01.2018
 */

public class Levels {



    public static Level level1(){
        return new Level()
                .setPlaySounds( true )
                .setRecordingOn( true )
                .setHealthEnemy( 150 )
                .setRound( "Round 1" );
    }


    public static Level level2(){
        return new Level()
                .setPlaySounds( true )
                .setRecordingOn( true )
                .setHealthSelf( 150 )
                .setHealthEnemy( 250 )
                .setRound( "Round 2" );
    }


    public static Level level3(){
        return new Level()
                .setPlaySounds( true )
                .setRecordingOn( true )
                .setHealthSelf( 200 )
                .setHealthEnemy( 300 )
                .setRound( "Round 3" );
    }


    public static Level training1(){
        return new Level()
                .setPlaySounds( true )
                .setRecordingOn( false )
                .setRound( "Training 1" )
                .setDamageEnemy( 0 )
                .setDamageSelf( 0 )
                .setUseRetryGestureProvider( true )
                .setHasSkipButton( true );
    }

    public static Level training2(){
        return new Level()
                .setPlaySounds( true )
                .setRecordingOn( false )
                .setRound( "Training 2" )
                .setDamageEnemy( 0 )
                .setDamageSelf( 0 )
                .setHasSkipButton( true );
    }


    public static Level trainingSingleGesture( String gesture ){
        return new Level()
                .setPlaySounds( false )
                .setRecordingOn( false )
                .setRound( gesture )
                .setSingleGesture( gesture )
                .setDamageEnemy( 0 )
                .setDamageSelf( 0 )
                .setUseRetryGestureProvider( true )
                .setHasSkipButton( true );
    }
}
