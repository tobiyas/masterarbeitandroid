package de.tu_darmstadt.informatik.gesturereader.game.pvz.gui;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TickEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieDamagedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieDamagesPlantEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieDiedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieSpawnEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Blood;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

/**
 * Created by Toby on 23.10.2017
 */

public class ZombieLaneView extends View implements EventListener {

    /**
     * the white paint.
     */
    private final Paint whitePaint;

    /**
     * The lane this represents.
     */
    private int lane = -1;

    /**
     * The Zombies to draw.
     */
    private final Set<Zombie> toDraw = new HashSet<>();

    /**
     * The EventSystem to hook to.
     */
    private EventSystem eventSystem = null;

    /**
     * A set of cached damaged Zombies.
     */
    private Set<Zombie> zombieDamaged = new HashSet<>();

    /**
     * A set of cached damaged plants.
     */
    private Set<Plant> plantDamaged = new HashSet<>();

    /**
     * The cache map of Class -> Bitmap.
     */
    private Map<Class<?>,Bitmap> bitmapCache;



    public ZombieLaneView( Context context ) {
        super( context );

        this.setWillNotDraw( false );

        whitePaint = new Paint();
        whitePaint.setColor( Color.WHITE );
    }


    public ZombieLaneView( Context context, AttributeSet set ) {
        super( context, set );

        this.setWillNotDraw( false );

        whitePaint = new Paint();
        whitePaint.setColor( Color.WHITE );
    }


    /**
     * Inits the View. This has to be called before something can be done.
     * @param row to use.
     * @param eventSystem the eventSystem to register to.
     */
    public void init( int row, EventSystem eventSystem, Map<Class<?>,Bitmap> bitmapCache ){
        //Already inited! Do not double init this!
        if( row < 0 ) return;

        this.lane = row;
        this.eventSystem = eventSystem;
        this.bitmapCache = bitmapCache;

        eventSystem.addListener( this );
    }


    @EventHandler
    public void zombieSpawned( ZombieSpawnEvent event ){
        if( event.getRow() != this.lane) return;
        toDraw.add( event.getZombie() );
        invalidate();
    }


    @EventHandler
    public void zombieDied( ZombieDiedEvent event ){
        if( event.getLane() != this.lane) return;

        toDraw.remove( event.getZombie() );
    }

    @EventHandler
    public void onZombieDamage( ZombieDamagedEvent event ){
        if( event.getDamaged().getLane() != this.lane) return;
        if( event.getDamage() <= 0) return;

        this.zombieDamaged.add( event.getDamaged() );
    }

    @EventHandler
    public void plantDamaged( ZombieDamagesPlantEvent event ){
        if( event.getZombie().getLane() != this.lane) return;
        if( event.getDamage() <= 0) return;

        this.plantDamaged.add( event.getPlant() );
    }


    @EventHandler
    public void onTick( TickEvent event ){
        this.invalidate();
    }



    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        setMeasuredDimension( widthMeasureSpec, heightMeasureSpec );
    }


    @Override
    protected void onDraw( Canvas canvas ) {
        super.onDraw(canvas);

        //We are not inited yet!
        if( lane < 0 ) return;

        float maxHeight = getHeight();

        //Draw the Zombies:
        for( Zombie zombie : toDraw ){
            boolean damaged = this.zombieDamaged.contains( zombie );

            //Get the cached Bitmap:
            Bitmap cachedBitmap = getCachedBitmap( zombie.getClass(), zombie.getResourceID() );
            if( cachedBitmap == null )continue;

            //Only do more if needed:
            float position = (float) ( ( zombie.getPosition() / Zombie.getMaxPosition() ) * maxHeight);
            canvas.drawBitmap( cachedBitmap, 0, position, whitePaint );

            //if zombieDamaged, overlay blood effect:
            if( damaged ) {
                Bitmap cachedBlood = getCachedBitmap( Blood.class, R.drawable.blood );
                if( cachedBlood != null ) canvas.drawBitmap( cachedBlood, 0, position, whitePaint );
            }
        }


        //Draw plant damage effect:
        for( Plant plant : plantDamaged ){
            float position = (float) ( plant.getSlot() / Zombie.getMaxPosition() ) * getHeight();
            Bitmap cachedBlood = getCachedBitmap( Blood.class, R.drawable.blood );

            if( cachedBlood != null ) canvas.drawBitmap( cachedBlood, 0, position, whitePaint );
        }

        //Clear the damaged, since thex are drawn now:
        zombieDamaged.clear();
        plantDamaged.clear();
    }

    /**
     * Gets the Cached Result of the Bitmap.
     * If not present, caches the Result.
     *
     * @param clazz to use.
     * @param reloadID to use when not present.
     * @return the cached Zombie.
     */
    private Bitmap getCachedBitmap( Class<?> clazz, int reloadID ){
        if( bitmapCache == null ) return null;

        Bitmap cached = bitmapCache.get( clazz );
        if( cached == null ) {
            Bitmap tmp = BitmapFactory.decodeResource( getResources(), reloadID );
            cached = Bitmap.createScaledBitmap( tmp, getWidth(), getWidth(), false );
            bitmapCache.put( clazz, cached );
        }

        return cached;
    }

    /**
     * Does an init on the Lane.
     * @param viewProvider to use for getting the View.
     * @param lane the lane number.
     * @param viewID the ID of the View.
     * @param eventSystem the event System to hook.
     */
    public static void initLane( ViewProvider viewProvider, int lane, int viewID, EventSystem eventSystem, Map<Class<?>,Bitmap> bitmapCache ) {
        View view = viewProvider.provideView( viewID );
        if( view instanceof ZombieLaneView ){
            ZombieLaneView laneView = ( ZombieLaneView ) view;
            laneView.init( lane, eventSystem, bitmapCache );
        }
    }



}