package de.tu_darmstadt.informatik.gesturereader.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.detect.DebugViewAdapter;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;

/**
 * Created by Toby on 02.09.2017
 */

public abstract class OwnActivity extends AppCompatActivity {

    /**
     * The debug adapter to use.
     */
    protected DebugViewAdapter debugAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.debugAdapter = new DebugViewAdapter( this );

        super.onCreate(savedInstanceState);
    }


    @Override
    protected void onResume() {
        super.onResume();

        debugAdapter.registerAdapter();
    }

    @Override
    protected void onPause() {
        super.onPause();

        debugAdapter.unregisterAdapter();
    }


    /**
     * A wrapper for the Back pressed method.
     * @param unused pretty much unused.
     */
    public void back(View unused){
        this.onBackPressed();
    }

    /**
     * Interact with the view adapter.
     * @return
     */
    public DebugViewAdapter getDebugAdapter() {
        return debugAdapter;
    }

    /**
     * Gets the current Session.
     * @return the current session.
     */
    public GestureRecSession session(){
        return MainApplication.fromContext( this ).getSession();
    }

}
