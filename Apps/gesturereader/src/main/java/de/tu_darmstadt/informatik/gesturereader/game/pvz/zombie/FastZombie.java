package de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie;

import de.tu_darmstadt.informatik.gesturereader.R;

/**
 * Created by Tobias on 25.10.2017
 */

public class FastZombie extends Zombie {


    public FastZombie( int lane ) {
        super( R.drawable.zombie_fast, lane, 15, Zombie.DEFAULT_MOVEMENT_SPEED * 2, 4, 1 );
    }


}
