package de.tu_darmstadt.informatik.gesturereader.record;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnActivity;
import de.tu_darmstadt.informatik.gesturereader.utils.ViewUtils;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.recorder.DataRecorder;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;
import java8.util.stream.IntStreams;

import static java8.util.stream.StreamSupport.stream;

/**
 * The Activity to use for recording Gestures.
 *
 * Created by Toby on 30.08.2017
 */
public class GestureRecorderActivity extends OwnActivity {

    private final Duration recordTime = Duration.standardSeconds( 2 );
    private GestureRecSession session;
    private Gesture fixedRecordGesture = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.session = MainApplication.fromContext( this ).getSession();

        setContentView(R.layout.activity_record);

        findViewById( R.id.button ).setOnClickListener(this::click);
        findViewById( R.id.back ).setOnLongClickListener( this::showAmount );

        CheckBox debugBox = findViewById( R.id.debug_view );
        debugBox.setOnCheckedChangeListener( (a,b) -> { if(b) debugAdapter.show(); else debugAdapter.hide(); } );
    }

    private boolean showAmount( View view ) {
        String user = session.getUserManager().getCurrentUserName();
        String[] items = stream( session.getGestureManager().getAllWithoutUnknown() )
                .map( g -> {
                    int amount = session.getSampleManager().query()
                            .addGestures(g)
                            .addUsers(user)
                            .applyFilter()
                            .size();
                    return new Pair<>( g,amount);
                } )
                .filter( e -> e.getSecond() > 0 )
                .map( e -> e.getFirst() + " - " + e.getSecond() )
                .sorted()
                .toArray( String[]::new );

        new AlertDialog.Builder( this )
                .setTitle( "You have:" )
                .setItems( items, (g,i) -> {})
                .create().show();

        return true;
    }

    private void click(View view) {
        view.setEnabled( false );

        view.postDelayed( () -> updateProgress( DateTime.now() ), 500 );
    }

    private void updateProgress( DateTime startTime ){
        ProgressBar bar = findViewById(R.id.progress);

        //calc percent:
        Duration taken = new Duration(startTime, DateTime.now());
        double val = (double) taken.getMillis() / (double) recordTime.getMillis();
        val = Math.max(0 ,Math.min( 1, val ));

        bar.setProgress((int) (val*100D));
        if(val >= 1){
            done( startTime );
        }else{
            bar.postDelayed(() -> updateProgress( startTime ), 50);
        }
    }

    /**
     * We are done!
     * @param startTime to use.
     */
    private void done( DateTime startTime) {
        DateTime endTime = startTime.plus( recordTime );

        if( fixedRecordGesture == null ){
            session.Dialogs().OpenGestureSingleSelectionDialog( this, g -> gestureSelectedAndSave( startTime, endTime, g ) );
            findViewById(R.id.button).setEnabled( true );
        }else{
            gestureSelectedAndSave( startTime, endTime, fixedRecordGesture );
        }
    }


    private void gestureSelectedAndSave( DateTime start, DateTime end, Gesture gesture ){
        DataRecorder recorder = session.getDataRecorder();
        recorder.startCollectingDataAndSaveAsSample( start, end, Duration.millis( 1000 ), this::completed, gesture, MainThread.MTHandler() );
    }


    private void completed( SampleProxy proxy ) {
        String g = proxy == null ? "" : proxy.getGesture().getName();
        Toast.makeText( this, proxy == null ? "FAILED" : ( "Saved as " + g ), Toast.LENGTH_LONG ).show();
    }


    public void startBatch( View view ){
        session().Dialogs().OpenGestureSingleSelectionDialog( this, this::batchSelectedGesture );
    }

    private void batchSelectedGesture( Gesture gesture ){
        ViewUtils.buildInputDialogNumber(
                this,
                "Amount?",
                "10",
                "Next",
                "Cancel",
                a -> batchSelectTimeBetween( gesture, a ),
                () -> {} )
            .show();
    }

    private void batchSelectTimeBetween( Gesture gesture, long number ){
        ViewUtils.buildInputDialogNumber(
                this,
                "Time between in MS?",
                "500",
                "Start",
                "Cancel",
                a -> startBatchRecord( gesture, number, a ),
                () -> {} )
                .show();
    }


    private void startBatchRecord( Gesture gesture, long amount, long milliBetween ) {
        final long fMilliBetween = Math.max( 500, milliBetween );

        if( amount <= 0 ){
            Toast.makeText( this, amount + " has to be positive!", Toast.LENGTH_LONG ).show();
            return;
        }

        this.findViewById( R.id.button ).setEnabled( false );
        final Button button = this.findViewById( R.id.batch );
        button.setEnabled( false );

        this.fixedRecordGesture = gesture;


        Runnable reenable = () -> {
            this.findViewById( R.id.button ).setEnabled( true );
            this.findViewById( R.id.batch ).setEnabled( true );

            button.setText( "Start Batch Recording" );
        };


        IntStreams.range(0,(int)amount)
            .forEach( i -> {
                long time = i * ( recordTime.getMillis() + 1000L + fMilliBetween );
                MainThread.runOnMTLater( () -> {
                    click( findViewById( R.id.button ) );
                    button.setText( "Run: " + (i+1) + "/" + amount );
                }, time );
            });

        MainThread.runOnMTLater( reenable, ( recordTime.getMillis() + 1500L ) * amount );
    }
}
