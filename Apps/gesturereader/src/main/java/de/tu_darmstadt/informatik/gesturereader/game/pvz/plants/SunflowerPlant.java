package de.tu_darmstadt.informatik.gesturereader.game.pvz.plants;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.AddResourceEvent;

import static de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem.TICKS_PER_SECOND;

/**
 * Created by Tobias on 22.10.2017
 */

public class SunflowerPlant extends Plant {


    private final int SUN_EVERY_TICKS = TICKS_PER_SECOND * 5;


    public SunflowerPlant( int lane, int slot ) {
        super( lane, slot, "SunFlower", R.drawable.sun_flower, 1, 50, 10000, 0, 0);
    }

    public SunflowerPlant(){
        this( -1, -1 );
    }


    @Override
    protected void tickIntern( EventSystem eventSystem ) {
        if ( currentTick % TICKS_PER_SECOND == 0 ){
            eventSystem.fireEvent( new AddResourceEvent( 2 ) );
        }
    }

}
