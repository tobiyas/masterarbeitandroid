package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

/**
 * Created by Toby on 23.10.2017
 */

public class ZombieDiedEvent extends Event {

    /**
     * The Row the Zombie moved.
     */
    private final int lane;


    private final Zombie zombie;

    public ZombieDiedEvent( int lane, Zombie zombie ) {
        this.lane = lane;
        this.zombie = zombie;
    }

    public ZombieDiedEvent(Zombie zombie) {
        this( zombie.getLane(), zombie );
    }



    public int getLane() {
        return lane;
    }

    public Zombie getZombie() {
        return zombie;
    }
}
