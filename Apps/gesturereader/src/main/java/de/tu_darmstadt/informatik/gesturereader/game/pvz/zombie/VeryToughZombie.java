package de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem;

/**
 * Created by Tobias on 25.10.2017
 */

public class VeryToughZombie extends Zombie {

    public VeryToughZombie(int lane ) {
        super( R.drawable.zombie_very_tough, lane, 40, Zombie.DEFAULT_MOVEMENT_SPEED, TickSystem.TICKS_PER_SECOND, 1 );
    }
}
