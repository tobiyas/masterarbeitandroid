package de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie;

import de.tu_darmstadt.informatik.gesturereader.R;

import static de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem.TICKS_PER_SECOND;

/**
 * Created by Toby on 24.10.2017
 */

public class DefaultZombie extends Zombie {


    private static final double MOVEMENT_SPEED = 0.01;
    private static final double MAX_HEALTH = 10;


    public DefaultZombie( int row ) {
        super( rand( R.drawable.zombie_1, R.drawable.zombie_female ), row, MAX_HEALTH, MOVEMENT_SPEED, TICKS_PER_SECOND, 4 );
    }


}
