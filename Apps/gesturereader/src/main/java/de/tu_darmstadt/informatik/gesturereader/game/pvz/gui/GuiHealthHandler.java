package de.tu_darmstadt.informatik.gesturereader.game.pvz.gui;

import android.view.View;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerHealthChangedEvent;

/**
 * Class that handles the Gui for the Health.
 *
 * Created by Toby on 24.10.2017
 */

public class GuiHealthHandler implements EventListener {

    /**
     * The Provider for Views.
     */
    private final ViewProvider viewProvider;


    public GuiHealthHandler(ViewProvider viewProvider, EventSystem eventSystem) {
        this.viewProvider = viewProvider;

        eventSystem.addListener( this );
    }


    @EventHandler
    public void onHealthChanged( PlayerHealthChangedEvent event ){
        View h1 = viewProvider.provideView( R.id.health_1 );
        View h2 = viewProvider.provideView( R.id.health_2 );
        View h3 = viewProvider.provideView( R.id.health_3 );

        int health = event.getNewHealth();
        h1.setVisibility( health >= 1 ? View.VISIBLE : View.INVISIBLE );
        h2.setVisibility( health >= 2 ? View.VISIBLE : View.INVISIBLE );
        h3.setVisibility( health >= 3 ? View.VISIBLE : View.INVISIBLE );
    }


}
