package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Created by Tobias on 09.11.2017
 */

public class AddResourceEvent extends Event {

    /**
     * The amount of resources to add.
     */
    private final int amount;


    /**
     * Adds the Resources passed to the system.
     * This is followed by an Update event.
     *
     * @param amount to add.
     */
    public AddResourceEvent( int amount ) {
        this.amount = amount;
    }

    /**
     * The Amount added.
     *
     * @return the amount added.
     */
    public int getAmount() {
        return amount;
    }
}
