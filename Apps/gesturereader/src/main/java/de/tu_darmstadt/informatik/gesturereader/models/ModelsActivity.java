package de.tu_darmstadt.informatik.gesturereader.models;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnFragmentActivity;

/**
 * Created by Toby on 16.10.2017
 */

public class ModelsActivity extends OwnFragmentActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.activity_models );

        ViewPager pager = findViewById( R.id.pager );
        pager.setAdapter( new OwnPageListener( getSupportFragmentManager() ) );
    }


    private class OwnPageListener extends FragmentStatePagerAdapter {

        OwnPageListener(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch ( position ){
                case 0 : return new ModelsFragment();
                case 1 : return new GestureFragment();
                case 2 : return new UsersFragment();
                case 3 : return new EvalFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }


}
