package de.tu_darmstadt.informatik.gesturereader.detect;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.models.EvalFragment.IgnoringSeekBarChangedListener;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnActivity;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResult;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.recorder.PermanentDetector;
import java8.util.stream.IntStreams;
import java8.util.stream.RefStreams;

import static de.tu_darmstadt.informatik.gesturereader.utils.RNDUtils.rand;

/**
 * Created by Toby on 20.12.2017
 */
public class ActivityPermDetect extends OwnActivity {

    /**
     * The used Perm detector.
     */
    private PermanentDetector permanentDetector;


    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_perm_detect );

        //Init the Timer at the Top.
        TextView timeView = findViewById( R.id.time );
        updateTime( timeView );


        //Get the Model and create it.
        String modelName = getIntent().getStringExtra( "model" );
        GestureRecSession session = MainApplication.fromContext(this).getSession();
        ModelProxy model = session.getModelManager().getForName( modelName );


        //Create the Perm-Detector.
        this.permanentDetector = new PermanentDetector( session, 2_000L, 500L, model );
        this.permanentDetector.addCallback( this::onCallback );
        this.permanentDetector.skipSameGestureFor( Duration.millis( 1_000L ) );


        //Set to auto-scroll the Text.
        TextView permStatus = findViewById( R.id.detect_results );
        permStatus.setMovementMethod( new ScrollingMovementMethod() );


        //Init the Mode spinner:
        final String[] modes = RefStreams.of( PermanentDetector.Mode.values() ).map( Enum::name ).toArray( String[]::new );
        Spinner spinner = findViewById( R.id.mode_spinner );
        spinner.setAdapter( new ArrayAdapter<>( this, android.R.layout.simple_list_item_1, modes ) );

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) { permanentDetector.setDetectMode(PermanentDetector.Mode.valueOf( modes[i] ) ); }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {}
        });


        //Init the Seek bar for Accuracy:
        SeekBar seek = findViewById( R.id.seek_accuracy );
        seek.setProgress( 50 );
        seek.setOnSeekBarChangeListener( new IgnoringSeekBarChangedListener( i -> permanentDetector.changeMinAccuracy( ((double)i) / 100d ) ) );
    }

    /**
     * We got a callback.
     *
     * @param batch that was created
     * @param results that was detected
     */
    private void onCallback( SensorDataBatch batch, GestureResultWrapper results ) {
        DateTime end = batch.getEndTime();
        GestureResult result = results.getTopResult();

        Gesture gesture = result.getFirst();
        double good = result.getSecond();

        String text = gesture + " - " + ( (int)( good*100d ) ) + "% - " + format( end );
        TextView statusView = findViewById( R.id.status );
        statusView.setText( text );

        TextView permStatus = findViewById( R.id.detect_results );
        permStatus.append( "\nPerm- " + text );
    }


    /**
     * Updates the Time in the Time-View.
     * @param view to use.
     */
    private void updateTime( TextView view ) {
        view.setText( format( DateTime.now() ) );
        view.postDelayed( () -> updateTime( view ), 50 );
    }

    /**
     * Formats a new String.
     * @param time to use.
     * @return the created String.
     */
    private String format( DateTime time ){
        int minutes = time.getMinuteOfHour();
        int seconds = time.getSecondOfMinute();
        int millis = time.getMillisOfSecond();

        StringBuilder builder = new StringBuilder();
        if( minutes < 10 ) builder.append( "0" );
        builder.append( minutes ).append( ":" );

        if( seconds < 10 ) builder.append( "0" );
        builder.append( seconds ).append( ":" );

        if( millis < 100 ) builder.append( "0" );
        if( millis < 10 ) builder.append( "0" );
        builder.append( millis );

        return builder.toString();
    }


    /**
     * Bind to the Start-Stop view.
     * @param view to bind to.
     */
    public void startStop( View view ){
        if( !( view instanceof Button ) ) return;

        boolean running = this.permanentDetector.isRunning();
        if( running ) this.permanentDetector.stop();
        else this.permanentDetector.start();

        //invert, since it has changed afterwards:
        running = !running;

        Button button = (Button) view;
        button.setText( running ? "STOP" : "START" );

        TextView statusView = findViewById( R.id.status );
        statusView.setText( running ? "Starting..." : "Stopped" );
    }


    /**
     * Button was pressed for simple detect.
     * @param view that was pressed.
     */
    public void simpleDetect( View view ){
        Button button = (Button) view;
        button.setEnabled( false );

        final ProgressBar progressBar = findViewById( R.id.progress );
        progressBar.setProgress( 0 );

        Gesture toSee = rand( permanentDetector.getDetector().getSupportedGestures() );
        button.setText( toSee.toString() );

        //Update the progress bar:
        IntStreams.range( 0, 20 )
            .forEach( i -> progressBar.postDelayed( () -> progressBar.setProgress( progressBar.getProgress() + 1 ), i * 100 ) );

        //Do a manual Detect:
        progressBar.postDelayed( () -> {
            progressBar.setProgress( 0 );
            button.setText( "DETECT" );
            button.setEnabled( true );

            detectNow( toSee );
        }, 2_050 );
    }


    private void detectNow( Gesture gesture ){
        SensorDataBatch batch = session().getDataRecorder().getDataNow( DateTime.now().minus(Duration.millis( 2000 ) ), DateTime.now() );
        GestureResultWrapper wrapper = permanentDetector.getDetector().analyseGesture( batch );

        GestureResult top = wrapper.getTopResult();

        TextView text = findViewById( R.id.detect_results );
        String t = "w: " + gesture + " G: " + top.getFirst() + " - " + ( (int)( top.getSecond()*100d ) ) + "% - " + format( batch.getEndTime() );
        text.append( "\n" + t );
    }


    @Override
    protected void onStop() {
        this.permanentDetector.close();

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        this.permanentDetector.close();

        super.onBackPressed();
    }
}
