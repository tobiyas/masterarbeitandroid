package de.tu_darmstadt.informatik.gesturereader.game.pvz.input;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.InputEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.gui.ViewProvider;

/**
 * Created by Tobias on 22.10.2017
 */

public class DebugGuiInputController {

    /**
     * The Eventsystem to call stuff on.
     */
    private final EventSystem eventSystem;


    public DebugGuiInputController( EventSystem eventSystem, ViewProvider viewProvider ) {
        this.eventSystem = eventSystem;

        viewProvider.provideView( R.id.go_left ).setOnClickListener( v -> fire( InputCommands.GO_LEFT ) );
        viewProvider.provideView( R.id.go_right ).setOnClickListener( v -> fire( InputCommands.GO_RIGHT ) );
        viewProvider.provideView( R.id.go_down ).setOnClickListener( v -> fire( InputCommands.GO_DOWN ) );
        viewProvider.provideView( R.id.go_up ).setOnClickListener( v -> fire( InputCommands.GO_UP ) );

        viewProvider.provideView( R.id.dig ).setOnClickListener( v -> fire( InputCommands.DIG ) );
        //viewProvider.provideView( R.id.place ).setOnClickListener( v -> fire( InputCommands.PLANT ) );

        //viewProvider.provideView( R.id.next_selection ).setOnClickListener( v -> fire( InputCommands.NEXT_PLANT ) );
        //viewProvider.provideView( R.id.prev_selection ).setOnClickListener( v -> fire( InputCommands.PREV_PLANT ) );
    }



    private void fire( InputCommands command ){
        eventSystem.fireEvent( new InputEvent( command ));
    }





}
