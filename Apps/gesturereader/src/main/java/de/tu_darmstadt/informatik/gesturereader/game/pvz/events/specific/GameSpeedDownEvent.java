package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Created by Toby on 24.10.2017
 */

public class GameSpeedDownEvent extends Event {

    /**
     * The value to decrease the Gamespeed.
     */
    private final double decrease;


    public GameSpeedDownEvent(double decrease) {
        this.decrease = decrease;
    }


    public double getDecrease() {
        return decrease;
    }
}
