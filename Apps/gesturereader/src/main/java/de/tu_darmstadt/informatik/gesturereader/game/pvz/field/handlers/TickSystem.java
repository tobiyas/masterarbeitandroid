package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers;

import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.GameSpeedDownEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.GameSpeedUpEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerLostGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerWonGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ShowDebugToastEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TickEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TimerContinueGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TimerPauseGameEvent;


/**
 * Created by Tobias on 22.10.2017
 */

public class TickSystem implements EventListener {


    private static final int START_AFTER_SECONDS = 10;
    public static final int TICKS_PER_SECOND = 5;


    /**
     * The Handlet to run on MT.
    */
    private final Handler handler = new Handler(Looper.getMainLooper() );

    /**
     * The system for calling Events.
     */
    private final EventSystem eventSystem;

    /**
     * The function to call on tick.
     */
    private final Runnable tickRunnable;

    /**
     * If the System is currently running.
     */
    private boolean running = false;

    /**
     * The current Tick
     */
    private int tick = 0;

    /**
     * The current Game-Speed
     */
    private double gameSpeed = 1;


    public TickSystem( EventSystem eventSystem ) {
        this.eventSystem = eventSystem;
        this.tickRunnable = this::tick;

        //Start the ticker:
        running = true;
        scheduleTick( START_AFTER_SECONDS * 1000 );

        eventSystem.addListener( this );
    }

    /**
     * Does a tick and fires that to the System.
     */
    private void tick() {
        //First schedule new Tick:
        double nextDelay = 1000d / ( (double) TICKS_PER_SECOND * gameSpeed );
        scheduleTick( (long) nextDelay );

        //Then fire Event:
        tick++;
        eventSystem.fireEvent( new TickEvent( tick ) );
    }

    /**
     * Schedules a Tick in x Milliseconds.
     * @param delay MS to wait.
     */
    private void scheduleTick( long delay ){
        handler.postDelayed( tickRunnable, delay);
    }

    /**
     * Stops the Ticks.
     */
    public void pause(){
        if(!running) return;

        handler.removeCallbacks( tickRunnable );
        running = false;
    }


    /**
     * Resumes the Ticks.
     */
    public void resume(){
        if(running) return;

        scheduleTick( 50 );
        running = true;
    }


    @EventHandler
    public void pauseEvent( TimerPauseGameEvent event ){
        pause();
    }

    @EventHandler
    public void continueEvent( TimerContinueGameEvent event ){
        resume();
    }

    @EventHandler
    public void playerWonEvent( PlayerWonGameEvent event ){
        pause();
    }

    @EventHandler
    public void playerLostEvent( PlayerLostGameEvent event ){
        pause();
    }


    @EventHandler
    public void gameFaster( GameSpeedUpEvent event ){
        this.gameSpeed *= event.getIncrease();

        eventSystem.fireEvent( new ShowDebugToastEvent( "GameSpeed: " + gameSpeed, Toast.LENGTH_LONG ) );
    }

    @EventHandler
    public void gameSlower( GameSpeedDownEvent event ){
        this.gameSpeed /= event.getDecrease();

        eventSystem.fireEvent( new ShowDebugToastEvent( "GameSpeed: " + gameSpeed, Toast.LENGTH_LONG ) );
    }

}
