package de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem;

/**
 * Created by Tobias on 25.10.2017
 */

public class ToughZombie extends Zombie {

    public ToughZombie( int lane ) {
        super( R.drawable.zombie_tough, lane, 20, Zombie.DEFAULT_MOVEMENT_SPEED, TickSystem.TICKS_PER_SECOND, 1 );
    }
}
