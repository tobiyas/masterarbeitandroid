package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;

/**
 * Created by Tobias on 22.10.2017
 */

public class SelectionChangedEvent extends Event {

    /**
     * The new Position.
     */
    private final Pair<Integer,Integer> newPosition;


    public SelectionChangedEvent(Pair<Integer, Integer> newPosition) {
        this.newPosition = newPosition;
    }

    public SelectionChangedEvent( int x, int y ) {
        this( new Pair<>( x, y ) );
    }


    public Pair<Integer, Integer> getNewPosition() {
        return newPosition;
    }


    public int getX(){
        return newPosition.getFirst();
    }

    public int getY(){
        return newPosition.getSecond();
    }
}
