package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;

/**
 * Created by Toby on 24.10.2017
 */

public class PlantShootsOnLaneEvent extends Event {

    /**
     * The Lane this is shot on.
     */
    private final int lane;

    /**
     * The plant shooting.
     */
    private final Plant plant;

    /**
     * The Range to use.
     */
    private final double range;


    public PlantShootsOnLaneEvent( int lane, Plant plant, double range ) {
        this.lane = lane;
        this.plant = plant;
        this.range = range;
    }

    public int getLane() {
        return lane;
    }

    public Plant getPlant() {
        return plant;
    }

    public double getRange() {
        return range;
    }
}
