package de.tu_darmstadt.informatik.gesturereader.game.pvz.plants;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;

/**
 * Created by Tobias on 22.10.2017
 */

public class FriesPlant extends Plant {


    public FriesPlant(  int lane, int slot ) {
        super( lane, slot, "Fries", R.drawable.fries, 400, 120, 0, 0, 0);
    }


    public FriesPlant(){
        this( -1, -1 );
    }


    @Override
    protected void tickIntern( EventSystem eventSystem ) {
        //Not needed for now!
    }

}
