package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * An Event that tells the Movement detection to pause if running.
 *
 * Created by Toby on 06.11.2017
 */

public class PauseMovementDetectionEvent extends Event {}
