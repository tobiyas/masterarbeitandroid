package de.tu_darmstadt.informatik.gesturereader.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.widget.EditText;

import java8.util.function.Consumer;

/**
 * Created by Toby on 16.10.2017
 */

public class ViewUtils {


    /**
     * Creates a dialog to open.
     *
     * @param context to use.
     * @param title to set.
     * @param defaultText to set in the input box.
     * @param callback to call when done.
     *
     * @return the generated Dialog.
     */
    public static Dialog buildSaveInputDialog( Context context, String title, String defaultText, InputDialogCallback callback ){
        return buildInputDialog( context, title, defaultText, "SAVE", "ABORT", callback );
    }


    /**
     * Creates a dialog to open.
     *
     * @param context to use.
     * @param title to set.
     * @param defaultText to set in the input box.
     * @param positiveText the text on the positive button
     * @param negativeText the text on the negative button.
     * @param callback to call when done.
     *
     * @return the generated Dialog.
     */
    public static Dialog buildInputDialog( Context context, String title, String defaultText,
                                           String positiveText, String negativeText,
                                           InputDialogCallback callback ){

        final EditText edit = new EditText( context );
        edit.setHint( defaultText );

        return new AlertDialog.Builder( context )
                .setTitle( title )
                .setView( edit )
                .setPositiveButton( positiveText, (a,b) -> callback.acceptClicked( a, edit.getText().toString() ) )
                .setNegativeButton( negativeText, (a,b) -> callback.cancelClicked( a ) )
                .setOnCancelListener( callback::cancelClicked )
                .setOnDismissListener( callback::cancelClicked )
                .create();
    }

    /**
     * Creates a dialog to open.
     *
     * @param context to use.
     * @param title to set.
     * @param defaultText to set in the input box.
     * @param callback to call when done.
     *
     * @return the generated Dialog.
     */
    public static Dialog buildSaveInputDialogNumber( Context context, String title, long defaultText, InputDialogCallbackNumber callback ){
        return buildInputDialogNumber( context, title, String.valueOf( defaultText ), "SAVE", "ABORT", callback );
    }


    /**
     * Creates a dialog to open.
     *
     * @param context to use.
     * @param title to set.
     * @param defaultText to set in the input box.
     * @param positiveText the text on the positive button
     * @param negativeText the text on the negative button.
     * @param callback to call when done.
     *
     * @return the generated Dialog.
     */
    public static Dialog buildInputDialogNumber( Context context, String title, String defaultText,
                                           String positiveText, String negativeText,
                                           InputDialogCallbackNumber callback ){

        final EditText edit = new EditText( context );
        edit.setHint( defaultText );
        edit.setInputType( InputType.TYPE_CLASS_PHONE );
        edit.setTransformationMethod( null );

        return new AlertDialog.Builder( context )
                .setTitle( title )
                .setView( edit )
                .setPositiveButton( positiveText, (a,b) -> callback.acceptClicked( a, Long.valueOf( edit.getText().toString() ) ) )
                .setNegativeButton( negativeText, (a,b) -> callback.cancelClicked( a ) )
                .setOnCancelListener( callback::cancelClicked )
                .setOnDismissListener( callback::cancelClicked )
                .create();
    }



    /**
     * Creates a dialog to open.
     *
     * @param context to use.
     * @param title to set.
     * @param defaultText to set in the input box.
     * @param positiveText the text on the positive button
     * @param negativeText the text on the negative button.
     * @param positiveCallback to call when accepted.
     * @param abortedCallback call when aborted.
     *
     * @return the generated Dialog.
     */
    public static Dialog buildInputDialog(Context context, String title, String defaultText,
                                          String positiveText, String negativeText,
                                          Consumer<String> positiveCallback, Runnable abortedCallback ) {

        return buildInputDialog( context, title, defaultText, positiveText, negativeText, buildCallback( positiveCallback, abortedCallback ) );
    }

    /**
     * Creates a dialog to open.
     *
     * @param context to use.
     * @param title to set.
     * @param defaultText to set in the input box.
     * @param positiveText the text on the positive button
     * @param negativeText the text on the negative button.
     * @param positiveCallback to call when accepted.
     * @param abortedCallback call when aborted.
     *
     * @return the generated Dialog.
     */
    public static Dialog buildInputDialogNumber(Context context, String title, String defaultText,
                                          String positiveText, String negativeText,
                                          Consumer<Long> positiveCallback, Runnable abortedCallback ) {

        return buildInputDialogNumber( context, title, defaultText, positiveText, negativeText, buildCallbackNumber( positiveCallback, abortedCallback ) );
    }



    private static InputDialogCallback buildCallback( Consumer<String> positiveCallback, Runnable abortedCallback  ){
        return new InputDialogCallback() {
            @Override
            public void acceptClicked(DialogInterface dialog, String text) {
                if( positiveCallback != null ) positiveCallback.accept( text );
            }

            @Override
            public void cancelClicked(DialogInterface dialog) {
                if( abortedCallback != null ) abortedCallback.run();
            }
        };
    }


    private static InputDialogCallbackNumber buildCallbackNumber( Consumer<Long> positiveCallback, Runnable abortedCallback  ){
        return new InputDialogCallbackNumber() {
            @Override
            public void acceptClicked(DialogInterface dialog, long number) {
                if( positiveCallback != null ) positiveCallback.accept( number );
            }

            @Override
            public void cancelClicked(DialogInterface dialog) {
                if( abortedCallback != null ) abortedCallback.run();
            }
        };
    }


    public interface InputDialogCallback{
        void acceptClicked( DialogInterface dialog, String text );
        void cancelClicked( DialogInterface dialog );
    }


    public interface InputDialogCallbackNumber{
        void acceptClicked( DialogInterface dialog, long number );
        void cancelClicked( DialogInterface dialog );
    }
}
