package de.tu_darmstadt.informatik.gesturereader.game.pvz.config;

import java.util.HashMap;
import java.util.Map;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.input.InputCommands;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;

/**
 * Created by Toby on 14.12.2017
 */

public class GestureKeyConfig {


    /**
     * The Map of mapped commands.
     */
    private final Map<InputCommands,Gesture> mappedCommands = new HashMap<>();


    public GestureKeyConfig() {
        for( InputCommands command : InputCommands.values() ){
            mappedCommands.put( command, GestureManager.UNKNOWN() );
        }
    }


    /**
     * Changes the current Mapping.
     * @param gesture to map.
     * @param command to map to.
     */
    public void changeMapping( Gesture gesture, InputCommands command ) {
        for( Map.Entry<InputCommands,Gesture> entry : mappedCommands.entrySet() ){
            if( entry.getValue().equals( gesture ) ){
                mappedCommands.put( entry.getKey(), GestureManager.UNKNOWN() );
                break;
            }
        }

        mappedCommands.put( command, gesture );
    }

    /**
     * Unwraps the Mapping for the Gesture.
     *
     * @param gesture to unmap
     * @return the unmapped input or null.
     */
    public InputCommands unmap( Gesture gesture ){
        if( gesture == null || gesture == GestureManager.UNKNOWN() ) return null;

        for( Map.Entry<InputCommands,Gesture> entry : mappedCommands.entrySet() ){
            if( entry.getValue().equals( gesture ) ){
                return entry.getKey();
            }
        }

        return null;
    }

    /**
     * Unwraps the Mapping for the InputCommand.
     *
     * @param command to unmap
     * @return the unmapped gesture or null.
     */
    public Gesture unmap( InputCommands command ){
        return mappedCommands.get( command );
    }

}
