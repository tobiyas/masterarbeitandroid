package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.base;

import java.util.Arrays;
import java.util.List;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.InputEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlantPlantedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlantRemovedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PreviewChangedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.SelectionChangedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.PlantProvider;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.PreviewProvider;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.ResourceManager;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.input.InputCommands;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.BananaPlant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.BroccoliPlant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.FriesPlant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.PicklePlant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.SunflowerPlant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.TomatoPlant;
import java8.util.function.BiFunction;

/**
 * Created by Tobias on 22.10.2017
 */

public class PvZGameField implements EventListener, PreviewProvider {



    /**
     * The index of the currently selected plant.
     */
    private int plantSelectionIndex = 1;


    /**
     * The current suppliers for Plants.
     */
    private final List<BiFunction<Integer,Integer,Plant>> plantConstructors = Arrays.asList(
            SunflowerPlant::new,
            TomatoPlant::new,
            PicklePlant::new,
            BroccoliPlant::new,
            FriesPlant::new,
            BananaPlant::new
    );


    /**
     * The used Event System.
     */
    private final EventSystem eventSystem;

    /**
     * The resourceManager to use for checking Resources.
     */
    private final ResourceManager resourceManager;

    /**
     * the provider for plants.
     */
    private final PlantProvider plantProvider;


    private int selectedX = 1;
    private int selectedY = 1;



    public PvZGameField( EventSystem eventSystem, ResourceManager resourceManager, PlantProvider plantProvider ) {
        this.eventSystem = eventSystem;
        this.resourceManager = resourceManager;
        this.plantProvider = plantProvider;

        eventSystem.addListener( this );

        this.selectedX = 1;
        this.selectedY = 1;
    }


    @EventHandler
    public void onInput( InputEvent event ) {
        InputCommands command = event.getInputCommands();

        switch (command) {
            case GO_LEFT:
                goLeft();
                break;
            case GO_RIGHT:
                goRight();
                break;
            case GO_UP:
                goUp();
                break;
            case GO_DOWN:
                goDown();
                break;

            case GO_TO_CENTER:
                goToCenter();
                break;

            case DIG:
                dig();
                break;

            case PLANT_1:
                plant1();
                break;

            case PLANT_2:
                plant2();
                break;
        }
    }



    /**
     * Tells the Layout to go left.
     */
    private void goLeft(){
        this.selectedX = Math.max( 0, selectedX - 1 );
        eventSystem.fireEvent( new SelectionChangedEvent( selectedX, selectedY ) );
    }

    /**
     * Tells the Layout to go right.
     */
    private void goRight(){
        this.selectedX = Math.min( 2, selectedX + 1 );
        eventSystem.fireEvent( new SelectionChangedEvent( selectedX, selectedY ) );
    }

    /**
     * Tells the Layout to go up.
     */
    private void goUp(){
        this.selectedY = Math.max( 0, selectedY - 1 );
        eventSystem.fireEvent( new SelectionChangedEvent( selectedX, selectedY ) );
    }

    /**
     * Tells the Layout to go down.
     */
    private void goDown(){
        this.selectedY = Math.min( 2, selectedY + 1 );
        eventSystem.fireEvent( new SelectionChangedEvent( selectedX, selectedY ) );
    }

    /**
     * Tells the Layout back to the center.
     */
    private void goToCenter(){
        this.selectedX = 1;
        this.selectedY = 1;

        eventSystem.fireEvent( new SelectionChangedEvent( selectedX, selectedY ) );
    }

    /**
     * Plants the current Selection.
     */
    private void plantCurrent(){
        if( plantProvider.getPlantAt( selectedX, selectedY ) != null) return;

        Plant plant = this.plantConstructors.get( plantSelectionIndex ).apply( selectedX, selectedY );
        int cost = plant.getCost();
        if( resourceManager.getResources() < cost ) return;

        eventSystem.fireEvent( new PlantPlantedEvent( plant, selectedX, selectedY ));
        resourceManager.removeResources( cost );
    }

    /**
     * Plants a Tomato plant.
     */
    private void plant1(){
        if( plantProvider.getPlantAt( selectedX, selectedY ) != null) return;

        Plant plant = new TomatoPlant( selectedX, selectedY );
        int cost = plant.getCost();
        if( resourceManager.getResources() < cost ) return;

        eventSystem.fireEvent( new PlantPlantedEvent( plant, selectedX, selectedY ));
        resourceManager.removeResources( cost );
    }


    /**
     * Plants a Sunflower.
     */
    private void plant2(){
        if( plantProvider.getPlantAt( selectedX, selectedY ) != null) return;

        Plant plant = new SunflowerPlant( selectedX, selectedY );
        int cost = plant.getCost();
        if( resourceManager.getResources() < cost ) return;

        eventSystem.fireEvent( new PlantPlantedEvent( plant, selectedX, selectedY ));
        resourceManager.removeResources( cost );
    }

    /**
     * Digs the current Selection.
     */
    private void dig(){
        Plant plant = plantProvider.getPlantAt( selectedX, selectedY );
        if( plant == null ) return;

        eventSystem.fireEvent( new PlantRemovedEvent( plant, selectedX, selectedY ));
    }


    /**
     * Goes to the next plant in the selection.
     */
    private void nextPreview(){
        plantSelectionIndex = ( plantSelectionIndex + 1 ) % this.plantConstructors.size();
        Plant newPreview = plantConstructors.get( plantSelectionIndex ).apply( -1, -1 );

        eventSystem.fireEvent( new PreviewChangedEvent( newPreview ) );
    }

    /**
     * Goes to the previous plant in the selection.
     */
    private void prevPreview(){
        if( --plantSelectionIndex < 0 ) plantSelectionIndex = plantConstructors.size() - 1;
        Plant newPreview = plantConstructors.get( plantSelectionIndex ).apply( -1, -1 );

        eventSystem.fireEvent( new PreviewChangedEvent( newPreview ) );
    }


    @Override
    public Plant getPreview() {
        return plantConstructors.get( plantSelectionIndex ).apply( -1, -1 );
    }

}
