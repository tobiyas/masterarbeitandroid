package de.tu_darmstadt.informatik.gesturereader.game.pvz.gui;

import android.view.View;
import android.widget.TextView;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerLostGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerWonGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TimerContinueGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TimerPauseGameEvent;

/**
 * Handles Splash screen notifications.
 * Created by Toby on 24.10.2017
 */

public class GuiSplashTextHandler implements EventListener {

    /**
     * The Provider for the Views.
     */
    private final ViewProvider viewProvider;


    public GuiSplashTextHandler( EventSystem eventSystem, ViewProvider viewProvider ) {
        this.viewProvider = viewProvider;

        eventSystem.addListener( this );
    }



    @EventHandler
    public void onPause( TimerPauseGameEvent event ){
        setText( R.string.pause, R.color.blue );
    }

    @EventHandler
    public void onResume( TimerContinueGameEvent event ){
        hideText();
    }

    @EventHandler
    public void onLose( PlayerLostGameEvent event ){
        setText( R.string.you_lose, R.color.red );
    }

    @EventHandler
    public void onWin( PlayerWonGameEvent event ){
        setText( R.string.you_win, R.color.green );
    }

    /**
     * Sets the text to the wanted Resource.
     * @param resourceID to set to.
     */
    private void setText( int resourceID, int colorID ){
        TextView text = (TextView) viewProvider.provideView( R.id.status_text );
        text.setVisibility( View.VISIBLE );

        text.setAlpha( 1 );
        text.setTextColor( colorID );
        text.setText( resourceID );
    }

    private void hideText(){
        viewProvider.provideView( R.id.status_text ).setVisibility( View.GONE );
    }

}
