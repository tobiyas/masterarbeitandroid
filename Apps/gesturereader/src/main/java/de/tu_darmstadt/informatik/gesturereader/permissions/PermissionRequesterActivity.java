package de.tu_darmstadt.informatik.gesturereader.permissions;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import de.tu_darmstadt.informatik.gesturereader.MainActivity;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.utils.VersionUtils;

import static android.os.Build.VERSION_CODES.M;

/**
 * This activity is only for Granting Permissions!
 * This should be made way prettier!
 */

public class PermissionRequesterActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.give_perms_activity);
        update();
    }

    /**
     * Updates the Views with visibility and progresses if possible.
     */
    private void update(){
        int location = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int bodySensors = VersionUtils.g23() ? ContextCompat.checkSelfPermission(this, Manifest.permission.BODY_SENSORS) : PackageManager.PERMISSION_GRANTED;
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);


        //All Permissions got:
        if(location == PackageManager.PERMISSION_GRANTED &&
                bodySensors == PackageManager.PERMISSION_GRANTED &&
                storage == PackageManager.PERMISSION_GRANTED ){

            goToNext();
            return;
        }


        findViewById(R.id.perm_location_fine).setVisibility(location == PackageManager.PERMISSION_GRANTED ? View.INVISIBLE : View.VISIBLE);
        findViewById(R.id.perm_body_sensors).setVisibility(bodySensors == PackageManager.PERMISSION_GRANTED ? View.INVISIBLE : View.VISIBLE);
        findViewById(R.id.perm_storage).setVisibility(storage == PackageManager.PERMISSION_GRANTED ? View.INVISIBLE : View.VISIBLE);
    }


    /**
     * Closes the Current Activity and goes to the next View.
     */
    private void goToNext(){
        this.startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }


    /**
     * Starts a Popup to get the Permissions.
     * @param perm to get
     */
    @TargetApi(value = M)
    private void getPerms(String... perm){
        this.requestPermissions(perm, (int)(Math.random()* 100000));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        update();
    }


    /// Called from the XML resource Layout file

    public void requestLocation(View view){
        getPerms(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT_WATCH)
    public void requestBodySensors(View view){
        getPerms(Manifest.permission.BODY_SENSORS);
    }

    public void requestStorage(View view){
        getPerms(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

}
