package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Created by Tobias on 22.10.2017
 */

public class TickEvent extends Event {

    /**
     * The current Tick.
     */
    private final int tick;


    public TickEvent(int tick) {
        this.tick = tick;
    }


    public int getTick() {
        return tick;
    }
}
