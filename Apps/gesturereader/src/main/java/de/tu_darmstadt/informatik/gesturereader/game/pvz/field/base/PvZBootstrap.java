package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.base;

import android.view.View;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.config.GestureKeyConfig;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.DamageHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.HealthHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.PlantHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.ResourceManager;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.ZombieHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.level.ZombieLevel;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.level.ZombieSpawner;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.gui.CheatGuiHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.gui.GuiHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.gui.GuiHealthHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.gui.GuiSplashTextHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.gui.ViewProvider;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.input.DebugGuiInputController;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.input.GestureInput;

/**
 * Created by Toby on 24.10.2017.
 */

public class PvZBootstrap {


    /**
     * The current Gamefield.
     */
    private PvZGameField gameField;

    /**
     * The used EventSystem.
     */
    private EventSystem eventSystem;

    /**
     * The Used ResourceManager.
     */
    private ResourceManager resourceManager;

    /**
     * The Zombie handler to use.
     */
    private ZombieHandler zombieHandler;

    /**
     * The spawner for Zombies.
     */
    private ZombieSpawner zombieSpawner;

    /**
     * The Handler for Damage.
     */
    private DamageHandler damageHandler;

    /**
     * The handler for health.
     */
    private HealthHandler healthHandler;

    /**
     * the system ticking ticks.
     */
    private TickSystem tickSystem;

    /**
     * the handler for plants.
     */
    private PlantHandler plantHandler;



    /**
     * The actual Game actor:
     */
    private GuiHandler guiHandler;

    /**
     * The health handler gui.
     */
    private GuiHealthHandler guiHealthHandler;

    /**
     * The handler for the Splash notices.
     */
    private GuiSplashTextHandler guiSplashTextHandler;


    /**
     * The view provider to use for init.
     */
    private final ViewProvider viewProvider;


    public PvZBootstrap( ViewProvider viewProvider ){
        this.viewProvider = viewProvider;
    }

    /**
     * Return true if this is build.
     */
    public boolean isBuild(){
        return eventSystem != null;
    }


    /**
     * Builds the System.
     */
    public PvZBootstrap buildBase(){
        if(isBuild()) return this;

        eventSystem = new EventSystem();
        tickSystem = new TickSystem( eventSystem );
        resourceManager = new ResourceManager( eventSystem );
        plantHandler = new PlantHandler( eventSystem );
        gameField = new PvZGameField( eventSystem, resourceManager, plantHandler );
        zombieHandler = new ZombieHandler( eventSystem, plantHandler );

        zombieSpawner = ZombieLevel.populateLevel1( new ZombieSpawner( eventSystem ) );
        damageHandler = new DamageHandler( eventSystem );
        healthHandler = new HealthHandler( eventSystem );

        guiHandler = new GuiHandler( eventSystem, viewProvider, resourceManager, gameField );
        guiHealthHandler = new GuiHealthHandler( viewProvider, eventSystem );
        guiSplashTextHandler = new GuiSplashTextHandler( eventSystem, viewProvider );

        return this;
    }

    /**
     * Tears down the System.
     */
    public PvZBootstrap teardown(){
        //TODO....

        eventSystem = null;
        return this;
    }


    /**
     * Builds the Debug controls.
     */
    public PvZBootstrap buildDebugControls(){
        new DebugGuiInputController( eventSystem, viewProvider );
        new CheatGuiHandler( eventSystem, viewProvider, resourceManager );
        return this;
    }


    /**
     * Disables the Debug controls.
     */
    public PvZBootstrap disableDebugControls(){
        viewProvider.provideView( R.id.debug_check ).setVisibility( View.GONE );
        viewProvider.provideView( R.id.debug_nav ).setVisibility( View.GONE );
        return this;
    }


    /**
     * Disables the Debug controls.
     */
    public PvZBootstrap enableDebugControls(){
        viewProvider.provideView( R.id.debug_check ).setVisibility( View.VISIBLE );
        viewProvider.provideView( R.id.debug_nav ).setVisibility( View.VISIBLE );
        return this;
    }


    /**
     * Builds the Movement controller.
     */
    public PvZBootstrap buildMovementControls(String model, GestureKeyConfig config){
        if( model == null || model.isEmpty() ) model = "pvz";

        new GestureInput( eventSystem, viewProvider, model, config );
        return this;
    }

    /**
     * Gets the used EventSystem.
     * @return the event system.
     */
    public EventSystem getEventSystem() {
        return eventSystem;
    }
}
