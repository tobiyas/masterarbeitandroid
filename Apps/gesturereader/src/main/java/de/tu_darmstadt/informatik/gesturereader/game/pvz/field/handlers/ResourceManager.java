package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.AddResourceEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.RemoveResourceEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ResourcesChangedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TickEvent;

import static de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem.TICKS_PER_SECOND;

/**
 * Created by Tobias on 22.10.2017
 */
public class ResourceManager implements EventListener {

    private static final int RESOURCE_EVERY_TICKS = TICKS_PER_SECOND * 5;
    private static final int RESOURCE_EVERY_GATHER = 5;
    private static final int START_RESOURCES = 50;


    /**
     * The resources the player has.
     */
    private int resources = START_RESOURCES;

    /**
     * The EventSystem to use.
     */
    private final EventSystem eventSystem;


    public ResourceManager(EventSystem eventSystem) {
        this.eventSystem = eventSystem;

        eventSystem.addListener( this );
    }


    /**
     * Ticks the container.
     */
    @EventHandler
    public void tick( TickEvent event ){
        //Add resources per ticks:
        if ( event.getTick() % RESOURCE_EVERY_TICKS  == 0 ) addResources( RESOURCE_EVERY_GATHER );
    }


    @EventHandler
    public void addResources( AddResourceEvent event ){
        if( event.getAmount() <= 0 ) return;

        this.addResources( event.getAmount() );
    }


    @EventHandler
    public void removeResources( RemoveResourceEvent event ){
        if( event.getAmount() <= 0 ) return;

        this.removeResources( event.getAmount() );
    }

    /**
     * Gers the current resources.
     * @return the current resources.
     */
    public int getResources() {
        return resources;
    }


    /**
     * Adds resources.
     */
    public void addResources( int amount ){
        if( amount <= 0 ) return;

        resources += amount;
        eventSystem.fireEvent( new ResourcesChangedEvent( resources ));
    }


    /**
     * removes resources.
     * May never drop below 0!
     */
    public void removeResources( int amount ){
        if( amount <= 0 ) return;

        resources = Math.max( 0, resources - amount );
        eventSystem.fireEvent( new ResourcesChangedEvent( resources ));
    }

}
