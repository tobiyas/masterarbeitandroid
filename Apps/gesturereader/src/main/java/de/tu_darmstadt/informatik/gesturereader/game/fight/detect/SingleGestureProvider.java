package de.tu_darmstadt.informatik.gesturereader.game.fight.detect;

import org.joda.time.DateTime;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;

/**
 * Created by Toby on 09.01.2018.
 */

public class SingleGestureProvider implements NewGestureProvider {

    /**
     * The Gesture to provide.
     */
    private final Gesture gesture;


    public SingleGestureProvider(Gesture gesture) {
        this.gesture = gesture;
    }


    @Override
    public List<Gesture> getNextGestures(Collection<Gesture> possibleGestures) {
        return Collections.singletonList( gesture );
    }

    @Override
    public void gestureExecuted(DateTime start, DateTime end, Gesture wanted, Gesture got) {

    }
}
