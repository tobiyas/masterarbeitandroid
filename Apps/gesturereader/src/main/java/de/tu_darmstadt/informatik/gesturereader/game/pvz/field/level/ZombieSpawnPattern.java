package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.level;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

/**
 * This is a pattern when a Zombie is spawned where.
 *
 * Created by Toby on 24.10.2017
 */

public class ZombieSpawnPattern {

    /**
     * The row the Zombie is spawned.
     */
    private final int row;

    /**
     * The Zombie to spawn.
     */
    private final Zombie zombie;

    /**
     * The tick in which the Zombie is spawned.
     */
    private final int tick;

    /**
     * If the Zombie is spawned.
     */
    private boolean spawned = false;


    /**
     * Creates a spawn pattern.
     * @param row to spawn in.
     * @param zombie to spawn.
     * @param tick to spawn at.
     */
    public ZombieSpawnPattern(int row, Zombie zombie, int tick) {
        this.row = row;
        this.zombie = zombie;
        this.tick = tick;
    }


    /**
     * Gets the Row the Zombie spawns in.
     * @return the row.
     */
    public int getRow() {
        return row;
    }

    /**
     * Gets the Zombie to spawn.
     * @return Zombie
     */
    public Zombie getZombie() {
        return zombie;
    }

    /**
     * Gets the Tick the zombie is spawned at.
     * @return spawn tick.
     */
    public int getTick() {
        return tick;
    }

    /**
     * If the Zombie is NOT spawned.
     * @return true if not spawned.
     */
    public boolean canSpawn(){
        return !spawned;
    }

    /**
     * Returns true if spawned.
     */
    public boolean isSpawned() {
        return spawned;
    }

    public void setSpawned() {
        this.spawned = true;
    }
}
