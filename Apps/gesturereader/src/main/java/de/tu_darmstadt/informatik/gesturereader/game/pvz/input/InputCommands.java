package de.tu_darmstadt.informatik.gesturereader.game.pvz.input;

/**
 * Created by Tobias on 22.10.2017
 */

public enum InputCommands {

    GO_LEFT, GO_RIGHT,
    GO_UP, GO_DOWN,
    GO_TO_CENTER,

    PLANT_1, PLANT_2,
    DIG
}
