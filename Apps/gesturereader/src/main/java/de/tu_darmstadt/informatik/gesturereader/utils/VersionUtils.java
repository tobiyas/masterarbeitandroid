package de.tu_darmstadt.informatik.gesturereader.utils;

import android.os.Build;

/**
 * Created by Toby on 22.05.2017
 */

public class VersionUtils {


    public static boolean g23(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public static boolean g24(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.N;
    }

    public static boolean g26(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }


}
