package de.tu_darmstadt.informatik.gesturereader.evaluation;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import java8.util.stream.IntStreams;

/**
 * Created by Toby on 12.01.2018
 */
public class SampleBatchArrayAdapter extends BaseAdapter {


    private final int colorSelected;
    private final int colorUnselected;
    private final int colorDelete;


    /**
     * The Data of the Adapter.
     */
    private final List<SampleProxy> samples = new ArrayList<>();


    /**
     * The gestures to use.
     */
    private final List<Gesture> gestures;

    /**
     * The session to use.
     */
    private final GestureRecSession session;

    /**
     * the used context.
     */
    private final Context context;


    public SampleBatchArrayAdapter( @NonNull Context context, GestureRecSession session, SampleBatch batch ) {
        this.context = context;

        //Set the data:
        this.session = session;
        this.gestures = new ArrayList<>( batch.getGestures() );
        Collections.sort( gestures, new GestureNameSorter() );

        //Set the Samples:
        this.samples.addAll( batch.getSamples() );
        Collections.sort( this.samples, new SampleTimeSorter() );


        //Init the colors:
        this.colorSelected = R.color.green;
        this.colorUnselected = R.color.gray;
        this.colorDelete = R.color.red;
    }


    @Override
    public SampleProxy getItem( int position ) {
        return samples.get( position );
    }


    @Override
    public long getItemId( int position ) {
        return position;
    }


    @Override
    public int getCount() {
        return samples.size();
    }


    @NonNull
    @Override
    public View getView( int position, @Nullable View convertView, @NonNull ViewGroup parent ) {
        LayoutInflater inflater = LayoutInflater.from( context );
        if( convertView == null ){
            convertView = inflater.inflate( R.layout.listitem_change, parent, false );
        }

        SampleProxy sample = getItem( position);
        if( sample == null ) return convertView;

        TextView main = convertView.findViewById( R.id.text );
        main.setVisibility( View.VISIBLE );

        LinearLayout linearLayout = convertView.findViewById( R.id.options );
        linearLayout.setVisibility( View.GONE );


        //Setup visible + invis stuff:
        String text = ( position + 1 ) + ". " + sample.getGesture().getName();
        main.setText( text );
        main.setOnClickListener( v ->
            linearLayout.setVisibility( linearLayout.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE )
        );

        //Removes all child views:
        linearLayout.removeAllViews();

        //Add a new View for all Gestures:
        for( final Gesture gesture : gestures ){
            TextView gestureView = (TextView) inflater.inflate( R.layout.list_item_change_sub_item, linearLayout, false );
            gestureView.setText( gesture.getName() );
            linearLayout.addView( gestureView );

            if( gesture.equals( sample.getGesture() ) ) gestureView.setBackgroundResource(colorSelected);
            else gestureView.setBackgroundResource(colorUnselected);

            gestureView.setOnClickListener( v -> {
                int color = ((ColorDrawable)v.getBackground()).getColor();
                if( color == colorSelected) return;

                v.setBackgroundResource( colorSelected );
                IntStreams.range( 0, linearLayout.getChildCount() - 1 )
                        .mapToObj( linearLayout::getChildAt )
                        .filter( o -> o != v )
                        .forEach( o -> o.setBackgroundResource( colorUnselected ) );

                //Do the actual Change:
                changeGesture( position, main, gesture );
            } );
        }

        //Add a delete label at the end!
        TextView deleteView = (TextView) inflater.inflate( R.layout.list_item_change_sub_item, linearLayout, false );
        deleteView.setText( R.string.delete );
        deleteView.setBackgroundResource( colorDelete );
        deleteView.setOnClickListener(new View.OnClickListener() {
            private long last = 0;
            @Override public void onClick(View view) {
                long now = System.currentTimeMillis();
                if( Math.abs( last - now ) > 2_000L ){
                    last = System.currentTimeMillis();
                    Toast.makeText( context, "Press again to delete", Toast.LENGTH_LONG ).show();
                    return;
                }

                //Delete the thing:
                samples.remove( sample );
                notifyDataSetChanged();

                session.getSampleManager().deleteSample( sample );
            }
        });
        linearLayout.addView( deleteView );


        return convertView;
    }


    private void changeGesture( int position, TextView textToChange, Gesture gesture ) {
        SampleProxy sample = getItem( position );
        if( sample.getGesture().equals( gesture ) ) return;

        //change gesture:
        sample = session.getSampleManager().changeGestureOfSample( sample, gesture );
        this.samples.set( position, sample );

        //Update the view at that position!
        String text = ( position + 1 ) + ". " + sample.getGesture().getName();
        textToChange.setText( text );
    }


    private static class SampleTimeSorter implements Comparator<SampleProxy> {
        @Override
        public int compare(SampleProxy proxy, SampleProxy t1) {
            return proxy.getRecordTime().compareTo( t1.getRecordTime()  );
        }
    }


    private static class GestureNameSorter implements Comparator<Gesture> {
        @Override
        public int compare(Gesture gesture, Gesture g2) {
            return gesture.getName().compareTo( g2.getName() );
        }
    }

}
