package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Event fired, when the player loses all his health.
 * Created by Toby on 24.10.2017
 */

public class PlayerLostGameEvent extends Event {}
