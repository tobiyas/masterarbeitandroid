package de.tu_darmstadt.informatik.gesturereader.game.fight.detect;

import android.os.Handler;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameActivity;
import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameModel;
import de.tu_darmstadt.informatik.gesturereader.utils.Animations;
import de.tu_darmstadt.informatik.gesturereader.utils.QuadConsumer;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResult;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;

/**
 * Created by Tobias on 02.01.2018
 */

public class FightGestureDetector implements FightGameModel.GestureDetectorStarter {


    /**
     * The Length of a gesture.
     */
    protected static final Duration GESTURE_LENGTH = Duration.millis( 2_000 );

    /**
     * The time the fading takes.
     */
    protected static final int FADE_TIME = 1_000;

    /**
     * The used MT handler.
     */
    protected final Handler mt = MainThread.MTHandler();

    /**
     * The bar to use to show when a gesture starts and ends.
     */
    protected final ProgressBar progressBar;

    /**
     * The view for the next gesture to show.
     */
    protected final ImageView nextGestureView;

    /**
     * The model to use.
     */
    protected final ModelProxy model;

    /**
     * The used session.
     */
    protected final GestureRecSession session;

    /**
     * The saver to use.
     */
    protected final EvalSaver saver;


    /**
     * The animation to fade out a view.
     */
    protected final Animation fadeOutAnimation = Animations.fadeOut( FADE_TIME );

    /**
     * The animation to fade in a view.
     */
    protected final Animation fadeInAnimation  = Animations.fadeIn( FADE_TIME );


    public FightGestureDetector( ProgressBar progressBar, ImageView nextGestureView,
                                 ModelProxy model, GestureRecSession session,
                                 EvalSaver saver ) {

        this.progressBar = progressBar;
        this.nextGestureView = nextGestureView;
        this.model = model;
        this.session = session;
        this.saver = saver;
    }


    /**
     * Starts recording a new Gesture NOW!
     */
    private void startRecording( QuadConsumer<DateTime,DateTime,Gesture,Gesture> callback, Gesture gesture ) {
        //Start the recording:
        DateTime start = DateTime.now().plus( Duration.millis( FADE_TIME + 500 ) );
        session.getDataRecorder().startCollectingDataAndEvaluate( start, GESTURE_LENGTH, Duration.millis( 500 ), (a,b) -> gotData(a,b,callback,gesture), model );

        //Init the timer for the progress bar:
        for( int i = 0; i <= 20; i++ ) {
            int timeOffset = i * 10;
            mt.postDelayed( () -> this.progressBar.setProgress( timeOffset ), ( timeOffset * 10 ) + FADE_TIME + 500 );
        }

        //Set the picture to do.
        mt.post( () -> setToPreviewGesture( gesture ) );
    }


    /**
     * Sets the Gesture image passed to the Gui.
     *
     * @param gesture to set.
     */
    private void setToPreviewGesture( Gesture gesture ) {
        nextGestureView.setImageResource( gestureToId( gesture ) );
        nextGestureView.setImageAlpha( 0 );

        nextGestureView.startAnimation( fadeInAnimation );
        nextGestureView.postDelayed( () -> nextGestureView.setImageAlpha( 255 ),FADE_TIME );
    }


    /**
     * Tries to resolve picture IDs by the Name of the Gestures.
     *
     * @param gesture to resolve.
     * @return the used Gesture.
     */
    private int gestureToId( Gesture gesture ){
        if( gesture == null ) return R.drawable.cancel;
        boolean altKickMode = FightGameActivity.USE_ALT_KICK;

        String name = gesture.getName();
        if( name.equalsIgnoreCase( "kick_left" ) ) return altKickMode ? R.drawable.kick_left_alt : R.drawable.kick_left;
        if( name.equalsIgnoreCase( "kick_right" ) ) return altKickMode ? R.drawable.kick_right_alt : R.drawable.kick_right;

        if( name.equalsIgnoreCase( "attack" ) ) return R.drawable.attack;
        if( name.equalsIgnoreCase( "block" ) ) return R.drawable.block;

        if( name.equalsIgnoreCase( "jump" ) ) return R.drawable.jump;
        if( name.equalsIgnoreCase( "duck" ) ) return R.drawable.duck;

        if( name.equalsIgnoreCase( "idle" ) ) return R.drawable.idle;

        return R.drawable.cancel;
    }


    //Does a stop.
    public void stop(){

    }


    /**
     * Got a new Batch.
     * @param batch to use.
     */
    private void gotData( SensorDataBatch batch, GestureResultWrapper resultWrapper, QuadConsumer<DateTime,DateTime,Gesture,Gesture> quadConsumer, Gesture wanted ){
        if( batch == null || resultWrapper == null ) {
            quadConsumer.accept( DateTime.now().minus( 2_000L ), DateTime.now(), wanted, GestureManager.UNKNOWN() );
            return;
        }

        GestureResult top = resultWrapper.getTopResult();
        Gesture result = top.getFirst();

        boolean wasCorrect = wanted.equals( result );
        removePreview( wasCorrect );

        DateTime start = batch.getStartTime();
        DateTime end = batch.getEndTime();

        mt.post( () -> quadConsumer.accept( start, end, wanted, result ) );

        //Save the stuff if wanted:
        if( saver != null ) saver.save( batch, wanted );
    }

    /**
     * Removes the Preview.
     * @param wasCorrect if it was correct.
     */
    private void removePreview( boolean wasCorrect ) {
        this.nextGestureView.startAnimation( fadeOutAnimation );
        this.nextGestureView.setImageAlpha( 255 );

        mt.postDelayed( () -> this.nextGestureView.setImageAlpha( 0 ), FADE_TIME );
    }


    @Override
    public void startDetectingGesture(QuadConsumer<DateTime,DateTime,Gesture,Gesture> callback, Gesture gesture ) {
        startRecording( callback, gesture );
    }

}
