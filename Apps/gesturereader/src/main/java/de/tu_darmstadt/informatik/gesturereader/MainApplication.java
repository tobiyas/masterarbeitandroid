package de.tu_darmstadt.informatik.gesturereader;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import net.danlew.android.joda.JodaTimeAndroid;

import org.apache.commons.io.FileUtils;

import java.io.File;

import de.tu_darmstadt.informatik.gesturereader.features.LeaveLimbsOutFeatureFactory;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.features.FeatureFactory;
import de.tu_darmstadt.informatik.kom.gesturerec.file.DefaultFileConstants;
import de.tu_darmstadt.informatik.kom.gesturerec.file.FileConstants;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.Limb;
import java8.util.stream.RefStreams;

/**
 * This application is ONLY for Joda Time init, since it needs a global init timer.
 */
public class MainApplication extends MultiDexApplication {

    /**
     * The Session to be available throughout the complete Application.
     */
    private GestureRecSession session;


    @Override
    public void onCreate() {
        super.onCreate();

        long before = System.currentTimeMillis();
        JodaTimeAndroid.init( this );
        recreateSession();
        long after = System.currentTimeMillis();

        Log.e( "TIMINGS", "Init took " + ( after - before ) + "ms." );
    }

    /**
     * Recreates the session.
     * This also adds all the Sources present currently to the System.
     */
    public GestureRecSession recreateSession() {
        if(session != null) {
            session.close();
            session = null;
        }

        //Build Feature-Factory::
        FeatureFactory featureFactory = null;

        //Remove Hands / Arms:
        //featureFactory = new LeaveLimbsOutFeatureFactory( Limb.LEFT_FOOT, Limb.RIGHT_FOOT );  //Not Feet
        //featureFactory = new LeaveLimbsOutFeatureFactory( Limb.LEFT_HAND, Limb.RIGHT_HAND );  //No Hands
        //featureFactory = new LeaveLimbsOutFeatureFactory( Limb.LEFT_FOOT, Limb.RIGHT_FOOT, Limb.RIGHT_HAND );  //No Feet and only right Arm

        //Remove only 1 Limb:
        //featureFactory = new LeaveLimbsOutFeatureFactory( Limb.RIGHT_FOOT );                  //No Right Foot
        //featureFactory = new LeaveLimbsOutFeatureFactory( Limb.LEFT_FOOT );                   //No Left Foot
        //featureFactory = new LeaveLimbsOutFeatureFactory( Limb.LEFT_HAND );                   //No left Hand
        //featureFactory = new LeaveLimbsOutFeatureFactory( Limb.RIGHT_HAND );                  //No right Hand

        if( featureFactory == null ) featureFactory = new LeaveLimbsOutFeatureFactory();
        FileConstants constants = new DefaultFileConstants();

        //Actually build:
        this.session = new GestureRecSession( this, featureFactory, constants );
        //this.session.addPhoneSensorsToSession(); //Not using the Phone at this time.
        this.session.scanForWearables();
        this.session.scanForIMUs();

        return session;
    }


    /**
     * Gets the Application Context from Wherever you are.
     * @param context to getGesture from.
     * @return the application Context.
     */
    public static MainApplication fromContext( Context context ) {
        return (MainApplication) context.getApplicationContext();
    }

    /**
     * Gests the Session.
     * @return the session present.
     */
    public GestureRecSession getSession() {
        return session;
    }


    /**
     * This has to be called before a session exists.
     * @param session to use for getting the path.
     * @param old to use.
     * @param neu to use.
     */
    private static void renameUser( GestureRecSession session, String old, String neu ){
        File base = session.getFileConstants().getBaseGestureFolder();

        File[] gestureFolders = base.listFiles();
        RefStreams.of( gestureFolders )
                .map( File::listFiles )
                .flatMap( RefStreams::of )
                .filter( file -> file.getName().startsWith( old + "_" ) )
                .forEach( file -> {
                    File nuFile = new File( file.getParent(), neu + "_" + file.getName().substring( old.length() + 1 ) );
                    try{ FileUtils.moveFile( file,  nuFile ); }catch ( Throwable exp ){ exp.printStackTrace(); }
                } );
    }

}
