package de.tu_darmstadt.informatik.gesturereader.detect;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnActivity;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResult;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.GestureDetector;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;
import java8.util.Optional;
import java8.util.stream.Collectors;

import static de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager.UNKNOWN;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 02.09.2017
 */

public class ActivityDetect extends OwnActivity {

    /**
     * The Tag to use for log entries:
     */
    private static final String TAG = "DETECTION_A";

    /**
     * The Executor to use for async stuff.
     */
    private static ExecutorService executor = Executors.newSingleThreadScheduledExecutor();


    /**
     * The currently used detector.
     */
    private String usedDetector = "gestures";

    /**
     * The usable detectors present.
     */
    private List<String> usableDetectors = new ArrayList<>();

    /**
     * The currently used detector.
     */
    private GestureDetector detector;

    /**
     * The used Session.
     */
    private GestureRecSession session;

    /**
     * If we should save the data.
     */
    private boolean save = false;


    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_detect );
        this.session = MainApplication.fromContext( this ).getSession();

        Collection<ModelProxy> models = MainApplication.fromContext( this ).getSession().getModelManager().getAllModels();
        usableDetectors.addAll( stream( models ).map( ModelProxy::getName ).collect(Collectors.toSet() ) );

        //Set to the first slot:
        if( usableDetectors.size() >= 1 ) this.usedDetector = usableDetectors.get( 0 );

        Spinner spinner = findViewById( R.id.detector_selection );
        spinner.setAdapter(new ArrayAdapter<>( this, android.R.layout.simple_spinner_item, usableDetectors ));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) { setCurrentDetector( usableDetectors.get( pos ) ); }
            @Override public void onNothingSelected(AdapterView<?> adapterView) { usedDetector = "gestures"; }
        });

        CheckBox saveCheck = findViewById( R.id.save_check );
        saveCheck.setOnCheckedChangeListener( this::checkSaveChanged );

        findViewById( R.id.start_button ).setOnLongClickListener( this::openBatchSelectView );
    }


    private boolean openBatchSelectView(View view) {
        session().Dialogs().OpenUserSingleDialog( this, this::openGesturesDialog );
        return true;
    }

    private void openGesturesDialog( String users ){
        session().Dialogs().OpenGestureSingleSelectionDialog( this, g -> openSamplesDialog( users, g ) );

    }

    private void openSamplesDialog( String user, Gesture g ) {
        SampleBatch batch = session.getSampleManager().query().addGestures(g).addUsers( user ).applyFilter();
        String[] samples = stream( batch.getSamples() )
                .map( SampleProxy::getRecordTime )
                .map( DateTime::getMillis )
                .map( String::valueOf )
                .sorted()
                .toArray( String[]::new );

        session.Dialogs().OpenStringSingleSelectionDialog( this, samples, s -> this.detect( decode( batch, s ) ) );
    }

    private SampleProxy decode( SampleBatch batch, String timeString ){
        long time = Long.valueOf( timeString );
        return stream( batch.getSamples() )
            .filter( s -> s.getRecordTime().getMillis() == time )
            .findFirst()
            .orElse( null );
    }



    private void detect( SampleProxy sample ){
        if( sample == null ){
            Toast.makeText( this, "ERROR!", Toast.LENGTH_LONG ).show();
            return;
        }

        Toast.makeText( this, "Detecting with: " + detector.getModelName() + " should be: " + sample.getGesture(), Toast.LENGTH_LONG ).show();
        detector.analyseGestureAsyncMT( sample.getBatch(), this::detectionSuccess, this::detectionFailed );
    }


    /**
     * Sets the current Detector to the one wanted.
     * @param name to set.
     */
    private void setCurrentDetector( String name ){
        executor.execute( () -> {
            try{
                this.usedDetector = name;
                this.detector = Optional.of( name )
                        .map( n -> session.getModelManager().getForName( n ) )
                        .map( ModelProxy::getGestureDetector )
                        .orElse( null );
            }catch ( Throwable exp ){
                exp.printStackTrace();
                MainThread.runOnMT( () -> Toast.makeText( this, "Error on creating Detector: " + usedDetector, Toast.LENGTH_LONG ).show() );
            }
        });
    }


    private void checkSaveChanged(CompoundButton unused, boolean newState) {
        this.save = newState;
    }


    public void startDetection( View view ){
        //Do not detect when not enabled.
        if( !view.isEnabled() ) return;

        //Be sure to only run one instance at a time!
        view.setEnabled( false );

        ProgressBar bar = findViewById( R.id.progress );
        bar.setMax( 25 );
        bar.setProgress( 0 );

        startDetection( Duration.millis( 2L * 1000L));

        //Start animation for Bar:
        bar.postDelayed( () -> setBarPercent( bar ), 100 );
    }

    /**
     * Sets the bar percentage 1 Tick further.
     * @param bar to use.
     */
    private void setBarPercent( ProgressBar bar ){
        if( bar.getProgress() >= 25 ) return;

        bar.setProgress( bar.getProgress() + 1 );
        bar.postDelayed( () -> setBarPercent( bar ), 100 );
    }


    /**
     * Starts an immediate Detection NOW!
     * @param duration to use.
     */
    private void startDetection( Duration duration ){
        if( detector == null ){
            Toast.makeText( this, "Detector not loaded! Can not start!", Toast.LENGTH_LONG ).show();
            return;
        }

        session().getDataRecorder().startCollectingData(
                duration,
                Duration.millis( 500 ),
                b -> detector.analyseGestureAsyncMT( b, this::detectionSuccess, this::detectionFailed ) );
    }


    /**
     * Failed.
     * @param sensorDataBatch that failed.
     * @param throwable that occured.
     */
    private void detectionFailed( SensorDataBatch sensorDataBatch, Throwable throwable ) {
        new Handler( Looper.getMainLooper() ).post(() ->
            {
                Toast.makeText( this, "Error on Detection! :(", Toast.LENGTH_LONG).show();
                throwable.printStackTrace();
            }
        );
    }


    /**
     * We have a result!
     *
     * @param batch to use.
     * @param result to use.
     */
    private void detectionSuccess( SensorDataBatch batch, GestureResultWrapper result ) {
        if( batch == null ) Log.w( TAG, "Could not read Batch. Something is fishy!!!" );
        if( result == null ) Log.w( TAG, "Could not read Result. Something is fishy!!!" );

        //Enable the Start Button again.
        findViewById(R.id.start_button).setEnabled( true );
        if(result == null) return;

        GestureResult maxValue = result.getTopResult();

        Log.i(TAG, "RESULT is using " + detector.getModelName() + " as Model.");
        stream( result.getResultSet() )
                .forEach( a -> Log.i( TAG, "RESULT: " + a.getFirst() + " - " + (a.getSecond()*100d) + "%" ) );


        Gesture detectedGesture = maxValue.getFirst();
        double percent = maxValue.getSecond() * 100d;

        TextView text = findViewById(R.id.result);
        text.setText( String.format( Locale.getDefault(), "Result: %s - %.2f%% - %d", detectedGesture, percent, result.getTotalTimeTaken()) );

        //Try to map to an image:
        ImageView image = findViewById( R.id.image );
        image.setImageResource( mapToImage( detectedGesture ) );

        //If we have a gesture and should save it:
        if( save && detectedGesture != UNKNOWN() && batch != null ) {
            session.Dialogs().OpenGestureSingleSelectionDialog(
                    this,
                    g -> session.getSampleManager().addSampleCurrentUser( g, batch.getStartTime(), batch )
            );
        }
    }


    private int mapToImage( Gesture gesture ){
        if( gesture.getName().equalsIgnoreCase("unknown") ) return R.drawable.cancel;

        if( gesture.getName().equalsIgnoreCase("idle")) return R.drawable.idle;

        if( gesture.getName().equalsIgnoreCase("attack") ) return R.drawable.attack;
        if( gesture.getName().equalsIgnoreCase("block") ) return R.drawable.block;

        if( gesture.getName().equalsIgnoreCase("jump") ) return R.drawable.jump;
        if( gesture.getName().equalsIgnoreCase("duck") ) return R.drawable.duck;

        if( gesture.getName().equalsIgnoreCase("kick_left") ) return R.drawable.kick_left;
        if( gesture.getName().equalsIgnoreCase("kick_right") ) return R.drawable.kick_right;

        return R.drawable.cancel;
    }

}
