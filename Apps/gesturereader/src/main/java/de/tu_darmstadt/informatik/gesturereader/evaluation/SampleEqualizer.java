package de.tu_darmstadt.informatik.gesturereader.evaluation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 05.01.2018
 */

public class SampleEqualizer {


    /**
     * This creates a new SampleBatch from the 2 batches present.
     * It uses the Second batch to 100% and fills with the first batch.
     *
     *
     * @param session to use for session stuff.
     * @param batch1 to use.
     * @param batch2 to use.
     *
     * @return the generated result batch.
     */
    public static SampleBatch fillBatch2WithBatch1( GestureRecSession session, SampleBatch batch1, SampleBatch batch2, int desiredPerGesture ){
        Collection<Gesture> gestures = new HashSet<>();
        gestures.addAll( batch1.getGestures() );
        gestures.addAll( batch2.getGestures() );

        //The uses samples for the result.
        SampleBatch result = new SampleBatch();

        //Do this for every Gesture.
        for( Gesture gesture : gestures ){
            //Add all the old samples first:
            SampleBatch old = batch2.query( session ).addGestures( gesture ).applyFilter();
            int oldSize = old.size();

            result.addBatch( old );

            //check how many where added:
            SampleBatch toFillWith = batch1.query( session ).addGestures( gesture ).applyFilter();
            int toAdd = desiredPerGesture - oldSize;

            if( toAdd > 0 ) result.addBatch( getXRandomSamples( toFillWith, toAdd ) );
        }

        return result;
    }


    private static SampleBatch getXRandomSamples( SampleBatch batch, int x ){
        Collection<SampleProxy> proxies = new ArrayList<>();
        if( batch.size() < x ) {
            proxies.addAll( batch.getSamples() );
            return new SampleBatch( proxies );
        }

        Collection<SampleProxy> samples = batch.getSamples();
        //Add values till we have enough:
        while( proxies.size() < x ){
            SampleProxy rand = rand( samples );
            samples.remove( rand );
            proxies.add( rand );
        }

        return new SampleBatch( proxies );
    }


    private static Random rand = new Random();
    private static <T> T rand( Collection<T> collection ){
        if( collection == null ) return null;
        if( collection.isEmpty() ) return null;

        int size = collection.size();
        List<T> list = collection instanceof  List ? (List<T>) collection : new ArrayList<>( collection );

        return list.get( rand.nextInt( size ) );
    }


    /**
     * Gets X Values of each gesture.
     * @param x to get.
     * @return the batch with X of each gesture.
     */
    public static SampleBatch getXOfEachGesture( SampleBatch toGetFrom, int x ){
        SampleBatch newBatch = new SampleBatch();

        for( Gesture gesture : toGetFrom.getGestures() ){
            Collection<SampleProxy> samples = stream( toGetFrom.getSamples() )
                    .filter( s -> s.getGesture() == gesture )
                    .collect( Collectors.toSet() );

            newBatch.addBatch( getXRandomSamples( new SampleBatch( samples ), x ) );
        }

        return newBatch;
    }

}
