package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;

/**
 * Created by Tobias on 22.10.2017
 */

public class PlantPlantedEvent extends Event {

    /**
     * The planted plant.
     */
    private final Plant plant;

    /**
     * The Place this was planted.
     */
    private final Pair<Integer,Integer> point;


    public PlantPlantedEvent(Plant plant, Pair<Integer, Integer> point) {
        this.plant = plant;
        this.point = point;
    }


    public PlantPlantedEvent(Plant plant, int x, int y ) {
        this ( plant, new Pair<>( x, y ) );
    }


    public Plant getPlant() {
        return plant;
    }

    public Pair<Integer, Integer> getPoint() {
        return point;
    }

    public int getX(){
        return point.getFirst();
    }

    public int getY(){
        return point.getSecond();
    }
}
