package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerHealthChangedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerLostGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieReachedEndOfLaneEvent;

/**
 * This handles the Health of the player AND his Loss!
 * Created by Toby on 24.10.2017
 */

public class HealthHandler implements EventListener {


    private static final int MAX_HEALTH = 3;


    /**
     * the Event System to use.
     */
    private final EventSystem eventSystem;

    /**
     * The current Health of the Player.
     */
    private int currentHealth = MAX_HEALTH;


    public HealthHandler( EventSystem eventSystem ){
        this.eventSystem = eventSystem;

        eventSystem.addListener( this );
    }


    @EventHandler
    public void zombieReachedEndOfLane( ZombieReachedEndOfLaneEvent event ){
        //Kill the zombie:
        event.getZombie().kill();

        //Drain health:
        currentHealth--;
        eventSystem.fireEvent( new PlayerHealthChangedEvent( currentHealth ));

        //Check if lost:
        if( currentHealth <= 0 ) eventSystem.fireEvent( new PlayerLostGameEvent() );
    }

}
