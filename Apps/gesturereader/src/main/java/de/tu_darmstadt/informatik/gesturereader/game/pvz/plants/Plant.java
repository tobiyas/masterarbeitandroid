package de.tu_darmstadt.informatik.gesturereader.game.pvz.plants;

import java.util.Comparator;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlantShootsOnLaneEvent;

/**
 * This is the base class of a plant.
 * Sub-Classes for plants may change.
 *
 * Created by Tobias on 22.10.2017
 */
public abstract class Plant {

    /**
     * The ID of the Image to use for the plant.
     */
    private final int imageID;

    /**
     * The name of the Plant.
     */
    private final String name;

    /**
     * The max health to set.
     */
    private final double maxHealth;

    /**
     * The cost of the plant.
     */
    private final int cost;

    /**
     * Every how many ticks this plant shoots.
     */
    private final int shotsEveryTicks;

    /**
     * The damage of the Projectile.
     */
    private final double projectileDamage;

    /**
     * The range of the plant.
     */
    protected final double range;

    /**
     * The current health of the plant.
     */
    private double currentHealth = 0;

    /**
     * The current tick of the plant.
     */
    protected int currentTick = 0;

    /**
     * The lane to store this plant.
     */
    protected final int lane;

    /**
     * the slot to store this plant.
     */
    protected final int slot;


    public Plant(int lane, int slot,
                 String name, int imageID,
                 double maxHealth, int cost,
                 int shotsEveryTicks, double projectileDamage,
                 double range) {

        this.lane = lane;
        this.slot = slot;

        this.name = name;
        this.imageID = imageID;

        this.cost = cost;
        this.maxHealth = maxHealth;

        this.shotsEveryTicks = shotsEveryTicks;
        this.projectileDamage = projectileDamage;
        this.range = range;

        this.currentHealth = maxHealth;
    }


    /**
     * Ticks the plant.
     */
    public final void tick( EventSystem eventSystem ) {
        this.currentTick ++;

        //This is shooting. Only shoot if we do damage.:
        if(    shotsEveryTicks > 0
            && projectileDamage > 0
            && currentTick % shotsEveryTicks == 0
            ) {
            shootProjectile( eventSystem );
        }

        tickIntern( eventSystem );
    }


    /**
     * Fires a projectile. This may be overwritten.
     * @param eventSystem to use.
     */
    protected void shootProjectile( EventSystem eventSystem ){
        eventSystem.fireEvent( new PlantShootsOnLaneEvent( lane, this, range ));
    }

    /**
     * Does an internal tick. This is done AFTER the Main tick!
     *
     * @param eventSystem to use for firing events internally.
     */
    protected abstract void tickIntern( EventSystem eventSystem );


    /**
     * Damages the Plant.
     * @param damage the damage to do.
     */
    public void damage( double damage ) {
        this.currentHealth -= damage;
    }

    public boolean isAlive(){
        return this.currentHealth > 0;
    }

    public boolean isDead(){
        return !isAlive();
    }


    public String getName() {
        return name;
    }

    public int getCurrentTick() {
        return currentTick;
    }

    public int getImageID() {
        return imageID;
    }

    public double getMaxHealth() {
        return maxHealth;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public int getShotsEveryTicks() {
        return shotsEveryTicks;
    }

    public double getProjectileDamage() {
        return projectileDamage;
    }

    public int getCost() {
        return cost;
    }

    public int getSlot() {
        return slot;
    }

    public int getLane() {
        return lane;
    }

    public double getRange() {
        return range;
    }

    public static class NameSorter implements Comparator<Plant> {
        @Override public int compare(Plant first, Plant second) {
            return first.getName().compareTo( second.getName() );
        }
    }

    public static class CostSorter implements Comparator<Plant> {
        @Override public int compare(Plant first, Plant second) {
            return Integer.compare( first.getCost(), second.getCost() );
        }
    }
}
