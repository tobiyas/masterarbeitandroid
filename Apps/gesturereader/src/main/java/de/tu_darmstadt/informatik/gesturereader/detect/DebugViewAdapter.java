package de.tu_darmstadt.informatik.gesturereader.detect;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.StreamingDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.Limb;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.SourceLinker;
import java8.util.Optional;

/**
 * Created by Toby on 09.10.2017
 */

public class DebugViewAdapter {


    private static final int DEFAULT_UPDATE_INTERVAL_MS = 300;


    /**
     * The Threshold of error to when show movement.
     */
    private static final double MOVEMENT_DETECTION_THRESHOLD = 3;

    /**
     * The activity to link
     */
    private final Activity activity;

    /**
     * The registered runnable to cancel
     */
    private Runnable registeredRunnable = null;


    public DebugViewAdapter( Activity activity ){
        this.activity = activity;
    }


    /**
     * Does an unregister.
     */
    public void unregisterAdapter( ){
        if( registeredRunnable == null ) return;

        View root = activity.findViewById( R.id.debug_root );
        if( root != null ) root.removeCallbacks( registeredRunnable );

        registeredRunnable = null;
    }

    /**
     * Registers the Debug adapter. This searches the View and registers a task for updating.
     * @return true if registering worked, false if could not register.
     */
    public boolean registerAdapter( ) {
        return registerAdapter( DEFAULT_UPDATE_INTERVAL_MS );
    }

    /**
     * Registers the Debug adapter. This searches the View and registers a task for updating.
     * @param updateIntervalMS the interval for updates.
     *
     * @return true if registering worked, false if could not register.
     */
    public boolean registerAdapter( int updateIntervalMS ) {
        //Be sure to unregister first:
        unregisterAdapter();

        View root = activity.findViewById( R.id.debug_root );

        View armLeft = activity.findViewById( R.id.debug_left_arm );
        View armRight = activity.findViewById( R.id.debug_right_arm );
        View footLeft = activity.findViewById( R.id.debug_left_foot );
        View footRight = activity.findViewById( R.id.debug_right_foot );

        //Only if they are present:
        if( root == null || armLeft == null || armRight == null || footLeft == null || footRight == null ) return false;

        //Register runner:
        SourceLinker linker = MainApplication.fromContext( activity ).getSession().getLinker();
        registeredRunnable = () -> runDetection( root, armLeft, armRight, footLeft, footRight, linker, updateIntervalMS);
        root.postDelayed( registeredRunnable, updateIntervalMS);
        return true;
    }


    /**
     * Runs the Detection.
     *
     * @param root to check for visibility.
     * @param armLeft to use.
     * @param armRight to use.
     * @param footLeft to use.
     * @param footRight to use.
     */
    private void runDetection( View root, View armLeft, View armRight, View footLeft, View footRight, SourceLinker linker, int updateIntervalMS ){
        //Reregister and ignore errors on that!
        try{ root.postDelayed( registeredRunnable, updateIntervalMS ); }catch ( Throwable ignored ){}

        //Break on invisibility:
        if( root.getVisibility() != View.VISIBLE ) return;

        movementDetection( Limb.LEFT_HAND, armLeft, linker, updateIntervalMS );
        movementDetection( Limb.RIGHT_HAND, armRight, linker, updateIntervalMS );
        movementDetection( Limb.LEFT_FOOT, footLeft, linker, updateIntervalMS );
        movementDetection( Limb.RIGHT_FOOT, footRight, linker, updateIntervalMS );
    }


    /**
     * Does a detection for the Movement.
     * @param limb to use.
     * @param view to post on.
     * @param linker to use for source detection.
     * @param updateIntervalMS to use for reading.
     */
    private void movementDetection(Limb limb, View view, SourceLinker linker, int updateIntervalMS ) {
        DataSource source = linker.getSource( limb );

        if (source != null) {
            double motion = getMotionMSE( source, updateIntervalMS );
            view.setBackgroundColor(
                    motion <= 0 ?
                            Color.BLUE :
                            (motion > MOVEMENT_DETECTION_THRESHOLD ?
                                    Color.RED :
                                    Color.GREEN
                            )
            );
        }else{
            view.setBackgroundColor( Color.TRANSPARENT );
        }
    }


    /**
     * Gets the Motion of the Source linked.
     *
     * @param source the source to read from.
     * @param updateIntervalMS the interval to use for reading.
     *
     * @return Value :
     *      < 0 if the source does not seem to have a motion element.
     *      > 0 if the source has a motion component and it can be evaluated.
     */
    private double getMotionMSE( DataSource source, int updateIntervalMS ){
        if(! ( source instanceof StreamingDataSource ) ) return -1;

        StreamingDataSource streamingDataSource = (StreamingDataSource) source;
        Optional<SensorDataBatch> oBatch = streamingDataSource.collectStreamedData( DateTime.now().minusMillis(updateIntervalMS), DateTime.now() );
        if( !oBatch.isPresent() ) return -1;

        Optional<AccelerationData> oData = oBatch.map( batch -> batch.getData( AccelerationData.class ) ).orElse( null );
        if( !oData.isPresent() ) return -1;

        AccelerationData data = oData.get();
        double mse = data.getMeanSquareError();

        //mse with less values are pretty bad, so we need to up them a bit.
        double values = data.getT().length;
        if(values <= 10) mse *= 20 - values;

        return mse;
    }


    /**
     * Hide the GUI.
     */
    public void hide(){
        View root = activity.findViewById( R.id.debug_root );
        if( root != null ) root.setVisibility( View.INVISIBLE );
    }

    /**
     * Shows the GUI.
     */
    public void show(){
        View root = activity.findViewById( R.id.debug_root );
        if( root != null ) root.setVisibility( View.VISIBLE );
    }

}
