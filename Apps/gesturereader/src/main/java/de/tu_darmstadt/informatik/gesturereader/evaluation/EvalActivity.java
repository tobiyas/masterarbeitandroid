package de.tu_darmstadt.informatik.gesturereader.evaluation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameActivity;
import de.tu_darmstadt.informatik.gesturereader.game.fight.level.Level;
import de.tu_darmstadt.informatik.gesturereader.game.fight.level.Levels;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnActivity;
import de.tu_darmstadt.informatik.gesturereader.utils.ViewUtils;
import de.tu_darmstadt.informatik.gesturereader.utils.Zipper;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import java8.util.function.Supplier;
import java8.util.stream.Collectors;
import java8.util.stream.IntStreams;
import java8.util.stream.RefStreams;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 31.12.2017
 */

public class EvalActivity extends OwnActivity {


    private static final String DEFAULT_MODEL_NAME = "fight_rf";


    private static final int REQUEST_CODE_LEVEL_TRAINING_0 = 1334;
    private static final int REQUEST_CODE_LEVEL_TRAINING_1 = 1335;
    private static final int REQUEST_CODE_LEVEL_TRAINING_2 = 1336;
    private static final int REQUEST_CODE_LEVEL_1 = 1337;
    private static final int REQUEST_CODE_LEVEL_2 = 1338;
    private static final int REQUEST_CODE_LEVEL_3 = 1339;


    private String currentEvalName;
    private String currentModelName;
    private ModelProxy defaultModel;

    @Override
    protected void onCreate( @Nullable Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.activity_eval );
        updateModels();

        reset( findViewById( R.id.next_user ) );

        findViewById( R.id.start_game ).setOnLongClickListener( this::selectLevel );
        findViewById( R.id.next_user ).setOnLongClickListener( this::forceUser );


        //Set Checkboxes for Fight:
        CheckBox landscape = findViewById( R.id.landscape );
        landscape.setChecked( FightGameActivity.USE_LANDSCAPE );
        landscape.setOnCheckedChangeListener( (v,s) -> FightGameActivity.USE_LANDSCAPE = s );

        CheckBox useAltKick = findViewById( R.id.alt_kick );
        useAltKick.setChecked( FightGameActivity.USE_ALT_KICK );
        useAltKick.setOnCheckedChangeListener( (v,s) -> FightGameActivity.USE_ALT_KICK = s );
    }


    private boolean forceUser( View view ) {
        ViewUtils.buildInputDialogNumber( this, "User NR?", "0", "APPLY", "CANCEL", s ->
            ( ( TextView ) findViewById( R.id.current_name ) ).setText( currentEvalName = "Eval" + s )
        , () -> {} );
        return true;
    }


    private boolean selectLevel( View view ) {
        String[] levels = { "Level 1", "Level 2", "Level 3" };
        new AlertDialog.Builder( this )
                .setTitle( "Which Level?" )
                .setNegativeButton( android.R.string.cancel, (v,i) -> v.dismiss() )
                .setItems( levels, (v,i) -> {
                    switch ( i ) {
                        case 0 : startNormal( view ); break;
                        case 1 : loadingDialogTillDone( "Starting Level 2", buildNewModelAndStartLevel2(), true ); break;
                        case 2 : loadingDialogTillDone( "Starting Level 3", buildNewModelAndStartLevel3(), true ); break;
                   }
                }).create().show();

        return true;
    }


    private void updateModels() {
        GestureRecSession session = session();
        String[] models = stream( session.getModelManager().getAllModelNames() ).sorted().toArray( String[]::new );
        int slotDefault = IntStreams.range(0,models.length)
                .filter( i -> models[i].equalsIgnoreCase( DEFAULT_MODEL_NAME ) )
                .findFirst()
                .orElse( 0 );

        Spinner spinner = findViewById( R.id.model );
        spinner.setAdapter( new ArrayAdapter<>( this, android.R.layout.simple_list_item_1, models ) );
        spinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) { currentModelName = models[pos]; defaultModel = session.getModelManager().getForName( currentModelName ); }
            @Override public void onNothingSelected(AdapterView<?> adapterView) { currentModelName = null; defaultModel = null; }
        });

        //Change to the default Model:
        spinner.setSelection( slotDefault );
    }


    public void reset( View view ){
        currentEvalName = generateEvalName();
        ((TextView)findViewById( R.id.current_name )).setText( currentEvalName );

        ((TextView)findViewById( R.id.game_1 )).setText( R.string.game_1 );
        ((TextView)findViewById( R.id.game_2 )).setText( R.string.game_2 );
        ((TextView)findViewById( R.id.game_3 )).setText( R.string.game_3 );
    }


    /**
     * Generates the next Eval nr.
     * @return the next eval nr.
     */
    private String generateEvalName() {
        String prefix = "Eval";

        Collection<String> present = Arrays.asList( session().getUserManager().readAllPresentUsers() );
        for( int i = 0; i < Integer.MAX_VALUE; i++ ){
            String tmpName = prefix + i;
            if(    present.contains( tmpName )
                || present.contains( tmpName + "-1" )
                || present.contains( tmpName + "-2" )
                || present.contains( tmpName + "-3" )
                ) continue;

            return tmpName;
        }

        return "ALL HAIL MEGATRON";
    }


    public void startNormal( View view ) {
        if( currentEvalName == null || session().getModelManager().getForName( currentModelName ) == null ){
            Toast.makeText( this, "No Model chosen", Toast.LENGTH_LONG ).show();
            return;
        }

        startLevel( currentEvalName + "-1", Levels.level1(), REQUEST_CODE_LEVEL_1, null );
    }

    public void startTrainingGesture( View view ) {
        if( currentEvalName == null || session().getModelManager().getForName( currentModelName ) == null ){
            Toast.makeText( this, "No Model chosen", Toast.LENGTH_LONG ).show();
            return;
        }

        session().Dialogs().OpenGestureSingleSelectionDialog( this, defaultModel.getGestures(), g ->
            startLevel( null, Levels.trainingSingleGesture( g.getName() ), REQUEST_CODE_LEVEL_TRAINING_0, null )
        );
    }

    public void startTraining1( View view ) {
        if( currentEvalName == null || session().getModelManager().getForName( currentModelName ) == null ){
            Toast.makeText( this, "No Model chosen", Toast.LENGTH_LONG ).show();
            return;
        }

        startLevel( null, Levels.training1(), REQUEST_CODE_LEVEL_TRAINING_1, null );
    }


    public void startTraining2( View view ) {
        if( currentEvalName == null || session().getModelManager().getForName( currentModelName ) == null ){
            Toast.makeText( this, "No Model chosen", Toast.LENGTH_LONG ).show();
            return;
        }

        startLevel( null, Levels.training2(), REQUEST_CODE_LEVEL_TRAINING_2, null );
    }


    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        super.onActivityResult(requestCode, resultCode, data);

        int enemyHealth = data.getIntExtra( FightGameActivity.ENEMY_HEALTH_PATH, -1 );
        int selfHealth = data.getIntExtra( FightGameActivity.SELF_HEALTH_PATH, -1 );
        double precision = data.getDoubleExtra( FightGameActivity.PRECISION, 0 ) * 100d;

        String pre = new DecimalFormat( "0.0" ).format( precision );
        String text = enemyHealth == selfHealth && enemyHealth == -1 ? "NOT PLAYED" :
                enemyHealth > 0 ? "LOSE" : "WIN";
        text += " P: " + pre + "%";

        switch ( requestCode ) {
            case REQUEST_CODE_LEVEL_1:
                ( ( TextView ) findViewById( R.id.game_1 ) ).setText( "Game 1: " + text );
                openCheckAndLoadingDialog( 1 , () -> loadingDialogTillDone( "Starting Level 2", buildNewModelAndStartLevel2(), true ) );
                break;

            case REQUEST_CODE_LEVEL_2:
                ( ( TextView ) findViewById( R.id.game_2 ) ).setText( "Game 2: " + text );
                openCheckAndLoadingDialog( 2 , () -> loadingDialogTillDone( "Starting Level 3", buildNewModelAndStartLevel3(), true ) );
                break;

            case REQUEST_CODE_LEVEL_3:
                ( ( TextView ) findViewById( R.id.game_3 ) ).setText( "Game 3: " + text );
                openCheckAndLoadingDialog( 3 , () -> loadingDialogTillDone( "Evaluating...", buildNewModel4(), true ) );
                break;
        }
    }


    private void openCheckAndLoadingDialog( int stage, Runnable positiveCallback ){
        SampleBatch samples = session().getSampleManager().query().addUsers(currentEvalName+"-"+stage).applyPurgeFilter();

        VerifySamplesDialog dialog = new VerifySamplesDialog( this );
        dialog.setSamples( samples );
        dialog.setOnDoneCallback( positiveCallback );
        dialog.show();
    }


    private final Executor executor = Executors.newSingleThreadExecutor();
    private void loadingDialogTillDone( final String text, Pair<Supplier<Boolean>,Runnable> runs, boolean cancelable ){
        AtomicBoolean canceled = new AtomicBoolean( false );
        ProgressDialog dialog = ProgressDialog.show( this, "Loading...", text, true, cancelable );
        dialog.setOnCancelListener( c -> canceled.set( true ) );

        executor.execute( () -> {
            boolean worked;
            try{ worked = runs.getFirst().get(); }
            catch ( Throwable exp ){
                exp.printStackTrace();
                runOnUiThread( () -> Toast.makeText( this, "Error", Toast.LENGTH_LONG ).show() );
                worked = false;
            }

            //Only run next if worked.
            final boolean fWorked = worked;
            runOnUiThread( () -> {
                dialog.dismiss();
                if( fWorked && runs.getSecond() != null && !canceled.get() )  runs.getSecond().run();
            });
        } );
    }


    /**
     * Builds a new Model and starts level 2.
     */
    private Pair<Supplier<Boolean>,Runnable> buildNewModelAndStartLevel2() {
        String modelName = currentEvalName + "_rf_half";
        ModelProxy present = session().getModelManager().getForName( modelName );
        if( present != null ) session().getModelManager().deleteModel( present );

        Supplier<Boolean> run1 = () -> {
            SampleBatch batch = session().getSampleManager().query().addUsers( currentEvalName + "-1" ).addGestures( defaultModel.getGestures() ).applyFilter();
            ModelProxy model = session().SimpleModelCreator().createNewModel( modelName, WekaBaseClassifier.RANDOM_FOREST, batch, false );

            //If model fails: tell!
            if( model == null ) {
                Toast.makeText( this, "Model creation failed!", Toast.LENGTH_LONG ).show();
                return false;
            }

            return true;
        };

        Runnable run2 = () -> startLevel( currentEvalName + "-2", Levels.level2(), REQUEST_CODE_LEVEL_2, modelName );
        return new Pair<>( run1,run2 );
    }


    /**
     * Builds a new Model and starts level 3.
     */
    private Pair<Supplier<Boolean>,Runnable> buildNewModelAndStartLevel3() {
        String modelName = currentEvalName + "_rf_full";
        ModelProxy present = session().getModelManager().getForName( modelName );
        if( present != null ) session().getModelManager().deleteModel( present );

        Supplier<Boolean> run1 = () -> {
            SampleBatch batch = session().getSampleManager().query().addUsers(currentEvalName + "-1", currentEvalName + "-2").addGestures( defaultModel.getGestures() ).applyFilter();
            ModelProxy model = session().SimpleModelCreator().createNewModel(modelName, WekaBaseClassifier.RANDOM_FOREST, batch, false);

            //If model fails: tell!
            if (model == null) {
                Toast.makeText(this, "Model creation failed!", Toast.LENGTH_LONG).show();
                return false;
            }

            return true;
        };

        Runnable run2 = () -> startLevel( currentEvalName + "-3", Levels.level3(), REQUEST_CODE_LEVEL_3, modelName );
        return new Pair<>( run1, run2 );
    }


    /**
     * Builds a new Model for stage 4.
     */
    private Pair<Supplier<Boolean>,Runnable>  buildNewModel4() {
        String modelName = currentEvalName + "_rf_finish";
        ModelProxy present = session().getModelManager().getForName( modelName );
        if( present != null ) session().getModelManager().deleteModel( present );

        Supplier<Boolean> run1 = () -> {
            SampleBatch batch = session().getSampleManager().query().addUsers(currentEvalName + "-1", currentEvalName + "-2", currentEvalName + "-3").addGestures( defaultModel.getGestures() ).applyFilter();
            ModelProxy model = session().SimpleModelCreator().createNewModel( modelName, WekaBaseClassifier.RANDOM_FOREST, batch, false);

            //If model fails: tell!
            if (model == null) {
                Toast.makeText(this, "Model creation failed!", Toast.LENGTH_LONG).show();
                return false;
            }


            //Save all Models + Samples to Zip-File:
            File zipFolder = new File( Environment.getExternalStorageDirectory(), "EvalResults" );
            if( !zipFolder.exists() ) zipFolder.mkdirs();

            //Get all fight files:
            File fightBase = new File( new File( Environment.getExternalStorageDirectory(), "GestureRec" ), "FightRecords" );
            Collection<File> files = RefStreams.of( fightBase.listFiles() )
                    .filter( f -> f.getName().contains( currentEvalName ) )
                    .map( File::listFiles )
                    .flatMap( RefStreams::of )
                    .collect( Collectors.toList() );


            //This is only a best effort try:
            try{
                new Zipper( new File( zipFolder, currentEvalName + ".zip" ) )
                        .addModel( model )
                        .addModel( session().getModelManager().getForName( currentEvalName + "_rf_half" ) )
                        .addModel( session().getModelManager().getForName( currentEvalName + "_rf_full" ) )
                        .addSamples( batch )
                        .addFiles( files )
                        .create();
            }catch ( IOException exp ){
                exp.printStackTrace();
            }

            return true;
        };

        Runnable run2 = () -> Toast.makeText( this, "Fertig!", Toast.LENGTH_LONG ).show();
        return new Pair<>( run1, run2 );
    }


    /**
     * Starts the next Level.
     * @param username to use.
     * @param nextLevel to start.
     * @param newRequestCode request code to use.
     */
    private void startLevel( String username, Level nextLevel, int newRequestCode, String forcedModelName ) {
        String model = forcedModelName == null ? currentModelName : forcedModelName;
        if( currentEvalName == null || session().getModelManager().getForName( model ) == null ){
            Toast.makeText( this, "No Model chosen", Toast.LENGTH_LONG ).show();
            return;
        }

        final Intent intent = new Intent( this, FightGameActivity.class );
        if( username != null ) intent.putExtra( "user", username );
        intent.putExtra( "model", model );
        nextLevel.saveTo( intent );


        //Go Dialog!
        new AlertDialog.Builder( this )
                .setTitle( "Press GO when ready" )
                .setMessage( "Ready to start " + nextLevel.getRound() + "?" )
                .setPositiveButton( "GO", (v,d) -> startActivityForResult( intent, newRequestCode ) )
                .setNegativeButton( "ABORT", (v,d) -> v.dismiss() )
                .setCancelable( false )
                .create()
                .show();
    }

}
