package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

/**
 * This event represents a Zombie damaging a plant.
 * Created by Toby on 24.10.2017
 */

public class ZombieDamagesPlantEvent extends Event {


    /**
     * The Chewing Zombie.
     */
    private final Zombie zombie;

    /**
     * The plant beeing eaten.
     */
    private final Plant plant;

    /**
     * the damage dealt.
     */
    private double damage;


    public ZombieDamagesPlantEvent(Zombie zombie, Plant plant, double damage) {
        this.zombie = zombie;
        this.plant = plant;
        this.damage = damage;
    }


    public Zombie getZombie() {
        return zombie;
    }

    public Plant getPlant() {
        return plant;
    }

    public double getDamage() {
        return damage;
    }
}
