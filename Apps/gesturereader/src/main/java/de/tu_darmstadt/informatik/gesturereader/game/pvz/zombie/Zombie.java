package de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie;


import java.util.Comparator;
import java.util.Random;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieDamagesPlantEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieReachedEndOfLaneEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.PlantProvider;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;

/**
 * Created by Toby on 23.10.2017
 */

public abstract class Zombie {


    private static final double START_POSITION = 6;
    protected static final double DEFAULT_MOVEMENT_SPEED = 0.03;

    /**
     * Rand Gen.
     */
    private static final Random rand = new Random();


    /**
     * The ID of the Zombie.
     */
    private final long ID = rand.nextLong();

    /**
     * The ID of the Resource used.
     */
    private final int resourceID;

    /**
     * The max health of the Zombie.
     */
    private final double maxHealth;

    /**
     * The lane this is walking on.
     */
    private final int lane;

    /**
     * The used Position.
     */
    private double position = START_POSITION;

    /**
     * The MovementSpeed per tick.
     */
    private double movementSpeed = DEFAULT_MOVEMENT_SPEED;

    /**
     * The current Health of the Zombie.
     */
    private double health = 42;

    /**
     * The tick currently chewing.
     */
    private int chewTick = 0;

    /**
     * the ticks needed till damage is done.
     */
    private final int ticksToDamage;

    /**
     * the damage this can do..
     */
    private final double damage;


    public Zombie( int resourceID, int lane, double maxHealth, double movementSpeed, int ticksToDamage, double damage ) {
        this.resourceID = resourceID;
        this.maxHealth = maxHealth;
        this.movementSpeed = movementSpeed;
        this.lane = lane;

        this.health = maxHealth;
        this.ticksToDamage = ticksToDamage;
        this.damage = damage;
    }

    /**
     * Ticks the Zombie.
     */
    public void tick( EventSystem eventSystem, PlantProvider plantProvider ){
        double oldPosition = position;
        double newPosition = position - movementSpeed;

        //Check for chewing:
        if(newPosition > 0 && oldPosition <= 3 && ( (oldPosition % 1) != (newPosition % 1) ) ){
            //Check if a plant is at that position:
            Plant plant = plantProvider.getPlantAt( lane, (int) newPosition );
            if( plant != null) {

                //There is a plant in our way! Start eating it up!
                chewTick ++;

                if( ( chewTick % ticksToDamage ) == 0 ){
                    ZombieDamagesPlantEvent event = new ZombieDamagesPlantEvent( this, plant, damage );
                    eventSystem.fireEvent( event );
                }

                return;
            }
        }

        //No chewing -> Move:
        chewTick = 0;
        position = newPosition;

        if( position < 0 ) eventSystem.fireEvent( new ZombieReachedEndOfLaneEvent( this, lane ) );
    }

    /**
     * Damages the Zombie.
     * @param damage to deal.
     */
    public void damage( double damage ){
        this.health -= damage;
    }

    /**
     * Heals the Zombie.
     * @param health to add.
     */
    public void heal( double health ){
        this.health = Math.min( maxHealth, this.health + health );
    }

    /**
     * Instantly kills the Zombie.
     */
    public void kill(){
        this.health = 0;
    }

    /**
     * Returns if the Zombie is dead.
     * @return true if dead.
     */
    public boolean isDead(){
        return health <= 0;
    }

    /**
     * Returns if the Zombie is alive.
     * @return true if alive.
     */
    public boolean isAlive(){
        return !isDead();
    }

    /**
     * Gets the Position of the Zombie.
     * The end of the Row = 0.
     * The end of the first tile = 1;
     * The end of the second tile = 2;
     * The end of the third tile = 3;
     * The end of the Map = 5;
     *
     * @return the position.
     */
    public double getPosition() {
        return position;
    }


    /**
     * Gets the Start position.
     * @return start position.
     */
    public static double getMaxPosition(){
        return START_POSITION;
    }


    /**
     * Gets the current health of the Zombie.
     * @return the current Health.
     */
    public double getHealth() {
        return health;
    }

    /**
     * The ID of the Zombie. This is unique to the Zombie.
     * @return the ID of the Zombie.
     */
    public long getID() {
        return ID;
    }

    /**
     * Gets the Row this is walking on.
     */
    public int getLane() {
        return lane;
    }

    /**
     * Gets the used resource ID.
     * @return the used ID.
     */
    public int getResourceID() {
        return resourceID;
    }

    @Override
    public boolean equals( Object obj ) {
        if( obj instanceof  Zombie ){
            Zombie other = (Zombie) obj;
            return ID == other.getID();
        }

        return super.equals( obj );
    }


    @Override
    public int hashCode() {
        return Long.valueOf( ID ).hashCode();
    }


    public static final class FirstPositionSorter implements Comparator<Zombie> {

        @Override
        public int compare(Zombie o1, Zombie o2) {
            return Double.compare( o1.getPosition(), o2.getPosition() );
        }
    }


    /**
     * Gets a random element of the Array.
     *
     * @param values to search.
     * @return a random element.
     */
    protected static <T> T rand( T... values ){
        if( values == null || values.length == 0 ) return null;

        int index = new Random().nextInt( values.length );
        return values[index];
    }

}
