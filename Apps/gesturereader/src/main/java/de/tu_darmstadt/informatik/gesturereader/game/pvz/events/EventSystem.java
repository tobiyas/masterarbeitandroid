package de.tu_darmstadt.informatik.gesturereader.game.pvz.events;

import android.util.Log;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.tu_darmstadt.informatik.kom.contextsense.utils.DefaultHashMapSupplier;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 22.10.2017
 */

public class EventSystem {

    /**
     * This is the Call System for Events.
     */
    private final Map<Class<?extends Event>, Set<Pair<EventListener,Method>>> listeners = new DefaultHashMapSupplier<>( HashSet::new );


    /**
     * Adds an Event Listener to the System.
     */
    @SuppressWarnings("SuspiciousMethodCalls")
    public void addListener(EventListener listener ){
        for( Method method : listener.getClass().getDeclaredMethods() ){
            Class<?>[] types = method.getParameterTypes();
            boolean hasAnnotation = method.isAnnotationPresent( EventHandler.class );
            boolean has1Arg = types.length == 1;
            boolean isInstance = has1Arg && Event.class.isAssignableFrom( types[0] );

            //Found one:
            if( hasAnnotation && isInstance){
                Pair<EventListener,Method> pair = new Pair<>( listener, method );

                Set<Pair<EventListener,Method>> list = listeners.get( types[0] );
                list.add( pair );
                continue;
            }

            //Instance wrong:
            if( hasAnnotation ){
                Log.w( "EventSystem", "Handler: '" + listener.getClass().getName() + "' tried to register method '" + method.getName() + "' But did not have 1 Event as Arg." );
            }
        }

    }


    /**
     * Clears the system.
     */
    public void clear(){
        listeners.clear();
    }


    /**
     * Fires an Event.
     * @param event to fire.
     */
    public void fireEvent( Event event ){
        if( event == null ) return;

        Class<? extends Event> clazz = event.getClass();
        stream( this.listeners.get( clazz ) )
                .forEach( p -> invoke( p, event ) );
    }


    /**
     * Invokes the passed stuff.
     * @param pair to use
     * @param toInvoke to pass.
     */
    private void invoke( Pair<EventListener,Method> pair, Event toInvoke ) {
        try{
            pair.getSecond().invoke( pair.getFirst(), toInvoke );
        }catch ( Throwable exp ){
            exp.printStackTrace();
        }
    }


}
