package de.tu_darmstadt.informatik.gesturereader.models;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.evaluation.SampleEqualizer;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnFragment;
import de.tu_darmstadt.informatik.gesturereader.utils.ViewUtils;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaDetector;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.evaluate.EvaluationResults;
import de.tu_darmstadt.informatik.kom.gesturerec.evaluate.EvaluationType;
import de.tu_darmstadt.informatik.kom.gesturerec.evaluate.EvaluatorProperties;
import de.tu_darmstadt.informatik.kom.gesturerec.evaluate.SimpleEvaluator;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;
import java8.util.function.Consumer;
import java8.util.stream.Collectors;
import java8.util.stream.RefStreams;

import static de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread.ToastLong;
import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 08.01.2018
 */
public class EvalFragment extends OwnFragment {

    private static final String[] VARIANTS = RefStreams.of( WekaBaseClassifier.values() )
            .filter( v -> v != WekaBaseClassifier.NAIVE_BAYES_MULTINOMIAL )
            .filter( v -> v != WekaBaseClassifier.M5P )
            .filter( v -> v != WekaBaseClassifier.MULTILAYER_PERCEPTRON )
            .map( WekaBaseClassifier::name )
            .sorted()
            .toArray( String[]::new );

    private static final String[] MODES = RefStreams.of( EvaluationType.values() )
            .map( Enum::name )
            .toArray( String[]::new );


    private final EvaluatorProperties properties = new EvaluatorProperties();
    private GestureRecSession session;
    private EvaluationType type = EvaluationType.SPLIT;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate( R.layout.fragment_eval, container, false );
        this.session = session();

        Context context = this.getContext();
        if( context == null ) return null;

        //Add the Weka-Detector types:
        Spinner spinner = view.findViewById( R.id.spinner_weka_detector );
        spinner.setAdapter( new ArrayAdapter<>( context, android.R.layout.simple_list_item_1, VARIANTS ) );
        spinner.setOnItemSelectedListener( new IgnoreNothingListener<>( this::changeVariant ) );

        //Add the Modes present:
        Spinner modesSpinner = view.findViewById( R.id.spinner_mode );
        modesSpinner.setAdapter( new ArrayAdapter<>( getContext(), android.R.layout.simple_list_item_1, MODES ) );
        modesSpinner.setOnItemSelectedListener( new IgnoreNothingListener<>( this::changeModes ) );


        //Set update listeners for buttons:
        view.findViewById( R.id.select_gestures ).setOnClickListener( this::updateGestures );
        view.findViewById( R.id.select_users ).setOnClickListener( this::updateUsers );
        view.findViewById( R.id.select_users_test ).setOnClickListener( this::changeTestUsers );


        //Set the Seekbar to adjust levels:
        SeekBar seek = view.findViewById( R.id.split );
        seek.setOnSeekBarChangeListener( new IgnoringSeekBarChangedListener( this::seekChanged ) );
        view.findViewById( R.id.split_value ).setOnLongClickListener( this::promptNumber );


        //add the Build listener:
        view.findViewById( R.id.build ).setOnClickListener( this::build );

        //Set the Checkbox for Random
        CheckBox randomCheck = view.findViewById( R.id.check_random_split );
        randomCheck.setOnCheckedChangeListener( this::randomChanged );

        //Add the current Eval stuff:
        view.findViewById( R.id.eval_users ).setOnClickListener( this::evalUsersMerged );
        view.findViewById( R.id.eval_users ).setOnLongClickListener( this::evalUsersMergedAllClassifiers );
        view.findViewById( R.id.cross_eval_user ).setOnClickListener( this::crossEvalUser );
        view.findViewById( R.id.cross_eval_user ).setOnLongClickListener( this::crossEvalUserAllClassifiers );


        //Set longclick for buttons, select all!
        view.findViewById( R.id.select_gestures ).setOnLongClickListener( this::selectAllGestrues );
        view.findViewById( R.id.select_users ).setOnLongClickListener( this::selectAllUsers );
        view.findViewById( R.id.select_users_test ).setOnLongClickListener( this::selectAllUserTest );

        //Debug output:
        randomCheck.setOnLongClickListener( this::debugLongClick );

        return view;
    }

    private boolean selectAllUserTest(View view) {
        if( !view.isEnabled() ) return true;

        properties.clearTestUsers();
        properties.addTestUsers( Arrays.asList( session.getUserManager().readAllPresentUsers() ) );
        updateStatus();

        Toast.makeText( getContext(), "Selected all Test-Users", Toast.LENGTH_LONG ).show();
        return true;
    }

    private boolean selectAllUsers(View view) {
        if( !view.isEnabled() ) return true;

        properties.clearUsers();
        properties.addUsers( Arrays.asList( session.getUserManager().readAllPresentUsers() ) );
        updateStatus();

        Toast.makeText( getContext(), "Selected all Users", Toast.LENGTH_LONG ).show();
        return true;
    }

    private boolean selectAllGestrues(View view) {
        if( !view.isEnabled() ) return true;

        properties.clearGestures();
        properties.addGestures( session.getGestureManager().getAllWithoutUnknown() );
        updateStatus();

        Toast.makeText( getContext(), "Selected all Gestures", Toast.LENGTH_LONG ).show();
        return true;
    }


    /**
     * This is a simple utility method to link the
     * @param view the textView clicked on.
     * @return always true
     */
    private boolean promptNumber( View view ) {
        if( !(view instanceof  TextView) ) return true;

        if( type == EvaluationType.SPLIT || type == EvaluationType.CROSS_VALIDATION ){
            final TextView textView = (TextView) view;
            ViewUtils.buildSaveInputDialogNumber( getContext(), "Amount?", (long)properties.getValue(),
                new ViewUtils.InputDialogCallbackNumber(){
                    @Override public void acceptClicked(DialogInterface dialog, long number) {
                        if( getView() == null ) return;

                        SeekBar seekBar = getView().findViewById( R.id.split );
                        if( number > 0L && number <= seekBar.getMax() ){
                            seekBar.setProgress( (int)number );
                            textView.setText( String.valueOf( number ) );
                        }
                    }

                    @Override public void cancelClicked( DialogInterface dialog ) { dialog.dismiss(); }
                }
            ).show();

        }

        return true;
    }

    /**
     * This may be deleted!
     */
    private boolean debugLongClick( View unused ) {
        final ProgressDialog dialog = ProgressDialog.show(
                getContext(),
                "Creating",
                "STARTING...",
                true,
                false);

        Consumer<String> setMessage = s -> MainThread.runOnMT( () -> dialog.setMessage( s ) );
        MainThread.runAsyncResultMT(
                () -> {
                    try {
                        setMessage.accept("Building...");
                        Pair<WekaDetector,EvaluationResults> worst = null;
                        int max = 10;
                        for( int i = 1; i<= max; i++ ) {
                            int worstAcc = worst == null ? 0 : (int)(worst.getSecond().getAccuracy()*100d);
                            setMessage.accept("Doing " + i + "/" + max + "(Worst: " + worstAcc + ")" );
                            SampleBatch userBatch = session.getSampleManager().query().addUsers(properties.getUsers()).addGestures(properties.getGestures()).applyFilter();

                            SampleBatch trainingSet = SampleEqualizer.getXOfEachGesture(userBatch, properties.getValue());
                            SampleBatch testSet = userBatch.removeSamples(trainingSet);

                            Pair<WekaDetector,EvaluationResults> next = SimpleEvaluator.buildBatchAgainstBatch(session, trainingSet, testSet, WekaBaseClassifier.RANDOM_FOREST);
                            if( worst == null || next.getSecond().getAccuracy() < worst.getSecond().getAccuracy() ) worst = next;
                        }

                        return worst;
                    }catch ( Throwable exp ){
                        MainThread.runOnMT( dialog::dismiss );
                        MainThread.runOnMT( () -> Toast.makeText( this.getContext(), "Error: " + exp.getMessage(), Toast.LENGTH_LONG ).show() );
                        exp.printStackTrace();
                        return null;
                    }
                },
                r -> {
                    //Does a dismiss:
                    dialog.dismiss();

                    //Show results:
                    displayResults( r.getSecond() );
                }
        );

        return true;
    }


    private void changeTestUsers( View unused ) {
        session.Dialogs().OpenStringMultiSelectionDialog(
                this.getContext(),
                "Test Users",
                session.getUserManager().readAllPresentUsersOrDefault(),
                properties.getTestUsers(),
                this::updateTestUsers
        );
    }


    public boolean crossEvalUserAllClassifiers ( View unused ) {
        String[] usersUnsorted = session.getUserManager().readAllPresentUsers();
        Collection<String> filteredUsers = RefStreams.of(usersUnsorted)
                .filter( u -> u.contains( "-" ) )
                .map( u -> u.endsWith( "-1" ) ? u.substring(0,u.length()-2) : u )
                .map( u -> u.endsWith( "-2" ) ? u.substring(0,u.length()-2) : u )
                .map( u -> u.endsWith( "-3" ) ? u.substring(0,u.length()-2) : u )
                .distinct().sorted().collect( Collectors.toList() );

        final ProgressDialog dialog = ProgressDialog.show(
                getContext(),
                "Creating",
                "STARTING...",
                true,
                false);

        Consumer<String> setMessage = s -> MainThread.runOnMT( () -> dialog.setMessage( s ) );


        session.Dialogs().OpenStringMultiSelectionDialog( getContext(), "Classifiers", VARIANTS, Arrays.asList( VARIANTS ), c -> {
            MainThread.runAsyncResultMT(
                    () -> {
                        try{
                            setMessage.accept( "Building..." );

                            File root = new File( session.getFileConstants().getBaseGestureFolder().getParentFile(), "EvalMatrixesCrossAll");
                            root = new File( root, System.currentTimeMillis() + "" );
                            if( !root.mkdirs() ) Log.w("File", "Could not create folders: " + root.getAbsolutePath() );

                            for( String typeName : c ) {
                                long before = System.currentTimeMillis();
                                MainThread.runOnMT( () -> dialog.setTitle( "Building " + typeName + "..." ) );
                                WekaBaseClassifier type = WekaBaseClassifier.valueOf( typeName );
                                Consumer<String> messageSetter = s -> setMessage.accept( type.name() + ": " + s);

                                EvaluationResults result = new EvaluationResults();
                                for (String user : filteredUsers) {
                                    Collection<String> userNames = Arrays.asList(user + "-1", user + "-2", user + "-3");
                                    Pair<WekaDetector, EvaluationResults> part = SimpleEvaluator.buildAllAgainstOneUserData(session, properties.getGestures(), userNames, type, messageSetter);
                                    result.mergeResults(part.getSecond());
                                }

                                File saveFile =  new File( root, typeName + ".csv" );
                                result.saveToCSF( saveFile );

                                //Write the time taken:
                                long took = System.currentTimeMillis() - before;

                                String timeText = "\r\n\r\ntime:;" + took;
                                FileUtils.write( saveFile, timeText, Charset.defaultCharset(), true );
                                Log.i( "TIMINGS", "Took " + took + "ms to eval " + typeName );
                            }

                            return null;
                        }catch ( Throwable exp ){
                            MainThread.runOnMT( dialog::dismiss );
                            MainThread.runOnMT( () -> Toast.makeText( this.getContext(), "Error: " + exp.getMessage(), Toast.LENGTH_LONG ).show() );
                            exp.printStackTrace();
                            return null;
                        }
                    },
                    r -> {
                        //Does a dismiss:
                        dialog.dismiss();
                        Toast.makeText( this.getContext(), "Done!", Toast.LENGTH_LONG ).show();
                    }
            );

        } );
        return true;
    }


    public void crossEvalUser ( View unused ){
        String[] usersUnsorted = session.getUserManager().readAllPresentUsers();
        Collection<String> filteredUsers = RefStreams.of( usersUnsorted )
                .filter( u -> u.contains( "-" ) )
                .map( u -> u.endsWith( "-1" ) ? u.substring(0,u.length()-2) : u )
                .map( u -> u.endsWith( "-2" ) ? u.substring(0,u.length()-2) : u )
                .map( u -> u.endsWith( "-3" ) ? u.substring(0,u.length()-2) : u )
                .distinct().sorted().collect( Collectors.toList() );

        final ProgressDialog dialog = ProgressDialog.show(
                getContext(),
                "Creating " + properties.getBaseDetector().toString(),
                "STARTING...",
                true,
                false);

        Consumer<String> setMessage = s -> MainThread.runOnMT( () -> dialog.setMessage( s ) );
        MainThread.runAsyncResultMT(
                () -> {
                    try{
                        setMessage.accept( "Building..." );
                        EvaluationResults result = new EvaluationResults();
                        for( String user : filteredUsers ){
                            Collection<String> userNames = Arrays.asList( user+"-1",user+"-2",user+"-3" );
                            Pair<WekaDetector, EvaluationResults> part = SimpleEvaluator.buildAllAgainstOneUserData( session, properties.getGestures(), userNames, properties.getBaseDetector(), setMessage );
                            result.mergeResults( part.getSecond() );
                        }

                        return result;
                    }catch ( Throwable exp ){
                        MainThread.runOnMT( dialog::dismiss );
                        MainThread.runOnMT( () -> Toast.makeText( this.getContext(), "Error: " + exp.getMessage(), Toast.LENGTH_LONG ).show() );
                        exp.printStackTrace();
                        return null;
                    }
                },
                r -> {
                    if( r == null ) return;

                    dialog.dismiss();
                    displayResults( r );

                    File root = new File( session.getFileConstants().getBaseGestureFolder().getParentFile(), "EvalMatrixes");
                    if( !root.mkdirs() ) Log.w("File", "Could not create folders: " + root.getAbsolutePath() );

                    r.saveToCSF( new File( root, System.currentTimeMillis() + "_" + properties.getBaseDetector().name() + ".csv" ) );
                }
        );
    }


    /**
     * Do an Eval with testusers.
     * @param unused not used variable.
     */
    public void evalUsersMerged( View unused ) {
        evalUsersMerged( properties.getBaseDetector(), true, null );
    }


    private void evalUsersMerged( WekaBaseClassifier classifier, boolean displayResults, Runnable onFinish ){
      String[] usersUnsorted = session.getUserManager().readAllPresentUsers();
        Collection<String> filteredUsers = RefStreams.of( usersUnsorted )
                .map( u -> u.endsWith( "-1" ) ? u.substring(0,u.length()-2) : u )
                .map( u -> u.endsWith( "-2" ) ? u.substring(0,u.length()-2) : u )
                .map( u -> u.endsWith( "-3" ) ? u.substring(0,u.length()-2) : u )
                .distinct().collect( Collectors.toSet() );

        Collection<SampleBatch> batches = stream( filteredUsers )
            .map( u -> session.getSampleManager()
                    .query()
                    .addUsers( u+"-1",u+"-2",u+"-3",u )
                    .applyFilter()
                    .purgeUnusedGestures()
                ).collect( Collectors.toList() );

        final ProgressDialog dialog = ProgressDialog.show(
                getContext(),
                "Creating " + properties.getBaseDetector().toString(),
                "STARTING...",
                true,
                false);

        Consumer<String> setMessage = s -> MainThread.runOnMT( () -> dialog.setMessage( s ) );


        MainThread.runAsyncResultMT(
                () -> {
                    try{
                        setMessage.accept( "Building..." );
                        return SimpleEvaluator.buildAllBatchesAgainstEachOther( session, properties.getBaseDetector(), batches, setMessage );
                    }catch ( Throwable exp ){
                        MainThread.runOnMT( dialog::dismiss );
                        MainThread.runOnMT( () -> Toast.makeText( this.getContext(), "Error: " + exp.getMessage(), Toast.LENGTH_LONG ).show() );
                        exp.printStackTrace();
                        return null;
                    }
                },
                r -> {
                    if( r == null ) return;

                    dialog.dismiss();
                    if( displayResults ) displayResults( r.getSecond() );

                    //Run finish when done:
                    if( onFinish != null ) MainThread.runOnMT( onFinish );

                    File root = new File( session.getFileConstants().getBaseGestureFolder().getParentFile(), "EvalMatrixesUsers");
                    root = new File( root, System.currentTimeMillis() + "" );
                    if( !root.mkdirs() ) Log.w("File", "Could not create folders: " + root.getAbsolutePath() );

                    r.getSecond().saveToCSF( new File( root, System.currentTimeMillis() + "_" + properties.getBaseDetector().name() + ".csv" ) );
                }
        );
    }

    private boolean evalUsersMergedAllClassifiers( View view ){
        String[] usersUnsorted = session.getUserManager().readAllPresentUsers();
        Collection<String> filteredUsers = RefStreams.of( usersUnsorted )
                .map( u -> u.endsWith( "-1" ) ? u.substring(0,u.length()-2) : u )
                .map( u -> u.endsWith( "-2" ) ? u.substring(0,u.length()-2) : u )
                .map( u -> u.endsWith( "-3" ) ? u.substring(0,u.length()-2) : u )
                .distinct().collect( Collectors.toSet() );

        Collection<SampleBatch> batches = stream( filteredUsers )
                .map( u -> session.getSampleManager()
                        .query()
                        .addUsers( u+"-1",u+"-2",u+"-3",u )
                        .applyFilter()
                        .purgeUnusedGestures()
                ).collect( Collectors.toList() );

        final ProgressDialog dialog = ProgressDialog.show(
                getContext(),
                "Creating " + properties.getBaseDetector().toString(),
                "STARTING...",
                true,
                false);

        Consumer<String> setMessage = s -> MainThread.runOnMT( () -> dialog.setMessage( s ) );

        session.Dialogs().OpenStringMultiSelectionDialog( getContext(), "Classifiers", VARIANTS, Arrays.asList( VARIANTS ), c -> {
            MainThread.runAsyncResultMT(
                    () -> {
                        File root = new File(session.getFileConstants().getBaseGestureFolder().getParentFile(), "EvalMatrixesAll");
                        root = new File(root, System.currentTimeMillis() + "");
                        if (!root.mkdirs()) Log.w("File", "Could not create folders: " + root.getAbsolutePath());

                        try {
                            for (String typeName : c ) {
                                long before = System.currentTimeMillis();

                                MainThread.runOnMT( () -> dialog.setTitle("Building " + typeName + "...") );
                                WekaBaseClassifier type = WekaBaseClassifier.valueOf(typeName);
                                Consumer<String> messageSetter = s -> setMessage.accept(type.name() + ": " + s);

                                Pair<WekaDetector, EvaluationResults> result = SimpleEvaluator.buildAllBatchesAgainstEachOther(session, type, batches, messageSetter);

                                File saveTo = new File(root, typeName + ".csv");
                                result.getSecond().saveToCSF( saveTo );


                                //Write the time taken:
                                long took = System.currentTimeMillis() - before;

                                String timeText = "\r\n\r\ntime:;" + took;
                                FileUtils.write( saveTo, timeText, Charset.defaultCharset(), true );
                                Log.i( "TIMINGS", "Took " + took + "ms to eval " + typeName );
                            }

                            return null;
                        } catch (Throwable exp) {
                            MainThread.runOnMT(dialog::dismiss);
                            MainThread.runOnMT(() -> Toast.makeText(this.getContext(), "Error: " + exp.getMessage(), Toast.LENGTH_LONG).show());
                            exp.printStackTrace();
                            return null;
                        }
                    },
                    r -> {
                        dialog.dismiss();
                        Toast.makeText(this.getContext(), "Done!", Toast.LENGTH_LONG).show();
                    }
            );
        });

        return true;
    }


    private void updateTestUsers( Collection<String> testUsers ){
        properties.clearTestUsers();
        properties.addTestUsers( testUsers );
        updateStatus();
    }


    private void randomChanged(CompoundButton compoundButton, boolean newValue ) {
        properties.setShuffleSamples( newValue );
    }


    private void changeModes( String modeName ) {
        this.type = EvaluationType.valueOf( modeName );
        if( getView() == null ) return;

        //True = Normal mode with split data.
        //False = CrossValidatiaon
        SeekBar seek = getView().findViewById( R.id.split );
        TextView seekText = getView().findViewById( R.id.split_value );
        CheckBox randomSplit = getView().findViewById( R.id.check_random_split );
        Button selectTestUsers = getView().findViewById( R.id.select_users_test );

        switch ( type ){
            case CROSS_VALIDATION:
                selectTestUsers.setEnabled( false );
                seek.setEnabled( true );
                seek.setMax( 20 );
                seek.setProgress( 10 );
                seekText.setText( String.valueOf( seek.getProgress() ) );

                randomSplit.setChecked( true );
                randomSplit.setEnabled( false );
                break;

            case SPLIT:
                selectTestUsers.setEnabled( false );
                seek.setEnabled( true );
                seek.setMax( 100 );
                seek.setProgress( 66 );
                seekText.setText( String.valueOf( seek.getProgress() ) );


                randomSplit.setChecked( properties.isShuffleSamples() );
                randomSplit.setEnabled( true );
                break;

            case AGAINST_USER_DATA:
                selectTestUsers.setEnabled( true );

                seek.setEnabled( false );
                seek.setMax( 1 );
                seek.setProgress( 1 );
                seekText.setText( R.string.off );

                randomSplit.setChecked( properties.isShuffleSamples() );
                randomSplit.setEnabled( true );
                break;

            case ALL_USERS_AGAINST_ONE:
                selectTestUsers.setEnabled( false );

                seek.setEnabled( false );
                seek.setMax( 1 );
                seek.setProgress( 1 );
                seekText.setText( R.string.off );
                randomSplit.setEnabled( false );
                //break;
        }

        updateStatus();
    }

    /**
     * Starts a build.
     * @param view unused
     */
    private void build( View view ){
        final ProgressDialog dialog = ProgressDialog.show(
                view.getContext(),
                "Creating " + properties.getBaseDetector().toString(),
                "STARTING...",
                true,
                false);

        Consumer<String> setMessage = s -> MainThread.runOnMT( () -> dialog.setMessage( s ) );
        MainThread.runAsyncResultMT(
                () -> {
                    try{
                        setMessage.accept( "Building..." );
                        return SimpleEvaluator.evaluate( session, properties, setMessage, type );
                    }catch ( Throwable exp ){
                        MainThread.runOnMT( dialog::dismiss );
                        MainThread.runOnMT( () -> Toast.makeText( this.getContext(), "Error: " + exp.getMessage(), Toast.LENGTH_LONG ).show() );
                        exp.printStackTrace();
                        return null;
                    }
                },
                r -> {
                    if( r == null ) return;

                    dialog.dismiss();
                    displayResults( r.getSecond() );
                }
        );
    }


    private void displayResults( EvaluationResults results ) {
        Context context = this.getContext();
        if( context == null ) return;

        if( results == null ){
            ToastLong( context, "Error in creating! Result == NULL!" );
            return;
        }


        new AlertDialog.Builder( context )
                .setTitle( "Accuracy: " + (int)( results.getAccuracy() * 100d ) + " for: " + properties.getBaseDetector().name() )
                .setView( buildTableLayout( results ) )
                .create()
                .show();
    }


    private View buildTableLayout( EvaluationResults results ){
        List<Gesture> gestures = stream( results.getPrecisionPerGesture().keySet() )
                .sorted( (a,b) -> a.getName().compareTo( b.getName() ) )
                .collect( Collectors.toList() );

        //Add a description row:
        TableLayout table = new TableLayout( this.getContext() );
        TableRow rootRow = new TableRow( this.getContext() );
        rootRow.addView( new TextView( this.getContext() ) );

        for( Gesture gesture : gestures){
            TextView textView = new TextView( this.getContext() );
            textView.setText( " " + gesture.getName() + " " );
            rootRow.addView( textView );
        }

        table.addView( rootRow );
        for (int i = 0; i < gestures.size(); i++) {
            TableRow row = new TableRow( this.getContext() );
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
            Gesture x = gestures.get(i);

            TextView root = new TextView( this.getContext() );
            root.setText( x.getName() );
            row.addView( root );

            for (int j = 0; j < gestures.size(); j++) {
                Gesture y = gestures.get(j);
                TextView text = new TextView( this.getContext() );
                text.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

                text.setText( String.valueOf( results.getEntry( x, y ) ) );
                row.addView(text);
            }

            table.addView(row);
        }

        return table;
    }


    private void seekChanged( int newValue ){
        if( getView() == null ) return;

        TextView valueView = getView().findViewById( R.id.split_value );
        valueView.setText( String.valueOf( newValue ) );

        properties.setValue( newValue );
    }


    private void updateGestures( View view ) {
        session.Dialogs().OpenGestureMultiSelectionDialog(
                this.getContext(),
                properties.getGestures(),
                g -> {
                    properties.clearGestures();
                    properties.addGestures( g );

                    updateStatus();
                }
        );
    }


    private void updateUsers( View view ) {
        session.Dialogs().OpenStringMultiSelectionDialog( this.getContext(), "Users",
                session.getUserManager().readAllPresentUsersOrDefault(),
                properties.getUsers(),
                u -> {
                    properties.clearUsers();
                    properties.addUsers( u );

                    updateStatus();
                }
        );
    }

    private void updateStatus(){
        if( getView() == null ) return;

        int gestures = properties.getGestures().size();
        int users = properties.getUsers().size();
        int samples = getAmountSamplesInBuilder( properties );

        String text = "GESTURES: %d, USERS: %d, SAMPLES: %d";
        TextView statusView = getView().findViewById( R.id.status_gestures_users );
        statusView.setText( String.format( Locale.getDefault(), text, gestures, users, samples ) );
    }


    private int getAmountSamplesInBuilder( EvaluatorProperties crossValidationMode ){
        Collection<String> gestures = stream( properties.getGestures() )
                .map( Gesture::getName )
                .collect( Collectors.toSet() );

        Collection<String> users = properties.getUsers();

        return (int) RefStreams.of( session.getFileConstants().getBaseGestureFolder() )
                .map( File::listFiles )
                .flatMap( RefStreams::of )
                .filter( f -> gestures.contains( f.getName() ) )
                .map( File::listFiles )
                .flatMap( RefStreams::of )
                .filter( f -> users.contains( f.getName().split(Pattern.quote( "_" ) )[0] ) )
                .count();
    }


    private void changeVariant( String variant ){
        properties.setBaseDetector( WekaBaseClassifier.valueOf( variant ) );
    }



    private static class IgnoreNothingListener <T> implements AdapterView.OnItemSelectedListener{

        private final Consumer<T> consumer;

        IgnoreNothingListener(Consumer<T> consumer){
            this.consumer = consumer;
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onItemSelected( AdapterView<?> adapterView, View view, int i, long l ) {
            Object selected = adapterView.getItemAtPosition(i);
            consumer.accept( (T) selected );
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {}
    }

    public static class IgnoringSeekBarChangedListener implements SeekBar.OnSeekBarChangeListener {

        private final Consumer<Integer> consumer;

        public IgnoringSeekBarChangedListener(Consumer<Integer> consumer) {
            this.consumer = consumer;
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            consumer.accept( i );
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    }

}