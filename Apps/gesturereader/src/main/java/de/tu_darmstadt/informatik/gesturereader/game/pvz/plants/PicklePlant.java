package de.tu_darmstadt.informatik.gesturereader.game.pvz.plants;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;

import static de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem.TICKS_PER_SECOND;

/**
 * Created by Tobias on 22.10.2017
 */

public class PicklePlant extends Plant {


    public PicklePlant( int lane, int slot ) {
        super( lane, slot, "Pickle", R.drawable.pickle, 10, 60, TICKS_PER_SECOND, 2, 2);
    }

    public PicklePlant(){
        this( -1, -1 );
    }


    @Override
    protected void tickIntern( EventSystem eventSystem ) {
        //Not needed for now!
    }

}
