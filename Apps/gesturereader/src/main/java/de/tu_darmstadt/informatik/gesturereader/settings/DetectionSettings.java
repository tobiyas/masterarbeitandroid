package de.tu_darmstadt.informatik.gesturereader.settings;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Toby on 05.09.2017
 */

public class DetectionSettings {


    private static final String SETTINGS_PATH = "settings_detection";
    private static final String RIGHT_HAND_MODE_PATH = "rightHandMode";


    /**
     * Gets if we are in right or left hand mode.
     * @param context to use while loading.
     * @return true if in Right hand mode.
     */
    public static boolean isRightHandMode( Context context ) {
        return context == null || get(context).getBoolean(RIGHT_HAND_MODE_PATH, true);

    }

    /**
     * Gets if we are in right or left hand mode.
     * @param context to use while loading.
     * @param rightHandMode the value to set to.
     */
    public static void setRightHandMode( Context context, boolean rightHandMode ){
        if( context == null ) return;
        get( context ).edit().putBoolean( RIGHT_HAND_MODE_PATH, rightHandMode ).apply();
    }


    /**
     * Gets the used shared Prefs.
     * @param context to use for fetching.
     * @return the shared prefs.
     */
    private static SharedPreferences get( Context context ){
        return context.getSharedPreferences( SETTINGS_PATH, Context.MODE_PRIVATE );
    }

}
