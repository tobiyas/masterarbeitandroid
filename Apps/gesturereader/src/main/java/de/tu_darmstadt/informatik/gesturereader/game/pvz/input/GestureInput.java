package de.tu_darmstadt.informatik.gesturereader.game.pvz.input;

import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.joda.time.Duration;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.config.GestureKeyConfig;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ContinueMovementDetectionEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.InputEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PauseMovementDetectionEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerLostGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerWonGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ShowDebugToastEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.gui.ViewProvider;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResult;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureResultWrapper;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.GestureDetector;
import de.tu_darmstadt.informatik.kom.gesturerec.models.detect.WekaGestureDetector;
import de.tu_darmstadt.informatik.kom.gesturerec.recorder.DataRecorder;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;

/**
 * Created by Tobias on 25.10.2017
 */

public class GestureInput implements EventListener {

    private static final int DETECTION_TIME =   2_000;
    private static final int COOLDOWN_TIME  =   500;
    private static final int PREPARE_FOR_NEXT_GESTURE_TIME_TIME  =   2_000;


    /**
     * The system to fire events to.
     */
    private final EventSystem eventSystem;

    /**
     * The provider for views.
     */
    private final ViewProvider viewProvider;

    /**
     * The used DataRecorder
     */
    private final DataRecorder dataRecorder;

    /**
     * The Config of Gesture -> Input.
     */
    private final GestureKeyConfig gestureConfig;

    /**
     * The used Detector.
     */
    private GestureDetector detector;

    /**
     * the current Runnable.
     */
    private Runnable currentRunnable;

    /**
     * Stops the Ticker.
     */
    private boolean stop = false;


    public GestureInput( EventSystem eventSystem, ViewProvider viewProvider, String model, GestureKeyConfig gestureConfig ) {
        this.eventSystem = eventSystem;
        this.viewProvider = viewProvider;
        this.gestureConfig = gestureConfig;

        this.dataRecorder = MainApplication.fromContext( viewProvider.getContext() ).getSession().getDataRecorder();

        new Handler().post( () -> initDetector( model ) );
        eventSystem.addListener( this );

        setProgress( 0 );
    }


    /**
     * Inits the detector.
     * @param modelName to use for loading.
     */
    private void initDetector( String modelName ){
        try{
            GestureRecSession session = MainApplication.fromContext( viewProvider.getContext() ).getSession();
            ModelProxy proxy = session.getModelManager().getForName( modelName );
            this.detector = WekaGestureDetector.generate ( session, proxy );
        }catch (Throwable exp){
            exp.printStackTrace();
            eventSystem.fireEvent( new ShowDebugToastEvent( "Loading model " + modelName + " failed!", Toast.LENGTH_LONG ));
        }
    }


    /**
     * Sets the Progress and Schedules a new Timer.
     * @param percent to set. between 0 and 100
     */
    private void setProgress ( double percent ){
        this.currentRunnable = null;

        ProgressBar progress = (ProgressBar) viewProvider.provideView( R.id.progress );
        if( progress == null ) return;

        //Clamp between 0 and 100.
        percent = Math.max( 0, Math.min( 100, percent ) );

        //Set the percent:
        progress.setMax( 100 );
        progress.setProgress( (int) percent );

        //If done -> Wait for cooldown and reschedule:
        if( percent >= 100 ){
            progress.postDelayed( () ->
                    {
                        setProgress( 0 );
                        startCollecting();
                    },
                    COOLDOWN_TIME + PREPARE_FOR_NEXT_GESTURE_TIME_TIME
            );
            return;
        }

        int delay = DETECTION_TIME / 30;
        double percentPerTick = ( (double)delay / (double)DETECTION_TIME ) * 100;
        double nextPercent = Math.min( 100, Math.max( 0, percent + percentPerTick ) );

        this.currentRunnable = () -> setProgress( nextPercent );
        progress.postDelayed( this.currentRunnable, delay );
    }

    /**
     * Stops the System.
     */
    private void stopCurrent(){
        this.stop = true;
        if( currentRunnable == null ) return;

        View view = viewProvider.provideView( R.id.progress );
        view.removeCallbacks( currentRunnable );

        currentRunnable = null;
    }


    /**
     * Starts collecting Data for the Gestures.
     */
    private void startCollecting() {
        dataRecorder.startCollectingData( Duration.millis( DETECTION_TIME ), Duration.millis( COOLDOWN_TIME) , this::gotData, new Handler() );
    }

    /**
     * Called when we got data.
     * @param sensorDataBatch patch to use.
     */
    private void gotData( SensorDataBatch sensorDataBatch ) {
        //If the System stopped, we don't need to evaluate inputs!
        if( stop ) return;

        //If something went wrong with detection, break!
        if( sensorDataBatch == null || detector == null ) return;

        GestureResultWrapper result = detector.analyseGesture(sensorDataBatch);
        GestureResult top = result.getTopResult();

        //No input:
        if( top == null || top.getFirst() == GestureManager.UNKNOWN() ) return;

        //Evaluate the input and map it:
        Gesture gesture = top.getFirst();
        if( gesture == null ) return;

        //If a mapping is present -> Set that:
        InputCommands mappedInput = gestureConfig.unmap( gesture );
        if( mappedInput == null ){
            switch ( gesture.getName().toLowerCase() ) {
                case "left":
                    mappedInput = InputCommands.GO_LEFT;
                    break;

                case "right":
                    mappedInput = InputCommands.GO_RIGHT;
                    break;

                case "up":
                    mappedInput = InputCommands.GO_UP;
                    break;

                case "down":
                    mappedInput = InputCommands.GO_DOWN;
                    break;

                case "dig":
                    mappedInput = InputCommands.DIG;
                    break;

                case "plant_1":
                    mappedInput = InputCommands.PLANT_1;
                    break;

                case "plant_2":
                    mappedInput = InputCommands.PLANT_2;
                    break;
            }
        }


        if( mappedInput != null ) {
            final InputCommands finalInput = mappedInput;
            //TODO remove debug output:
            eventSystem.fireEvent( new ShowDebugToastEvent("Input: " + mappedInput.name(), Toast.LENGTH_SHORT) );
            MainThread.runOnMT( () ->  eventSystem.fireEvent( new InputEvent( finalInput )) );
        }
    }

    @EventHandler
    public void onGameEnd( PlayerLostGameEvent event ){
        stopCurrent();
    }

    @EventHandler
    public void onGameEnd( PlayerWonGameEvent event ){
        stopCurrent();
    }

    @EventHandler
    public void onMotionShalPause( PauseMovementDetectionEvent event ){
        stopCurrent();
    }

    @EventHandler
    public void onMotionShalContinue( ContinueMovementDetectionEvent event ){
        //Check if already running:
        if( !stop ) return;

        this.stop = false;
        setProgress( 0 );
    }

}
