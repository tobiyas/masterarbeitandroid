package de.tu_darmstadt.informatik.gesturereader.game.fight.detect;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;


/**
 * Created by Tobias on 02.01.2018
 */
public class EvalSaver {

    /**
     * The executor to use.
     */
    private static final ExecutorService executor = Executors.newSingleThreadExecutor();

    /**
     * The session to use.
     */
    private final GestureRecSession session;

    /**
     * The user to save as.
     */
    private final String user;


    /**
     * Saves the Data to a specific user.
     * @param user to save to.
     */
    public EvalSaver( GestureRecSession session, String user ) {
        this.session = session;
        this.user = user;

        if( user == null || user.isEmpty() || user.contains( "_" ) ) throw new IllegalArgumentException( "user" );
    }


    /**
     * This saves the data if wanted.
     *
     * @param batch to save.
     * @param wanted to save.
     */
    public void save( SensorDataBatch batch, Gesture wanted ) {
        executor.execute( () -> session.getSampleManager().addSample( wanted, batch.getStartTime(), user, batch) );
    }
}
