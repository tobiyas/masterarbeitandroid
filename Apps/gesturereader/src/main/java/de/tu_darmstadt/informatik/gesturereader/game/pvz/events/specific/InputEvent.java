package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;


import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.input.InputCommands;

/**
 * Created by Tobias on 22.10.2017
 */

public class InputEvent extends Event {

    /**
     * The InputType done.
     */
    private final InputCommands inputCommands;

    public InputEvent(InputCommands type) {
        this.inputCommands = type;
    }

    public InputCommands getInputCommands() {
        return inputCommands;
    }
}
