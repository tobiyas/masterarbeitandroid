package de.tu_darmstadt.informatik.gesturereader.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.detect.DebugViewAdapter;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;

/**
 * Created by Toby on 02.09.2017
 */

public abstract class OwnDialog extends Dialog {

    public OwnDialog(@NonNull Context context) {
        super(context);
    }

    public OwnDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected OwnDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }



    /**
     * Gets the current Session.
     * @return the current session.
     */
    public GestureRecSession session(){
        return MainApplication.fromContext( getContext() ).getSession();
    }

}
