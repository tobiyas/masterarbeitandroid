package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers;

import java.util.HashSet;
import java.util.Set;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TickEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieDiedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieSpawnEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 22.10.2017
 */

public class ZombieHandler implements EventListener {

    /**
     * The EventSystem to use.
     */
    private final EventSystem eventSystem;

    /**
     * The provider for plants
     */
    private final PlantProvider plantProvider;

    /**
     * The set of currently present Zombies.
     */
    private final Set<Zombie> presentZombies = new HashSet<>();

    /**
     * The set of dead zombies.
     */
    private final Set<Zombie> deadZombies = new HashSet<>();


    public ZombieHandler( EventSystem eventSystem, PlantProvider plantProvider ) {
        this.eventSystem = eventSystem;
        this.plantProvider = plantProvider;

        eventSystem.addListener( this );
    }


    @EventHandler
    public void zombieSpawned( ZombieSpawnEvent event ){
        this.presentZombies.add( event.getZombie() );
    }


    @EventHandler
    public void tick( TickEvent event ){
        //Tick them:
        stream( presentZombies ).forEach( z -> z.tick( eventSystem, plantProvider ) );

        //Remove dead:
        for( Zombie zombie : new HashSet<>( presentZombies ) ){
            if( zombie.isDead() && !deadZombies.contains( zombie ) ){
                deadZombies.add( zombie );
                presentZombies.remove( zombie );
                eventSystem.fireEvent( new ZombieDiedEvent( zombie ));
            }
        }
    }
}
