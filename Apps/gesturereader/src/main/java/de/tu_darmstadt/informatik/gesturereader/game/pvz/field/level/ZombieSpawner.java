package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.level;

import java.util.HashSet;
import java.util.Set;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlayerWonGameEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TickEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieDiedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieSpawnEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 23.10.2017
 */

public class ZombieSpawner implements EventListener {

    /**
     * The set of patterns to use.
     */
    private final Set<ZombieSpawnPattern> patterns = new HashSet<>();

    /**
     * a set of dead zombies.
     */
    private final Set<Zombie> dead = new HashSet<>();


    /**
     * The Eventsystem to use for Stuff.
     */
    private final EventSystem eventSystem;


    public ZombieSpawner( EventSystem eventSystem ){
        this.eventSystem = eventSystem;

        eventSystem.addListener( this );
    }


    /**
     * Adds a new Pattern.
     * @param pattern to add.
     */
    public void addPattern( ZombieSpawnPattern pattern ){
        this.patterns.add( pattern );
    }


    @EventHandler
    public void tick( TickEvent event ){
        int tick = event.getTick();

        //If any zombie needs to be spawned -> Spawn it:
        stream( patterns )
            .filter( ZombieSpawnPattern::canSpawn )
            .filter( p -> p.getTick() <= tick )
            .forEach( this::spawn );

        //If all Zombies are spawned -> done!
    }

    @EventHandler
    public void onZombieDeath( ZombieDiedEvent  event ){
        dead.add( event.getZombie() );

        //We are done!
        if( allZombiesSpawned() && dead.size() == patterns.size()){
            eventSystem.fireEvent( new PlayerWonGameEvent() );
        }
    }

    /**
     * Returns true if all Zombies where spawned.
     */
    private boolean allZombiesSpawned(){
        return stream( patterns )
            .filter( ZombieSpawnPattern::isSpawned )
            .count() <= 0;
    }

    /**
     * Spawns the Zombie from the pattern.
     * @param zombieSpawnPattern to use.
     */
    private void spawn( ZombieSpawnPattern zombieSpawnPattern ) {
        if( zombieSpawnPattern.isSpawned() ) return;

        zombieSpawnPattern.setSpawned();
        eventSystem.fireEvent( new ZombieSpawnEvent( zombieSpawnPattern.getRow(), zombieSpawnPattern.getZombie() ));
    }

}
