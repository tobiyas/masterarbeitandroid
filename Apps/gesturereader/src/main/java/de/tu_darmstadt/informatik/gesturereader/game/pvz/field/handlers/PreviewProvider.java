package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;

/**
 * Created by Tobias on 22.10.2017
 */

public interface PreviewProvider {

    /**
     * Gets the current Preview.
     * @return the current Preview.
     */
    public Plant getPreview();


}
