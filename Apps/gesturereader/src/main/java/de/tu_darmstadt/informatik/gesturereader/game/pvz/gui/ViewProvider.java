package de.tu_darmstadt.informatik.gesturereader.game.pvz.gui;

import android.content.Context;
import android.view.View;

/**
 * Created by Tobias on 22.10.2017
 */

public interface ViewProvider {


    /**
     * Provides a View.
     * @param viewId to search
     * @return the found View.
     */
    public View provideView( int viewId );

    /**
     * Gets a context.
     * @return the context.
     */
    public Context getContext();

}
