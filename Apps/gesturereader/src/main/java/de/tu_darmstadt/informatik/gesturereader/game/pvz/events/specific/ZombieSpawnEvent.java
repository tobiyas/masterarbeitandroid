package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

/**
 * Created by Toby on 23.10.2017
 */

public class ZombieSpawnEvent extends Event {

    /**
     * The Row the Zombie moved.
     */
    private final int row;


    private final Zombie zombie;

    public ZombieSpawnEvent(int row, Zombie zombie) {
        this.row = row;
        this.zombie = zombie;
    }


    public int getRow() {
        return row;
    }

    public Zombie getZombie() {
        return zombie;
    }
}
