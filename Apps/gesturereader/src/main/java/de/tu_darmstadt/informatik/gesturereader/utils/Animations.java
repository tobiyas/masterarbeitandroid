package de.tu_darmstadt.informatik.gesturereader.utils;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

/**
 * Created by Tobias on 02.01.2018
 */

public class Animations {


    /**
     * creates a Fade out animation.
     *
     * @param timeInMs to use.
     * @return the created animation.
     */
    public static Animation fadeOut(int timeInMs ){
        AlphaAnimation animation = new AlphaAnimation( 1, 0 );
        animation.setInterpolator( new AccelerateInterpolator() );
        animation.setDuration( timeInMs );
        animation.setFillAfter( true );

        return animation;
    }

    /**
     * creates a Fade in animation.
     *
     * @param timeInMs to use.
     * @return the created animation.
     */
    public static Animation fadeIn(int timeInMs ){
        AlphaAnimation animation = new AlphaAnimation( 0, 1 );
        animation.setInterpolator( new AccelerateInterpolator() );
        animation.setDuration( timeInMs );
        animation.setFillAfter( true );

        return animation;
    }


    /**
     * Returns a simple Up-Back movement.
     * @param pixels to go.
     * @param timeInMs to animate.
     *
     * @return the generated Animation.
     */
    public static Animation up( int pixels, int timeInMs ){
        TranslateAnimation animation = new TranslateAnimation( 0, 0, 0, -pixels );
        animation.setDuration( timeInMs );
        animation.setRepeatMode( Animation.REVERSE );
        animation.setRepeatCount( 1 );
        animation.setFillAfter( true );

        return animation;
    }


    /**
     * Returns a simple Up-Back movement.
     * @param pixels to go.
     * @param timeInMs to animate.
     *
     * @return the generated Animation.
     */
    public static Animation down( int pixels, int timeInMs ){
        TranslateAnimation animation = new TranslateAnimation( 0, 0, 0, pixels );
        animation.setDuration( timeInMs );
        animation.setRepeatMode( Animation.REVERSE );
        animation.setRepeatCount( 1 );
        animation.setFillAfter( true );

        return animation;
    }


    /**
     * Returns a simple Up-Back movement.
     * @param scaleTo to go.
     * @param timeInMs to animate.
     *
     * @return the generated Animation.
     */
    public static Animation scaleY( double scaleTo, int timeInMs ){
        ScaleAnimation animation = new ScaleAnimation( 1, 1, 1, (float)scaleTo, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1 );
        animation.setDuration( timeInMs );
        animation.setRepeatMode( Animation.REVERSE );
        animation.setRepeatCount( 1 );
        animation.setFillAfter( true );

        return animation;
    }


    /**
     * Returns a simple Up-Back movement.
     * @param scaleTo to go.
     * @param timeInMs to animate.
     *
     * @return the generated Animation.
     */
    public static Animation scaleX( double scaleTo, int timeInMs ){
        ScaleAnimation animation = new ScaleAnimation( 1, (float)(scaleTo), 1, 1, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1  );
        animation.setDuration( timeInMs );
        animation.setRepeatMode( Animation.REVERSE );
        animation.setRepeatCount( 1 );
        animation.setFillAfter( true );

        return animation;
    }


    /**
     * Returns a simple Rotate Animation.
     * @param timeInMs to animate.
     *
     * @return the generated Animation.
     */
    public static Animation rotateLeftOnce( int timeInMs ){
        RotateAnimation animation = new RotateAnimation( 0, 360 );
        animation.setDuration( timeInMs );
        animation.setFillAfter( true );

        return animation;
    }

    /**
     * Returns a simple Rotate Animation.
     * @param timeInMs to animate.
     *
     * @return the generated Animation.
     */
    public static Animation rotateRightOnce( int timeInMs ){
        RotateAnimation animation = new RotateAnimation( 0, -360 );
        animation.setDuration( timeInMs );
        animation.setFillAfter( true );

        return animation;
    }

}
