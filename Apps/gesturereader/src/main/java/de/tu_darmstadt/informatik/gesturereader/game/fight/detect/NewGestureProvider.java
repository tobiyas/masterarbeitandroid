package de.tu_darmstadt.informatik.gesturereader.game.fight.detect;

import java.util.Collection;
import java.util.List;

import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameModel;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;

/**
 * Created by Toby on 09.01.2018
 */
public interface NewGestureProvider extends FightGameModel.GestureExecutedListener {


    /**
     * Generates a list of next Gestures.
     * @param possibleGestures
     * @return the generated list of next Gestures.
     */
    List<Gesture> getNextGestures( Collection<Gesture> possibleGestures );

}
