package de.tu_darmstadt.informatik.gesturereader.utils;

import android.support.v4.app.Fragment;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;

/**
 * Created by Toby on 08.01.2018
 */
public abstract class OwnFragment extends Fragment {


    /**
     * Gets the current Session.
     * @return the current Session.
     */
    public GestureRecSession session(){
        return MainApplication.fromContext( getActivity() ).getSession();
    }


}
