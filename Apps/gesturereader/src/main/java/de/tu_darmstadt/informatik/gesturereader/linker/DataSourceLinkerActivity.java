package de.tu_darmstadt.informatik.gesturereader.linker;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnActivity;
import de.tu_darmstadt.informatik.kom.contextrec.phone.imu.ImuDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.CalibrateableDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.StreamingDataSource;
import de.tu_darmstadt.informatik.kom.contextsense.Vibrateable;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.AccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.GyroscopeSensorData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.LinearAccelerationData;
import de.tu_darmstadt.informatik.kom.contextsense.sensordata.OrientationData;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.Limb;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.SourceLinker;
import java8.util.Optional;
import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Toby on 29.08.2017
 */

public class DataSourceLinkerActivity extends OwnActivity {

    /**
     * The Threshold of error to when show movement.
     */
    private static final double MOVEMENT_DETECTION_THRESHOLD = 3;

    /**
     * The amount of seconds to search for Frequency.
     */
    private static final double FREQ_READ_TIME_IN_SEC = 3;


    /**
     * The Linker to use.
     */
    private SourceLinker linker;


    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linker);

        this.linker = MainApplication.fromContext( this ).getSession().getLinker();

        //Set click listener:
        findViewById(R.id.left_arm).setOnClickListener( v -> clicked(Limb.LEFT_HAND) );
        findViewById(R.id.right_arm).setOnClickListener( v -> clicked(Limb.RIGHT_HAND) );
        findViewById(R.id.left_foot).setOnClickListener( v -> clicked(Limb.LEFT_FOOT) );
        findViewById(R.id.right_foot).setOnClickListener( v -> clicked(Limb.RIGHT_FOOT) );

        //Set long press listener
        findViewById(R.id.left_arm).setOnLongClickListener( v -> longClicked(Limb.LEFT_HAND) );
        findViewById(R.id.right_arm).setOnLongClickListener( v -> longClicked(Limb.RIGHT_HAND) );
        findViewById(R.id.left_foot).setOnLongClickListener( v -> longClicked(Limb.LEFT_FOOT) );
        findViewById(R.id.right_foot).setOnLongClickListener( v -> longClicked(Limb.RIGHT_FOOT) );

        //Schedule Movement Detection:
        findViewById(R.id.left_arm).postDelayed( this::movementDetection ,500 );

        //Other:
        findViewById( R.id.calibrate ).setOnLongClickListener( this::rescanWear );
    }

    private boolean rescanWear( View view ) {
        Toast.makeText( this, "Rescanning Wear and IMUs...", Toast.LENGTH_LONG ).show();

        session().scanForWearables();
        session().scanForIMUs();

        return true;
    }

    private void movementDetection() {
        movementDetection(Limb.LEFT_HAND, R.id.left_arm);
        movementDetection(Limb.RIGHT_HAND, R.id.right_arm);
        movementDetection(Limb.LEFT_FOOT, R.id.left_foot);
        movementDetection(Limb.RIGHT_FOOT, R.id.right_foot);

        findViewById(R.id.right_foot).postDelayed( this::movementDetection, 500 );
    }


    private void movementDetection(Limb limb, int id){
        DataSource source = linker.getSource(limb);
        View view = findViewById(id);
        if(source != null){
            double motion = getMotionMSE( source );
            view.setBackgroundColor(
                    motion <= 0 ?
                            Color.BLUE :
                            ( motion > MOVEMENT_DETECTION_THRESHOLD ?
                                    Color.RED :
                                    Color.GREEN
                            )
            );
        }else{
            view.setBackgroundColor( Color.WHITE );
        }
    }

    private void clicked(Limb limb){
        new DataSourceSelectDialog( this )
            .set( linker.getSource(limb), limb )
            .setCallback( this::gotChange )
            .show();
    }

    private boolean longClicked( Limb limb ){
        DataSource source = linker.getSource( limb );
        ArrayList<String> options = new ArrayList<>();
        options.addAll( Arrays.asList( "Name","DataTypes","Calibrate","Restart","Vibrate","DataFrequency" ) );
        if( source != null && source instanceof ImuDataSource ) options.add( "Battery" );

        new AlertDialog.Builder( this )
                .setTitle( limb.getName() + ":" )
                .setItems( options.toArray(new String[0]) , (a,i) -> op( options.get(i), limb, source ) )
                .create().show();

        return true;
    }

    private void op( String option, Limb limb, DataSource source ){
        if( source == null ){
            Toast.makeText( this, "No Source on " + limb.getName(), Toast.LENGTH_LONG ).show();
            return;
        }

        if( option.equalsIgnoreCase( "Name" )) {
            String device = source.getDeviceName();
            Toast.makeText( this, limb.getName() + " has " + device + " attached", Toast.LENGTH_LONG ).show();
            return;
        }

        if( option.equalsIgnoreCase( "Battery" ) ){
            ImuDataSource imu = (ImuDataSource) source;
            double battery = imu.getBatteryLevel();

            if( battery == -1 ) Toast.makeText( this, imu.getDeviceName() + " did not tell us yet", Toast.LENGTH_LONG ).show();
            else Toast.makeText( this, imu.getDeviceName() + " has " + imu.getBatteryLevel() + "% battery left", Toast.LENGTH_LONG ).show();
            return;
        }

        if( option.equalsIgnoreCase( "DataTypes" ) ) {
            if( !( source instanceof StreamingDataSource ) ){
                Toast.makeText( this, source.getName() + " ist nicht Streamable!", Toast.LENGTH_LONG ).show();
                return;
            }

            StreamingDataSource streamingDataSource = ( StreamingDataSource ) source;
            Optional<SensorDataBatch> batchOptional = streamingDataSource.collectStreamedData( DateTime.now().minus(Duration.millis( 500 ) ), DateTime.now() );

            if( !batchOptional.isPresent() ){
                Toast.makeText( this, "Keine daten für " + source.getDeviceName() + " :(", Toast.LENGTH_LONG ).show();
                return;
            }

            List<String> types = batchOptional.stream()
                    .map( SensorDataBatch::getAllData )
                    .flatMap( StreamSupport::stream )

                    //Check if they have data:
                    .filter( data -> !(data instanceof GyroscopeSensorData) || ((GyroscopeSensorData) data).getT().length > 0)
                    .filter( data -> !(data instanceof AccelerationData) || ((AccelerationData) data).getT().length > 0)
                    .filter( data -> !(data instanceof LinearAccelerationData) || ((LinearAccelerationData) data).getT().length > 0)
                    .filter( data -> !(data instanceof OrientationData) || ((OrientationData) data).getT().length > 0)

                    //Now map and tell:
                    .map( data -> data.getClass().getSimpleName() )
                    .distinct()
                    .sorted()
                    .collect( Collectors.toList() );

            if( types.isEmpty() ){
                Toast.makeText( this, "Keine Daten für " + source.getDeviceName() + " :-(", Toast.LENGTH_LONG ).show();
                return;
            }

            Toast.makeText( this, Arrays.toString( types.toArray( new String[0] ) ), Toast.LENGTH_LONG ).show();
            return;
        }

        if( option.equalsIgnoreCase( "Calibrate" ) ) {
            if( source instanceof CalibrateableDataSource){
                ((CalibrateableDataSource) source).calibrate();
                Toast.makeText( this, "Calibrating " + source.getName(), Toast.LENGTH_LONG ).show();
                return;
            }

            Toast.makeText( this, "Source " + source.getName() + " is not calibrateable.", Toast.LENGTH_LONG ).show();
            return;
        }

        if( option.equalsIgnoreCase( "Vibrate" ) ) {
            //Also vibrate!
            if( source instanceof Vibrateable) {
                ((Vibrateable) source).vibrate();
                return;
            }

            Toast.makeText( this, "Can not vibrate " + source.getName(), Toast.LENGTH_LONG ).show();
            return;
        }

        if( option.equalsIgnoreCase( "Restart" ) ) {
            if( source instanceof StreamingDataSource ){
                StreamingDataSource sSource = (StreamingDataSource) source;
                sSource.stopStreaming();
                sSource.startStreaming();

                Toast.makeText( this, "Restarted " + source.getDeviceName(), Toast.LENGTH_LONG ).show();
                return;
            }

            Toast.makeText( this, "No Streaming Sensor!", Toast.LENGTH_LONG ).show();
            return;
        }

        if( option.equalsIgnoreCase( "DataFrequency" ) ){
            double accelFreq = getAccelFreq( source );
            Toast.makeText( this, String.format(Locale.getDefault(), "Device: '%s' Freq: '%.1f'", source.getDeviceName(), accelFreq ), Toast.LENGTH_LONG).show();
            return;
        }
    }


    public boolean openVibrateView( View unused ){
        final String[] vibrateable = stream( MainApplication.fromContext( this ).getSession().getContextRecSession().getDataSources() )
                .filter( s -> s instanceof Vibrateable)
                .map( DataSource::getDeviceName )
                .toArray( String[]::new );

        if( vibrateable.length <= 0 ) {
            Toast.makeText( this, "No vibrateable Sources", Toast.LENGTH_LONG ).show();
            return true;
        }

        //Show dialog.
        new AlertDialog.Builder( this )
            .setItems( vibrateable, (d,w) -> vibrateSource( vibrateable[w] ) )
            .create()
            .show();

        return true;
    }

    private void vibrateSource( String sourceName ){
        Log.d( "Linker", "Vibrating: " + sourceName );

        Optional<Vibrateable> source = stream( MainApplication.fromContext( this ).getSession().getContextRecSession().getDataSources() )
                .filter( s -> s.getDeviceName().equals( sourceName ) )
                .filter( s -> s instanceof Vibrateable )
                .map( Vibrateable.class::cast )
                .findFirst();

        //Vibrate or die!
        source.ifPresentOrElse(
                Vibrateable::vibrate,
                () -> Toast.makeText( this, "Can not vibrate.", Toast.LENGTH_LONG ).show()
        );
    }

    private double getAccelFreq( DataSource source ){
        if( source == null ) return -2;

        if( source instanceof StreamingDataSource ){
            StreamingDataSource sSource = (StreamingDataSource) source;
            Optional<SensorDataBatch> batch = sSource.collectStreamedData( new DateTime().minusMillis( (int)FREQ_READ_TIME_IN_SEC * 1000 + 200 ), new DateTime().minus(200) );
            if(!batch.isPresent()) return 0;

            Optional<AccelerationData> data = batch.get().getData( AccelerationData.class );
            if(!batch.isPresent()) return -1;

            return (double)data.get().getT().length / FREQ_READ_TIME_IN_SEC;
        }

        return 0;
    }

    private void gotChange(String dataSource, Limb limb) {
        linker.change( dataSource, limb);
    }

    public void startCalibration(View unused){
        MainApplication.fromContext( this ).getSession().getContextRecSession().doCalibration();
        Toast.makeText( this, "Starting Calibration", Toast.LENGTH_LONG ).show();
    }


    /**
     * Open a view that can restart a streaming device!
     */
    public void restart_gui(View unused){
        new RestartSourceDialog( this ).show();
    }


    /**
     * Gets the Motion of the Source linked.
     *
     * @return Value :
     *      < 0 if the source does not seem to have a motion element.
     *      > 0 if the source has a motion component and it can be evaluated.
     */
    private double getMotionMSE(DataSource source){
        if(! ( source instanceof StreamingDataSource ) ) return -1;

        StreamingDataSource streamingDataSource = (StreamingDataSource) source;
        Optional<SensorDataBatch> oBatch = streamingDataSource.collectStreamedData( DateTime.now().minusMillis(500), DateTime.now() );
        if(!oBatch.isPresent()) return -1;

        SensorDataBatch batch = oBatch.get();
        Optional<AccelerationData> oData = batch.getData(AccelerationData.class);
        if(!oData.isPresent()) return -1;

        AccelerationData data = oData.get();
        double mse = data.getMeanSquareError();

        //mse with less values are pretty bad, so we need to up them a bit.
        double values = data.getT().length;
        if(values <= 10) mse *= 20 - values;

        return mse;
    }


}
