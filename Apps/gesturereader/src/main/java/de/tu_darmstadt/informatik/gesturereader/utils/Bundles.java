package de.tu_darmstadt.informatik.gesturereader.utils;

import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Toby on 04.01.2018.
 */

public class Bundles {



    /**
     * Gets the String extra when present, a null otherwise.
     *
     * @param intent to search.
     * @param key to use.
     *
     * @return the found value or null.
     */
    public static String getExtraString( Intent intent, String key ){
        return getExtraString( intent, key, null );
    }

    /**
     * Gets the String extra when present, a default value otherwise.
     *
     * @param intent to search.
     * @param key to use.
     * @param defaultValue to use if not found.
     *
     * @return the found value or the default value.
     */
    public static String getExtraString( Intent intent, String key, String defaultValue ){
        if( intent == null ) return null;

        Bundle bundle = intent.getExtras();
        if( bundle == null ) return null;

        return bundle.getString( key, defaultValue );
    }

}
