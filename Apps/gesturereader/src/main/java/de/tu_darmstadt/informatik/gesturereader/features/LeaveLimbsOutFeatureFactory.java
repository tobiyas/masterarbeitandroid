package de.tu_darmstadt.informatik.gesturereader.features;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaFeatureBuilder;
import de.tu_darmstadt.informatik.kom.gesturerec.features.DefaultFeatureFactory;
import de.tu_darmstadt.informatik.kom.gesturerec.linker.Limb;

/**
 * Created by Toby on 21.01.2018
 */

public class LeaveLimbsOutFeatureFactory extends DefaultFeatureFactory {


    /**
     * The Limbs to leave out.
     */
    private final Collection<Limb> leaveOut = new ArrayList<>();



    public LeaveLimbsOutFeatureFactory(Collection<Limb> leaveOut ) {
        this.leaveOut.addAll( leaveOut );
    }


    public LeaveLimbsOutFeatureFactory(Limb... leaveOut ) {
        this( Arrays.asList( leaveOut ) );
    }


    @Override
    public void registerFeatures(WekaFeatureBuilder registry, Limb limb ) {
        if( leaveOut.contains( limb )  ) return;
        else super.registerFeatures( registry, limb );
    }

}
