package de.tu_darmstadt.informatik.gesturereader.game.fight;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.fight.detect.CompleteFightRecorder;
import de.tu_darmstadt.informatik.gesturereader.game.fight.detect.DefaultNewGestureProvider;
import de.tu_darmstadt.informatik.gesturereader.game.fight.detect.EvalSaver;
import de.tu_darmstadt.informatik.gesturereader.game.fight.detect.FightGestureDetector;
import de.tu_darmstadt.informatik.gesturereader.game.fight.detect.NewGestureProvider;
import de.tu_darmstadt.informatik.gesturereader.game.fight.detect.ProgressiveGestureProvider;
import de.tu_darmstadt.informatik.gesturereader.game.fight.detect.SingleGestureProvider;
import de.tu_darmstadt.informatik.gesturereader.game.fight.gui.AfterDoneAnimator;
import de.tu_darmstadt.informatik.gesturereader.game.fight.gui.ExecutePlayerAnimation;
import de.tu_darmstadt.informatik.gesturereader.game.fight.gui.HealthGuiUpdater;
import de.tu_darmstadt.informatik.gesturereader.game.fight.gui.SoundPlayer;
import de.tu_darmstadt.informatik.gesturereader.game.fight.gui.TimerView;
import de.tu_darmstadt.informatik.gesturereader.game.fight.gui.WinLooseDisplay;
import de.tu_darmstadt.informatik.gesturereader.game.fight.level.Level;
import de.tu_darmstadt.informatik.gesturereader.utils.Animations;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnActivity;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 01.01.2018
 */

public class FightGameActivity extends OwnActivity {

    public static boolean USE_LANDSCAPE = false;
    public static boolean USE_ALT_KICK = false;


    public static final String GAME_DONE_PATH = "gameDone";
    public static final String SELF_HEALTH_PATH = "endSelfHealth";
    public static final String ENEMY_HEALTH_PATH = "endEnemyHealth";
    public static final String PRECISION = "precision";


    /**
     * The used Game-Model.
     */
    private FightGameModel gameModel;

    /**
     * The used Timer.
     */
    private TimerView timer;

    /**
     * The used Level.
     */
    private Level level;

    /**
     * What to call after completion to record the complete session.
     */
    private CompleteFightRecorder recorder;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        if( !getIntent().hasExtra( "model" ) ){
            Toast.makeText( this, "No Model provided", Toast.LENGTH_LONG ).show();
            this.finish();
            return;
        }

        String modelName = getIntent().getStringExtra( "model" );
        ModelProxy model = session().getModelManager().getForName( modelName );

        if( model == null ){
            Toast.makeText( this, "Could not load Model!!!", Toast.LENGTH_LONG ).show();
            this.finish();
            return;
        }

        //Read level:
        level = new Level( getIntent() );

        //Add user for eval:
        String saveUser = getIntent().hasExtra( "user" ) ? getIntent().getStringExtra( "user" ) : null;
        EvalSaver saver = ( !level.isRecordingOn() || saveUser == null ) ? null : new EvalSaver( session(), saveUser );


        //Set view to be in landscape or Portrait:
        int toRequest = USE_LANDSCAPE ? ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        if( getRequestedOrientation() != toRequest ) {
            setRequestedOrientation( toRequest );
            return;
        }

        //First set Variables:
        setContentView( USE_LANDSCAPE ? R.layout.activity_game_fight_landscape : R.layout.activity_game_fight );

        FightGestureDetector detector = new FightGestureDetector(
                findViewById(R.id.detect_progress),
                findViewById(R.id.gesture_to_do),
                model, session(),
                saver );

        NewGestureProvider provider = level.isUseRetryGestureProvider() ? new ProgressiveGestureProvider( model.getGestures() ) : new DefaultNewGestureProvider();

        //Check if we are on single gesture mode:
        if( !level.getSingleGesture().isEmpty() ) {
            Gesture gesture = stream( model.getGestures() )
                    .filter(g -> g.getName().equals( level.getSingleGesture() ) )
                    .findFirst()
                    .orElse( null );

            if( gesture != null ) provider = new SingleGestureProvider( gesture );
        }

        timer = findViewById( R.id.timer );
        gameModel = new FightGameModel( detector, model, level, provider );


        //Then add listeners:
        HealthGuiUpdater healthHandler = new HealthGuiUpdater( findViewById( R.id.health_self ), findViewById( R.id.health_other ) );
        gameModel.addHealthUpdateListener( healthHandler );
        gameModel.addWinLooseCallback( new EndListener( timer::pause ) );
        gameModel.addWinLooseCallback( new EndListener( gameModel::pause ) );
        gameModel.addWinLooseCallback( new EndListener( this::done ) );
        gameModel.addWinLooseCallback( new WinLooseDisplay( this ) );
        gameModel.addWinLooseCallback( new AfterDoneAnimator( findViewById( R.id.self ), findViewById( R.id.other ), this ) );
        gameModel.addGestureExecutedListener( recorder );
        gameModel.addGestureExecutedListener( new ExecutePlayerAnimation( session(), findViewById( R.id.self ), findViewById( R.id.other ), findViewById( R.id.self_item ), findViewById( R.id.other_item ) ) );

        //Add Sound playing if wanted:
        if( level.isPlaySounds() ) gameModel.addGestureExecutedListener( new SoundPlayer( this ) );

        //Only set if we record stuff.
        if( level.isRecordingOn() ){
            recorder = new CompleteFightRecorder( session(), saveUser );
            gameModel.addWinLooseCallback( recorder );
        }

        //Be smart and start a calibration:
        timer.postDelayed( () -> session().getContextRecSession().doCalibration(), 1000 );

        //Now start the timer!
        startCountdownTimer();
    }


    /**
     * Notifies that we are done.
     */
    private void done(){
        timer.postDelayed( () -> finish( true ), 4_000 );
    }


    private void startCountdownTimer(){
        TextView countdown = findViewById( R.id.count_down );
        TextView round = findViewById( R.id.round_label );
        countdown.setVisibility( View.VISIBLE );
        round.setVisibility( View.VISIBLE );

        round.setText( level.getRound() );
        countdown.setText( "3" );

        timer.postDelayed( () -> countdown.setText( "2" ), 1000 );
        timer.postDelayed( () -> countdown.setText( "1" ), 2000 );
        timer.postDelayed( () -> countdown.setText( R.string.go ), 3000 );
        timer.postDelayed( () -> countdown.startAnimation(Animations.fadeOut( 500 ) ), 3000 );
        timer.postDelayed( () -> countdown.setVisibility( View.INVISIBLE ), 3500 );
        timer.postDelayed( () -> round.setVisibility( View.INVISIBLE ), 3500 );

        timer.postDelayed( this::start, 3500 );

        animateFiguresStart();
    }


    /**
     * Starts the Animation for the Fighters to get to center.
     */
    private void animateFiguresStart(){
        //Start move to center:
        Animation transRight = new TranslateAnimation( -400, 0, 0, 0 );
        transRight.setDuration( 2000 );

        Animation transLeft = new TranslateAnimation( 400, 0, 0, 0 );
        transLeft.setDuration( 2000 );

        findViewById( R.id.self ).startAnimation( transRight );
        findViewById( R.id.other ).startAnimation( transLeft );
    }




    /**
     * Starts the thing!
     */
    private void start() {
        if( recorder != null ) recorder.started();

        gameModel.start();
        timer.start();
    }


    private void finish( boolean endedCorrect ){
        timer.pause();
        gameModel.pause();

        Intent resultIntent = new Intent();
        resultIntent.putExtra( GAME_DONE_PATH, endedCorrect );
        resultIntent.putExtra( SELF_HEALTH_PATH, (int) gameModel.getHealthSelf() );
        resultIntent.putExtra( ENEMY_HEALTH_PATH, (int) gameModel.getHealthEnemy() );
        resultIntent.putExtra( PRECISION, gameModel.getPrecision() );

        this.setResult( 1, resultIntent );
        this.finish();
    }



    @Override
    public void onBackPressed() {
        doubleClickBack( findViewById( R.id.gesture_to_do ) );
    }



    //TMP variable:
    private DateTime lastBack = new DateTime( 0 );

    /**
     * This is a simple method to call for back button.
     * @param view to use.
     */
    public void doubleClickBack( View view ) {
        if( !level.isHasSkipButton() ) return;

        DateTime now = DateTime.now();
        Duration between = new Duration( lastBack, now );

        lastBack = now;
        if( between.getMillis() < 1000 ) finish( false);
    }


    public static class EndListener implements FightGameModel.WinLooseCallback {
        private final Runnable run;
        public EndListener( Runnable run ) { this.run = run; }
        @Override public void playerLost() { run.run(); }
        @Override public void playerWon() { run.run(); }
    }
}
