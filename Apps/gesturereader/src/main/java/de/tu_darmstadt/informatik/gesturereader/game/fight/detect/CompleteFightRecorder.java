package de.tu_darmstadt.informatik.gesturereader.game.fight.detect;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;

import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameModel;
import de.tu_darmstadt.informatik.gesturereader.utils.Quadruple;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.sensordata.JsonSensorDataSerializer;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import java8.util.stream.Collectors;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 02.01.2018
 */

public class CompleteFightRecorder implements FightGameModel.GestureExecutedListener, FightGameModel.WinLooseCallback {


    /**
     * The used session for recording.
     */
    private final GestureRecSession session;

    /**
     * The executed gestures from-to.
     */
    private final Collection<Quadruple<DateTime,DateTime,Gesture,Gesture>> gesturesExecuted = new ArrayList<>();

    /**
     * The user to use.
     */
    private final String user;

    /**
     * The start time.
     */
    private DateTime start = null;


    public CompleteFightRecorder(GestureRecSession session, String user) {
        this.session = session;
        this.user = user == null ? "UNKNOWN" : user;
    }


    /**
     * Notifies that it ended.
     * Also triggers the Save:
     */
    public void ended(){
        Executors.newSingleThreadExecutor().execute( this::recordAndSave );
    }


    /**
     * Starts a Record and does a save of it.
     */
    private void recordAndSave(){
        DateTime end = DateTime.now();
        SensorDataBatch data = session.getDataRecorder().getDataNow( start, end );

        File saveFolder = session.getFileConstants().getBaseGestureFolder().getParentFile();
        saveFolder = new File( saveFolder, "FightRecords" );
        saveFolder = new File( saveFolder, start.getMillis() + "_" + user );

        if( saveFolder.exists() ) saveFolder.mkdirs();

        File batchSave = new File( saveFolder, "batch" );
        File gestureSave = new File( saveFolder, "gestures" );

        //Save batch:
        JsonSensorDataSerializer serializer = new JsonSensorDataSerializer();
        byte[] serialized = serializer.serialize( data );
        try{ FileUtils.writeByteArrayToFile( batchSave, serialized ); }catch ( Throwable exp ){ exp.printStackTrace(); }

        //Save gestures:
        Collection<String> text = stream( gesturesExecuted )
            .map( e -> "s: " + e.getFirst().getMillis() + " e:" + e.getSecond().getMillis() + " w: " + e.getThird().getName() + " g:" + e.getFourth().getName() )
            .collect( Collectors.toList() );

        try{ FileUtils.writeLines( gestureSave, text ); }catch ( Throwable exp ){ exp.printStackTrace(); }
    }



    /**
     * Sets the time this started.
     */
    public void started(){
        this.start = DateTime.now();
    }


    /**
     * Notifies that a gesture was executed.
     *
     * @param start when the gesture started.
     * @param end when the gesture ended.
     * @param wanted what gesture that should be.
     * @param got what gesture that we got.
     */
    public void gestureExecuted( DateTime start, DateTime end, Gesture wanted, Gesture got ) {
        gesturesExecuted.add( new Quadruple<>( start, end, wanted, got ) );
    }


    @Override
    public void playerWon() {
        ended();
    }


    @Override
    public void playerLost() {
        ended();
    }
}
