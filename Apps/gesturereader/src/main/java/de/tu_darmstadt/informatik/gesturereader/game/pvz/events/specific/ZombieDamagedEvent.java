package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

/**
 * Created by Toby on 24.10.2017
 */

public class ZombieDamagedEvent extends Event {

    /**
     * The zombie damaged.
     */
    private final Zombie damaged;

    /**
     * The damage dealt.
     */
    private final double damage;

    /**
     * the plant damaging.
     */
    private final Plant plant;


    public ZombieDamagedEvent(Zombie damaged, double damage, Plant plant) {
        this.damaged = damaged;
        this.damage = damage;
        this.plant = plant;
    }


    public Zombie getDamaged() {
        return damaged;
    }

    public double getDamage() {
        return damage;
    }

    public Plant getPlant() {
        return plant;
    }
}
