package de.tu_darmstadt.informatik.gesturereader;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import de.tu_darmstadt.informatik.gesturereader.detect.ActivityDetect;
import de.tu_darmstadt.informatik.gesturereader.detect.ActivityPermDetect;
import de.tu_darmstadt.informatik.gesturereader.evaluation.EvalActivity;
import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameActivity;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.PvZGui;
import de.tu_darmstadt.informatik.gesturereader.linker.DataSourceLinkerActivity;
import de.tu_darmstadt.informatik.gesturereader.models.ModelsActivity;
import de.tu_darmstadt.informatik.gesturereader.record.GestureRecorderActivity;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnActivity;
import de.tu_darmstadt.informatik.gesturereader.utils.ViewUtils;
import de.tu_darmstadt.informatik.kom.contextsense.DataSource;
import de.tu_darmstadt.informatik.kom.contextsense.batch.SensorDataBatch;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.sensordata.JsonSensorDataSerializer;
import de.tu_darmstadt.informatik.kom.contextsense.serialization.sensordata.SensorDataSerializer;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;

import static java8.util.stream.StreamSupport.stream;

public class MainActivity extends OwnActivity {


    /**
     * The Executor to use in this class.
     */
    private final Executor executor = Executors.newSingleThreadExecutor();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //Update the UserName.
        Button userButton = findViewById( R.id.user );
        userButton.setText( session().getUserManager().getCurrentUserName() );

        findViewById( R.id.detect ).setOnLongClickListener( this::openPermDetect );
        findViewById( R.id.to_arff ).setOnLongClickListener( this::doRecord );
        findViewById( R.id.fight ).setOnLongClickListener( this::clickFightLong );
    }


    private boolean doRecord(View view) {
        DateTime start = DateTime.now().plus( Duration.millis( 500 ) );

        view.postDelayed( () -> Toast.makeText( this, "GO!", Toast.LENGTH_SHORT ).show(), 500 );
        view.postDelayed( () -> {
            DateTime end = start.plus( Duration.millis( 2000 ) );
            SensorDataBatch batch = session().getDataRecorder().getDataNow( start, end );
            if( batch == null ){
                Toast.makeText(  this, "No batch created?!? WTF?", Toast.LENGTH_LONG ).show();
                return;
            }

            SensorDataSerializer serializer = new JsonSensorDataSerializer();

            try{
                File file =  new File( session().getFileConstants().getBaseGestureFolder().getParentFile(), "tmp.batch");
                if( file.exists() ) file.delete();
                FileUtils.writeByteArrayToFile( file, serializer.serialize( batch ) );
            }catch ( IOException exp ){
                exp.printStackTrace();
                Toast.makeText( this, "Failed!", Toast.LENGTH_SHORT ).show();
                return;
            }

            Toast.makeText( this, "Done!", Toast.LENGTH_SHORT ).show();
        }, 3000 );

        return true;
    }


    /**
     * Update the Usernam.
     * @param view to set.
     */
    public void changeUserName( View view ){
        Button userButton = findViewById( R.id.user );

        String old = userButton.getText().toString();
        ViewUtils.buildInputDialog( this, "Enter Username", old, "Save", "Cancel", this::changeUsername, null ).show();
    }


    /**
     * Changes the Username internaly.
     * @param newName to set.
     */
    private void changeUsername( String newName ){
        //Check that it's not empty:
        if( newName == null || newName.isEmpty() ){
            Toast.makeText( this, "Can not be empty.", Toast.LENGTH_LONG ).show();
            return;
        }

        //Check for restraints:
        if( newName.contains( "_" ) ){
            Toast.makeText( this, "Name may not contain '_' (Underscore)", Toast.LENGTH_LONG ).show();
            return;
        }

        session().getUserManager().setCurrentUserName( newName );

        //Update the UserName.
        Button userButton = findViewById( R.id.user );
        userButton.setText( session().getUserManager().getCurrentUserName() );
    }


    public void click_detect(View unused){
        startActivity(new Intent(this, ActivityDetect.class));
    }


    private boolean openPermDetect( View unused ) {
        String[] models = session().getModelManager().getAllModelNames().toArray( new String[0] );
        session().Dialogs().OpenStringSingleSelectionDialog( this, models, m -> {
            Intent intent = new Intent(this, ActivityPermDetect.class);
            intent.putExtra("model", m);

            startActivity( intent );
        } );

        return true;
    }


    public void arff_clicked(View view) {
        session().Dialogs().OpenGestureMultiSelectionAndUsersDialog( this, this::saveToARFF );
    }


    private void saveToARFF( List<Gesture> gestures, List<String> users ){
        Toast.makeText(this, "Starting..", Toast.LENGTH_SHORT).show();
        executor.execute( () ->{
            long before = System.currentTimeMillis();

            try{
                File dir = new File(Environment.getExternalStorageDirectory(), "GData_ARFF");
                File file = new File(dir, "data_" + System.currentTimeMillis() + ".arff");

                //Filter + Save:
                GestureRecSession session = session();
                session.getSampleManager().query()
                        .addUsers( users )
                        .addGestures( gestures )
                        .applyFilter()
                        .saveToARFFFile( session, file );
            }catch ( Exception exp){
                exp.printStackTrace();
                this.runOnUiThread( () -> Toast.makeText( this, "Error on creating AARF. Look into LOG!", Toast.LENGTH_LONG ).show() );
            }finally {
                long took = System.currentTimeMillis() - before;
                this.runOnUiThread( () -> {
                    Toast.makeText(this, "Took " + took + "ms.", Toast.LENGTH_SHORT).show();
                    TextView status = findViewById(R.id.status);
                    status.setText( R.string.done );
                } );
            }
        });
    }

    public void link_clicked(View view) {
        startActivity(new Intent(this, DataSourceLinkerActivity.class ));
    }

    public void record_clicked(View view) {
        startActivity(new Intent(this, GestureRecorderActivity.class ));
    }


    public void click_models(View view) {
        startActivity( new Intent( this, ModelsActivity.class ) );
    }

    public void cancel_clicked( final View view ){
        String[] items = stream( session().getContextRecSession().getDataSources() )
                .map( DataSource::getDeviceName )
                .sorted()
                .toArray( String[]::new );

        new AlertDialog.Builder( this )
                .setItems( items, (s,i) -> {} )
                .create()
                .show();
    }

    public void click_PVZ(View view) {
        String[] models = session().getModelManager().getAllModelNames().toArray( new String[0] );
        session().Dialogs().OpenStringSingleSelectionDialog( this, models, this::openPvZ );
    }

    private void openPvZ( String model ) {
        Intent intent = new Intent( this, PvZGui.class );
        intent.putExtra( "model", model );
        startActivity( intent );
    }


    public void clickFight( View view ) {
        String[] models = session().getModelManager().getAllModelNames().toArray( new String[0] );
        session().Dialogs().OpenStringSingleSelectionDialog( this, models, m -> openFight(m,null) );
    }

    public boolean clickFightLong( View view ) {
        String user = generateEvalName();

        String[] models = session().getModelManager().getAllModelNames().toArray( new String[0] );
        session().Dialogs().OpenStringSingleSelectionDialog( this, models, m -> openFight(m,user) );

        return true;
    }


    /**
     * Generates the next Eval nr.
     * @return the next eval nr.
     */
    private String generateEvalName(){
        String prefix = "Eval";

        Collection<String> present = Arrays.asList( session().getUserManager().readAllPresentUsers() );
        for( int i = 0; i < Integer.MAX_VALUE; i++ ){
            String tmpName = prefix + i;
            if( !present.contains( tmpName ) ) return tmpName;
        }

        return "ALL HAIL MEGATRON";
    }


    private void openFight( String model, String user ) {
        Intent intent = new Intent( this, FightGameActivity.class );
        intent.putExtra( "model", model );
        if( user != null ) intent.putExtra( "user", user );
        startActivity( intent );
    }


    public void click_eval(View view) {
        Intent intent = new Intent( this, EvalActivity.class );
        startActivity( intent );
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder( this )
                .setTitle( "Close?" )
                .setPositiveButton( "Yes", this::finalClose )
                .setNegativeButton( "No", (v,i) -> v.dismiss() )
                .create().show();
    }


    private final void finalClose( Object obj, int unused ){
        this.session().stopStreaming();

        this.finish();
        android.os.Process.killProcess( android.os.Process.myPid() );
    }


    @Override
    protected void onStart() {
        super.onStart();

        GestureRecSession session = session();
        if( session == null || session.getContextRecSession().isClosed() ) {
            session = MainApplication.fromContext( this ).recreateSession();
        }

        session.startStreaming();
    }


    @Override
    protected void onDestroy() {
        session().close();

        super.onDestroy();
    }

}
