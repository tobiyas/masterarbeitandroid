package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.level;

import java.util.Random;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.DefaultZombie;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.FastZombie;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.ToughZombie;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.VeryToughZombie;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;

import static de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem.TICKS_PER_SECOND;

/**
 * Created by Toby on 24.10.2017
 */
public class ZombieLevel {


    /**
     * Generates level 1.
     * @param eventSystem to use.
     * @return the generated Level.
     */
    public static ZombieSpawner generateEmptyLevel1( EventSystem eventSystem ){
        return populateLevel1( new ZombieSpawner( eventSystem ) );
    }

    /**
     * Populates the Spawner for level 1.
     * @param spawner to populate.
     * @return the passed spawner for chaning.
     */
    public static ZombieSpawner populateLevel1( ZombieSpawner spawner ){
        Random rand = new Random( 42 );

        //Add individual:
        int offset = rand.nextInt( TICKS_PER_SECOND ) + ( TICKS_PER_SECOND * 3 );
        final int MAX_ROUNDS = 20;
        for( int i = 0; i < MAX_ROUNDS; i++ ){
            int lane = rand.nextInt( 3 );
            offset += rand.nextInt( TICKS_PER_SECOND * 8 ) + ( TICKS_PER_SECOND * 8 ) ;
            spawner.addPattern( new ZombieSpawnPattern( lane, getZombie( rand, i, MAX_ROUNDS, lane ), offset ) );
        }

        //Add waves:


        return spawner;
    }


    /**
     * spawns a random zombie depending on the rounds.
     * @return the generated Zombie.
     */
    private static Zombie getZombie( Random rand, int spawnRound, int maxRounds, int lane ){
        double percent = (double) spawnRound / (double) maxRounds ;

        //First 25%
        if( percent < 0.25 ) return new DefaultZombie( lane );

        //To 50%
        if( percent < 0.5 ) {
            double random = rand.nextDouble();
            if( random < 0.5 ) return new DefaultZombie( lane );
            if( random < 0.75 ) return new ToughZombie( lane );

            return new FastZombie( lane );
        }

        //To 75%
        if( percent < 0.75 ){
            double random = rand.nextDouble();
            if( random < 0.25 ) return new DefaultZombie( lane );
            if( random < 0.5 ) return new FastZombie( lane );
            if( random < 0.75 ) return new ToughZombie( lane );

            return new VeryToughZombie( lane );
        }


        //Rest to 100% -> Escalate!
        double random = rand.nextDouble();
        if( random < 0.1 ) return new DefaultZombie( lane );
        if( random < 0.3 ) return new FastZombie( lane );
        if( random < 0.7 ) return new ToughZombie( lane );

        return new VeryToughZombie( lane );
    }

}
