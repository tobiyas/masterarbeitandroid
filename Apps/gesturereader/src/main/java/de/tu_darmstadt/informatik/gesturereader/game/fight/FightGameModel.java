package de.tu_darmstadt.informatik.gesturereader.game.fight;

import android.widget.Toast;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import de.tu_darmstadt.informatik.gesturereader.game.fight.detect.DefaultNewGestureProvider;
import de.tu_darmstadt.informatik.gesturereader.game.fight.detect.NewGestureProvider;
import de.tu_darmstadt.informatik.gesturereader.game.fight.level.Level;
import de.tu_darmstadt.informatik.gesturereader.utils.QuadConsumer;
import de.tu_darmstadt.informatik.kom.contextsense.utils.Pair;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.GestureManager;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;

import static java8.util.stream.StreamSupport.stream;

/**
 * Created by Tobias on 01.01.2018
 */

public class FightGameModel {

    private static final long TIME_BETWEEN_DETECT = 1_000L;
    private static final long TIME_ANIMATION = 2_000L;

    /**
     * A Scheduler for running the detections.
     */
    private ScheduledExecutorService executor;


    /**
     * The used starter for gestures
     */
    private final GestureDetectorStarter detectorStarter;

    /**
     * The used model.
     */
    private final ModelProxy model;

    /**
     * The used level.
     */
    private final Level level;


    /**
     * The current health of the Player.
     */
    private double healthSelf;

    /**
     * The current Health of the Enemy.
     */
    private double healthEnemy;


    /**
     * The classes that want an update for health changes.
     */
    private final Collection<HealthUpdatedListener> healthUpdatedListeners = new HashSet<>();

    /**
     * The classes that want an update on Win / Loose.
     */
    private final Collection<WinLooseCallback> winLooseCallbacks = new HashSet<>();

    /**
     * The classes that want an update when a gesture is executed.
     */
    private final Collection<GestureExecutedListener> gestureExecutedListeners = new HashSet<>();

    /**
     * The queue of Gestures to query for next gestures.
     */
    private final Queue<Gesture> gestureQueue = new LinkedList<>();

    /**
     * The list of wanted to got:
     */
    private final Collection<Pair<Gesture,Gesture>> wantedGotList = new ArrayList<>();

    /**
     * Provides new Gestures.
     */
    private final NewGestureProvider gestureProvider;


    public FightGameModel( GestureDetectorStarter detectorStarter,
                           ModelProxy model,
                           Level level ) {

        this( detectorStarter, model, level, new DefaultNewGestureProvider() );
    }


    public FightGameModel( GestureDetectorStarter detectorStarter,
                           ModelProxy model,
                           Level level,
                           NewGestureProvider provider ){

        this.detectorStarter = detectorStarter;
        this.model = model;
        this.level = level;
        this.gestureProvider = provider;

        this.healthSelf = level.getHealthSelf();
        this.healthEnemy = level.getHealthEnemy();

        //Add the Provider to the Execution list:
        this.addGestureExecutedListener( provider );

        //Generate the predefined gestures to use:
        fillQueueWithData();
    }

    /**
     * Fills the Queue with the Gestures present in random order.
     */
    private void fillQueueWithData() {
        if( !gestureQueue.isEmpty() ) return;
        gestureQueue.addAll( gestureProvider.getNextGestures( model.getGestures() ) );
    }


    /**
     * Does damage to self.
     * @param value to damage.
     */
    public void damageSelf( double value ){
        this.healthSelf -= value;

        stream( healthUpdatedListeners )
                .forEach( l -> l.ownHealthUpdated( healthSelf, level.getHealthSelf() ) );

        //Lost:
        if( healthSelf <= 0 ){
            stream( winLooseCallbacks )
                    .forEach( WinLooseCallback::playerLost );
        }
    }

    /**
     * Does damage to enemy.
     * @param value to damage.
     */
    public void damageEnemy( double value ){
        this.healthEnemy -= value;

        stream( healthUpdatedListeners )
                .forEach( l -> l.enemyHealthUpdated(healthEnemy, level.getHealthEnemy()) );

        //Win:
        if( healthEnemy <= 0 ){
            stream( winLooseCallbacks )
                .forEach( WinLooseCallback::playerWon );
        }
    }

    /**
     * Does healing to self.
     * @param value to heal.
     */
    public void healSelf( double value ){
        this.healthSelf += value;
        this.healthSelf = Math.min( level.getHealthSelf(), healthSelf );

        stream( healthUpdatedListeners )
                .forEach( l -> l.ownHealthUpdated( healthSelf, level.getHealthSelf() ) );
    }


    /**
     * Does healing to enemy.
     * @param value to heal.
     */
    public void healEnemy( double value ){
        this.healthEnemy -= value;
        this.healthEnemy = Math.min( level.getHealthEnemy(), healthEnemy);

        stream( healthUpdatedListeners )
                .forEach( l -> l.enemyHealthUpdated(healthEnemy, level.getHealthEnemy()) );
    }



    /**
     * Adds a new Health update listener.
     *
     * @param listener to add.
     */
    public void addHealthUpdateListener( HealthUpdatedListener listener ){
        if( listener != null ) this.healthUpdatedListeners.add( listener );
    }

    /**
     * Removes a Health update listener.
     *
     * @param listener to remove.
     */
    public void removeHealthUpdateListener( HealthUpdatedListener listener ){
        if( listener != null ) this.healthUpdatedListeners.remove( listener );
    }


    /**
     * Adds a new Win/Loose update listener.
     *
     * @param listener to add.
     */
    public void addWinLooseCallback( WinLooseCallback listener ){
        if( listener != null ) this.winLooseCallbacks.add( listener );
    }

    /**
     * Removes a gesture executed update listener.
     *
     * @param listener to remove.
     */
    public void removeWinLooseCallback( WinLooseCallback listener ){
        if( listener != null ) this.winLooseCallbacks.remove( listener );
    }


    /**
     * Adds a new Win/Loose update listener.
     *
     * @param listener to add.
     */
    public void addGestureExecutedListener( GestureExecutedListener listener ){
        if( listener != null ) this.gestureExecutedListeners.add( listener );
    }

    /**
     * Removes a gesture executed update listener.
     *
     * @param listener to remove.
     */
    public void removeGestureExecutedListener( GestureExecutedListener listener ){
        if( listener != null ) this.gestureExecutedListeners.remove( listener );
    }


    /**
     * Got a gesture executed.
     * @param start when the gesture started recording
     * @param end when the gesture ended recording
     * @param wanted that was wanted.
     * @param got the gesture we got.
     */
    private void executedGesture( DateTime start, DateTime end, Gesture wanted, Gesture got ) {
        Gesture fWanted = wanted == null ? GestureManager.UNKNOWN() : wanted;
        Gesture fGot = got == null ? GestureManager.UNKNOWN() : got;

        boolean same = fWanted.equals( fGot );
        wantedGotList.add( new Pair<>( fWanted, fGot ) );

        stream( gestureExecutedListeners )
            .forEach( e -> e.gestureExecuted( start, end, fWanted, fGot ) );

        if( same ) damageEnemy( level.getDamageEnemy() );
        else damageSelf( level.getDamageSelf() );
    }


    /**
     * Starts the Game.
     */
    public void start() {
        long timeBetween = 2_000 //Gesture Record time
                + TIME_ANIMATION        //The Time the animation takes.
                + TIME_BETWEEN_DETECT   //time between detects
                ;

        this.executor = Executors.newSingleThreadScheduledExecutor();
        this.executor.scheduleAtFixedRate( this::startDetect, 100, timeBetween, TimeUnit.MILLISECONDS );
    }


    /**
     * Pauses the System.
     */
    public void pause() {
        this.executor.shutdownNow();
    }


    /**
     * Starts a detection run.
     */
    private void startDetect(){
        fillQueueWithData();
        detectorStarter.startDetectingGesture( this::executedGesture, gestureQueue.poll() );
    }


    public double getHealthEnemy() {
        return healthEnemy;
    }

    public double getHealthSelf() {
        return healthSelf;
    }


    /**
     * Gets the precision for the current state.
     * Values between 0 and 1.
     *
     * @return the precision.
     */
    public double getPrecision(){
        double correct = stream( wantedGotList )
                .filter( e -> e.getFirst().equals( e.getSecond() ) )
                .count();

        double total = wantedGotList.size();
        return total <= 0 ? 0 : correct / total;
    }


    public interface HealthUpdatedListener {

        /**
         * This is called when the Health is updated.
         *
         * @param ownHealth that is now current.
         * @param ownMaxHealth that is now current.
         */
        void ownHealthUpdated( double ownHealth, double ownMaxHealth );

        /**
         * This is called when the Health is updated.
         *
         * @param enemyHealth that is now current.
         * @param enemyMaxHealth that is now current.
         */
        void enemyHealthUpdated( double enemyHealth, double enemyMaxHealth );
    }


    public interface WinLooseCallback {

        /**
         * The player won the match.
         */
        void playerWon();

        /**
         * The player lost the match.
         */
        void playerLost();
    }



    public interface GestureDetectorStarter {

        /**
         * A Detector to start a Gesture recognition.
         * @param callback to call when done. Wanted -> Got.
         * @param gesture to detect.
         */
        void startDetectingGesture( QuadConsumer<DateTime,DateTime,Gesture,Gesture> callback, Gesture gesture );
    }



    public interface GestureExecutedListener {


        /**
         * Notifies that a gesture was executed.
         *
         * @param start when the gesture started.
         * @param end when the gesture ended.
         * @param wanted what gesture that should be.
         * @param got what gesture that we got.
         */
        void gestureExecuted(DateTime start, DateTime end, Gesture wanted, Gesture got );
    }

}
