package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Event fired, when the health of the player changes.
 * Created by Toby on 24.10.2017
 */

public class PlayerHealthChangedEvent extends Event {

    /**
     * The new Health of the Player.
     */
    private final int newHealth;


    public PlayerHealthChangedEvent(int newHealth) {
        this.newHealth = newHealth;
    }


    public int getNewHealth() {
        return newHealth;
    }
}
