package de.tu_darmstadt.informatik.gesturereader.models;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import de.tu_darmstadt.informatik.gesturereader.MainApplication;
import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.utils.ViewUtils;
import de.tu_darmstadt.informatik.kom.contextsense.detectors.weka.WekaBaseClassifier;
import de.tu_darmstadt.informatik.kom.gesturerec.GestureRecSession;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.models.IncrementalLearningModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;
import java8.util.stream.RefStreams;

/**
 * Created by Toby on 08.01.2018
 */
public class ModelsFragment extends Fragment {


    private static final String[] VARIANTS =
            RefStreams.of( WekaBaseClassifier.values() )
                    .filter( v -> v != WekaBaseClassifier.NAIVE_BAYES_MULTINOMIAL )
                    .filter( v -> v != WekaBaseClassifier.M5P )
                    .filter( v -> v != WekaBaseClassifier.MULTILAYER_PERCEPTRON )
                    .map( WekaBaseClassifier::name )
                    .sorted()
                    .toArray( String[]::new );

    private static final Executor executor = Executors.newSingleThreadExecutor();

    private View root;

    private GestureRecSession session;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate( R.layout.fragment_saved_models, container, false );
        this.session = MainApplication.fromContext( this.getContext() ).getSession();

        this.updateAdapter();

        Button create = root.findViewById( R.id.create );
        create.setOnClickListener( this::createClicked );

        Button learn = root.findViewById( R.id.learn );
        learn.setEnabled( false );
        learn.setOnClickListener( this::learnClicked );

        return root;
    }

    private void updateAdapter() {
        Context context = this.getContext();
        if( context == null ) return;

        Collection<String> models = session.getModelManager().getAllModelNames();
        ListView list = root.findViewById( R.id.list );
        list.setChoiceMode( ListView.CHOICE_MODE_SINGLE );

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                context,
                android.R.layout.simple_list_item_1,
                new ArrayList<>( models )
        );

        list.setAdapter( adapter );
        list.setOnItemClickListener( this::selectedModel );
        adapter.notifyDataSetChanged();

        list.setOnItemLongClickListener( this::askDelete );
    }


    private ModelProxy selectedModel = null;
    private void selectedModel(AdapterView<?> aView, View view, int position, long id ){
        String modelSelected = aView.getItemAtPosition( position ).toString();

        selectedModel  = session.getModelManager().getForName( modelSelected );
        root.findViewById( R.id.learn ).setEnabled( selectedModel != null && selectedModel instanceof IncrementalLearningModelProxy);
    }


    private boolean askDelete( AdapterView<?> adapterView, View unused1, int slot, long unused2 ) {
        String data = (String) adapterView.getItemAtPosition( slot );

        Context context = this.getContext();
        if( context == null ) return true;


        new AlertDialog.Builder( context )
                .setTitle( "DELETE " + data + "?" )
                .setPositiveButton( "YES", (a,b) -> deleteDetector( data ) )
                .setNegativeButton( "NO", (a,b) -> a.dismiss() )
                .create()
                .show();

        return true;
    }

    /**
     * The Gui clicked the learn button.
     * @param view to use.
     */
    public void learnClicked( View view ){
        if( !view.isEnabled() || selectedModel == null ) return;

        session.Dialogs().OpenUserDialog( this.getContext(), this::IncrementalLearn );
    }

    private void IncrementalLearn( List<String> users ){
        SampleBatch batch = session.getSampleManager().query()
                .addGestures( selectedModel.getGestures() )
                .addUsers( users )
                .applyFilter();

        IncrementalLearningModelProxy model = (IncrementalLearningModelProxy) selectedModel;
        SampleBatch old = model.getPresentSamples();

        SampleBatch total = new SampleBatch( old ).addBatch( batch );
        int add = total.size() - old.size();

        if( add <= 0){
            Toast.makeText( this.getContext(), "No new Samples selected!", Toast.LENGTH_LONG).show();
            return;
        }

        ProgressDialog dialog = ProgressDialog.show( this.getContext(), "Training...", "Doing fancy Training...", true );
        executor.execute( () -> {
            try{
                model.incrementalLearn( batch );
                MainThread.ToastLong( this.getContext(), "Training Worked with " + total + " new samples." );
            }catch ( Exception exp ){
                exp.printStackTrace();
                MainThread.ToastLong( this.getContext(), "Training Failed. :(" );
            }finally {
                MainThread.runOnMT( dialog::dismiss );
            }

        } );
    }


    /**
     * Deletes the Detector with the name passed.
     * @param name to delete.
     */
    private void deleteDetector( String name ){
        ModelProxy proxy = session.getModelManager().getForName( name );
        if( proxy == null ){
            Toast.makeText( this.getContext(), name + " not found.", Toast.LENGTH_LONG ).show();
            return;
        }

        session.getModelManager().deleteModel( proxy );
        Toast.makeText( this.getContext(), name + " deleted.", Toast.LENGTH_LONG ).show();
        this.updateAdapter();
    }


    private void createClicked( View unused ) {
        Context context = this.getContext();
        if( context == null ) return;

        new AlertDialog.Builder( context )
                .setTitle( "Incremental Learning?" )
                .setPositiveButton( "YES", (v,i) -> selectedIncrementalLearning( true ) )
                .setNegativeButton( "NO", (v,i) -> selectedIncrementalLearning( false ) )

                .create().show();
    }


    private void selectedIncrementalLearning( boolean IncrementalLearner ){
        Context context = this.getContext();
        if( context == null ) return;

        AlertDialog.Builder builder = new AlertDialog.Builder( context );
        builder.setTitle( "WHICH CLASSIFIER?" )
                .setItems(
                        VARIANTS,
                        (u,slot) -> selectGlobalOrPersonal( IncrementalLearner, slot )
                )
                .create().show();
    }


    private void selectGlobalOrPersonal( boolean IncrementalLearner, int slot ){
        String variant = VARIANTS[slot];
        WekaBaseClassifier classifier = WekaBaseClassifier.valueOf( variant );

        session.Dialogs().OpenStringMultiSelectionDialog(
                this.getContext(),
                "Users",
                session.getUserManager().readAllPresentUsersOrDefault(),
                u -> onUsersSelected( IncrementalLearner, classifier, u )
        );
    }


    private void onUsersSelected( boolean IncrementalLearner, WekaBaseClassifier classifier, List<String> users ){
        session.Dialogs().OpenGestureMultiSelectionDialogWithSampleAmount(
                this.getContext(),
                g -> onGesturesSelected( IncrementalLearner, classifier, g, users )
        );
    }


    private void onGesturesSelected( boolean IncrementalLearner,
                                     WekaBaseClassifier classifier,
                                     List<Gesture> gestures,
                                     List<String> users ){
        ViewUtils.buildSaveInputDialog(
                this.getContext(),
                "Name?",
                "Name here",
                new ViewUtils.InputDialogCallback() {
                    @Override
                    public void acceptClicked(DialogInterface dialog, String name ) {
                        createNewClassifier( IncrementalLearner, classifier, name, users, gestures );
                    }

                    //Not needed:
                    @Override public void cancelClicked(DialogInterface dialog) {}
                }).show();
    }


    private void createNewClassifier(final boolean IncrementalLearner,
                                     final WekaBaseClassifier classifier,
                                     final String name,
                                     final List<String> users,
                                     final List<Gesture> gestures) {

        boolean exists = session.getModelManager().presentForName( name );
        if( exists){
            ViewUtils.buildSaveInputDialog(
                    this.getContext(),
                    "Name " + name + " already present!",
                    "other name",
                    new ViewUtils.InputDialogCallback() {
                        @Override
                        public void acceptClicked( DialogInterface dialog, String name ) {
                            createNewClassifier( IncrementalLearner, classifier, name, users, gestures);
                        }

                        //Not needed:
                        @Override public void cancelClicked(DialogInterface dialog) {}
                    }).show();
            return;
        }

        final ProgressDialog dialog = ProgressDialog.show(
                this.getContext(), "" +
                        "Creating " + classifier.toString(),
                "STARTING...",
                true,
                false);

        executor.execute( () -> {
            MainThread.runOnMT( () -> dialog.setMessage( "LOADING SAMPLES..." ) );
            long before = System.currentTimeMillis();
            SampleBatch batch = session.getSampleManager().query().addGestures( gestures ).addUsers( users ).applyFilter();
            long afterReadSamples = System.currentTimeMillis();


            MainThread.runOnMT( () -> dialog.setMessage( "TRAINING..." ) );
            if( IncrementalLearner ) session.getModelManager().trainNewModelIncrementalLearner( name, classifier, batch );
            else session.getModelManager().trainNewModel( name, classifier, batch );
            long afterTraining = System.currentTimeMillis();

            MainThread.runOnMT( () -> dialog.setMessage( "DONE!" ) );
            long readingSamples = afterReadSamples - before;
            long trainingTook = afterTraining - afterReadSamples;
            long savingTook = System.currentTimeMillis() - afterTraining;
            Log.i("MODELS", "Reading Samples took: " + readingSamples + "ms, Training " + classifier.toString() + " took " + trainingTook + "ms, Saving took " + savingTook + "ms.");

            //Dismiss after:
            MainThread.runOnMT( () -> {
                dialog.dismiss();
                updateAdapter();
            } );
        });

    }

}
