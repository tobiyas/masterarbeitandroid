package de.tu_darmstadt.informatik.gesturereader.evaluation;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.utils.OwnDialog;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;

/**
 * Created by Toby on 11.01.2018
 */
public class VerifySamplesDialog extends OwnDialog {

    /**
     * The samples to check.
     */
    private SampleBatch samples;


    /**
     * The callback when we are done!
     */
    private Runnable onDoneCallback;


    public VerifySamplesDialog(@NonNull Context context) {
        super( context );

        setCancelable( false );
    }

    public VerifySamplesDialog(@NonNull Context context, int themeResId) {
        super( context, themeResId );

        setCancelable( false );
    }

    protected VerifySamplesDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super( context, cancelable, cancelListener );

        setCancelable( false );
    }


    public void setSamples( SampleBatch samples ){
        this.samples = samples;
    }


    public void setOnDoneCallback(Runnable onDoneCallback) {
        this.onDoneCallback = onDoneCallback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if( samples == null ){
            this.dismiss();
            return;
        }


        setContentView( R.layout.dialog_fix_samples );
        getWindow().setBackgroundDrawable( new ColorDrawable( android.graphics.Color.TRANSPARENT ) );

        findViewById( R.id.abort ).setOnClickListener( this::abort );
        findViewById( R.id.done ).setOnClickListener( this::dismiss );

        //Now build the List for every Item:
        ListView list = findViewById( R.id.list );
        BaseAdapter adapter = new SampleBatchArrayAdapter( this.getContext(), session(), samples );
        list.setAdapter( adapter );
    }


    /**
     * Dismisses the View.
     * @param view unused.
     */
    public void dismiss( View view ){
        this.dismiss();

        if( onDoneCallback != null ) MainThread.runOnMT( onDoneCallback );
    }



    private DateTime lastClicked = new DateTime( 0 );

    /**
     * Does a abort.
     * @param view unused.
     */
    public void abort( View view ){
        DateTime end = DateTime.now().minus( Duration.millis( 1_000L ) );
        if( end.isBefore( lastClicked ) ) this.dismiss();

        this.lastClicked = DateTime.now();
        Toast.makeText( getContext(), "Press again to abort", Toast.LENGTH_LONG ).show();
    }
}
