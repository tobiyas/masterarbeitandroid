package de.tu_darmstadt.informatik.gesturereader.game.fight.gui;

import android.content.Context;
import android.media.MediaPlayer;

import org.joda.time.DateTime;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameModel;
import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;

/**
 * Created by Toby on 16.01.2018
 */
public class SoundPlayer implements FightGameModel.GestureExecutedListener {

    /**
     * The context to use for the Player.
     */
    private final Context context;


    public SoundPlayer(Context context) {
        this.context = context;
    }


    @Override
    public void gestureExecuted(DateTime start, DateTime end, Gesture wanted, Gesture got) {
        boolean correct = wanted.equals( got );

        int sound = gestureToSound( got, correct );
        if( sound != 0 ){
            MainThread.runOnMT( () -> {
                //Play sound and release after:
                MediaPlayer player = MediaPlayer.create( context, sound );
                player.setOnPreparedListener( MediaPlayer::start );
                player.setOnCompletionListener( MediaPlayer::release );
            } );
        }
    }


    /**
     * Does a Gesture -> Sound lookup.
     *
     * @param gesture to lookup
     * @param correct if the gesture was correct.
     * @return the id of the Sound.
     */
    private int gestureToSound( Gesture gesture, boolean correct ){
        if( !correct ) return R.raw.wrong;

        String name = gesture.getName();
        if( name.equals( "attack" ) ) return R.raw.attack;
        if( name.equals( "block" ) ) return R.raw.block;

        if( name.equals( "kick_left" ) ) return R.raw.kick;
        if( name.equals( "kick_right" ) ) return R.raw.kick;

        if( name.equals( "duck" ) ) return R.raw.duck;
        if( name.equals( "jump" ) ) return R.raw.jump;

        return 0;
    }

}
