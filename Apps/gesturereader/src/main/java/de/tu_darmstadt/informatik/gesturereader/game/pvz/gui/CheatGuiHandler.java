package de.tu_darmstadt.informatik.gesturereader.game.pvz.gui;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.GameSpeedDownEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.GameSpeedUpEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.ResourceManager;

/**
 * Created by Tobias on 22.10.2017
 */

public class CheatGuiHandler {



    public CheatGuiHandler( EventSystem eventSystem, ViewProvider viewProvider, ResourceManager resourceManager ) {
        viewProvider.provideView( R.id.give_resources ).setOnClickListener( v -> resourceManager.addResources( 100 ) );
        viewProvider.provideView( R.id.speed_up ).setOnClickListener( v -> eventSystem.fireEvent( new GameSpeedUpEvent( 1.5 ) ) );
        viewProvider.provideView( R.id.speed_down ).setOnClickListener( v -> eventSystem.fireEvent( new GameSpeedDownEvent( 1.5 ) ) );
    }


}
