package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlantPlantedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlantRemovedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.TickEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieDamagesPlantEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;
import java8.util.Objects;
import java8.util.stream.RefStreams;

/**
 * This handler holds all Plants on the field.
 * Created by Toby on 24.10.2017
 */

public class PlantHandler implements EventListener, PlantProvider {


    /**
     * The planted plants.
     */
    private final Plant[][] plants = new Plant[][]{
            {null, null, null},
            {null, null, null},
            {null, null, null}
    };


    /**
     * the used system.
     */
    private final EventSystem eventSystem;



    public PlantHandler(EventSystem eventSystem) {
        this.eventSystem = eventSystem;

        eventSystem.addListener(this);
    }


    @EventHandler
    public void onTick( TickEvent event ){
        RefStreams.of( plants ).flatMap( RefStreams::of ).flatMap( RefStreams::of )
                .filter( Objects::nonNull )
                .forEach( p -> p.tick( eventSystem ) );
    }


    @EventHandler
    public void plantPlanted( PlantPlantedEvent event ){
        int x = event.getX();
        int y = event.getY();
        Plant plant = event.getPlant();

        this.plants[x][y] = plant;
    }


    @EventHandler
    public void plantRemoved( PlantRemovedEvent event ){
        int x = event.getX();
        int y = event.getY();

        this.plants[x][y] = null;
    }

    @EventHandler
    public void plantDamage( ZombieDamagesPlantEvent event ){
        Plant plant = event.getPlant();
        plant.damage( event.getDamage() );

        //If dead: Remove!
        if( plant.isDead() ) eventSystem.fireEvent( new PlantRemovedEvent( plant, plant.getLane(), plant.getSlot() ) );
    }






    @Override
    public Plant getPlantAt(int x, int y) {
        return plants[x][y];
    }
}
