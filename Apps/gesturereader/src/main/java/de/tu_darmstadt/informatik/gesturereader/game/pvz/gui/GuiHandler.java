package de.tu_darmstadt.informatik.gesturereader.game.pvz.gui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;
import java.util.WeakHashMap;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlantPlantedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlantRemovedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PreviewChangedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ResourcesChangedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.SelectionChangedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ShowDebugToastEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.PreviewProvider;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.ResourceManager;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;
import java8.util.stream.IntStreams;
import java8.util.stream.RefStreams;

import static de.tu_darmstadt.informatik.gesturereader.R.id.square_1_1;
import static de.tu_darmstadt.informatik.gesturereader.R.id.square_1_2;
import static de.tu_darmstadt.informatik.gesturereader.R.id.square_1_3;
import static de.tu_darmstadt.informatik.gesturereader.R.id.square_2_1;
import static de.tu_darmstadt.informatik.gesturereader.R.id.square_2_2;
import static de.tu_darmstadt.informatik.gesturereader.R.id.square_2_3;
import static de.tu_darmstadt.informatik.gesturereader.R.id.square_3_1;
import static de.tu_darmstadt.informatik.gesturereader.R.id.square_3_2;
import static de.tu_darmstadt.informatik.gesturereader.R.id.square_3_3;

/**
 * Created by Tobias on 22.10.2017
 */
public class GuiHandler implements EventListener {


    private static final int EMPTY_FIELD = R.drawable.tile_empty;


    /**
     * The Matrix of Images represented x - y
     */
    private final int[][] imageMatrix = new int[][]{
            {square_1_1, square_2_1, square_3_1},
            {square_1_2, square_2_2, square_3_2},
            {square_1_3, square_2_3, square_3_3}
    };

    /**
     * Handler to post something to the MT.
     */
    private final Handler mtHandler = new Handler( Looper.getMainLooper() );


    /**
     * The Provider to update Views.
     */
    private final ViewProvider viewProvider;

    /**
     * The provider for the Preview.
     */
    private final PreviewProvider previewProvider;

    /**
     * The Resource Manager to use.
     */
    private final ResourceManager resourceManager;


    public GuiHandler(EventSystem eventSystem, ViewProvider viewProvider, ResourceManager resourceManager, PreviewProvider previewProvider) {
        this.viewProvider = viewProvider;
        this.previewProvider = previewProvider;
        this.resourceManager = resourceManager;

        eventSystem.addListener( this );

        //Init gui:
        updateResources( new ResourcesChangedEvent( resourceManager.getResources() ) );
        updatePreview( new PreviewChangedEvent( previewProvider.getPreview() ) );
        updateSelection( new SelectionChangedEvent( 1, 1 ) );

        //Init Lanes:
        //The Bitmap cache is a simple hack to save Resources!
        Map<Class<?>,Bitmap> zombieBitmapCache = new WeakHashMap<>();
        ZombieLaneView.initLane( viewProvider, 0, R.id.lane_1, eventSystem, zombieBitmapCache );
        ZombieLaneView.initLane( viewProvider, 1, R.id.lane_2, eventSystem, zombieBitmapCache );
        ZombieLaneView.initLane( viewProvider, 2, R.id.lane_3, eventSystem, zombieBitmapCache );
    }



    @EventHandler
    public void updateResources( ResourcesChangedEvent event ){
        int money = event.getNewResources();

        TextView text = (TextView) viewProvider.provideView( R.id.resource_text );
        text.setText( String.valueOf( money ) );

        //Also update color for cost of preview:
        TextView costView = (TextView) viewProvider.provideView( R.id.preview_cost );
        boolean has = money >= previewProvider.getPreview().getCost();
        costView.setTextColor( has ? Color.GREEN : Color.RED );
    }


    @EventHandler
    public void updatePlantsPlanted( PlantPlantedEvent event ){
        ImageView image = (ImageView) viewProvider.provideView( imageMatrix[event.getX()][event.getY()] );
        image.setImageResource( event.getPlant().getImageID() );
    }

    @EventHandler
    public void updatePlantsRemoved( PlantRemovedEvent event ){
        ImageView image = (ImageView) viewProvider.provideView( imageMatrix[event.getX()][event.getY()] );
        image.setImageResource( EMPTY_FIELD );
    }

    @EventHandler
    public void updateSelection( SelectionChangedEvent event ){
        //Set transparent:
        RefStreams.of( imageMatrix ).flatMap( RefStreams::of ).flatMapToInt( IntStreams::of )
            .mapToObj( viewProvider::provideView )
            .forEach( v -> v.setBackgroundColor( Color.TRANSPARENT ) );

        //Set red:
        viewProvider.provideView( imageMatrix[event.getX()][event.getY()] ).setBackgroundColor( Color.RED );
    }


    @EventHandler
    public void updatePreview( PreviewChangedEvent event ){
        Plant preview = event.getNewPreview();

        ImageView previewImage = (ImageView) viewProvider.provideView( R.id.preview_plant );
        previewImage.setImageResource( preview.getImageID() );


        boolean hasMoney = resourceManager.getResources() >= preview.getCost();
        TextView costView = (TextView) viewProvider.provideView( R.id.preview_cost );
        costView.setText( String.valueOf( preview.getCost() ) );
        costView.setTextColor( hasMoney ? Color.GREEN : Color.RED );
    }


    @EventHandler
    public void debugToast( ShowDebugToastEvent event ){
        final String text = event.getText();
        final int length = event.getLength();

        mtHandler.post( () -> Toast.makeText( viewProvider.getContext(), text, length ).show() );
    }

}