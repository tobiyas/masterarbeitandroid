package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Event fired, when the player wins by killing all Zombies.
 * Created by Toby on 24.10.2017
 */

public class PlayerWonGameEvent extends Event {}
