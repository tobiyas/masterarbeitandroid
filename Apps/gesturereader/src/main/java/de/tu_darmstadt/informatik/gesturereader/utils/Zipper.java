package de.tu_darmstadt.informatik.gesturereader.utils;

import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import de.tu_darmstadt.informatik.kom.gesturerec.models.ModelProxy;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleBatch;
import de.tu_darmstadt.informatik.kom.gesturerec.samples.SampleProxy;

/**
 * Created by Toby on 14.01.2018
 */
public class Zipper {

    /**
     * The Files we want to zip
     */
    private final Collection<ModelProxy> models = new HashSet<>();

    /**
     * The Samples to save.
     */
    private final Collection<SampleProxy> samples = new HashSet<>();

    /**
     * A collection of files to also save.
     */
    private final Collection<File> files = new HashSet<>();


    /**
     * the file to zip to.
     */
    private final File toZipTo;


    public Zipper(File toZipTo) {
        this.toZipTo = toZipTo;
    }


    public Zipper addModel( ModelProxy proxy ){
        if( proxy != null ) this.models.add( proxy );
        return this;
    }


    /**
     * Adds a Sample to the List to save.
     * @param proxy to use.
     */
    public Zipper addSample( SampleProxy proxy ){
        if( proxy != null ) this.samples.add( proxy );
        return this;
    }


    /**
     * adds all Samples from the batch.
     * @param batch to get the samples from.
     */
    public Zipper addSamples( SampleBatch batch ){
        if( batch != null ) this.samples.addAll( batch.getSamples() );
        return this;
    }

    /**
     * Adds a generic file.
     * @param file to use.
     * @return this for chaining
     */
    public Zipper addFile( File file ){
        if( file != null ) this.files.add( file );
        return this;
    }

    /**
     * Adds generic files.
     * @param files to add.
     * @return this for chaining
     */
    public Zipper addFiles( Collection<File> files ){
        if( files != null ) this.files.addAll( files );
        return this;
    }


    /**
     * Adds generic files.
     * @param files to add.
     * @return this for chaining
     */
    public Zipper addFiles( File... files ){
        if( files == null ) return this;
        else return addFiles(Arrays.asList( files ) );
    }


    /**
     * Creates the Zip file.
     */
    public Zipper create() throws IOException {
        if( toZipTo.exists() ) throw new IOException( "file already exists" );

        ZipOutputStream out = new ZipOutputStream( new FileOutputStream( toZipTo ) );

        //First write the Models:
        for( ModelProxy model : models ){
            String name = model.getName();
            out.putNextEntry( new ZipEntry( "models/" + name + ".meta" ) );
            out.write( FileUtils.readFileToByteArray( model.getMetaFile() ) );

            out.putNextEntry( new ZipEntry( "models/" + name + ".model" ) );
            out.write( FileUtils.readFileToByteArray( model.getModelFile() ) );
        }

        //Now write the Samples:
        for( SampleProxy sample : samples ){
            String gesture = sample.getGesture().getName();
            String fileName = sample.getFile().getName();

            out.putNextEntry( new ZipEntry( "samples/" + gesture + "/" + fileName ) );
            out.write( FileUtils.readFileToByteArray( sample.getFile() ) );
        }


        //save arbitrary files:
        for( File file : files ){
            if( !file.exists() ) {
                Log.w("ZipFiles", "Should zip '" + file.getAbsolutePath() + "' but it does not exist");
                continue;
            }

            String name = file.getName();
            String parent = file.getParentFile().getName();

            out.putNextEntry( new ZipEntry( "other/" + parent + "/" + name ) );
            out.write( FileUtils.readFileToByteArray( file ) );
        }

        out.flush();
        out.close();

        return this;
    }

}
