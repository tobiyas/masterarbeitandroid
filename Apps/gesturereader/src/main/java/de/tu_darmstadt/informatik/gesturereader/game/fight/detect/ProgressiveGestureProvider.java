package de.tu_darmstadt.informatik.gesturereader.game.fight.detect;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;

/**
 * Created by Toby on 09.01.2018.
 */
public class ProgressiveGestureProvider implements NewGestureProvider {

    /**
     * The last gesture
     */
    private Gesture currentlyWanted = null;


    /**
     * The current list of Gestures.
     */
    private final Gesture[] gestures;

    /**
     * The current Index.
     */
    private int index = 0;


    public ProgressiveGestureProvider( Collection<Gesture> gestures ) {
        this.gestures = gestures.toArray( new Gesture[ gestures.size() ] );
        this.currentlyWanted = this.gestures[0];
    }


    @Override
    public List<Gesture> getNextGestures(Collection<Gesture> possibleGestures) {
        return Collections.singletonList( currentlyWanted );
    }

    @Override
    public void gestureExecuted(DateTime start, DateTime end, Gesture wanted, Gesture got) {
        if( currentlyWanted == wanted && currentlyWanted == got ){
            //Got the correct one!
            index++;
            if( index >= gestures.length ) index = 0;

            this.currentlyWanted = gestures[ index ];
        }
    }
}
