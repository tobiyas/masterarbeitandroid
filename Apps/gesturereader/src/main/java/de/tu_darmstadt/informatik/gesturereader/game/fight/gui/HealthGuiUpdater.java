package de.tu_darmstadt.informatik.gesturereader.game.fight.gui;

import android.content.res.ColorStateList;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameModel;
import de.tu_darmstadt.informatik.kom.gesturerec.utils.MainThread;

/**
 * Created by Tobias on 01.01.2018
 */
public class HealthGuiUpdater implements FightGameModel.HealthUpdatedListener {


    private static final int GREEN = 0xFF669900;
    private static final int ORANGE = 0xFFff8800;
    private static final int RED = 0xFFcc0000;


    /**
     * The Own progress bar.
     */
    private final ProgressBar selfBar;

    /**
     * The Other progress bar.
     */
    private final ProgressBar enemyBar;


    /**
     * The cached animation for shaking.
     */
    private final Animation shake;



    public HealthGuiUpdater( ProgressBar selfBar,
                             ProgressBar enemyBar) {


        this.selfBar = selfBar;
        this.enemyBar = enemyBar;

        this.shake = AnimationUtils.loadAnimation( selfBar.getContext(), R.anim.shake );
    }



    @Override
    public void ownHealthUpdated( double ownHealth, double ownMaxHealth ){
        MainThread.runOnMT( () -> healthUpdatedIntern( selfBar, ownHealth, ownMaxHealth ) );
    }

    @Override
    public void enemyHealthUpdated( double enemyHealth, double enemyMaxHealth ){
        MainThread.runOnMT( () -> healthUpdatedIntern( enemyBar, enemyHealth, enemyMaxHealth ) );
    }


    /**
     * Same as the other method above, but run on MT!
     */
    private void healthUpdatedIntern( ProgressBar bar, double health, double maxHealth ){
        int percent = (int) ( ( health / maxHealth ) * 100d );

        //Be sure to clamp between 0 and max:
        percent = Math.max( 0, Math.min( percent, 100 ) );

        //Set the Max value, if not set & current values:
        bar.setMax( 100 );
        bar.setProgress( percent );

        //Check for tinting changes:
        tint( bar, percent );


        //Shake the bar + player:
        bar.startAnimation( shake );
    }


    /**
     * Sets the tinting dependent on the percent.
     *
     * @param bar to set.
     * @param percent value between 0 and 100.
     */
    private void tint( ProgressBar bar, int percent ){
        int color = GREEN;
        if( percent <= 60 ) color = ORANGE;
        if( percent <= 30 ) color = RED;

        ColorStateList list = ColorStateList.valueOf( color );
        bar.setProgressTintList( list );
    }

}