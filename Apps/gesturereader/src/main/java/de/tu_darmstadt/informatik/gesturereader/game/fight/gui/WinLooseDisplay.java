package de.tu_darmstadt.informatik.gesturereader.game.fight.gui;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.fight.FightGameModel;

/**
 * Created by Tobias on 01.01.2018
 */

public class WinLooseDisplay implements FightGameModel.WinLooseCallback {


    private final Activity activity;
    private final Handler handler;


    public WinLooseDisplay(Activity activity ) {
        this.activity = activity;
        this.handler = new Handler( Looper.getMainLooper() );

        //Post view gone!
        handler.post( () -> activity.findViewById( R.id.over_text ).setVisibility( View.GONE ) );
    }


    @Override
    public void playerWon() {
        handler.post( () -> {
            TextView finishText = activity.findViewById( R.id.over_text );
            finishText.setVisibility( View.VISIBLE );
            finishText.setText( R.string.you_win );

            TranslateAnimation animation = new TranslateAnimation( 0, 0, 500, 0);
            animation.setDuration( 2000 );
            animation.setFillAfter( true );
            animation.setInterpolator( new LinearInterpolator() );
            finishText.startAnimation( animation );
        });

        handler.post( () -> {
            ImageView nextImage = activity.findViewById( R.id.gesture_to_do );
            nextImage.setImageResource( R.drawable.idle );
            nextImage.setImageAlpha( 0 );
        });
    }


    @Override
    public void playerLost() {
        handler.post( () -> {
            TextView finishText = activity.findViewById( R.id.over_text );
            finishText.setVisibility( View.VISIBLE );
            finishText.setText( R.string.you_lose );

            TranslateAnimation animation = new TranslateAnimation( 0, 0, 500, 0);
            animation.setDuration( 2000 );
            animation.setFillAfter( true );
            animation.setInterpolator( new LinearInterpolator() );
            finishText.startAnimation( animation );
        });

        handler.post( () -> {
            ImageView nextImage = activity.findViewById( R.id.gesture_to_do );
            nextImage.setImageResource( R.drawable.idle );
            nextImage.setImageAlpha( 0 );
        });
    }
}
