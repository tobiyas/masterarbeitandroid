package de.tu_darmstadt.informatik.gesturereader.game.fight.detect;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.tu_darmstadt.informatik.kom.gesturerec.gestures.Gesture;

/**
 * Created by Toby on 09.01.2018
 */
public class DefaultNewGestureProvider implements NewGestureProvider {


    @Override
    public List<Gesture> getNextGestures(Collection<Gesture> possibleGestures) {
        List<Gesture> next = new ArrayList<>( possibleGestures );
        Collections.shuffle( next );

        return next;
    }


    @Override
    public void gestureExecuted(DateTime start, DateTime end, Gesture wanted, Gesture got) {}
}
