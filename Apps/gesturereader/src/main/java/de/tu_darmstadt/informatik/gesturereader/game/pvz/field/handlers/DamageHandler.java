package de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers;

import java.util.HashSet;
import java.util.Set;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventHandler;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventListener;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.PlantShootsOnLaneEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieDamagedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieDiedEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific.ZombieSpawnEvent;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.plants.Plant;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.zombie.Zombie;
import java8.util.Optional;

import static java8.util.stream.StreamSupport.stream;

/**
 * Handles the Damage of the Plants to the Zombie.
 *
 * Created by Toby on 24.10.2017
 */

public class DamageHandler implements EventListener {

    /**
     * The EventSystem used.
     */
    private final EventSystem eventSystem;

    /**
     * The Set of present Zombies.
     */
    private final Set<Zombie> zombiesPresent = new HashSet<>();



    public DamageHandler(EventSystem eventSystem) {
        this.eventSystem = eventSystem;

        eventSystem.addListener( this );
    }


    @EventHandler
    public void zombieSpawn( ZombieSpawnEvent event ){
        this.zombiesPresent.add( event.getZombie() );
    }

    @EventHandler
    public void zombieDeath( ZombieDiedEvent event ){
        this.zombiesPresent.remove( event.getZombie() );
    }

    @EventHandler
    public void plantShotEvent( PlantShootsOnLaneEvent event ){
        int lane = event.getLane();
        int slot = event.getPlant().getSlot();
        Plant shooter = event.getPlant();
        double maxRange = shooter.getSlot() + shooter.getRange();

        //get the first Zombie on the Lane:
        Optional<Zombie> damagee = stream( zombiesPresent )
                .filter( z -> z.getLane() == lane )
                .filter( z -> z.getPosition() > slot)
                .filter( z -> z.getPosition() < maxRange )
                .filter( Zombie::isAlive )
                .sorted( new Zombie.FirstPositionSorter() )
                .findFirst();

        damagee.ifPresent( z -> {
            z.damage( shooter.getProjectileDamage() );
            eventSystem.fireEvent( new ZombieDamagedEvent( z, shooter.getProjectileDamage(), shooter ));
        } );
    }

}
