package de.tu_darmstadt.informatik.gesturereader.utils;

/**
 * Created by Tobias on 02.01.2018
 */

public interface QuadConsumer<R,S,T,U> {

    void accept( R r, S s, T t, U u );
}
