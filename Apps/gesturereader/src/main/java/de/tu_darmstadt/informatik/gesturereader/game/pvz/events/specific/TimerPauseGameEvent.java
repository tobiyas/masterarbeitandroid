package de.tu_darmstadt.informatik.gesturereader.game.pvz.events.specific;

import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.Event;

/**
 * Event fired, when the game should be paused.
 * Created by Toby on 24.10.2017
 */

public class TimerPauseGameEvent extends Event {}
