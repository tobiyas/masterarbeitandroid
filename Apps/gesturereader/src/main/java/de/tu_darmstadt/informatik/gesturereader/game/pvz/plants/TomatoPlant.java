package de.tu_darmstadt.informatik.gesturereader.game.pvz.plants;

import de.tu_darmstadt.informatik.gesturereader.R;
import de.tu_darmstadt.informatik.gesturereader.game.pvz.events.EventSystem;

import static de.tu_darmstadt.informatik.gesturereader.game.pvz.field.handlers.TickSystem.TICKS_PER_SECOND;

/**
 * Created by Tobias on 22.10.2017
 */

public class TomatoPlant extends Plant {


    public TomatoPlant( int lane, int slot ) {
        super( lane, slot, "Tomato", R.drawable.tomato, 20, 50, TICKS_PER_SECOND * 2, 1, 10);
    }

    public TomatoPlant(){
        this( -1, -1 );
    }


    @Override
    protected void tickIntern( EventSystem eventSystem ) {
        //Not needed for now!
    }

}
